package com.shr.server.wbms;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.server.dao.BaseServerDao;
import com.shr.server.db.TAConnection;
import com.smartgwt.client.util.SC;

public class ORDER_JFGZServer {

	/**
	 * 删除子表信息
	 * @param ids
	 * @return
	 */
	public String delChild(String ids) {
		
		if(ids == null  || ids == "undefined") {
			return "true";
		}
		
		String[] xmArray = ids.split(",");
		List<String> sqlList = new ArrayList<String>();
		
		for(String xmTemp : xmArray)
		{
			String updateSQL = " delete ORDER_JFGZ_B where pk_head = '"+ xmTemp +"' ";
			sqlList.add(updateSQL);
		}
		try {
			BaseServerDao.exeSQLs(sqlList.toArray(new String[]{}));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}
	
	/**
	 * 出库单确认 2/取消确认1
	 * @param state 确认 2/取消确认1
	 * @param ids
	 * @return
	 */
	public String chuKquRen(String ids,String state) {
		
		if(ids == null  || ids == "undefined") {
			return "true";
		}
		
//		String[] xmArray = ids.split(",");
		List<String> sqlList = new ArrayList<String>();
		
//		for(String xmTemp : xmArray)
//		{		

			String updateSQL = " update WBMS_CKDDXX_H_ED set wms_status ="+state+" where pk_ckddxx_h in ("+ ids +") ";
			sqlList.add(updateSQL);
//		}
		try {
			BaseServerDao.exeSQLs(sqlList.toArray(new String[]{}));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}
	/**
	 * 入库单确认 2/取消确认1
	 * @param state 确认 2/取消确认1
	 * @param ids
	 * @return
	 */
	public String ruKquRen(String ids,String state) {
		
		if(ids == null  || ids == "undefined") {
			return "true";
		}
		
//		String[] xmArray = ids.split(",");
		List<String> sqlList = new ArrayList<String>();
//		
//		for(String xmTemp : xmArray)
//		{
			String updateSQL = " update WBMS_RKDDSJ_H_ED set wms_status ="+state+" where pk_rkddsj_h in ("+ ids +") ";
			sqlList.add(updateSQL);
//		}
		try {
			BaseServerDao.exeSQLs(sqlList.toArray(new String[]{}));
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}

	
	
	public Map<String , String > selectByTable(String tableCode) {
		
		Map<String , String > map = new HashMap<String ,String >();
		
		String tableName = "tableName";
		
		if(tableCode == null  || tableCode == "undefined") {
			return map;
			
		}else if(tableCode.equals("100001")){
			tableName = "BMS_CCF";
		}else if(tableCode.equals("100002")){
			tableName = "BMS_JCF";
		}else if(tableCode.equals("100003")){
			tableName = "BMS_ZXF";
		}else if(tableCode.equals("100007")){
			tableName = "BMS_DDCLF";
		}else {
			tableName = tableCode;
		}
		
		TAConnection conn = null;
		
		String sSQL = "Select B.name As tableName,A.name As colName,C.value As colInfo,B.type " + 
				"From syscolumns A " + 
				"Inner Join sysobjects B on A.id=B.id " + 
				"Left Join sys.extended_properties C on A.id=c.major_id and a.colid=C.minor_id "+ 
				"Where B.type='U' And B.name='"+tableName+"' order by C.value";
		
		ResultSet rs = null;
		
		try {
			
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			
			while (rs.next()) {
				
				String colName = rs.getString("colName");
				String colInfo = rs.getString("colInfo");
				map.put(colName, colInfo);
//				map.put("adsfsdf", "sdfdsfdfd");
				
			}
			
			rs.close();
			conn.close();
			
			return map;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return map;
		
	}
	
	 /**
	  * 将库存快照中所有的状态中的未确认改为已确认
	  * @param 
	  * @return
	  */
     public String updateAll() {
			String updateSQL = "UPDATE WBMS_KCKZ_ED SET wms_status = '2' where wms_status = '1' ";
		try {
			BaseServerDao.exeSQL(updateSQL);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}
     
    /** 入库订单同步方法
     * @param map
     * @return
     */
    public String synchronous(Map<Object, Object> map) {
    	String sql = "select *from WBMS_RKDDSJ_H_ED where pk_rkddsj_h ='"+map.get("pk_rkddsj_h")+"'";
	try {
		boolean b = BaseServerDao.isExit(sql);
		if (b == true) {
			String deleteSql = "delete from WBMS_RKDDSJ_H_ED where pk_rkddsj_h='"+map.get("pk_rkddsj_h")+"'";
			BaseServerDao.exeSQL(deleteSql);
		}
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String saveSql = "insert into WBMS_RKDDSJ_H_ED"
				+ " values ('"+map.get("pk_rkddsj_h")+"','"+map.get("po_number")+"','"+map.get("ot_it")+"','"+sf.format(date)+"'"
						+ ",'"+map.get("wh_id")+"','"+map.get("status")+"','"+map.get("client_code")+"','"+map.get("lock_flag")+"','"+map.get("md5")+"'"
							+ ",'"+map.get("source_type")+"','"+map.get("wms_status")+"')";
			BaseServerDao.exeSQL(saveSql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}
    
    /** 出库订单同步方法
     * @param map
     * @return
     */
    public String ckSynchronous(Map<Object, Object> map) {
    	String sql = "select *from WBMS_CKDDXX_H_ED where pk_ckddxx_h ='"+map.get("pk_ckddxx_h")+"'";
	try {
		boolean b = BaseServerDao.isExit(sql);
		if (b == true) {
			String deleteSql = "delete from WBMS_CKDDXX_H_ED where pk_ckddxx_h='"+map.get("pk_ckddxx_h")+"'";
			BaseServerDao.exeSQL(deleteSql);
		}
			SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date date = new Date();
			String saveSql = "insert into WBMS_CKDDXX_H_ED"
				+ " values ('"+map.get("pk_ckddxx_h")+"','"+map.get("order_number")+"','"+map.get("wh_id")+"','"+map.get("ot_it")+"'"
						+ ",'"+sf.format(date)+"','"+map.get("status")+"','"+map.get("client_code")+"','"+map.get("lock_flag")+"','"+map.get("md5")+"'"
							+ ",'"+map.get("source_type")+"','"+map.get("ship_to_state")+"','"+map.get("ship_to_city")+"','"+map.get("ship_to_addr2")+"',"
									+ "'"+map.get("ship_to_name")+"','"+map.get("ship_to_phone")+"','"+map.get("customer_name")+"','"+map.get("customer_phone")+"','"+map.get("wms_status")+"')";
			BaseServerDao.exeSQL(saveSql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}
    
    /** 库存快照同步方法
     * @param map
     * @return
     */
    public String kcSynchronous(List<Map<String, String>> list) {
    	try {
			/*String ids = "";
	    	Integer i = 1;*/
	    	for (Map<String, String> object : list) {
	    		String sql = "select *from WBMS_KCKZ_ED where id = '"+object.get("id")+"' and source_type = '"+object.get("source_type")+"'";
				boolean b = BaseServerDao.isExit(sql);
				if (b == true) {
					String deleteSql = "delete from WBMS_KCKZ_ED where id = '"+object.get("id")+"' and source_type = '"+object.get("source_type")+"'";
					BaseServerDao.exeSQL(deleteSql);
				}
				String saveSql = "insert into WBMS_KCKZ_ED select *from WBMS_KCKZ where id = '"+object.get("id")+"'";
				BaseServerDao.exeSQL(saveSql);
				/*ids += object.get("id");
				if (i < list.size()) {
					ids+=",";
				}
				i++;*/
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return "true";
		
	}


}



