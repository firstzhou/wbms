package com.shr.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.isomorphic.sql.SQLConnectionManager;
import com.shr.server.db.TAConnection;

public class BaseServerDao 
{
	/**
	 * 执行sql语句
	 * @param sSQL
	 * @throws Exception
	 */
	public static void exeSQL(String sSQL)	throws Exception 
	{
		TAConnection conn = null;
		try {
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			conn.execute();

		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally {
			conn.close();
		}
	}
	
	/**
	 * 批量执行sql
	 * @param sSQLS
	 * @throws Exception
	 */
	public static void exeSQLs(String[] sSQLS) throws Exception {
		TAConnection conn = null;
		try {
			conn = new TAConnection();
			conn.setAutoCommit(false);
			for (String sSQL : sSQLS) {
				if(sSQL == null){continue;}
				conn.SQL().clear();
				conn.SQL().Add(sSQL);
				conn.execute();
			}
			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		} 
		finally 
		{
			if(conn != null)
			{
				conn.close();
			}
		}
	}
	
	public static String exePSQL(String[] sSQLS, String connName) throws Exception
	{
		TAConnection conn = null;
		try 
		{
			if(connName == null)
			{
				conn = new TAConnection();
			}
			else
			{
				conn = new TAConnection(connName);
			}
			conn.setAutoCommit(false);
			for (String sSQL : sSQLS) {
				if(sSQL == null){continue;}
				conn.SQL().clear();
				conn.SQL().Add(sSQL);
				conn.execute();
			}
			conn.commit();
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
			conn.rollback();
			throw e;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			conn.rollback();
			throw e;
		} 
		finally 
		{
			conn.close();
		}
		return "设置成功";
	}
	
	/**
	 * 判断值是否存在
	 * @param sSQL
	 * @return
	 * @throws Exception
	 */
	public static boolean isExit(String sSQL)	throws Exception 
	{
		TAConnection conn = null;
		ResultSet rs = null;

		try {
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally 
		{
			if (rs != null) 
			{
				rs.close();
			}
			conn.close();
		}
	}
	
	/**
	 * 根据sql获得值
	 * @param sSQL
	 * @return
	 * @throws Exception
	 */
	public static String getValueByID(String sSQL)	throws Exception 
	{
		TAConnection conn = null;
		ResultSet rs = null;

		try {
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				return rs.getString(1);
			} else {
				return null;
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally 
		{
			if (rs != null) 
			{
				rs.close();
			}
			conn.close();
		}
	}
	
	/**
	 * 执行带参数的存储过程
	 * @param sSQL
	 * @param params
	 * @param servletRequest
	 * @throws Exception
	 */
	public static void exeProcMore(String sSQL, Map<String, String> params) throws Exception 
	{
		Connection conn = null;
		PreparedStatement pstmt = null;
		try {
			conn = SQLConnectionManager.getConnection();
			pstmt = conn.prepareStatement(sSQL);
		    
			if(params != null)
			{
				for(int i=1; i<10; i++)
				{
					String param = params.get(String.valueOf(i));
					if(param != null)
					{
						pstmt.setString(i, param);
					}
					else
					{
						break;
					}
				}
			}
			pstmt.execute();

		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally 
		{
			if(pstmt != null)
			{
				pstmt.close();
			}
			if(conn != null)
			{
				SQLConnectionManager.free(conn);
			}
		}
	}
	
	/**
	 * 执行带参数的存储过程,返回表格
	 * @param sSQL
	 * @param params
	 * @param servletRequest
	 * @throws Exception
	 */
	public static List<Map<String, String>> queryProcMore(String sSQL, Map<String, String> params, String[] columns) throws Exception 
	{
		Connection conn = null;
		PreparedStatement pstmt = null;
		List<Map<String, String>> result = new ArrayList<Map<String, String>>();
		try {
			conn = SQLConnectionManager.getConnection();
			pstmt = conn.prepareCall(sSQL);
		    
			if(params != null)
			{
				for(int i=1; i<20; i++)
				{
					String param = params.get(String.valueOf(i));
					if(param != null)
					{
						pstmt.setString(i, param);
					}
					else
					{
						break;
					}
				}
			}
			ResultSet rs = pstmt.executeQuery();
			int index =0;
			while(rs.next())
			{
				Map<String, String> value = new HashMap<String, String>();
				for(String column : columns)
				{
					value.put(column, rs.getString(column));
				}
				result.add(value);
			}
			
			return result;
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally 
		{
			if(pstmt != null)
			{
				pstmt.close();
			}
			if(conn != null)
			{
				SQLConnectionManager.free(conn);
			}
		}
	}
	
	public static String getValueBySQL(String querySQL, String columnName, TAConnection connection) 
	{
		TAConnection conn = null;
		ResultSet rs = null;
		try {
			if (null != connection) {
				conn = connection;
			} else {
				conn = new TAConnection();
			}
			conn.SQL().clear();
			conn.SQL().Add(querySQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				return rs.getString(columnName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	public static String getValueBySQL(String querySQL, String columnName) 
	{
		TAConnection conn = null;
		ResultSet rs = null;
		try {
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(querySQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				return rs.getString(columnName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	

	public static List<Map<String, Object>> getValuesBySQL(String querySQL, String...columnName) {
		TAConnection conn = null;
		ResultSet rs = null;
		List<Map<String, Object>> rows = new ArrayList<Map<String, Object>>();
		try 
		{
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(querySQL);
			rs = conn.executeQuery();
			while (rs.next())
			{
				Map<String, Object> mapValue = new HashMap<String, Object>();
				for(String column : columnName)
				{
					mapValue.put(column, rs.getString(column));
				}
				rows.add(mapValue);
			}
		} 
		catch (SQLException e) 
		{
			e.printStackTrace();
		}
		return rows;
	}
	
	public static void main(String[] str){
		Map<String, String> map = new HashMap<String, String>();
		map.put("1", "fdate>='2008-01-01' and fdate<='2008-12-31' and FCheckerID is not null and FBrNo='DJCYC'");
		map.put("2", "");
		map.put("3", "101");
		map.put("4", "501931");
		map.put("5", "");
		map.put("6", "");
		try {
			List<Map<String, String>> r = queryProcMore("{call pGetVoucherSeq(?,?,?,?,?,?)}", map, new String[] {"id", "fdate"});
			System.out.print(r);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
