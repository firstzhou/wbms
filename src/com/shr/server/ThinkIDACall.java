package com.shr.server;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadBase.InvalidContentTypeException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.isomorphic.servlet.IDACall;

public class ThinkIDACall extends IDACall {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException 
	{
		doUploadFile(request);
		processRequest(request, response);
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		processRequest(request, response);
	}
	
	public void doUploadFile(HttpServletRequest request) throws IOException, ServletException 
	{
		try {
			String uploadPath = request.getSession().getServletContext().getRealPath("images/wl");
			DiskFileItemFactory factory = new DiskFileItemFactory();
	        // set the size threshold, above which content will be stored on disk
	        factory.setSizeThreshold(1 * 1024 * 1024); // 1 MB
	        
			ServletFileUpload fu = new ServletFileUpload(factory);
			fu.setHeaderEncoding("UTF-8");
			List<?> fileItems = fu.parseRequest(request); // 得到所有的文件：
			Iterator<?> i = fileItems.iterator();
			// 依次处理每一个文件：
			while (i.hasNext()) { 
				FileItem fi = (FileItem) i.next();
				String fileName = fi.getName();// 获得文件名，这个文件名包括路径：
				//上传文件的字段名称为DEF1，目前该方法为CDocMangeClient类使用
				if (fileName != null && "DEF1".equals(fi.getFieldName())) {
					fileName = fileName.substring(fileName.lastIndexOf("\\")+1);
					fi.write(new File(uploadPath + "/" + fileName));
				}
			}
		}
		catch(InvalidContentTypeException typeException)
		{
			//这个地方只有上传文件的时候不报错，需要处理，现在没找到什么方法处理。。。。
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
}
