package com.shr.server.bms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.shr.server.dao.BaseServerDao;
import com.shr.server.excel.XlsDataImport;
import com.shr.server.excel.xml.CellValue;
import com.shr.server.excel.xml.Table;

public class SRXlsDataImport extends XlsDataImport {

	@Override
	public boolean checkXlsRepeat(Map<String, List<List<CellValue>>> tables, Map<String, Table> tabelMap)
	{
		boolean result = false;
		for(String tableName : tables.keySet())
		{
			Table tableConf = tabelMap.get(tableName);
			String keyFiled = tableConf.getKeyFiled();
			List<String> repeatFileds = new ArrayList<String>();
			for(String filed : keyFiled.split(";"))
			{
				repeatFileds.add(filed);
			}
			
			List<List<CellValue>> tableValue = tables.get(tableName);
			List<String> checkList = new ArrayList<String>();
			for(List<CellValue> cells : tableValue)
			{
				String tempStr = "";
				for(CellValue cell : cells)
				{
					if(repeatFileds.contains(cell.getFiledName()))
					{
						tempStr += cell.getValue();
					}
				}

				String checkSQL = " select * from BMS_ZD where ZD_XM+ZD_NY='"+tempStr+"' and ZD_ZT>'09' ";
				try
				{
					//没有校验过，且校验改项目已经上传账单
					if(!checkList.contains(tempStr) && BaseServerDao.isExit(checkSQL))
					{
						addImpResult(tempStr+"已经上传账单，不能导入。");
						result = true;
					}
					if(!checkList.contains(tempStr))
					{
						checkList.add(tempStr);
					}
				}
				catch (Exception e)
				{
					addImpResult("校验异常："+checkSQL);
					result = true;
				}
			}
		}
		return result;
	}
	
}
