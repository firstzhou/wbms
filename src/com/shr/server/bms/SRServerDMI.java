package com.shr.server.bms;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.isomorphic.datasource.DSField;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.datasource.DataSource;
import com.shr.server.BaseServerDMI;
import com.shr.server.dao.BaseServerDao;

public class SRServerDMI extends BaseServerDMI{

	@Override
	public DSResponse doOperation(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception 
	{
		
		if (DataSource.isAdd(dsRequest.getOperationType())) 
		{
			String xmmc = null;
			String gxny = null;

			List<DSField> f = dsRequest.getDataSource().getFields();
			for (int i = 0; i < f.size(); i++) 
			{
				DSField dsField = f.get(i);
				String xm = (String)dsField.get("isXM");
				if(xm != null && !"".equals(xm))
				{
					xmmc = String.valueOf(dsRequest.getFieldValue(dsField.getName()));
				}
				String ny = (String)dsField.get("isNY");
				if(ny != null && !"".equals(ny))
				{
					gxny = String.valueOf(dsRequest.getFieldValue(dsField.getName()));
				}
			}
			
			if(!checkZDEdit(xmmc, gxny))
			{
				throw new Exception("账单已经同步，不能新增。");
			}
			
		}
		else if(DataSource.isUpdate(dsRequest.getOperationType()))
		{
			String pkValue = String.valueOf(dsRequest.getFieldValue(dsRequest.getDataSource().getPrimaryKey()));
			if(!canEdit(pkValue))
			{
				throw new Exception("账单已经同步，不能编辑。");
			}
		}
		else if(DataSource.isRemove(dsRequest.getOperationType()))
		{
			String pkValue = String.valueOf(dsRequest.getFieldValue(dsRequest.getDataSource().getPrimaryKey()));
			if(!canEdit(pkValue))
			{
				throw new Exception("账单已经同步，不能删除。");
			}
		}
		return super.doOperation(dsRequest, servletRequest);
	}
	
	/**
	 * 校验明细数据是否能编辑或删除
	 * @param pk
	 * @return
	 * @throws Exception
	 */
	public boolean canEdit(String pk) throws Exception
	{
		String checkSql = " select * from BMS_ZDMX where ZDMX_SEND > '09' and PK_ZDMX='"+pk+"' ";
		return !BaseServerDao.isExit(checkSql);
	}
	
	/**
	 * 校验账单是否可以修改
	 * @param xm
	 * @param ny
	 * @return
	 * @throws Exception
	 */
	public boolean checkZDEdit(String xm, String ny) throws Exception
	{
		String checkSql = " select * from BMS_ZD where ZD_XM='"+xm+"' and ZD_NY='"+ny+"' and ZD_ZT>'09' ";
		
		return !BaseServerDao.isExit(checkSql);
	}
}
