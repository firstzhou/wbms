package com.shr.server.bms;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.server.dao.BaseServerDao;
import com.shr.server.system.task.KettleRun;

public class ZDSCServer {
	
	/**
	 * 生成账单
	 * @param ny
	 * @param xm
	 * @return
	 */
	public String doCreateZD(String ny, String xm)
	{
		try
		{
			List<String> xmList = new ArrayList<String>();
			String[] xmArray = xm.split(",");
			for(String xmTemp : xmArray)
			{
				String paramSql = " select ZD_XM from BMS_ZD where  ZD_NY='"+ny+"' and ZD_ZT not like '0%' ";
				paramSql += "   and ZD_XM='"+xmTemp+"' ";
				String strXM = BaseServerDao.getValueBySQL(paramSql, "ZD_XM");
				if(strXM.length()==0)
				{
					xmList.add(xmTemp);
				}
			}
			if(xmList.isEmpty())
			{
				return "ERROR:没有项目可以生成账单的。";
			}
			if(ny != null && ny.length()==6)
			{
				String nd = "'" + ny.substring(0, 4) + "'";
				String yf = "'" + ny.substring(4) + "'";
				String xms = "'" + xmList.toString() + "'";
				Map<String, String> map = new HashMap<String, String>();
				map.put("ND", nd);
				map.put("YF", yf);
				map.put("XM", xms);
				KettleRun.runJob(map, "kettles\\BMS_JOB_ZD.kjb");
			}
			else
			{
				return "ERROR:参数格式不合法。";
			}
			return "INFO:["+xmList.toString()+"]已生成。";
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "ERROR:执行异常："+e.getMessage();
		}
	}
	
	/**
	 * 审核账单
	 * @param pks
	 * @return
	 */
	public String doExamineZD(String zdhs)
	{
		try
		{
			for(String zdh : zdhs.split(","))
			{
				
				List<String> sqlList = new ArrayList<String>();
				String updateSQL1 = " update BMS_ZD set ZD_ZT='30' where ZD_ZDH='"+zdh+"'";
				sqlList.add(updateSQL1);
				
				BaseServerDao.exeSQLs(sqlList.toArray(new String[]{}));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "ERROR:"+e.getMessage();
		}
		return null;
	}
	
	/**
	 * 上传账单
	 * @param pks
	 * @return
	 */
	public String doSubmitZD(String zdhs)
	{
		try
		{
			for(String zdh : zdhs.split(","))
			{
				Map<String, String> procmap = new HashMap<String, String>();
				procmap.put("1", zdh);
				BaseServerDao.exeProcMore("{call c_zdmx(?)}", procmap); //执行存储过程上传账单
				
				List<String> sqlList = new ArrayList<String>();
				String updateSQL1 = " update BMS_ZD set ZD_ZT='10' where ZD_ZDH='"+zdh+"'";
				sqlList.add(updateSQL1);
				String updateSQL2 = " update BMS_ZDMX set ZDMX_SEND='10' where ZDMX_ZDH='"+zdh+"' and ZDMX_SEND like '0%'";
				sqlList.add(updateSQL2);
				
				List<Map<String, Object>> value = BaseServerDao.getValuesBySQL("select ZD_NY , ZD_XM from BMS_ZD where ZD_ZDH='"+zdh+"' ", "ZD_NY", "ZD_XM") ;
				String ny = String.valueOf(value.get(0).get("ZD_NY"));
				String xm = String.valueOf(value.get(0).get("ZD_XM"));
				sqlList.addAll(getKPSQL(ny, xm, "null", "是", "ALL"));
				
				BaseServerDao.exeSQLs(sqlList.toArray(new String[]{}));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "ERROR:"+e.getMessage();
		}
		return null;
	}
	
	/**
	 * 撤回账单
	 * @param pks
	 * @return
	 */
	public String doBackZDMX(String zdhs, String msg)
	{
		try
		{
			for(String mxpk : zdhs.split(","))
			{
				Map<String, String> procmap = new HashMap<String, String>();
				procmap.put("1", mxpk);
				procmap.put("2", msg);
				BaseServerDao.exeProcMore("{call c_boyol_cancelstanding(?,?)}", procmap); //执行存储过程上传账单
				
				List<String> sqlList = new ArrayList<String>();
				String updateSQL1 = " update BMS_ZDMX set ZDMX_SEND='04' where PK_ZDMX='"+mxpk+"'";
				sqlList.add(updateSQL1);
				String updateSQL2 = " update BMS_ZD set ZD_ZT='01' where ZD_ZDH in (select ZDMX_ZDH from BMS_ZDMX where PK_ZDMX='"+mxpk+"')";
				sqlList.add(updateSQL2);
				
				String key = BaseServerDao.getValueBySQL("select ZDMX_SOURCE_TABLE from BMS_ZDMX where PK_ZDMX='"+mxpk+"'", "ZDMX_SOURCE_TABLE");
				sqlList.addAll(getKPSQL("null", "null", mxpk, "否", key));
				
				BaseServerDao.exeSQLs(sqlList.toArray(new String[]{}));
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "ERROR:"+e.getMessage();
		}
		return null;
	}
	
	/**
	 * 设置导入的原始数据状态
	 * @param ny
	 * @param xm
	 * @param pk
	 * @param kp
	 * @param key
	 * @return
	 */
	private List<String> getKPSQL(String ny, String xm, String pk, String kp, String key)
	{
		List<String> sqlList = new ArrayList<String>();
		String updateSql = null;
		if("BMS_CCF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_CCF set CCF_KP='"+kp+"' where (CCF_NY='"+ny+"' and CCF_XM='"+xm+"') or PK_CCF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_JCF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_JCF set JCF_KP='"+kp+"' where (JCF_NY='"+ny+"' and JCF_KH='"+xm+"') or PK_JCF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_ZXF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_ZXF set ZXF_KP='"+kp+"' where (ZXF_NY='"+ny+"' and ZXF_KH='"+xm+"') or PK_ZXF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_TBF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_TBF set TBF_KP='"+kp+"' where (TBF_NY='"+ny+"' and TBF_KH='"+xm+"') or PK_TBF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_QDF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_QDF set QDF_KP='"+kp+"' where (QDF_NY='"+ny+"' and QDF_KH='"+xm+"') or PK_QDF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_SMF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_SMF set SMF_KP='"+kp+"' where (SMF_NY='"+ny+"' and SMF_KH='"+xm+"') or PK_SMF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_DDCLF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_DDCLF set DDCLF_KP='"+kp+"' where (DDCLF_NY='"+ny+"' and DDCLF_KH='"+xm+"') or PK_DDCLF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_LZF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_LZF set LZF_KP='"+kp+"' where (LZF_NY='"+ny+"' and LZF_KH='"+xm+"') or PK_LZF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_LJZF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_LJZF set LJZF_KP='"+kp+"' where (LJZF_NY='"+ny+"' and LJZF_KH='"+xm+"') or PK_LJZF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_CBHCF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_CBHCF set CBHCF_KP='"+kp+"' where (CBHCF_NY='"+ny+"' and CBHCF_KH='"+xm+"') or PK_CBHCF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_CZF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_CZF set CZF_KP='"+kp+"' where (CZF_NY='"+ny+"' and CZF_KH='"+xm+"') or PK_CZF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_JTPF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_JTPF set JTPF_KP='"+kp+"' where (JTPF_NY='"+ny+"' and JTPF_KH='"+xm+"') or PK_JTPF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_ZHXF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_ZHXF set ZHXF_KP='"+kp+"' where (ZHXF_NY='"+ny+"' and ZHXF_KH='"+xm+"') or PK_ZHXF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_TXF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_TXF set TXF_KP='"+kp+"' where (TXF_NY='"+ny+"' and TXF_KH='"+xm+"') or PK_TXF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_YCF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_YCF set YCF_KP='"+kp+"' where (YCF_SRCXYF='"+ny+"' and YCF_KH='"+xm+"') or PK_YCF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_GLF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_GLF set GLF_KP='"+kp+"' where (GLF_NY='"+ny+"' and GLF_XM='"+xm+"') or PK_GLF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_QTF".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_QTF set QTF_KP='"+kp+"' where (QTF_NY='"+ny+"' and QTF_KH='"+xm+"') or PK_QTF='"+pk+"'";
			sqlList.add(updateSql);
		}
		if("BMS_SRSC".equals(key) || "ALL".equals(key))
		{
			updateSql = "update BMS_SRSC set SRSC_KP='"+kp+"' where (SRSC_NY='"+ny+"' and SRSC_XM='"+xm+"') or PK_SRSC='"+pk+"'";
			sqlList.add(updateSql);
		}
		
		return sqlList;
	}
}
