package com.shr.server.bms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.server.Tools;
import com.shr.server.dao.BaseServerDao;
import com.shr.server.system.task.KettleRun;

public class TBDRSJServer {
	
	/**
	 * 计算数据
	 * @param params
	 * @return
	 */
	public String doSynData(Map<String, String> params)
	{
		final String user = params.get("1");
		final String nys = params.get("2");
		
		//启动同步线程
		new Thread(){
			@Override
			public void run() {
				runData(user, nys);
			}
		}.start();
		return null;
	}
	
	/**
	 * 计算数据，保证线程安全
	 * @param user
	 * @param ny
	 */
	public static synchronized void runData(String user, String nys)
	{
		String uuid = Tools.getTimeMillisSequence();
		String insertLog = "insert into BMS_TBDRSJ(PK_TBDRSJ, TBDRSJ_USER, TBDRSJ_NY, TBDRSJ_ZT, TBDRSJ_SYNDATE, TBDRSJ_BZ)";
		insertLog += " values('"+uuid+"', '"+user+"', '"+nys+"', '计算中', getDate(), '计算中，请点击查询按钮刷新查看最新状态。')";
		
		String updateLog = " update BMS_TBDRSJ set TBDRSJ_ENDDATE=getDate(), TBDRSJ_ZT='结束', TBDRSJ_BZ='计算成功' where PK_TBDRSJ='"+uuid+"'";
		try 
		{
			BaseServerDao.exeSQL(insertLog);
			for(String ny : nys.split(","))
			{
				String cr = checkCKMJ(ny);
				if(cr != null && !"".equals(cr))
				{
					throw new Exception("仓库定价总面积，与库区面积中总面积不一致。【"+cr+"】");
				}
				
				/** 去掉校验20190113
				cr = checkKWS(ny);
				if(cr != null && !"".equals(cr))
				{
					throw new Exception("项目占用中库位数，与库区信息中库位数不一致。【"+cr+"】");
				}
				**/
				Map<String, String> procmap = new HashMap<String, String>();
				procmap.put("1", ny);
				BaseServerDao.exeProcMore("{call p_calculate_price(?)}", procmap); //执行存储过程计算底稿单价和费用
				
				String checkPrc = " select * from BMS_CALLOG where CALLOG_PARAM='1' ";
				
				boolean isError = BaseServerDao.isExit(checkPrc);
				if(isError)
				{
					throw new Exception("过程执行错误，请检查详细日志。");
				}
				//执行kettle
				String nd = "'" + ny.substring(0, 4) + "'";
				String yf = "'" + ny.substring(4) + "'";
				Map<String, String> map = new HashMap<String, String>();
				map.put("ND", nd);
				map.put("YF", yf);
				KettleRun.runJob(map, "kettles\\BMS_JOB_MAIN.kjb");
			}
		} 
		catch (Exception e) 
		{
			String exc = e.getMessage();
			updateLog = " update BMS_TBDRSJ set TBDRSJ_ENDDATE=getDate(), TBDRSJ_ZT='结束', TBDRSJ_BZ='失败："+exc+"' where PK_TBDRSJ='"+uuid+"'";
		}
		finally
		{
			try
			{
				BaseServerDao.exeSQL(updateLog);
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 检查项目信息表中的仓库面积与库区面积是否相等
	 * @return
	 */
	private static String checkCKMJ(String ny)
	{
		String result = "";
		
		String checkMJ = " select distinct KQMJ_CKMC+'-'+KQMJ_ZCK CKMC from ";
		checkMJ += "       (";
		checkMJ += "       	   select KQMJ_ZCK, KQMJ_CKMC, sum(isnull(KQMJ_MJ,0)) KQMJ_MJ ";
		checkMJ += "       	   from BMS_KQMJ";
		checkMJ += "           where '"+ny+"' = KQMJ_NY";
		checkMJ += "       	   group by KQMJ_CKMC, KQMJ_ZCK";
		checkMJ += "       ) A,";
		checkMJ += "       (";
		checkMJ += "       	   select CKXX_ZCK, CKXX_CKMC, sum(isnull(CKXX_MJ,0)) CKXX_MJ ";
		checkMJ += "       	   from BMS_CKXX ";
		checkMJ += "           where 1=1 ";
		checkMJ += "       	   and CKXX_FYLX='200001'";
		checkMJ += "           and '"+ny+"'>=SUBSTRING(CKXX_ZLKSRQ,0,7) ";
		checkMJ += "       	   and '"+ny+"'<=SUBSTRING(CKXX_ZLJSRQ,0,7) ";
		checkMJ += "       	   group by CKXX_CKMC, CKXX_ZCK ";
		checkMJ += "       ) B";
		checkMJ += "       where A.KQMJ_CKMC=B.CKXX_CKMC and isnull(A.KQMJ_ZCK,'')=isnull(B.CKXX_ZCK,'') and cast(A.KQMJ_MJ as numeric(18,2))<>cast(B.CKXX_MJ as numeric(18,2))";
		
		List<Map<String, Object>> checkValue = BaseServerDao.getValuesBySQL(checkMJ, "CKMC");
		
		for(Map<String, Object> v : checkValue)
		{
			result += v.get("CKMC") + ";";
		}
		
		return result;
	}
	
	/**
	 * 检查库区面积中的库位数与项目占用表中的库位数是否相等
	 * @return
	 */
	private static String checkKWS(String ny)
	{
		String result = "";
		
		String checkMJ = " select distinct KQMJ_CKMC+XMZY_KQ JG from  ";
		checkMJ += "       (";
		checkMJ += "       	   select KQMJ_CKMC, KQMJ_KQ, sum(isnull(KQMJ_KWS,0)) KQMJ_KWS ";
		checkMJ += "       	   from BMS_KQMJ ";
		checkMJ += "           where '"+ny+"' = KQMJ_NY";
		checkMJ += "       	   group by KQMJ_CKMC, KQMJ_KQ";
		checkMJ += "       ) A,";
		checkMJ += "       (";
		checkMJ += "       	   select XMZY_CKMC, XMZY_KQ, sum(isnull(XMZY_KWS,0)) XMZY_KWS  ";
		checkMJ += "       	   from BMS_XMZY  ";
		checkMJ += "           where 1=1 ";
		checkMJ += "       	   and '"+ny+"'=XMZY_NY and XMZY_CKSX='立体仓' ";
		checkMJ += "       	   group by XMZY_CKMC, XMZY_KQ ";
		checkMJ += "       ) B";
		checkMJ += "       where A.KQMJ_CKMC=B.XMZY_CKMC and A.KQMJ_KQ=B.XMZY_KQ and cast(A.KQMJ_KWS as numeric(18,0))<>cast(B.XMZY_KWS as numeric(18,0)) ";
		
		List<Map<String, Object>> checkValue = BaseServerDao.getValuesBySQL(checkMJ, "JG");
		
		for(Map<String, Object> v : checkValue)
		{
			result += v.get("JG") + ";";
		}
		
		return result;
	}
	
	/**
	 * 设置收入是否已开票
	 * @param ny
	 * @param xm
	 * @param kp
	 * @param key
	 * @return
	 */
	public String doSetKP(String ny, String xm, String kp, String key)
	{
		String updateSql = null;
		if("BMS_CCF".equals(key))
		{
			updateSql = "update BMS_CCF set CCF_KP='"+kp+"' where CCF_NY='"+ny+"' and CCF_XM='"+xm+"'";
		}
		else if("BMS_JCF".equals(key))
		{
			updateSql = "update BMS_JCF set JCF_KP='"+kp+"' where JCF_NY='"+ny+"' and JCF_KH='"+xm+"'";
		}
		else if("BMS_ZXF".equals(key))
		{
			updateSql = "update BMS_ZXF set ZXF_KP='"+kp+"' where ZXF_NY='"+ny+"' and ZXF_KH='"+xm+"'";
		}
		else if("BMS_TBF".equals(key))
		{
			updateSql = "update BMS_TBF set TBF_KP='"+kp+"' where TBF_NY='"+ny+"' and TBF_KH='"+xm+"'";
		}
		else if("BMS_QDF".equals(key))
		{
			updateSql = "update BMS_QDF set QDF_KP='"+kp+"' where QDF_NY='"+ny+"' and QDF_KH='"+xm+"'";
		}
		else if("BMS_SMF".equals(key))
		{
			updateSql = "update BMS_SMF set SMF_KP='"+kp+"' where SMF_NY='"+ny+"' and SMF_KH='"+xm+"'";
		}
		else if("BMS_DDCLF".equals(key))
		{
			updateSql = "update BMS_DDCLF set DDCLF_KP='"+kp+"' where DDCLF_NY='"+ny+"' and DDCLF_KH='"+xm+"'";
		}
		else if("BMS_LZF".equals(key))
		{
			updateSql = "update BMS_LZF set LZF_KP='"+kp+"' where LZF_NY='"+ny+"' and LZF_KH='"+xm+"'";
		}
		else if("BMS_LJZF".equals(key))
		{
			updateSql = "update BMS_LJZF set LJZF_KP='"+kp+"' where LJZF_NY='"+ny+"' and LJZF_KH='"+xm+"'";
		}
		else if("BMS_CBHCF".equals(key))
		{
			updateSql = "update BMS_CBHCF set CBHCF_KP='"+kp+"' where CBHCF_NY='"+ny+"' and CBHCF_KH='"+xm+"'";
		}
		else if("BMS_CZF".equals(key))
		{
			updateSql = "update BMS_CZF set CZF_KP='"+kp+"' where CZF_NY='"+ny+"' and CZF_KH='"+xm+"'";
		}
		else if("BMS_JTPF".equals(key))
		{
			updateSql = "update BMS_JTPF set JTPF_KP='"+kp+"' where JTPF_NY='"+ny+"' and JTPF_KH='"+xm+"'";
		}
		else if("BMS_ZHXF".equals(key))
		{
			updateSql = "update BMS_ZHXF set ZHXF_KP='"+kp+"' where ZHXF_NY='"+ny+"' and ZHXF_KH='"+xm+"'";
		}
		else if("BMS_TXF".equals(key))
		{
			updateSql = "update BMS_TXF set TXF_KP='"+kp+"' where TXF_NY='"+ny+"' and TXF_KH='"+xm+"'";
		}
		else if("BMS_YCF".equals(key))
		{
			updateSql = "update BMS_YCF set YCF_KP='"+kp+"' where YCF_SRCXYF='"+ny+"' and YCF_KH='"+xm+"'";
		}
		else if("BMS_GLF".equals(key))
		{
			updateSql = "update BMS_GLF set GLF_KP='"+kp+"' where GLF_NY='"+ny+"' and GLF_XM='"+xm+"'";
		}
		else if("BMS_QTF".equals(key))
		{
			updateSql = "update BMS_QTF set QTF_KP='"+kp+"' where QTF_NY='"+ny+"' and QTF_KH='"+xm+"'";
		}
		else if("BMS_SRSC".equals(key))
		{
			updateSql = "update BMS_SRSC set SRSC_KP='"+kp+"' where QTF_NY='"+ny+"' and QTF_XM='"+xm+"'";
		}
		try
		{
			BaseServerDao.exeSQL(updateSql);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "ERROR:"+e.getMessage();
		}
		return null;
	}
	
	/**
	 * 刷新开票信息
	 * @param ny
	 * @param xm
	 * @return
	 */
	public String doRebuildKP(String ny, String xm)
	{
		
		String delSql = " delete from BMS_KPXX where KPXX_NY='"+ny+"' and KPXX_XM='"+xm+"'";
		
		String insSql = " insert into BMS_KPXX(KPXX_FRGS, KPXX_GLGS, KPXX_QY, KPXX_XM, KPXX_NY, KPXX_KPTT, KPXX_YJKM, ";
		insSql += " 	KPXX_FYKM, KPXX_XTSR, KPXX_KPSR, WHR, WHSJ, KPXX_BZ)";
		insSql += " select XM.XM_FRGS, XM.XM_GLGS, SRHZ.QYMC, SRHZ.XMMC, SRHZ.NY, XM.XM_KPTT ,";
		insSql += " 	SRHZ.KMLX, SRHZ.KMMC, sum(isnull(SRHZ.JE,0)) XTSR ,";
		insSql += " 	sum(case when SRHZ.KP='是' then SRHZ.JE else 0 end) KPSR,";
		insSql += " 	'system' WHR, GETDATE() WHSJ, '人工计算' BZ ";
		insSql += " from dbo.BI_BMS_SRHZ SRHZ";
		insSql += " inner join BEIYE_XM XM on SRHZ.XMMC=XM.XM_MC";
		insSql += " where SRHZ.NY ='"+ny+"' and SRHZ.XMMC='"+xm+"'";
		insSql += " group by XM.XM_FRGS, XM.XM_GLGS, SRHZ.QYMC, SRHZ.XMMC, SRHZ.NY, XM.XM_KPTT,	SRHZ.KMLX, SRHZ.KMMC ";
		
		try
		{
			BaseServerDao.exeSQLs(new String[]{delSql, insSql});
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return "ERROR:"+e.getMessage();
		}
		return null;
	}
}
