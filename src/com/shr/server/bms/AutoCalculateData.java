package com.shr.server.bms;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.shr.server.system.task.ISystemTask;

public class AutoCalculateData implements ISystemTask
{
	@Override
	public void run() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMM");
		Date date = new Date();
		String ny = formatter.format(date);
		TBDRSJServer.runData("system", ny);
	}
}
