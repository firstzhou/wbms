package com.shr.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Properties;

import javax.servlet.*;
import javax.servlet.http.*;

public class LoginFilter extends HttpServlet implements Filter {
	private FilterConfig filterConfig;

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain filterChain) throws IOException, ServletException {
		RequestDispatcher dispatcher = null;
		ServletContext context = filterConfig.getServletContext();
		try {
			HttpServletRequest httpServletRequest = (HttpServletRequest) request;
			HttpServletResponse httpServletResponse = (HttpServletResponse) response;
			HttpSession session = httpServletRequest.getSession();
			httpServletRequest.setCharacterEncoding("utf-8");
			httpServletResponse.setCharacterEncoding("UTF-8");
			String sServer_Root = httpServletRequest.getRealPath("");

			String userid = (String) session.getAttribute("usrid");
			String uri = httpServletRequest.getRequestURI();
			if (!uri.endsWith(".css") && !uri.endsWith(".jpg")
					&& !uri.endsWith(".gif") && !uri.endsWith(".png")
					&& !uri.endsWith(".jpeg") && !uri.endsWith(".png")
					&& !uri.endsWith("Login.do") && !uri.endsWith("index.jsp") && !uri.endsWith("App.jsp")
					&& (userid == null || userid.equals(""))) {

				session.invalidate();
				PrintWriter writer = httpServletResponse.getWriter();
				writer.print(LOGIN_REQUIRED_MARKER);
				httpServletResponse.flushBuffer();
			} else {
				filterChain.doFilter(request, response);
			}
		} catch (Exception iox) {
			iox.printStackTrace();
			filterConfig.getServletContext().log(iox.getMessage());
		}
	}

	public void init(FilterConfig filterConfig) throws ServletException {
		this.filterConfig = filterConfig;
	}

	public void destroy() {
	}

	protected static final String LOGIN_REQUIRED_MARKER = "<SCRIPT>//'\"]]>>isc_loginRequired\n"
			+ "//\n"
			+ "// Embed this whole script block VERBATIM into your login page to enable\n"
			+ "// SmartClient RPC relogin.\n"
			+ "\n"
			+ " top.location.href=\"index.jsp\";\n" + "</SCRIPT>";
}
