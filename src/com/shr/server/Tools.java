package com.shr.server;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.regex.Pattern;

import com.shr.server.db.TAConnection;

public class Tools {
	static public String trim(Object Value) {
		if (Value == null)
			return "";
		else
			return Value.toString().trim();
	}

	public static String getYearMonthXH(String tableName, String field) {
		String sYear = Tools.getYear();
		String sMonth = Tools.getMonth();
		String sDay = Tools.getDay();
		String result = "";
		int iXH = 0;
		String sql = "select isnull(max(convert(float, " + field
				+ ")), 0) as XH from " + tableName + " where " + field
				+ " like :pID ";

		TAConnection connection = new TAConnection();
		connection.SQL().clear();
		connection.SQL().Add(sql);
		connection.SQL().setString("pID", sYear + sMonth + sDay + "%");
		ResultSet rs = null;
		try {
			rs = connection.executeQuery();
			if (rs.next()) {
				iXH = rs.getInt("XH");
			}
			if (iXH != 0) {
				result = Integer.toString(iXH + 1);
			} else {
				result = sYear + sMonth + "01";
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			connection.close();
		}
		return result;
	}

	public static String getYearMonthDayXH(String tableName, String field,
			String defStartNum, String strHeader) {
		String sYear = Tools.getYear();
		String sMonth = Tools.getMonth();
		String sDay = Tools.getDay();
		String result = "";
		double iXH = 0;
		String sql = "select isnull(max(convert(float, replace(" + field + ",'"
				+ strHeader + "',''))), 0) as XH from " + tableName + " where "
				+ field + " like '" + strHeader + sYear + sMonth + sDay + "%' ";

		TAConnection connection = new TAConnection();
		connection.SQL().clear();
		connection.SQL().Add(sql);
		ResultSet rs = null;
		java.text.DecimalFormat formatter = new java.text.DecimalFormat(
				"############");

		try {
			rs = connection.executeQuery();
			if (rs.next()) {
				iXH = rs.getDouble("XH");
			}
			if (iXH != 0) {
				result = strHeader + formatter.format(iXH + 1);
			} else {
				result = strHeader + sYear + sMonth + sDay + defStartNum;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			connection.close();
		}
		return result;
	}

	public static String getToday() {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		result = formatter.format(date);
		return result;
	}

	public static String getYear() {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
		Date date = new Date();
		result = formatter.format(date);
		return result;
	}

	public static String getMonth() {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("MM");
		Date date = new Date();
		result = formatter.format(date);
		return result;
	}

	public static String getDay() {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("dd");
		Date date = new Date();
		result = formatter.format(date);
		return result;
	}

	public static String getNow() {
		String result = "";
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		result = formatter.format(date);
		return result;
	}

	/**
	 * 根据sql语句查询指定字段值
	 * 
	 * @param sql
	 *            sql语句
	 * @param field
	 *            待查询的字段
	 * @param dbType
	 *            数据库类型
	 * @return field 的值
	 */
	public static String getValueBySQL(String sql, String field, String dbType) {
		TAConnection conn = new TAConnection(dbType);
		ResultSet rs = null;
		try {
			conn.SQL().clear();
			conn.SQL().Add(sql);
			rs = conn.executeQuery();
			if (rs.next()) {
				return Tools.trim(rs.getObject(field));
			}
		} catch (Exception e) {
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
			}
			conn.close();
		}
		return "";
	}

	public static String getValueBySQL(String sql, String field,
			TAConnection conn) {
		ResultSet rs = null;
		try {
			conn.SQL().clear();
			conn.SQL().Add(sql);
			rs = conn.executeQuery();
			if (rs.next()) {
				return Tools.trim(rs.getObject(field));
			}
		} catch (Exception e) {
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
			}
		}
		return "";
	}

	public static float getFloatValueBySQL(String sql) {
		TAConnection conn = new TAConnection();
		ResultSet rs = null;
		try {
			conn.SQL().clear();
			conn.SQL().Add(sql);
			rs = conn.executeQuery();
			if (rs.next()) {
				return rs.getFloat(1);
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
			}
			conn.close();
		}
		return 0;
	}

	/**
	 * 根据sql语句查询指定字段值
	 * 
	 * @param sql
	 *            sql语句
	 * @param field
	 *            字段
	 * @return String field 的值
	 */
	public static String getValueBySQL(String sql, String field) {
		return getValueBySQL(sql, field, "DBERP");
	}

	public static void setArraListFromResultSet(ArrayList<Map> rows,
			ResultSet rs) {

		try {
			while (rs.next()) {
				Map map = new HashMap();
				for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
					if (rs.getMetaData().getColumnTypeName(i)
							.equals("datetime")) {
						map.put(rs.getMetaData().getColumnName(i),
								rs.getTimestamp(i));
					} else if (rs.getMetaData().getColumnTypeName(i)
							.equals("float")
							|| rs.getMetaData().getColumnTypeName(i)
									.equals("NUMBER")) {
						map.put(rs.getMetaData().getColumnName(i),
								rs.getFloat(i));
					} else {
						map.put(rs.getMetaData().getColumnName(i),
								trim(rs.getString(i)));
					}
				}
				rows.add(map);
				// taResult.
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public static boolean notExistSelect(String selectStr) {
		boolean flag = true;
		TAConnection connection = new TAConnection();
		ResultSet rs = null;
		try {
			connection.SQL().clear();
			connection.SQL().Add(selectStr);
			rs = connection.executeQuery();
			if (rs.next()) {
				flag = false;
			}
			// st.close();
		} catch (Exception e) {
			e.printStackTrace();
			flag = true;
		} finally {
			try {
				rs.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
			connection.close();
		}
		return flag;
	}

	public static boolean notExistSelect(String selectStr,
			TAConnection connection) {
		boolean flag = true;
		ResultSet rs = null;
		try {
			connection.SQL().clear();
			connection.SQL().Add(selectStr);
			rs = connection.executeQuery();
			if (rs.next()) {
				flag = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			flag = true;
		} finally {
			try {
				rs.close();
			} catch (Exception e2) {
				// TODO: handle exception
			}
		}
		return flag;
	}

	public static boolean isNullOrEmpty(Object str) {
		return trim(str).equals("");
	}
	
	/**
     * @return 形如 yyyyMMddHHmmssSSS-Z0000019558195832297 的(38位)保证唯一的递增的序列号字符串，
     * 主要用于数据库的主键，方便基于时间点的跨数据库的异步数据同步。
     * 前半部分是currentTimeMillis，后半部分是nanoTime（正数）补齐20位的字符串，
     * 如果通过System.nanoTime()获取的是负数，则通过nanoTime = nanoTime+Long.MAX_VALUE+1;
     * 转化为正数或零。
     */
    public static String getTimeMillisSequence()
    {
        long nanoTime = System.nanoTime();
        String preFix="";
        if (nanoTime<0)
        {
            nanoTime = nanoTime+Long.MAX_VALUE+1;
        }
        String nanoTimeStr = String.valueOf(nanoTime);
         
        int difBit=String.valueOf(Long.MAX_VALUE).length()-nanoTimeStr.length();
        preFix = getRandomCharAndNumr(difBit);
        nanoTimeStr = preFix+nanoTimeStr;
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmssSSS"); //24小时制
        String timeMillisSequence=sdf.format(System.currentTimeMillis())+nanoTimeStr; 
         
        return timeMillisSequence;      
    }
    
    /**
     * 随机生成字符串
     * @param length
     * @return
     */
	public static String getRandomCharAndNumr(Integer length) {
		String str = "";
		Random random = new Random();
		for (int i = 0; i < length; i++) {
			boolean b = random.nextBoolean();
			if (b) { // 字符串
				// int choice = random.nextBoolean() ? 65 : 97; 取得65大写字母还是97小写字母
				str += (char) (65 + random.nextInt(26));// 取得大写字母
			} else { // 数字
				str += String.valueOf(random.nextInt(10));
			}
		}
		return str;
	}
	
	
	/**
	 * 从26号开始
	 * @return
	 */
	public static String getBeginDate()
	{
		Date curentDate = new Date();
		String yy = new SimpleDateFormat("yyyy").format(curentDate);
		String mm = new SimpleDateFormat("MM").format(curentDate);
		int y = Integer.valueOf(yy);
		int m = Integer.valueOf(mm);
		if(m == 1)
		{
			return (y-1) + "-11-26";
		}
		else if(m == 2)
		{
			return (y) + "-01-01";
		}
		else if(m == 11)
		{
			return (y) + "-" + (m-2) + "-26";
		}
		else
		{
			return (y) + "-0" + (m-2) + "-26";
		}
	}
	
	/**
	 * 到25号结束
	 * @return
	 */
	public static String getEndDate()
	{
		Date curentDate = new Date();
		String yy = new SimpleDateFormat("yyyy").format(curentDate);
		String mm = new SimpleDateFormat("MM").format(curentDate);
		int y = Integer.valueOf(yy);
		int m = Integer.valueOf(mm);
		if(m == 1)
		{
			return (y-1) + "-12-31";
		}
		else if(m == 2)
		{
			return (y) + "-01-25";
		}
		else if(m == 11 || m == 12)
		{
			return (y) + "-" + (m-1) + "-25";
		}
		else
		{
			return (y) + "-0" + (m-1) + "-25";
		}
	}
	

	/**
	 * 获得该月的起始日期
	 * @param 年月 yyyyMM
	 * @return yyyy-MM-dd
	 */
	public static String getCurBeginDate(String NY)
	{
		Date curentDate = new Date();
		if(NY != null)
		{
			try {
				curentDate = new SimpleDateFormat("yyyyMM").parse(NY);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String yy = new SimpleDateFormat("yyyy").format(curentDate);
		String mm = new SimpleDateFormat("MM").format(curentDate);
		int y = Integer.valueOf(yy);
		int m = Integer.valueOf(mm);
		
		if(m == 1)
		{
			return (y) + "-01-01";
		}
		else if(m > 10)
		{
			return (y) + "-" + (m-1) + "-26";
		}
		else 
		{
			return (y) + "-0" + (m-1) + "-26";
		}
	}
	
	/**
	 * 到25号结束
	 * @return
	 */
	public static String getCurEndDate(String NY)
	{
		Date curentDate = new Date();
		if(NY != null)
		{
			try {
				curentDate = new SimpleDateFormat("yyyyMM").parse(NY);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		String yy = new SimpleDateFormat("yyyy").format(curentDate);
		String mm = new SimpleDateFormat("MM").format(curentDate);
		int y = Integer.valueOf(yy);
		int m = Integer.valueOf(mm);
		if(m == 12)
		{
			return (y) + "-12-31";
		}
		else if(m < 10)
		{
			return (y) + "-0" + (m) + "-25";
		}
		else
		{
			return (y) + "-" + (m) + "-25";
		}
	}
	
	public static Map sortMap(Map oldMap) {  
		 ArrayList<Map.Entry<String, Double>> list = new ArrayList<Map.Entry<String, Double>>(oldMap.entrySet());  
		      Collections.sort(list, new Comparator<Map.Entry<String, Double>>() {  
		
	           @Override  
	            public int compare(Entry<java.lang.String, Double> arg0,  
		                    Entry<java.lang.String, Double> arg1) {  
	        	   		if(arg0.getValue() > arg1.getValue())
	        	   		{
	        	   			return 1;
	        	   		}
	        	   		else
	        	   		{
	        	   			return -1;
	        	   		}
		            }  
		        });  
		      Map newMap = new LinkedHashMap();  
		       for (int i = 0; i < list.size(); i++) {  
		           newMap.put(list.get(i).getKey(), list.get(i).getValue());  
		       }  
		       return newMap;  
		   }  

	public static String getMValueBySQL(String sSQL) {
		ResultSet rs = null;
		TAConnection conn = new TAConnection();
		String result = "";
		try {
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			while (rs.next()) {
				result += Tools.trim(rs.getObject(1)) + ",";
			}
		} catch (Exception e) {
			result = "";
		} finally {
			try {
				rs.close();
			} catch (Exception e) {
			}
		}
		return result;
	}
	
	public static void main(String args[]){
	      String content = "入";
	 
	      String pattern = "月|日|(导入)";
	 
	      boolean isMatch = Pattern.matches(pattern, content);
	      System.out.println("字符串中是否包含了 'runoob' 子字符串? " + isMatch);
	   }

}
