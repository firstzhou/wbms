package thinkact.framework.util.sql;
import java_cup.runtime.*;

/* 遇到任何错误则抛出异常 */
parser code {:
  public void report_fatal_error(String message, Object info) throws Exception {
      done_parsing();
      throw new Exception(message);
  }

  public void report_error(String message, Object info){
  }

  public void syntax_error(Symbol cur_token){
  }

  public void unrecovered_syntax_error(Symbol cur_token) throws Exception {
    if(cur_token.value!=null){
      GtNode error_node = (GtNode)(cur_token.value);
      report_fatal_error("在第 "+error_node.getLineno()+" 行的 "+error_node.getData()+" 附近有错误！ ",null);
    }
    else{
      report_fatal_error("gtSQL目前不支持该语法",null);
    }
  }
:}

terminal GtNode ALL,AND,ANY,AS,ASC;
terminal GtNode BETWEEN,BY;
terminal GtNode CASE,CROSS;
terminal GtNode DELETE,DESC,DISTINCT;
terminal GtNode ELSE,END,ESCAPE,EXISTS;
terminal GtNode FALSE,FROM,FULL;
terminal GtNode GROUP;
terminal GtNode HAVING;
terminal GtNode IN,INNER,INSERT,INTO,IS;
terminal GtNode JOIN;
terminal GtNode LEFT,LIKE;
terminal GtNode NOT,NULL,NULLIF;
terminal GtNode ON,OR,ORDER;
terminal GtNode RIGHT;
terminal GtNode SELECT,SET,SOME;
terminal GtNode THEN,TOP,TRUE;
terminal GtNode UNION,UPDATE;
terminal GtNode VALUES;
terminal GtNode WHEN,WHERE;

terminal GtNode ABS,ACOS,ASCII,ASIN,ATAN,ATN2,AVG;
terminal GtNode CEILING,CHAR,CHARINDEX,CONVERT,COS,COT,COUNT;
terminal GtNode DATALENGTH,DATEADD,DATEDIFF,DATEPART,DAY,DECODE,DEGREES;
terminal GtNode EXP;
terminal GtNode FLOOR;
terminal GtNode GETDATE;
terminal GtNode ISNULL,ISNUMERIC;
terminal GtNode LEN,LOG,LOG10,LOWER,LPAD,LTRIM;
terminal GtNode MAX,MIN,MOD,MONTH;
terminal GtNode PI,POWER;
terminal GtNode RADIANS,REPLACE,REPLICATE,ROUND,RTRIM;
terminal GtNode SIGN,SIN,SPACE,SQRT,SQUARE,STR,STUFF,SUBSTRING,SUBSTRING2,SUM;
terminal GtNode TAN,TRUNC;
terminal GtNode UPPER;
terminal GtNode YEAR;

terminal GtNode ISZERO,EQ,GT,GE,LT,LE,NE,EQT;
terminal GtNode TOKEN,SQLPARAMETER,STRING;

terminal GtNode JIA,JIAN,CHENG,CHU,UMINUS;

terminal GtNode LKH,RKH,DOUHAO,DIAN,CONNECT;

terminal GtNode DENGYU,BUDENGYU,XIAOYU,DAYU,XDENGYU,DDENGYU;

terminal GtNode INTNUMBER,FLOATNUMBER;

non terminal GtNode gtsql_statement;
non terminal GtNode select_statement,insert_statement,update_statement,delete_statement;
non terminal GtNode table_name,insert_list,insert_value_list;
non terminal GtNode expression_bool,update_list;
non terminal GtNode select_clause,from_clause,where_clause,groupby_clause,having_clause,orderby_clause,options_clause,top_clause,select_list;
non terminal GtNode table_list,expression_list,scalar_expression,expression,column_ref,function_ref,gt_function_ref;
non terminal GtNode comparison,when_list_normal,else_expr,when_list_bool,convert_data_type,datepart,decode_list,decode_cond_res_expr;
non terminal GtNode case_expression_when_list_normal_else,case_when_list_bool_else;
non terminal GtNode function_convert_with_datatype_length,function_count,function_dateadd,function_datediff,function_datepart,function_sum,function_nullif,function_decode;
non terminal GtNode when_expression_then_expression,when_expression_bool_then_expression,function_paramlist,table_source;
non terminal GtNode derived_table,joined_table,join_list,join_list_element,orderby_list,orderby_element,orderby_option,update_element;

precedence left UNION;
precedence left INTNUMBER;
precedence left OR;
precedence left AND;
precedence left NOT;
precedence left DENGYU,BUDENGYU,XIAOYU,DAYU,XDENGYU,DDENGYU,LIKE;
precedence left JIA ,JIAN ,CONNECT;
precedence left CHENG ,CHU;
precedence left UMINUS;
precedence left LKH;
precedence nonassoc UMINUS;

start with gtsql_statement;

gtsql_statement ::= select_statement:s {: RESULT=s; :}
                    |insert_statement:s {: RESULT=s; :}
                    |update_statement:s {: RESULT=s; :}
                    |delete_statement:s {: RESULT=s; :};

/* 以下是 select 语句定义部分 */
select_statement ::=
        select_clause:sc
        from_clause:fc
        where_clause:wc
        groupby_clause:qc
        having_clause:hc
	orderby_clause:oc
        // 六个子节点的是 独立 select 语句
        {:
            RESULT=new GtNode(sym.select_statement, sc,fc,wc,qc,hc,oc);
        :}
     | select_statement:ss1 UNION options_clause:oc select_statement:ss2
        // 三个子节点的是 select union 语句
        {:
                    RESULT=new GtNode(sym.select_statement, ss1,oc,ss2);
        :};

/* select 子句 */
select_clause ::=
        SELECT options_clause:oc top_clause:tc select_list:sl
        {: RESULT = new GtNode(sym.select_clause, oc,tc,sl); :};

options_clause ::=
        ALL:o
        {: RESULT = new GtNode(sym.options_clause, o); :}
     |  DISTINCT:o
        {: RESULT = new GtNode(sym.options_clause, o); :}
     |  // 空值

        {: RESULT = new GtNode(sym.options_clause); :};

top_clause ::=
        TOP INTNUMBER:i
        {: RESULT = new GtNode(sym.top_clause, i); :} %prec UNION
     |  TOP INTNUMBER:i1 INTNUMBER:i2
        {: RESULT = new GtNode(sym.top_clause, i1,i2); :}
        // 优先于上一条

     | // 空值

        {: RESULT = new GtNode(sym.top_clause); :};

select_list ::=
        scalar_expression:se
        {: RESULT = new GtNode(sym.select_list, se); :}
     |  select_list:sl DOUHAO scalar_expression:se
        {: RESULT = new GtNode(sym.select_list, sl.getSubNodes(), se); :};

scalar_expression ::=
        expression:e
        {: RESULT = new GtNode(sym.scalar_expression, e); :}
     |  expression:e AS TOKEN:name
        {: RESULT = new GtNode(sym.scalar_expression, e,name); :}
     ;

expression ::=
        TRUE:b
        {: RESULT = b;    :}
     |  FALSE:b
        {: RESULT = b;    :}
     |  INTNUMBER:i
        {: RESULT = new GtNode(sym.expression, i);        :}
     |  FLOATNUMBER:f
        {: RESULT = new GtNode(sym.expression,f);     :}
     |  STRING:s
        {: RESULT = new GtNode(sym.expression, s); :}
     |  NULL:n
        {: RESULT = new GtNode(sym.expression, n); :}
     |  column_ref:cr
        {: RESULT = new GtNode(sym.expression,cr); :}
     |  SQLPARAMETER:sp
        {: RESULT = new GtNode(sym.expression, sp); :}
     |  function_ref:fr
        {: RESULT = new GtNode(sym.expression, fr); :}
     |  derived_table:dt
        {: RESULT = new GtNode(sym.expression,dt); :}
     |  JIAN:j expression:e // 仅仅指 一元减号、负号(-)
        {:
          RESULT = new GtNode(sym.expression, j, e);
        :}  %prec UMINUS
     |  expression:e1 JIA:j expression:e2
        {:
          RESULT = new GtNode(sym.expression,e1,j,e2);
        :}
     |  expression:e1 JIAN:j expression:e2
        {:
          RESULT = new GtNode(sym.expression, e1,j,e2);
        :}
     |  expression:e1 CHENG:c expression:e2
        {:
          RESULT = new GtNode(sym.expression, e1,c,e2);
        :}
     |  expression:e1 CHU:c expression:e2
        {:
          RESULT = new GtNode(sym.expression, e1,c,e2);
        :}
     |  expression:e1 CONNECT:c expression:e2
        {:
          RESULT = new GtNode(sym.expression, e1, c, e2);
        :}
     |  LKH:l expression:e RKH:r
        {:
          RESULT = new GtNode(sym.expression, l,e,r);
        :};

column_ref ::=
        TOKEN:name // 列名
        {:
          RESULT = new GtNode(sym.column_ref, name);        :}
     |  TOKEN:t DIAN TOKEN:n // 表名.列名
        {:
          RESULT = new GtNode(sym.column_ref,t,n);        :}
     |  TOKEN:u DIAN TOKEN:t DIAN TOKEN:n // 用户名.表名.列名
        {:
          RESULT = new GtNode(sym.column_ref,u,t,n);        :}
     |  TOKEN:d DIAN TOKEN:u DIAN TOKEN:t DIAN TOKEN:n // 数据库.用户名.表名.列名
        {:
          RESULT = new GtNode(sym.column_ref,d,u,t,n);        :}
     |  CHENG:c
        {: // *
          RESULT = new GtNode(sym.column_ref, c);        :}
     |  TOKEN:t DIAN CHENG:c // 表名.*
        {:
          RESULT = new GtNode(sym.column_ref, t, c);        :}
     |  TOKEN:u DIAN TOKEN:t DIAN CHENG:c // 用户名.表名.*
        {:
          RESULT = new GtNode(sym.column_ref, u, t, c);        :}
     |  TOKEN:d DIAN TOKEN:u DIAN TOKEN:t DIAN CHENG:c // 数据库.用户名.表名.*
        {:
          RESULT = new GtNode(sym.column_ref, d,u,t, c);        :};

function_ref ::=
        ABS:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  ACOS:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  ASCII:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  ASIN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  ATAN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  ATN2:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  AVG:f LKH options_clause:o expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,o,e);         :}
     |  case_expression_when_list_normal_else:c
        {: RESULT = c; :}
     |  case_when_list_bool_else:c
        {: RESULT = c; :}
     |  CEILING:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  CHAR:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  CHARINDEX:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);   :}
     |  CHARINDEX:f LKH expression:e1 DOUHAO expression:e2 DOUHAO expression:e3 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2, e3);   :}
     |  CONVERT:f LKH convert_data_type:c DOUHAO expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,c,e);        :}
     |  function_convert_with_datatype_length:c
        {: RESULT = c;        :}
     |  COS:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  COT:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  function_count:f
        {: RESULT = f;                           :}
     |  DATALENGTH:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  function_dateadd:f
        {: RESULT = f;  :}
     |  function_datediff:f
        {: RESULT = f;  :}
     |  function_datepart:f
        {: RESULT = f;  :}
     |  DAY:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  function_decode:f
        {: RESULT = f;       :}
     |  DEGREES:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  EXP:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  FLOOR:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  GETDATE:f LKH RKH
        {: RESULT = new GtNode(sym.function_ref,f);   :}
     |  ISNULL:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  ISNUMERIC:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  LEFT:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  LEN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  LOG:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  LOG10:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  LOWER:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  LPAD:f LKH expression:e1 DOUHAO expression:e2 DOUHAO expression:e3 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2,e3);         :}
     |  LTRIM:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  MAX:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  MIN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  MOD:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  MONTH:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  function_nullif:f
        {: RESULT = f;          :}
     |  PI:pi
        {: RESULT = pi;        :}
     |  POWER:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  RADIANS:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  REPLACE:f LKH expression:e1 DOUHAO expression:e2 DOUHAO expression:e3 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2,e3);         :}
     |  REPLICATE:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  RIGHT:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  ROUND:f LKH expression:e1 DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2);         :}
     |  RTRIM:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  SIGN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  SIN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  SPACE:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  SQRT:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  SQUARE:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  STR:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  STUFF:f LKH expression:e1 DOUHAO expression:e2 DOUHAO expression:e3 DOUHAO expression:e4 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2,e3,e4);         :}
     |  SUBSTRING:f LKH expression:e1 DOUHAO expression:e2 DOUHAO expression:e3 RKH
        {: RESULT = new GtNode(sym.function_ref,f,e1,e2,e3);         :}
     |  function_sum:f
        {: RESULT = f;     :}
     |  TAN:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  TRUNC:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  UPPER:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  YEAR:f LKH expression:e RKH
        {: RESULT = new GtNode(sym.function_ref,f,e);         :}
     |  TOKEN:user_defined_function LKH function_paramlist:fp RKH
        {: RESULT = new GtNode(sym.function_ref,user_defined_function,fp);         :}
        %prec UMINUS
        // 优先于 Column_ref ::= TOKEN
        ;

derived_table ::=
        LKH select_statement:ss RKH
        {:          RESULT = new GtNode(sym.derived_table, ss);        :};

case_expression_when_list_normal_else ::= CASE expression:e when_list_normal:w else_expr:ee END
        {: RESULT = new GtNode(sym.case_expression_when_list_normal_else,e,w,ee); :};

when_list_normal ::=
        when_expression_then_expression:w
        {:
          RESULT = new GtNode(sym.when_list_normal,w);
        :}
     |  when_list_normal:wl when_expression_then_expression:w
        {:
          RESULT = new GtNode(sym.when_list_normal, wl.getSubNodes(), w);
        :};

else_expr ::=
        ELSE expression:e
        {: RESULT = new GtNode(sym.else_expr, e);  :}
    |   // Else 部分可以为空
        {:
          RESULT = new GtNode(sym.else_expr); :};

when_expression_then_expression ::= WHEN expression:e1 THEN expression:e2
        {: RESULT = new GtNode(sym.when_expression_then_expression,e1,e2);  :};

case_when_list_bool_else ::= CASE when_list_bool:w else_expr:e END
        {: RESULT = new GtNode(sym.case_when_list_bool_else, w,e); :};

when_list_bool ::=
        when_expression_bool_then_expression:w
        {:
          RESULT = new GtNode(sym.when_list_bool,w);
        :}
     |  when_list_bool:wl when_expression_bool_then_expression:w
        {:
          RESULT = new GtNode(sym.when_list_bool, wl.getSubNodes(), w);
        :};

when_expression_bool_then_expression ::= WHEN expression_bool:eb THEN expression:e
        {: RESULT = new GtNode(sym.when_expression_bool_then_expression,eb,e);  :};

function_convert_with_datatype_length ::= CONVERT LKH convert_data_type:c LKH expression:e1 RKH DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_convert_with_datatype_length,c,e1,e2);        :};

convert_data_type ::=
        TOKEN:name
        {: RESULT = new GtNode(sym.convert_data_type, name);  :}
     |  CHAR:c
        {: RESULT = new GtNode(sym.convert_data_type, c);  :}
     ;

function_count ::= COUNT LKH options_clause:o expression:e RKH
        {: RESULT = new GtNode(sym.function_count, o,e);       :}
     |  COUNT LKH CHENG RKH
        // 没有子节点的是 count(*)
        {: RESULT = new GtNode(sym.function_count);            :};

function_dateadd ::= DATEADD LKH datepart:d DOUHAO expression:e1 DOUHAO expression:e2 RKH
        //  Datepart的合法值：hour,MINUTE,SECOND,day,week,month,quarter,year
        //  翻译时校验和处理
        {:
          RESULT = new GtNode(sym.function_dateadd,d,e1,e2);
        :};

datepart ::=
        TOKEN:name
        {: // millisecond dayofyear weekday hour minute second week quarter
          RESULT = name;  :}
     |  DAY:d
        {: RESULT = d;  :}
     |  MONTH:m
        {: RESULT = m;  :}
     |  YEAR:y
        {: RESULT = y;  :};

function_datediff ::= DATEDIFF LKH datepart:d DOUHAO expression:e1 DOUHAO expression:e2 RKH
        //datepart合法值 ::=  year, month, quarter, week, day, second
        // 翻译时校验和处理
        {:
           RESULT = new GtNode(sym.function_datediff,d,e1,e2);
        :};

function_datepart ::= DATEPART LKH datepart:d DOUHAO expression:e RKH
        //datepart合法值 ::=  week,weekday,hour,minute,second,day,month,quarter,year
        // 翻译时校验和处理
        {:
           RESULT = new GtNode(sym.function_datepart,d,e);
        :};

function_decode ::= DECODE LKH expression:e DOUHAO decode_list:d RKH
        {: RESULT = new GtNode(sym.function_decode,e,d);       :}
     |  DECODE LKH expression:e1 DOUHAO decode_list:d DOUHAO expression:e2 RKH
        {: RESULT = new GtNode(sym.function_decode,e1,d,e2);   :};

decode_list ::=
        decode_cond_res_expr:d
        {: RESULT = new GtNode(sym.decode_list, d);  :}
     |  decode_list:dl DOUHAO decode_cond_res_expr:de
        {: RESULT = new GtNode(sym.decode_list, dl.getSubNodes(), de);  :};

decode_cond_res_expr ::=
        expression:e1 DOUHAO expression:e2
        {: RESULT = new GtNode(sym.decode_cond_res_expr,e1,e2);  :};

function_nullif ::= NULLIF LKH expression:e1 DOUHAO expression:e2 RKH
        // Oracle 下用 Decode 实现，所以不支持布尔表达式

        {:
          RESULT = new GtNode(sym.function_nullif,e1,e2);
        :};

function_sum ::= SUM LKH options_clause:o expression:e RKH
        {: RESULT = new GtNode(sym.function_sum,o,e);     :};

function_paramlist ::=
        expression:e
        {:  RESULT = new GtNode(sym.function_paramlist, e);        :}
     |  function_paramlist:fp DOUHAO expression:e
        {:  RESULT = new GtNode(sym.function_paramlist, fp.getSubNodes(), e);        :}
     |  // 空参数

        {:  RESULT = new GtNode(sym.function_paramlist);        :};

/* from 子句 */
from_clause ::=
        FROM table_list:tl
        {: RESULT = new GtNode(sym.from_clause, tl);  :}
     |  // 允许 from_clause 为空, 在Oracle下不允许 From 子句为空, 可以添加 From Dual
        {:
          RESULT = new GtNode(sym.from_clause);   :};

table_list ::=
        table_source:t
        {: RESULT = new GtNode(sym.table_list, t);       :}
     |  table_list:tl DOUHAO table_source:t
        {: RESULT = new GtNode(sym.table_list, tl.getSubNodes(), t);       :};

table_source ::=
        table_name:tn
        {: RESULT = new GtNode(sym.table_source,tn);        :}
     |  table_name:tn TOKEN:ta
        {: RESULT = new GtNode(sym.table_source,tn,ta);        :}
     |  derived_table:dt
        {: RESULT = new GtNode(sym.table_source,dt);        :}
     |  derived_table:dt TOKEN:da // 子查询

        {:
          RESULT = new GtNode(sym.table_source,dt,da);  :}
     |  joined_table:jt
        {:
          RESULT = new GtNode(sym.table_source,jt);
        :};

table_name ::=
        TOKEN:name // 表名
        {:
          RESULT = new GtNode(sym.table_name, name);       :}
     |  TOKEN:u DIAN TOKEN:t // 用户名.表名
        {:
          RESULT = new GtNode(sym.table_name,u,t);
        :}
     |  TOKEN:d DIAN TOKEN:u DIAN TOKEN:t // 数据库名.用户名.表名
        {:
          RESULT = new GtNode(sym.table_name, d,u,t);        :};

joined_table ::=
        table_source:ts join_list:jl
        {:          RESULT = new GtNode(sym.joined_table, ts,jl);        :} %prec CHENG
    |   LKH:l joined_table:jt RKH:r
        {:          RESULT = new GtNode(sym.joined_table, l,jt,r);        :};

join_list ::=
        join_list_element:j
        {:
          RESULT = new GtNode(sym.join_list,j);
        :}
    |   join_list:jl join_list_element:j
        {:
          RESULT = new GtNode(sym.join_list, jl.getSubNodes(), j);
        :}
    |   LKH:l join_list:jl RKH:r
        {:
          RESULT = new GtNode(sym.join_list, l,jl,r);
        :};

// ts1 xxx JOIN ts2 yyy JION ts3 时认为是ts2,ts3分别与ts1的关系，而不是ts1 xxx JOIN ts2作为新的ts与ts3的关系

join_list_element ::=
        INNER:jt JOIN table_source:ts ON expression_bool:eb
        {: RESULT = new GtNode(sym.join_list_element, jt,ts,eb); :} %prec UMINUS
    |   LEFT:jt JOIN table_source:ts ON expression_bool:eb
        {: RESULT = new GtNode(sym.join_list_element, jt,ts,eb); :} %prec UMINUS
    |   RIGHT:jt JOIN table_source:ts ON expression_bool:eb
        {: RESULT = new GtNode(sym.join_list_element, jt,ts,eb); :} %prec UMINUS;

expression_bool ::=
       gt_function_ref:gf
        {: RESULT = gf;
        :}
     |  expression:e1 NOT:n LIKE:l expression:e2 ESCAPE:e expression:e3
        // expression 特指字符串表达式
        {:
          RESULT = new GtNode(sym.expression_bool,e1,n,l,e2,e,e3);
        :}  %prec LIKE
     |  expression:e1 NOT:n BETWEEN:b expression:e2 AND:a expression:e3
        // expression 特指字符串表达式
        {:
          RESULT = new GtNode(sym.expression_bool, e1,n,b,e2,a,e3);
        :} %prec LIKE
     |  expression:e1 NOT:n IN:i LKH:l expression_list:e2 RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, e1,n,i,l,e2,r);
        :} %prec LIKE
     |  expression:e1 NOT:n LIKE:l expression:e2
        {:
          RESULT = new GtNode(sym.expression_bool, e1,n,l,e2);
        :}
     |  expression:e NOT:n IN:i derived_table:d
        {:
          RESULT = new GtNode(sym.expression_bool, e,n,i,d);
        :} %prec LIKE
     |  expression:e1 LIKE:l expression:e2
        {:
          RESULT = new GtNode(sym.expression_bool, e1,l,e2);
        :}
     |  expression:e1 LIKE:l expression:e2 ESCAPE:e expression:e3
        {:
          RESULT = new GtNode(sym.expression_bool, e1,l,e2,e,e3);
        :}  %prec LIKE
     |  expression:e1 BETWEEN:b expression:e2 AND:a expression:e3
        {:
          RESULT = new GtNode(sym.expression_bool, e1,b,e2,a,e3);
        :}  %prec LIKE
     |  expression:e IS:i NULL:n
        {:
          RESULT = new GtNode(sym.expression_bool, e,i,n);
        :} %prec LIKE
     |  expression:e IS:i NOT:n NULL:nu
        {:
          RESULT = new GtNode(sym.expression_bool,e,i,n,nu);
        :} %prec LIKE
     |  expression:e IN:i derived_table:d
        {:
          RESULT = new GtNode(sym.expression_bool, e,i,d);
        :} %prec LIKE
     |  expression:e1 IN:i LKH:l expression_list:e2 RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, e1, i,l,e2,r);
        :}  %prec LIKE
     |  expression:e1 comparison:c ALL:a LKH:l select_statement:qe RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, e1,c,a,l,qe,r);
        :}  %prec LIKE
     |  expression:e1 comparison:c ANY:a LKH:l select_statement:qe RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, e1,c,a,l,qe,r);
        :}  %prec LIKE
     |  expression:e1 comparison:c SOME:s LKH:l select_statement:qe RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, e1,c,s,l,qe,r);
        :}  %prec LIKE
     |  EXISTS:e LKH:l select_statement:qe RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, e,l,qe,r);
        :}
     |  expression:e1 comparison:c expression:e2
        {:
          RESULT = new GtNode(sym.expression_bool, e1,c,e2);
        :}  %prec LIKE
     |  expression_bool:e1 AND:a expression_bool:e2
        {:
          RESULT = new GtNode(sym.expression_bool, e1,a,e2);
        :}
     |  expression_bool:e1 OR:o expression_bool:e2
        {:
          RESULT = new GtNode(sym.expression_bool,e1,o,e2);
        :}
     |  NOT:n expression_bool:e
        {:
          RESULT = new GtNode(sym.expression_bool, n,e);
        :}
     |  LKH:l expression_bool:e RKH:r
        {:
          RESULT = new GtNode(sym.expression_bool, l,e,r);
        :};


gt_function_ref ::= ISZERO:f LKH expression:e RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e);
        :}
     | ISZERO:f LKH expression:e DOUHAO INTNUMBER:i RKH
     {:
        RESULT = new GtNode(sym.gt_function_ref, f, e, i);
     :}
     |  EQ:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
        :}
     | EQ:f LKH expression:e1 DOUHAO expression:e2 DOUHAO INTNUMBER:i RKH
     {:
        RESULT=new GtNode(sym.gt_function_ref, f, e1, e2, i);
     :}
     |  EQT:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
        :}
     |  NE:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
        :}
     | NE:f LKH expression:e1 DOUHAO expression:e2 DOUHAO INTNUMBER:i RKH
     {:
          RESULT = new GtNode(sym.gt_function_ref, f,e1,e2,i);
     :}
     |  GT:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
        :}
     | GT:f LKH expression:e1 DOUHAO expression:e2 DOUHAO INTNUMBER:i RKH
     {:
        RESULT = new GtNode(sym.gt_function_ref, f,e1,e2,i);
     :}
     |  GE:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
        :}
     |  GE:f LKH expression:e1 DOUHAO expression:e2 DOUHAO INTNUMBER:i RKH
     {:
        RESULT = new GtNode(sym.gt_function_ref, f,e1,e2,i);
     :}
     |  LT:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
        :}
     |  LT:f LKH expression:e1 DOUHAO expression:e2 DOUHAO INTNUMBER:i RKH
     {:
        RESULT = new GtNode(sym.gt_function_ref, f,e1,e2,i);
     :}
     |  LE:f LKH expression:e1 DOUHAO expression:e2 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2);
         :}
     |  LE:f LKH expression:e1 DOUHAO expression:e2 DOUHAO INTNUMBER:i RKH
     {:
        RESULT = new GtNode(sym.gt_function_ref, f,e1,e2,i);
     :}
     |  SUBSTRING2:f LKH expression:e1 DOUHAO expression:e2 DOUHAO expression:e3 RKH
        {:
           RESULT = new GtNode(sym.gt_function_ref, f,e1,e2, e3);
        :};

expression_list ::=
        expression:e
        {:
          RESULT = new GtNode(sym.expression_list, e);
        :}
    |   expression_list:el DOUHAO expression:e
        {:
          RESULT = new GtNode(sym.expression_list, el.getSubNodes(), e);
        :};

comparison ::= DENGYU:c {: RESULT = c; :}
     |  BUDENGYU:c {: RESULT = c; :}
     |  XIAOYU:c {: RESULT = c; :}
     |  DAYU:c {: RESULT = c; :}
     |  XDENGYU:c {: RESULT = c; :}
     |  DDENGYU:c {: RESULT = c; :};

/* where 子句 */
where_clause ::=
        WHERE expression_bool:eb
        {:
          RESULT = new GtNode(sym.where_clause,eb);
        :}
    |   // 允许 where 子句为空
        {:
          RESULT = new GtNode(sym.where_clause);
        :};

/* group by 子句 */
groupby_clause ::=
        GROUP BY expression_list:el
        {:
          RESULT = new GtNode(sym.groupby_clause, el);
        :}
    |   // 允许 groupby 子句为空
        {:
          RESULT = new GtNode(sym.groupby_clause);
        :};

/* having by 子句 */
having_clause ::=
        HAVING expression_bool:eb
        {:
          RESULT = new GtNode(sym.having_clause, eb);
        :}
    |
        // 允许 having 子句为空 在 HAVING 子句中不能使用 text、image 和 ntext 数据类型
        {:
          RESULT = new GtNode(sym.having_clause);
        :};

/* order by 子句 */
orderby_clause ::=
        ORDER BY orderby_list:ol
        // SQL2K:对ORDER BY子句 中的 项目 个数 没有 限制,
        // 但是 , 对于排序所需的操作中间工作表的大小 有8060字节 的限制,
        // 这限制了 在ORDER BY子句中 指定的 列的 合计大小
        {:
          RESULT = new GtNode(sym.orderby_clause, ol);
        :}
   |    // 允许 order by 子句为空
        {:
          RESULT = new GtNode(sym.orderby_clause);
        :};

orderby_list ::=
        orderby_element:oe
        {:
          RESULT = new GtNode(sym.orderby_list, oe);
        :}
    |   orderby_list:ol DOUHAO orderby_element:oe
        {:
          RESULT = new GtNode(sym.orderby_list, ol.getSubNodes(), oe);
        :};

orderby_element ::= expression:e orderby_option:o
        {: RESULT = new GtNode(sym.orderby_element, e,o); :};

orderby_option ::=
        ASC:a
        {:
          RESULT = new GtNode(sym.orderby_option,a);
        :}
    |   DESC:d
        {:
          RESULT = new GtNode(sym.orderby_option,d);
        :}
    |   // 允许 orderby_option 为空 , [ASC|DESC]
        {:
          RESULT = new GtNode(sym.orderby_option);
        :};

/* update 语句 */
update_statement ::= UPDATE table_name:tn SET update_list:ul WHERE expression_bool:eb
                    {:
                    RESULT=new GtNode(sym.update_statement, tn,ul,eb);
                    :};

update_list ::=
        update_element:ue
        {:
          RESULT = new GtNode(sym.update_list, ue); :}
     |  update_list:ul DOUHAO update_element:ue
        {: RESULT = new GtNode(sym.update_list, ul.getSubNodes(), ue); :};

update_element ::= column_ref:cr DENGYU expression:e
          {: RESULT = new GtNode(sym.update_element, cr, e); :};

/* insert 语句 */
insert_statement ::= INSERT INTO table_name:tn LKH insert_list:il RKH insert_value_list:ivl
                    {:
                    RESULT=new GtNode(sym.insert_statement, tn,il,ivl);
                    :};

insert_list ::=
        column_ref:cr
        {: RESULT = new GtNode(sym.insert_list, cr); :}
     |  insert_list:il DOUHAO column_ref:cr
        {: RESULT = new GtNode(sym.insert_list, il.getSubNodes(), cr); :};

insert_value_list ::=
        VALUES LKH expression_list:el RKH
        {: RESULT = new GtNode(sym.insert_value_list, el);  :}
     |  select_statement:s
        {: RESULT = s; :};

/* update 语句 */
delete_statement ::= DELETE FROM table_name:tn WHERE expression_bool:eb
                    {:
                    RESULT=new GtNode(sym.delete_statement, tn,eb);
                    :};

/* END of gtSQL.cup */