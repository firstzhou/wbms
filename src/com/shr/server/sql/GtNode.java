package com.shr.server.sql;

import java.util.*;

/**
 * <p>Title: ����GtSQL�﷨�������Ľڵ�����</p>
 *
 * <p>Description: �ʷ�������JLex����GtSymbol���͵��ս����󣬸ö����value������GtNode���͵Ķ���

 *                 ��װԭ���﷨����Cup�е��û����벻�������GtSymbol�����ܹ��ܷ��������value����

 *                          �����﷨���������ֱ�ӷ��룬���Ǵ��������Լ򻯴���ṹ�����ȶ�

 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GtNode {
    private boolean terminal; // �ڵ����ս��
    private int sym;      // ���﷨������CUP��ɵ�interface sym�ı�ʾ�����ս��ͷ��ս��ĳ���
    private String data;  // �ս����ַ�ֵ�����ս��Ϊnull
    private int lineno;   // ��ԴGTSQL����е��кţ����ڱ�����Ϣ

    private Vector<GtNode> subNodes;  // ���ս����ӽڵ㣬�粻Ӱ�췭�룬��Լ����������Щ���ս���磺

                                      // <expr> as <aliasName> ���Թؼ���ս��as��ֻ����<expr>��<aliasName>�����ӽڵ�

                                      // <table>.<field>       ���Թؼ���ս��.��ֻ����<table>��<field>�����ӽڵ�


    public GtNode(int sym, boolean terminal, String data, int lineno, Vector<GtNode> subNodes){
      this.sym = sym;
      this.terminal = terminal;
      this.data = data;
      this.lineno = lineno;
      this.subNodes = subNodes;
    }

    /**
     * �����ս��ڵ�
     *
     * @param sym int
     * @param data String
     * @param lineno int
     */
    public GtNode(int sym, String data, int lineno){
        this(sym,true,data,lineno,null);
    }

    /**
     * �������ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNodes Vector
     */
    public GtNode(int sym, Vector<GtNode> subNodes){
        this(sym,false,null,0,subNodes);
    }

    /**
     * ������һ���ӽڵ�ķ��ս��

     *
     * @param sym int
     * @param subNode1 Vector
     */
    public GtNode(int sym, GtNode subNode1){
        this(sym,false,null,0,null);
        subNodes =new Vector<GtNode>();
        subNodes.add(subNode1);
    }

    /**
     * �����ж����ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNode1 Vector
     * @param subNode2 GtNode
     */
    public GtNode(int sym, GtNode subNode1, GtNode subNode2){
        this(sym,false,null,0,null);
        subNodes =new Vector<GtNode>();
        subNodes.add(subNode1);
        subNodes.add(subNode2);
    }

    /**
     * ����������ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNode1 Vector
     * @param subNode2 GtNode
     * @param subNode3 GtNode
     */
    public GtNode(int sym, GtNode subNode1, GtNode subNode2, GtNode subNode3){
        this(sym,false,null,0,null);
        subNodes =new Vector<GtNode>();
        subNodes.add(subNode1);
        subNodes.add(subNode2);
        subNodes.add(subNode3);
    }

    /**
     * �������ĸ��ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNode1 Vector
     * @param subNode2 GtNode
     * @param subNode3 GtNode
     * @param subNode4 GtNode
     */
    public GtNode(int sym, GtNode subNode1, GtNode subNode2, GtNode subNode3, GtNode subNode4){
        this(sym,false,null,0,null);
        subNodes =new Vector<GtNode>();
        subNodes.add(subNode1);
        subNodes.add(subNode2);
        subNodes.add(subNode3);
        subNodes.add(subNode4);
    }

    /**
     * ����������ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNode1 Vector
     * @param subNode2 GtNode
     * @param subNode3 GtNode
     * @param subNode4 GtNode
     * @param subNode5 GtNode
     */
    public GtNode(int sym, GtNode subNode1, GtNode subNode2, GtNode subNode3, GtNode subNode4, GtNode subNode5){
        this(sym,false,null,0,null);
        subNodes =new Vector<GtNode>();
        subNodes.add(subNode1);
        subNodes.add(subNode2);
        subNodes.add(subNode3);
        subNodes.add(subNode4);
        subNodes.add(subNode5);
    }

    /**
     * ����������ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNode1 Vector
     * @param subNode2 GtNode
     * @param subNode3 GtNode
     * @param subNode4 GtNode
     * @param subNode5 GtNode
     * @param subNode6 GtNode
     */
    public GtNode(int sym, GtNode subNode1, GtNode subNode2, GtNode subNode3, GtNode subNode4, GtNode subNode5, GtNode subNode6){
        this(sym,false,null,0,null);
        subNodes =new Vector<GtNode>();
        subNodes.add(subNode1);
        subNodes.add(subNode2);
        subNodes.add(subNode3);
        subNodes.add(subNode4);
        subNodes.add(subNode5);
        subNodes.add(subNode6);
    }

    /**
     * ������ս�������е��ӽڵ��б���׷��һ���ӽڵ�
     *
     * @param sym int
     * @param subNodes Vector
     * @param subNode GtNode
     */
    public GtNode(int sym, Vector<GtNode> subNodes, GtNode subNode){
        this(sym,false,null,0,subNodes);
        subNodes.add(subNode);
    }

    /**
     * �������ӽڵ�ķ��ս��
     *
     * @param sym int
     * @param subNodes Vector
     */
    public GtNode(int sym){
        this(sym,false,null,0,null);
    }

    public int getSym() {
        return sym;
    }

    public String getData() {
        return data;
    }

    public int getLineno() {
        return lineno;
    }

    public Vector<GtNode> getSubNodes() {
        return subNodes;
    }

    public boolean isTerminal() {
        return terminal;
    }

}