﻿package com.shr.server.sql;

import java.util.Vector;

public class OracleTrans extends DefaultTrans {

	//==============================================================================
	/**
	 * BEGIN TOP...END TOP部分均了翻译TopClause(TOP INTNUMBER:i1 INTNUMBER:i2)使用
	 * 其他地方不得随便使用
	 * 翻译中保留了TOP_A TOP_B rn_作为别名使用，开发中不得使用他们作为别名使用
	 */
	//BEGIN TOP
	    //翻译Top_Clause时，需要进行转化
	    private String TopClause;
	    private String select_clauseString;
	//END TOP
	//==============================================================================
	//==============================================================================
	/**
	 *BEGIN JOIN...END JOINB部分均为了翻译joined_table使用，不得用于其他地方
	 */
	//BEGIN JOIN
	    private String JoinTable;
	    private String JoinWhere;
	//END JOIN
	//==============================================================================
	    public OracleTrans() {
	        super();
	        JoinTable="";
	        JoinWhere="";
	        DBType = 2;
	    }

	    /**
	     * 翻译终止符
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transTerminal(GtNode node,String context) throws Exception {
	        if (node!=null){
	            switch(node.getSym()){
	               case sym.CONNECT:
	                   return "||";
	               case sym.PI:
	                   return "3.1415926535897931";
	               case sym.DATALENGTH:
	                   return "lengthB";
	               case sym.CHAR:
	                   return "chr";
	               case sym.CHARINDEX:
	                    return "instr";
	               case sym.LEFT:
	                   return "subStr";
	               case sym.LEN:
	                   return "length";
	               case sym.LOG:
	                   return "ln";
	               case sym.LOG10:
	                   return "log";
	               case sym.STR:
	                   return "to_char";
	               case sym.SUBSTRING:
	                   return "subStr";
	               case sym.TRUNC:
	                   return "trunc";
	               case sym.GETDATE:
	                   return "SysDate";
	               case sym.ATN2:
	                   return "ATAN2";
	               case sym.LPAD:
	                   return "lpad";
	               case sym.ISNULL:
	                   return "nvl";
	               case sym.CEILING:
	                   return "ceil";
	               case sym.DENGYU:
	                   if(context!=null && (context.startsWith("LJOIN") || context.startsWith("RJOIN")))
	                       return "(+)=";
	                   else
	                       return "=";
	               case sym.BUDENGYU:
	                  if(context!=null && (context.startsWith("LJOIN") || context.startsWith("RJOIN")))
	                       return "(+)<>";
	                   else
	                       return "<>";
	               case sym.XIAOYU:
	                  if(context!=null && (context.startsWith("LJOIN") || context.startsWith("RJOIN")))
	                       return "(+)<";
	                   else
	                       return "<";
	               case sym.DAYU:
	                  if(context!=null && (context.startsWith("LJOIN") || context.startsWith("RJOIN")))
	                       return "(+)>";
	                   else
	                       return ">";
	               case sym.XDENGYU:
	                   if(context!=null && (context.startsWith("LJOIN") || context.startsWith("RJOIN")))
	                       return "(+)<=";
	                   else
	                       return "<=";
	               case sym.DDENGYU:
	                   if(context!=null && (context.startsWith("LJOIN") || context.startsWith("RJOIN")))
	                       return "(+)>=";
	                   else
	                       return ">=";
	               default:
	                   return super.transTerminal(node,context);
	            }
	        }
	        else
	            return null;
	    }

	    protected String transSelect_Statement(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	            int nodeCount=subNodes.size();
	            if(nodeCount!=6) return super.transSelect_Statement(node,context);
	            else{
	                String SelectClause=null;
	                String FromClause=null;
	                String WhereClause=null;
	                String GroupByClause=null;
	                String HavigClause=null;
	                String OrderByClause=null;


	                for (int i = 0; i < nodeCount; i++){
	                    GtNode childNode=subNodes.get(i);
	                    switch(childNode.getSym()){
	                      case sym.select_clause:
	                          SelectClause=transNode(childNode, null);
	                      case sym.from_clause:
	                          FromClause=transNode(childNode, null);
	                      case sym.where_clause:
	                          WhereClause=transNode(childNode, null);
	                      case sym.groupby_clause:
	                          GroupByClause=transNode(childNode, null);
	                      case sym.having_clause:
	                          HavigClause=transNode(childNode, null);
	                      case sym.orderby_clause:
	                          OrderByClause=transNode(childNode, null);
	                    }
	                }
	                String Result=SelectClause;
	                if(FromClause!=null) Result+="\n"+FromClause;
	                if(WhereClause!=null) Result+="\n"+ WhereClause;
	                if(JoinWhere!=null && !JoinWhere.equalsIgnoreCase("")){
	                    if(WhereClause!=null) Result+=" and ("+JoinWhere+")";
	                    else Result+="\r\n"+"where  ("+JoinWhere+")";
	                }

	                if(!TopTwo && TopClause!=null){
	                    if(WhereClause!=null)  Result+=" and "+TopClause;
	                    else Result+="\r\n"+"where "+TopClause;
	                }
	                if (GroupByClause!=null) Result+="\n"+GroupByClause;
	                if(HavigClause!=null) Result+="\n"+HavigClause;
	                if(OrderByClause!=null) Result+="\n"+OrderByClause;
	                if(!TopTwo)return Result;
	                else{
	                    return "select "+select_clauseString+"\r\n"+
	                            "from("+Result+") "+TOP_A+"\r\n"+
	                            "where ("+RN_+" > "+TopNumber2+" - "+TopNumber1+") "+"\r\n"+
	                            "  and ("+RN_+" <= "+TopNumber2+")";

	                }
	            }
	        }
	        else
	            return null;

	    }

	    /**
	     * Select_Clause:10
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transSelect_Clause(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes!=null){
	            String SelectList="";
	            select_clauseString="";
	            for(int i=0;i<subNodes.size();i++){
	                GtNode subNode=subNodes.get(i);
	                String transNodeString= transNode(subNode, context);
	                if(transNodeString!=null) {
	                    if(subNode.getSym()!=sym.top_clause){
	                        if (i != 0)
	                        {
	                            SelectList += " " + transNodeString;
	                            select_clauseString+=" "+transNodeString;
	                        }
	                        else{
	                            SelectList = transNodeString;
	                            select_clauseString=transNodeString;
	                        }
	                    }
	                    else{
	                        if(TopTwo){
	                            SelectList+=transNodeString;
	                        }
	                    }
	                }
	            }
	            return "select "+SelectList;
	        }
	        else
	            return null;
	    }

	    /**
	     * From_Clause:11
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFrom_Clause(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes!=null){
	            return super.transFrom_Clause(node,context);
	        }
	        else
	          return "from Dual ";
	    }


	    /**
	     * Top_Clause:17
	     *     a)TOP INTNUMBER:i
	     *     b)TOP INTNUMBER:i1 INTNUMBER:i2
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */

	    protected  String transTop_Clause(GtNode node,String context) throws Exception{
	        TopClause=null;
	        String DataString=node.getData();
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes==null) return null;
	        else {
	            int nodeCount=subNodes.size();
	            if(nodeCount==1)
	            {
	                TopClause = "(rownum <= " + transNode(subNodes.get(0), context)+")";
	                return "(rownum <= " + transNode(subNodes.get(0), context)+")";
	            }
	            else if(nodeCount==2){
	                TopTwo=true;
	                TopNumber1=transNode(subNodes.get(0),null);
	                TopNumber2=transNode(subNodes.get(1),null);
	                return " rownum as rn_, ";
	            }
	            else return null;
	        }
	    }
	    /**
	     * Function_Ref:24
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_Ref(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	            int nodeCount=subNodes.size();
	            GtNode firstNode=subNodes.get(0);
	            if(firstNode.isTerminal()){
	                switch(firstNode.getSym()){
	                  case sym.DAY:
	                      return "to_number(to_char("+transNode(subNodes.get(1),null)+",'dd'))";
	                  case sym.CHARINDEX:{
	                      if(nodeCount==3)
	                          return transNode(firstNode,null)+"("+transNode(subNodes.get(2),null)+","+
	                                   transNode(subNodes.get(1),null)+",1)";
	                      else if(nodeCount==4)
	                          return transNode(firstNode,null)+"("+transNode(subNodes.get(2),null)+","+
	                                   transNode(subNodes.get(1),null)+","+
	                                   transNode(subNodes.get(3),null)+")";
	                       else return null;
	                  }
	                  case sym.CONVERT:
	                      String dataTypeStr=transNode(subNodes.get(1),null);
	                      String ExStr=transNode(subNodes.get(2),null);
	                      //CHAR在Ora中翻译的结果是chr
	                      if(dataTypeStr.equalsIgnoreCase("chr") ||dataTypeStr.equalsIgnoreCase("varChar"))
	                          return "to_char("+ExStr+")";
	                      else if(dataTypeStr.equalsIgnoreCase("float"))
	                          return "to_number("+ExStr+")";
	                      else if (dataTypeStr.equalsIgnoreCase("INTEGER"))
	                          return "ceil(to_number("+ExStr+"))";
	                      else if (dataTypeStr.equalsIgnoreCase("dateTime"))
	                          return "to_date("+ExStr+",'yyyy-mm-dd hh24:mi:ss')";
	                      else {
	                          throwExceptionMess(node.getLineno(),node.getData(),"convert 函数不支持 "+dataTypeStr+" 参数");
	                          return "<convert 函数不支持 "+dataTypeStr+" 参数>";}
	                  case sym.DEGREES:
	                      return "("+transNode(subNodes.get(1),null)+" * 180 / 3.1415926535897931) ";
	                  case sym.LEFT:
	                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+",1,"+transNode(subNodes.get(2),null)+")";
	                  case sym.LEN:
	                      return "nvl("+transNode(firstNode,null)+"(rTrim("+transNode(subNodes.get(1),null)
	                               +")),0)";
	                  case sym.LOG10:
	                      return transNode(firstNode,null)+"(10,"+transNode(subNodes.get(1),null)+")";
	                  case sym.MOD:
	                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+","+
	                              transNode(subNodes.get(2),null)+")";
	                  case sym.MONTH:
	                      return "to_number(to_char("+transNode(subNodes.get(1),null)+",'mm'))";
	                  case sym.RADIANS:
	                      return "("+transNode(subNodes.get(1),null)+"/180  * 3.1415926535897931)";
	                  case sym.REPLICATE:
	                      return "rPad("+transNode(subNodes.get(1),null)+",("+transNode(subNodes.get(2),null)+
	                              ")*lengthB("+transNode(subNodes.get(1),null)+"),"+
	                              transNode(subNodes.get(1),null)+")";
	                  case sym.RIGHT:
	                      String NodeString1=transNode(subNodes.get(1),null);
	                      String NodeString2=transNode(subNodes.get(2),null);
	                      return "decode(sign(lengthB("+NodeString1+")-"+
	                              NodeString2+"),1,subStr("+NodeString1+
	                              ",lengthB("+NodeString1+")-"+NodeString2+"+1,"+
	                              NodeString2+"),"+NodeString1+")";
	                  case sym.LPAD:
	                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+
	                              ","+transNode(subNodes.get(2),null)+","+
	                              transNode(subNodes.get(3),null)+")";
	                  case sym.SPACE:
	                      return "lPad(' ',"+transNode(subNodes.get(1),null)+",' ')";
	                  case sym.SQUARE:
	                      return "power("+transNode(subNodes.get(1),null)+",2)";
	                  case sym.STUFF:
	                      String NodeStr1=transNode(subNodes.get(1),null);
	                      return "replace("+NodeStr1+",subStr("+NodeStr1+","+
	                              transNode(subNodes.get(2),null)+","+transNode(subNodes.get(3),null)
	                              +"),"+transNode(subNodes.get(4),null)+")";
	                  case sym.TRUNC:
	                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+")";
	                  case sym.YEAR:
	                      return "to_number(to_char("+transNode(subNodes.get(1),null)+",'yyyy'))";
	                  default: return super.transFunction_Ref(node,context);
	                }
	            }
	            else
	                return null;
	        }
	        else return null;
	    }
	    /**
	     * Gt_Function_Ref:25
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transGt_Function_Ref(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	            if (subNodes.size() > 1) {
	                GtNode firstNode = subNodes.get(0);
	                if(firstNode.getSym()!=sym.EQT)
	                    return super.transGt_Function_Ref(node,context);
	                else{//EQT用来比较日期时间型
	                    return "(Trunc(("+transNode(subNodes.get(1),context)+
	                               " - To_Date('1000-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss')) * 24 * 3600)="+
	                            "Trunc((("+transNode(subNodes.get(2),context)+") - To_Date('1000-01-01 00:00:00', 'yyyy-mm-dd hh24:mi:ss')) * 24 * 3600))";
	                }
	            }
	            else return null;
	        }
	        else
	            return null;
	    }
	    /**
	     * When_List_Normal:27
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transWhen_List_Normal(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	           String Result="";
	           for(int i=0;i<subNodes.size();i++){
	               String transNodeString=transNode(subNodes.get(i),context);
	               if(transNodeString!=null){
	                   if(Result.equalsIgnoreCase(""))
	                       Result=transNodeString;
	                   else
	                       Result +=","+transNodeString;
	               }
	           }
	           return Result;
	        }
	        else
	            return null;

	    }
	    /**
	     * Else_Expr:28
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transElse_Expr(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null&&subNodes.size()==1){
	            return transNode(subNodes.get(0),null);
	        }
	        else return null;

	    }
	    /**
	     * When_List_Bool:29
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transWhen_List_Bool(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	           String Result="";
	           for(int i=0;i<subNodes.size();i++){
	               String transNodeString=transNode(subNodes.get(i),context);
	               if(transNodeString!=null){
	                   if(Result.equalsIgnoreCase(""))
	                       Result=transNodeString;
	                   else
	                       Result +=","+transNodeString;
	               }
	           }
	           return Result;
	        }
	        else return null;

	    }

	    /**
	     * Decode_List:32
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transDecode_List(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	           String Result="";
	           for(int i=0;i<subNodes.size();i++){
	               String transNodeString=transNode(subNodes.get(i),context);
	               if(transNodeString!=null)
	                   Result+=","+transNodeString;
	           }
	           return Result;
	        }
	        else
	            return null;
	    }
	    /**
	     * Decode_Cond_Res_Expr:33
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transDecode_Cond_Res_Expr(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null&&subNodes.size()==2){
	            return transNode(subNodes.get(0),null)+","+transNode(subNodes.get(1),null);
	        }
	        else
	            return null;
	    }

	    /**
	     * case_expression_when_list_normal_else:34
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transCase_Expression_When_List_Normal_Else(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null&&subNodes.size()==3){
	           String NodeStr1=transNode(subNodes.get(0),null);
	           String NodeStr2=transNode(subNodes.get(1),null);
	           String NodeStr3=transNode(subNodes.get(2),null);
	           if(NodeStr3!=null)
	               return "decode("+NodeStr1+","+NodeStr2+","+NodeStr3+")";
	           else
	               return "decode("+NodeStr1+","+NodeStr2+")";
	        }
	        else
	            return null;

	    }
	    /**
	     * Case_When_List_Bool_Else:35
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transCase_When_List_Bool_Else(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null&&subNodes.size()==2){
	           String NodeStr1=transNode(subNodes.get(0),null);
	           String NodeStr2=transNode(subNodes.get(1),null);
	           if(NodeStr2!=null)
	               return "decode("+NodeStr1+","+NodeStr2+")";
	           else
	               return "decode("+NodeStr1+")";
	        }
	        else
	            return null;

	    }


	    /**
	     * Function_Convert_With_Datatype_Length:36
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_Convert_With_Datatype_Length(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes!=null&&subNodes.size()==3){
	            String nodeStr1=transNode(subNodes.get(0),null);
	            String nodeStr2=transNode(subNodes.get(1),null);
	            String nodeStr3=transNode(subNodes.get(2),null);
	            if(nodeStr1.equalsIgnoreCase("chr")||nodeStr1.equalsIgnoreCase("varchar"))
	                return "rPad(to_char("+nodeStr3+"),"+nodeStr2+",' ')";
	            else {
	                throwExceptionMess(node.getLineno(),node.getData(),"convert 函数不支持 "+nodeStr1+"("+nodeStr2+") 参数");
	                return"<convert 函数不支持 "+nodeStr1+"("+nodeStr2+") 参数>";
	            }
	        }
	        else return null;
	    }
	    /**
	     * Function_Count:38
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_DateAdd(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes!=null && subNodes.size()==3){
	          GtNode firstNode=subNodes.get(0);
	          String firstNodeStr=transNode(firstNode,null);
	          if(firstNodeStr.equalsIgnoreCase("HOUR")){
	              return "to_date("+transNode(subNodes.get(2),null)+")+"+
	                      transNode(subNodes.get(1),null)+"/24";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("MINUTE")){
	              return "to_date("+transNode(subNodes.get(2),null)+")+"+
	                      transNode(subNodes.get(1),null)+"/1440";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("SECOND")){
	              return "to_date("+transNode(subNodes.get(2),null)+")+"+
	                      transNode(subNodes.get(1),null)+"/86400";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("DAY")){
	              return "to_date("+transNode(subNodes.get(2),null)+")+"+
	                      transNode(subNodes.get(1),null);
	          }
	          else if(firstNodeStr.equalsIgnoreCase("WEEK")){
	              return "to_date("+transNode(subNodes.get(2),null)+")+"+
	                      transNode(subNodes.get(1),null)+"*7";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("MONTH")){
	              return "add_Months(to_date("+transNode(subNodes.get(2),null)+"),"+
	                      transNode(subNodes.get(1),null)+")";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("QUARTER")){
	              return "add_Months(to_date("+transNode(subNodes.get(2),null)+"),"+
	                      transNode(subNodes.get(1),null)+"*3)";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("YEAR")){
	              return "add_Months(to_date("+transNode(subNodes.get(2),null)+"),"+
	                      transNode(subNodes.get(1),null)+"*12)";
	          }
	          else{
	              throwExceptionMess(firstNode.getLineno(),firstNode.getData(),"DateAdd不支持 "+firstNodeStr+" 参数");
	              return "<DateAdd不支持 "+firstNodeStr+" 参数>";
	          }
	        }
	        else return null;
	    }
	    /**
	     * Function_Dateiff:39
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_DateDIff(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes!=null && subNodes.size()==3){
	          GtNode firstNode=subNodes.get(0);
	          String firstNodeStr=transNode(firstNode,null);
	          String NodeStr1=transNode(subNodes.get(1),null);
	          String NodeStr2=transNode(subNodes.get(2),null);
	          if(firstNodeStr.equalsIgnoreCase("SECOND")){
	              return "((trunc(to_date("+NodeStr2+"))-"+
	                      "trunc(to_date("+NodeStr1+")))*24*3600+"+
	                      "to_number(to_char(to_date("+NodeStr2+"),'SSSSS'))-"+
	                      "to_number(to_char(to_date("+NodeStr1+"),'SSSSS')))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("DAY")){
	              return "trunc(to_date("+NodeStr2+"))-"+
	                      "trunc(to_date("+NodeStr1+"))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("WEEK")){
	              return "floor((to_date("+NodeStr2+")-"+
	                      "trunc(to_date("+NodeStr1+"),'day'))/7)";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("MONTH")){
	              return "(to_number(to_char(to_date("+NodeStr2+"),'mm'))-"+
	                      "to_number(to_char(to_date("+NodeStr1+"),'mm'))+"+
	                      "to_number(to_char(to_date("+NodeStr2+"),'yyyy'))*12-"+
	                      "to_number(to_char(to_date("+NodeStr1+"),'yyyy'))*12)";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("QUARTER")){
	              return "(ceil(to_number(to_char(to_date("+NodeStr2+"),'mm'))/3)-"+
	                      "ceil(to_number(to_char(to_date("+NodeStr1+"),'mm'))/3)+"+
	                      "to_number(to_char(to_date("+NodeStr2+"),'yyyy'))*4-"+
	                      "to_number(to_char(to_date("+NodeStr1+"),'yyyy'))*4)";

	          }
	          else if(firstNodeStr.equalsIgnoreCase("YEAR")){
	              return "(to_number(to_char(to_date("+NodeStr2+"),'yyyy'))-"+
	                      "to_number(to_char(to_date("+NodeStr1+"),'yyyy')))";
	          }
	          else{
	              throwExceptionMess(firstNode.getLineno(),firstNode.getData(),"DateDiff不支持 "+firstNodeStr+" 参数");
	              return "<DateDiff不支持 "+firstNodeStr+" 参数>";
	          }
	        }
	        else return null;
	    }
	    /**
	     * Function_Datepart:40
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_Datepart(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if (subNodes!=null && subNodes.size()==2){
	          GtNode firstNode=subNodes.get(0);
	          String firstNodeStr=transNode(firstNode,null);
	          String NodeStr1=transNode(subNodes.get(1),null);
	          if(firstNodeStr.equalsIgnoreCase("HOUR")){
	              return "to_number(to_char(to_date("+NodeStr1+"),'hh24'))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("MINUTE")){
	              return "to_number(to_char(to_date("+NodeStr1+"),'mi'))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("WEEK")){
	              return "ceil((trunc(to_date("+NodeStr1+"),'day')+7-"+
	                      "trunc(to_date("+NodeStr1+"),'year'))/7)";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("WEEKDAY")){
	              return "floor(to_date("+NodeStr1+")-"+
	                      "trunc(to_date("+NodeStr1+"),'day')+1)";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("SECOND")){
	              return "to_number(to_char(to_date("+NodeStr1+"),'ss'))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("DAY")){
	              return "to_number(to_char(to_date("+NodeStr1+"),'dd'))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("WEEK")){
	              return "ceil((trunc(to_date("+NodeStr1+"),'day')+7-trunc(to_date("+NodeStr1+"),'year'))/7)";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("MONTH")){
	              return "to_number(to_char(to_date("+NodeStr1+"),'mm'))";
	          }
	          else if(firstNodeStr.equalsIgnoreCase("QUARTER")){
	              return "ceil(to_number(to_char(to_date("+NodeStr1+"),'mm'))/3)";

	          }
	          else if(firstNodeStr.equalsIgnoreCase("YEAR")){
	              return "to_number(to_char(to_date("+NodeStr1+"),'yyyy'))";
	          }
	          else{
	              throwExceptionMess(firstNode.getLineno(),firstNode.getData(),"Datepart不支持 "+firstNodeStr+" 参数");
	              return "<Datepart不支持 "+firstNodeStr+" 参数>";
	          }
	        }
	        else return null;
	    }
	    /**
	     * Function_Nullif:42
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_Nullif(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null && subNodes.size()==2){
	            String NodeStr1=transNode(subNodes.get(0),null);
	            String NodeStr2=transNode(subNodes.get(1),null);
	            return "decode("+NodeStr1+","+NodeStr2+",null,"+NodeStr1+")";
	        }
	        else
	            return null;
	    }

	    /**
	     * Function_Decode:43
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transFunction_Decode(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	            int NodeCount=subNodes.size();
	            if(NodeCount==2){
	                return "decode("+transNode(subNodes.get(0),null)+""+transNode(subNodes.get(1),null)+")";
	            }
	            else if(NodeCount==3){
	                return "decode("+transNode(subNodes.get(0),null)+transNode(subNodes.get(1),null)+
	                        ","+transNode(subNodes.get(2),null)+")";
	            }
	            else{
	                throwExceptionMess(node.getLineno(),node.getData(),"decode定义错误");
	                return "<decode 定义错误>";
	            }
	        }
	        else
	            return null;

	    }
	    /**
	     * When_Expression_Then_Expression:44
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transWhen_Expression_Then_Expression(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null && subNodes.size()==2){
	            return transNode(subNodes.get(0),null)+","+transNode(subNodes.get(1),null);
	        }
	        else
	            return null;

	    }
	    /**
	     * When_Expression_Bool_Then_Expression:45
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transWhen_Expression_Bool_Then_Expression(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null && subNodes.size()==2){
	            return transNode(subNodes.get(0),"WhenBool")+","+transNode(subNodes.get(1),null);
	        }
	        else
	            return null;

	    }


	    /**
	     * Table_Source:47
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transTable_Source(GtNode node,String context) throws Exception{
	       Vector<GtNode> subNodes=node.getSubNodes();
	       if(subNodes!=null && subNodes.size()==1 && subNodes.get(0).getSym()==sym.joined_table){
	           JoinTable="";
	           JoinWhere="";
	           return transNode(subNodes.get(0),null);
	       }
	       else
	         return super.transTable_Source(node,context);
	    }
	    /**
	     * Joined_Table:49
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transJoined_Table(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	           if(subNodes.size()==3){
	              return transNode(subNodes.get(0),null)+ transNode(subNodes.get(1),null)+ transNode(subNodes.get(2),null);
	           }
	           else{
	              if (JoinTable.equalsIgnoreCase(""))
	                  JoinTable=transNode(subNodes.get(0),null);
	              else
	                  JoinTable+=','+transNode(subNodes.get(0),null);
	              transNode(subNodes.get(1),null);
	              return JoinTable;
	           }
	        }
	        else
	            return null;
	    }
	    /**
	     *Join_List:50
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transJoin_List(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	           if(subNodes.get(0).getSym()==sym.LKH){
	               transNode(subNodes.get(1),"JOIN");
	           }
	           else{
	               for(int i=0;i<subNodes.size();i++){
	                   transNode(subNodes.get(i),"JOIN");
	               }
	           }
	           return null;
	        }
	        else
	          return null;
	    }
	    /**
	     *Join_List_Element: 51
	     * @param node GtNode
	     * @param context String
	     * @return String
	     */
	    protected String transJoin_List_Element(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	            int nodeCount=subNodes.size();
	            if(nodeCount==3){
	                String JoinCon="";
	                switch(subNodes.get(0).getSym()){
	                   case sym.LEFT:JoinCon="LJOIN";break;
	                   case sym.RIGHT: JoinCon="RJOIN";break;
	                   case sym.INNER: JoinCon="IJOIN";break;
	                }
	                JoinTable+=","+transNode(subNodes.get(1),null);
	                transNode(subNodes.get(2),JoinCon);
	                return null;
	            }
	            else return null;
	        }
	        else{
	            return null;
	        }
	    }

	    protected String transExpression_Bool(GtNode node,String context) throws Exception{
	        Vector<GtNode> subNodes=node.getSubNodes();
	        if(subNodes!=null){
	            if(context!=null&&context.equalsIgnoreCase("WhenBool")&&subNodes.size()==3&&subNodes.get(1).isTerminal()){
	                String NodeStr1= transNode(subNodes.get(0), context);
	                String NodeStr2= transNode(subNodes.get(1), context);
	                String NodeStr3= transNode(subNodes.get(2), context);
	                if(NodeStr2.equalsIgnoreCase("and"))
	                    return "compAnd(("+NodeStr1+"),("+NodeStr3+"))";
	                else if(NodeStr2.equalsIgnoreCase("or"))
	                    return "compOr(("+NodeStr1+"),("+NodeStr3+"))";
	                else if(NodeStr2.equalsIgnoreCase("is"))
	                    return NodeStr1+" "+NodeStr2+" "+NodeStr3;
	                else
	                    return "compN("+NodeStr1+",'"+NodeStr2+"',"+NodeStr3+")";
	            }
	            else{
	                String Result = "";

	                /*对于JOIN..ON语句后面的条件，不返回值，直接拼凑到JoinWhere中*/
	                if (context == null || context.equals("")||
	                    (!context.startsWith("LJOIN")&&!context.startsWith("RJOIN")&&
	                     !context.startsWith("IJOIN"))){
	                    for (int i = 0; i < subNodes.size(); i++) {
	                       String transNodeString="";
	                       if(subNodes.get(i).getSym()==sym.select_statement){
	                           DefaultTrans thisTrans=new OracleTrans();
	                           transNodeString=thisTrans.transNode(subNodes.get(i),null);
	                       }
	                       else{
	                           transNodeString = transNode(subNodes.get(i), context);
	                       }
	                       if (transNodeString != null)
	                            Result += " " + transNodeString;
	                    }
	                    return Result;
	                }
	                else {
	                    for (int i = 0; i < subNodes.size(); i++) {
	                       String transNodeString="";
	                       if(subNodes.get(i).getSym()==sym.select_statement){
	                           DefaultTrans thisTrans=new OracleTrans();
	                           transNodeString=thisTrans.transNode(subNodes.get(i),null);
	                       }
	                       else{
	                           transNodeString = transNode(subNodes.get(i), context+"_S");
	                       }
	                       if (transNodeString != null)
	                            Result += " " + transNodeString;
	                    }
	                    if(context.endsWith("_S"))
	                        return Result;
	                    else{//只对首层返回值进行处理
	                        if (Result != null &&
	                            !Result.trim().equalsIgnoreCase("")) {
	                            if (JoinWhere == null ||
	                                JoinWhere.trim().equalsIgnoreCase(""))
	                                JoinWhere = Result;
	                            else {
	                                JoinWhere += " and " + Result;
	                            }
	                        }
	                        return null;
	                    }
	                }
	            }

	        }
	        else return null;
	    }

	}
