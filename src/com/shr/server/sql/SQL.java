package com.shr.server.sql;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Vector;
import java.util.Date;

import java.io.StringReader;

public class SQL {
	private String dbType;
	private boolean sqlModified = false;
	private StringBuffer gtText = new StringBuffer();
	private StringBuffer dbmsText = new StringBuffer();
	private StringBuffer jdbcText = new StringBuffer();
	private SqlParameters params = new SqlParameters();
	private Vector paramsMap = new Vector();

	
	public SQL(String dbType) {
		this.dbType = dbType;
	}
	private void transSql() {
		String result = gtText.toString();
		dbmsText.setLength(0);

		try {

			Yylex myScanner = new Yylex(new StringReader(result));
			parser myParser = new parser(myScanner);

			GtNode root = (GtNode) myParser.parse().value;
			DefaultTrans sMSSQLTrans, sOraTrans;
			if (dbType.equalsIgnoreCase("MSSQL") || dbType.equalsIgnoreCase("")) {
				sMSSQLTrans = new MSSQLTrans();
				result = sMSSQLTrans.transNode(root, null);
			}

			if (dbType.equalsIgnoreCase("ORACLE")) {
				sOraTrans = new OracleTrans();
				result = sOraTrans.transNode(root, null);
			}

			result = result.replaceAll("&#XA;&#XA;", " ");
		} catch (Exception e) {
			//e.printStackTrace();
		}

		dbmsText.append(result);
	}

	public void clear() {
		gtText.setLength(0);
		dbmsText.setLength(0);
		;
		jdbcText.setLength(0);
		params.Clear();
		paramsMap.clear();
		sqlModified = true;
	}

	private boolean IsNameDelimiter(char ch) {
		return (ch == ' ') || (ch == ',') || (ch == ';') || (ch == ')')
				|| (ch == '\n') || (ch == '\r') || (ch == '+') || (ch == '|');
	}

	private void parseSql() {
		String paramName;

		jdbcText.setLength(0);
		paramsMap.clear();
		int i = 0, StartPos = 0;
		int len = dbmsText.length();

		while (i < len) {
			switch (dbmsText.charAt(i)) {
			case '\'':

				do {
					i++;
				} while ((i < len) && (dbmsText.charAt(i) != '\''));
				i++;
				break;
			case ':':
				jdbcText.append(dbmsText.subSequence(StartPos, i)).append('?');

				int NameStart = ++i;

				while ((i < len) && !IsNameDelimiter(dbmsText.charAt(i))) {
					i++;
				}

				paramName = dbmsText.substring(NameStart, i).toUpperCase();
				paramsMap.add(paramName);

				StartPos = i;
				break;
			default:
				i++;
				break;
			}
		}
		if (StartPos < len) {
			jdbcText.append(dbmsText.substring(StartPos));
		}
	}

	public String getGtSql() {
		return gtText.toString();
	}

	private void processSql() {

		if (sqlModified) {
			transSql();
			parseSql();

			sqlModified = false;
		}
	}

	public String getDbmsSql() {
		processSql();
		return dbmsText.toString();
	}

	public String getJdbcSql() {
		processSql();
		return jdbcText.toString();

	}

	public Vector getParamsMap() {
		return paramsMap;
	}

	public void Add(String sqlLine) {
		gtText.append(sqlLine).append('\n').append('\r');
		sqlModified = true;
	}

	public SqlParameters getSqlParameters() {
		processSql();
		return params;
	}

	public void setString(String paramName, String paramValue) {
		params.AddParam(paramName, (paramValue == null) ? "" : paramValue);
	}

	public void setDouble(String paramName, double paramValue) {
		params.AddParam(paramName, new Double(paramValue));
	}

	public void setDate(String paramName, Date paramValue) {
		params.AddParam(paramName, new java.sql.Date(paramValue.getTime()));
	}

	public void setDate(String paramName, String paramValue) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (paramValue != null && paramValue.length() == 10) {
			paramValue = paramValue + " 00:00:00";
		}
		try {
			if ((paramValue == null ? "" : paramValue).equals("")) {
				this.setNull(paramName);
			} else {
				this.setTimestamp(paramName, formatter.parse(paramValue));
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			params.AddParam(paramName, null);
		}
	}

	public void setTime(String paramName, Date paramValue) {
		params.AddParam(paramName, new java.sql.Time(paramValue.getTime()));
	}

	public void setTimestamp(String paramName, Date paramValue) {
		params.AddParam(paramName, new java.sql.Timestamp(paramValue.getTime()));
	}

	public void setNull(String paramName) {
		params.AddParam(paramName);
	}

}