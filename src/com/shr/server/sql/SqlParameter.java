﻿package com.shr.server.sql;

/**
 * <p>Title: 查询参数类</p>
 *
 * <p>Description: 查询参数的键值对</p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SqlParameter {
    private String name;
    private Object value = null;

    /**
     * 得到参数名称
     * @return String
     */

    public String getName() {
        return name;
    }

    /**
     * 设置参数名称
     * @param name String
     */

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    /**
     * 得到参数的值
     * @return Object
     */

    public Object getValue() {
        return value;
    }

    /**
     * 设置参数的值
     * @param value Object
     */

    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * 按照指定名称构造SQLParameter对象
     * @param name string 参数名称
     */
    public SqlParameter(String name) {
        this.name = name.toUpperCase();
    }

    /**
     * 按照指定名称构造SQLParameter对象，将参数值设定为指定值

     *
     * @param name 参数名称
     * @param value 参数值

     */
    public SqlParameter(String name, Object value) {
        this.name = name.toUpperCase();
        this.value = value;
    }

    /**
     * 清空当前参数值

     */
    public void Clear() {
        this.value = null;
    }
}
