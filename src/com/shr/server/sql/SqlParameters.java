﻿package com.shr.server.sql;

import java.util.Hashtable;

/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */
public class SqlParameters {
    private Hashtable items = new Hashtable();

    /**
     * 获得指定名称的参数
     *
     * @param name String 参数名称
     * @return SqlParameter 指定名称的参数
     */
    public SqlParameter getSqlParameter(String name) {
        return (SqlParameter) items.get(name.toUpperCase());
    }


    /**
     * 清除所有参数
     */
    public void Clear() {
        items.clear();
    }

    /**
     * 按照指定的名称增加一个参数，参数值设置为null
     *
     * @param name String 参数名
     * @return SqlParameter 增加的参数
     */
    public SqlParameter AddParam(String name) {
        return AddParam(name, null);
    }

    /**
     * 按照给定的名称及参数值增加一个参数
     *
     * @param name String 参数名
     * @param value Object 参数值
     * @return SqlParameter 增加的参数
     */
    public SqlParameter AddParam(String name, Object value) {
        SqlParameter p = getSqlParameter(name);
        if (p == null) {
            p = new SqlParameter(name, value);
        } else {
            if (p.getValue().equals(value)) {
                return p;
            } else {
                items.remove(name);
                p = new SqlParameter(name, value);
            }
        }
        items.put(name.toUpperCase(), p);
        return p;
    }

    /**
     * 按照给定的名称增加一个参数, 如果参数已经存在，参数值不变，否则参数值为null
     * 用于扫描SQL语句得到参数列表时不影响已经设置好的参数值
     *
     * @param name String 参数名
     * @param value Object 参数值
     * @return SqlParameter 增加的参数
     */
    public SqlParameter AddParamName(String name) {
        SqlParameter p = getSqlParameter(name);
        if (p == null) {
            p = new SqlParameter(name, null);
            items.put(name.toUpperCase(), p);
        }
        return p;
    }
}
