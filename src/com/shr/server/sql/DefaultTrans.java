package com.shr.server.sql;

import java.util.*;
import java.lang.*;
/**
 * <p>Title: </p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company:����ʡ��˼ά��Ϣ�������޹�˾ </p>
 *
 * @author ���� ������
 * @version 1.0
 */
public class DefaultTrans {
//==============================================================================
/**
 * BEGIN TOP...END TOP���־�Ϊ�˷���TopClause(TOP INTNUMBER:i1 INTNUMBER:i2)ʹ��
 * ����ط��������ʹ��
 * �����б�����TOP_A TOP_B rn_��Ϊ����ʹ�ã������в���ʹ��������Ϊ����ʹ��
 */
//BEGIN TOP
    /**
     * Top���Ƿ������������ΪTop INTNUMBER:i1 INTNUMBER: i2��ʽ��
     * ��Ҫ��SelectClause���·���
     */
    protected boolean TopTwo;
    /**
     * ��Top INTNUMBER:i1 INTNUMBER: i2��ʽ����Ч����¼i1
     */
    protected String TopNumber1;
    /**
     *��Top INTNUMBER:i1 INTNUMBER: i2��ʽ����Ч����¼i2
     */
    protected String TopNumber2;
    /**
     *����Top INTNUMBER:i1 INTNUMBER: i2��ʽ��ʹ��
     * �����в���ʹ��TOP_A��Ϊ����ʹ��
     */
    protected static final String TOP_A="TOP_A";
    /**
     *����Top INTNUMBER:i1 INTNUMBER: i2��ʽ��ʹ��
     * �����в���ʹ��TOP_B��Ϊ����ʹ��
     */
    protected static final String TOP_B="TOP_B";
    /**
     * ����Top INTNUMBER:i1 INTNUMBER: i2��ʽ��ʹ��
     * �����в���ʹ��rn_��Ϊ����ʹ��
     */
    protected static final String RN_="rn_";
//END TOP
//==============================================================================
//==============================================================================
/**
 *BEGIN JOIN...END JOINB���־�Ϊ�˷���joined_tableʹ�ã�������������ط�
 */
//BEGIN JOIN

//END JOIN
//==============================================================================
    /**
     * 1:MSSQL
     * 2:ORACLE
     */
    protected int DBType;
    /**
     * ���캯��
     */
    public DefaultTrans() {
        TopTwo=false;
    }
    protected void throwExceptionMess(int lineNo,String Data,String info) throws Exception{
        throw new Exception("�ڵ�"+lineNo+"�е�"+Data+"������ڴ���"+info);
    }
    /**
     *
     * @param node GtNode
     * @param context String
     * @return String
     */
    public String transNode(GtNode node, String context)throws Exception {
            if (!node.isTerminal()) { //����ֹ��
                switch (node.getSym()) {
                case sym.select_statement: //1
                    return transSelect_Statement(node, context);
                case sym.insert_statement: //2
                    return transInsert_Statement(node, context);
                case sym.update_statement: //3
                    return transUpdate_Statement(node, context);
                case sym.delete_statement: //4
                    return transDelete_Statement(node, context);
                case sym.table_name: //5
                    return transTable_Name(node, context);
                case sym.insert_list: //6
                    return transInsert_List(node, context);
                case sym.insert_value_list: //7
                    return transInsert_Value_List(node, context);
                case sym.expression_bool: //8
                    return transExpression_Bool(node, context);
                case sym.update_list: //9
                    return transUpdate_List(node, context);
                case sym.select_clause: //10
                    return transSelect_Clause(node, context);
                case sym.from_clause: //11
                    return transFrom_Clause(node, context);
                case sym.where_clause: //12
                    return transWhere_Clause(node, context);
                case sym.groupby_clause: //13
                    return transGroupBy_Clause(node, context);
                case sym.having_clause: //14
                    return transHaving_Clause(node, context);
                case sym.orderby_clause: //15
                    return transOrderBy_Clause(node, context);
                case sym.options_clause: //16
                    return transOptions_Clause(node, context);
                case sym.top_clause: //17
                    return transTop_Clause(node, context);
                case sym.select_list: //18
                    return transSelect_List(node, context);
                case sym.table_list: //19
                    return transTable_List(node, context);
                case sym.expression_list: //20
                    return transExpression_List(node, context);
                case sym.scalar_expression: //21
                    return transScalar_Expression(node, context);
                case sym.expression: //22
                    return transExpression(node, context);
                case sym.column_ref: //23
                    return transColumn_Ref(node, context);
                case sym.function_ref: //24
                    return transFunction_Ref(node, context);
                case sym.gt_function_ref: //25
                    return transGt_Function_Ref(node, context);
                case sym.comparison: //26
                    return transComparison(node, context);
                case sym.when_list_normal: //27
                    return transWhen_List_Normal(node, context);
                case sym.else_expr: //28
                    return transElse_Expr(node, context);
                case sym.when_list_bool: //29
                    return transWhen_List_Bool(node, context);
                case sym.convert_data_type: //30
                    return transConvert_Data_Type(node, context);
                case sym.datepart: //31
                    return transDatepart(node, context);
                case sym.decode_list: //32
                    return transDecode_List(node, context);
                case sym.decode_cond_res_expr: //33
                    return transDecode_Cond_Res_Expr(node, context);
                case sym.case_expression_when_list_normal_else: //34
                    return transCase_Expression_When_List_Normal_Else(node, context);
                case sym.case_when_list_bool_else: //35
                    return transCase_When_List_Bool_Else(node, context);
                case sym.function_convert_with_datatype_length: //36
                    return transFunction_Convert_With_Datatype_Length(node,
                            context);
                case sym.function_count: //37
                    return transFunction_Count(node, context);
                case sym.function_dateadd: //38
                    return transFunction_DateAdd(node, context);
                case sym.function_datediff: //39
                    return transFunction_DateDIff(node, context);
                case sym.function_datepart: //40
                    return transFunction_Datepart(node, context);
                case sym.function_sum: //41
                    return transFunction_Sum(node, context);
                case sym.function_nullif: //42
                    return transFunction_Nullif(node, context);
                case sym.function_decode: //43
                    return transFunction_Decode(node, context);
                case sym.when_expression_then_expression: //44
                    return transWhen_Expression_Then_Expression(node, context);
                case sym.when_expression_bool_then_expression: //45
                    return transWhen_Expression_Bool_Then_Expression(node,
                            context);
                case sym.function_paramlist: //46
                    return transFunction_ParamList(node, context);
                case sym.table_source: //47
                    return transTable_Source(node, context);
                case sym.derived_table: //48
                    return transDerived_Table(node, context);
                case sym.joined_table: //49
                    return transJoined_Table(node, context);
                case sym.join_list: //50
                    return transJoin_List(node, context);
                case sym.join_list_element: //51
                    return transJoin_List_Element(node, context);
                case sym.orderby_list: //52
                    return transOrderBy_List(node, context);
                case sym.orderby_element: //53
                    return transOrderBy_Element(node, context);
                case sym.orderby_option: //54
                    return transOrderBy_Option(node, context);
                case sym.update_element: //55
                    return transUpdate_Element(node, context);
                default:
                    return null;
                }
            } else
                return transTerminal(node, context);

    }
    /**
     * ������ֹ��
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transTerminal(GtNode node,String context) throws Exception {
        /**
         * ������ֹ��ķ��룬һ�������ֻҪֱ��ȡData���ɣ�
         * ����һЩ���������DBTYPE�Ĳ�ͬ���������
        */
       if(node!=null){
           switch(node.getSym()){
              case sym.AND://3
                  return "and";
              case sym.BETWEEN://7
                  return "between";
              case sym.ESCAPE://16
                  return "escape";
              case sym.EXISTS://17
                  return "exists";
              case sym.LIKE://30
                  return "like";
              case sym.NOT://31
                  return "not";
              case sym.IN://23
                  return "in";
              case sym.IS://27
                  return "is";
              case sym.NULL://32
                  return "null";
              case sym.ALL://2
                  return "all";
              case sym.ANY://4
                  return "any";
              case sym.SOME://40
                  return "some";
              case sym.OR://35
                  return "or";
              case sym.CHARINDEX: //58 ORALCE��MSSQL��ͬ
                  return "charIndex";
              case sym.JIA://117 �Ӻ�
                  return "+";
              case sym.JIAN://118 ����
                  return "-";
              case sym.CHENG://119 �˺�
                  return "*";
              case sym.CHU://120 ���
                  return "/";
              case sym.UMINUS://121 ����
                  return "-";
              case sym.LKH://122 ������
                  return "(";
              case sym.RKH://123 ������
                  return ")";
              case sym.DOUHAO://124 ����
                  return ",";
              case sym.DIAN://125 ���
                  return ".";
              case sym.DENGYU://127 �Ⱥ�
                  return "=";
              case sym.BUDENGYU://128 ���Ⱥ�
                  return "<>";
              case sym.XIAOYU://129 С�ں�
                  return "<";
              case sym.DAYU://130 ���ں�
                  return ">";
              case sym.XDENGYU://131 С�ڵ���
                  return "<=";
              case sym.DDENGYU://132 ���ڵ���
                  return ">=";
              case sym.LOWER:
                  return "lower";
              case sym.LTRIM://80
                  return "lTrim";
              case sym.MAX://81
                  return "max";
              case sym.MIN://82
                  return "min";
              case sym.MOD://83
                  return "mod";
              case sym.RADIANS://87
                  return "radians";
              case sym.REPLICATE://89
                  return "replicate";
              case sym.RIGHT://37
                  return "right";
              case sym.ROUND://90
                  return "round";
              case sym.RTRIM://91
                  return "rTrim";
              case sym.SIGN://92
                  return "sign";
              case sym.SIN://93
                  return "sin";
              case sym.SQRT://95
                  return "sqrt";
              case sym.TAN://102
                  return "tan";
              case sym.UPPER://104
                  return "upper";
              case sym.DAY://67 ��Ϊ����ʱ��ORALCE��MSSQL��ͬ
                  return "day";
              case sym.MONTH://84 ��Ϊ����ʱ��ORALCE��MSSQL��ͬ
                  return "month";
              case sym.YEAR://105 ��Ϊ����ʱ��ORALCE��MSSQL��ͬ
                  return "year";
              case sym.EXP:
                  return "exp";
              case sym.FLOOR:
                  return "floor";
              case sym.POWER:
                  return "power";
              case sym.REPLACE:
                  return "replace";
              case sym.DISTINCT:
                  return "distinct";
              default:
                 return node.getData();
           }
       }
       else
           return null;
    }
    /**
     * Select_Statement: 1
     * �������ͣ����ɸ�ݽ���������֣�
     *     a)Select_Statement=select_clause
     *                        from_clause
     *                        where_clause
     *                        groupBy_clause
     *                        having_clause
     *                       orderBy_clause
     *     b)Select_Statement=Select_Statement
     *                        union options_clause
     *                        Select_Statement
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transSelect_Statement(GtNode node,String context) throws Exception{
        TopTwo=false;
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
            int nodeCount = subNodes.size();
            if (nodeCount==3){
                DefaultTrans thisTrans1,thisTrans2;
                if(DBType==1){
                    thisTrans1 = new MSSQLTrans();
                    thisTrans2 = new MSSQLTrans();
                }
                else{
                    thisTrans1 = new OracleTrans();
                    thisTrans2 = new OracleTrans();
                }
                String options_clause=transNode(subNodes.get(1), context);
                if ((options_clause==null) ||(options_clause.equalsIgnoreCase("distinct")) )
                        options_clause="";
                return thisTrans1.transNode(subNodes.get(0), context)+"\n"+"union "+
                       options_clause+"\n"+
                       thisTrans2.transNode(subNodes.get(2), context);
            }
            else if(nodeCount==6){
                //����6�����ģ����dbtype�Ĳ�ͬ��ͬ���������ദ��
                return null;
            }
            else
                return null;
        }
        else
            return null;
    }
    /**
     * Insert_Statement:2
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transInsert_Statement(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null&&subNodes.size()==3){
           return "insert into "+transNode(subNodes.get(0),null)+"(\n"+
                   "  "+transNode(subNodes.get(1),null)+")\n"+
                   transNode(subNodes.get(2),null);
       }
       else
           return null;
    }
    /**
     * Update_Statement:3
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transUpdate_Statement(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null&&subNodes.size()==3){
           String WhereStr=transNode(subNodes.get(2),null);
           if(WhereStr!=null && !WhereStr.trim().equalsIgnoreCase("")){
               return "update " + transNode(subNodes.get(0), null) + "\n" +
                       "  set " + transNode(subNodes.get(1), null) + "\n" +
                       "where " + transNode(subNodes.get(2), null);
           }
           else{
               throwExceptionMess(node.getLineno(),node.getData(),"update��������Ҫ��where�Ӿ䣡");
               return null;
           }
       }
       else
           return null;

    }
    /**
     * Delete_Statement:4
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transDelete_Statement(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null&&subNodes.size()==2){
            String WhereStr=transNode(subNodes.get(1),null);
            if(WhereStr!=null&&!WhereStr.trim().equalsIgnoreCase("")){
                return "delete from " + transNode(subNodes.get(0), null) + "\n" +
                        "where " + WhereStr;
            }
            else{
               throwExceptionMess(node.getLineno(),node.getData(),"delete��������Ҫ��where�Ӿ䣡");
               return null;
            }
        }
        else return null;
    }
    /**
     * Table_Name:5
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transTable_Name(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
            int nodeCount=subNodes.size();
            if(nodeCount==1){
               return transNode(subNodes.get(0),null);
            }
            else if(nodeCount==2){
               return transNode(subNodes.get(0),null)+'.'+transNode(subNodes.get(1),null);
            }
            else if(nodeCount==3){
              return transNode(subNodes.get(0),null)+'.'+transNode(subNodes.get(1),null)+
                      '.'+transNode(subNodes.get(2),null);
            }
            else return null;
        }
        else
          return null;
    }
    /**
     * Insert_List:6
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transInsert_List(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                   if(Result.equalsIgnoreCase(""))
                       Result=transNodeString;
                   else
                       Result += ", " + transNodeString;
               }
           }
           return Result;
        }
        else return null;
    }
    /**
     * ����ʱ�����账��Insert_Value_List ::=select_statement�������ֱ�ӷ��ص�Select_Statement
     * Insert_Value_List:7
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transInsert_Value_List(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null&&subNodes.size()==1){
            return "values("+transNode(subNodes.get(0),null)+")";
        }
        else
            return null;
    }
    /**
     * Expression_Bool:8
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transExpression_Bool(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString="";
               if(subNodes.get(i).getSym()==sym.select_statement){
                   DefaultTrans thisTrans;
                   if(DBType==1){
                        thisTrans = new MSSQLTrans();
                    }
                    else{
                        thisTrans = new OracleTrans();
                    }
                    transNodeString=thisTrans.transNode(subNodes.get(i),null);
               }
               else
                  transNodeString = transNode(subNodes.get(i),context);
               if(transNodeString!=null)
                   Result+=" "+transNodeString;
           }
           return Result;
       }
       else
           return null;
    }
    /**
     * Update_List:9
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transUpdate_List(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                   if(Result.equalsIgnoreCase(""))
                       Result=transNodeString;
                   else
                       Result += ", \n      " + transNodeString;
               }
           }
           return Result;
       }
       else
           return null;

    }
    /**
     * Select_Clause:10
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String  transSelect_Clause(GtNode node,String context) throws Exception{
      return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * From_Clause:11
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFrom_Clause(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
            if(subNodes.size()==1)
                return "from "+transNode(subNodes.get(0),context);
            else
                return null;
        }
        else
          return null;
    }
    /**
     * Where_Clause:12
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transWhere_Clause(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes==null) return null;
        else{
            if(subNodes.size()==1)
                return "where "+transNode(subNodes.get(0),null);
            else return null;
        }
    }
    /**
     * GroupBy_Clause:13
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transGroupBy_Clause(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes==null){
           return null;
       }
       else{
          String Result="group by ";
          for(int i=0;i<subNodes.size();i++){
              String transNodeString = transNode(subNodes.get(i), context);
              if (transNodeString != null)
                  Result += transNodeString;
          }
          return Result;
       }
    }
    /**
     * Having_Clause:14
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transHaving_Clause(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes==null){
           return null;
       }
       else if(subNodes.size()==1){
           return "having "+transNode(subNodes.get(0),null);
       }
       else return null;
    }
    /**
     * OrderBy_Clause:15
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transOrderBy_Clause(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes==null){
           return null;
       }
       else if(subNodes.size()==1){
           return "order by "+transNode(subNodes.get(0),null);
       }
       else return null;
    }
    /**
     * Options_Clause:16
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transOptions_Clause(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes==null) return null;
        else {
            String Result="";
            for (int i = 0; i < subNodes.size(); i++){
                String transNodeString=transNode(subNodes.get(i),context);
                if(transNodeString!=null) Result+=transNodeString;
            }
            if(!Result.equalsIgnoreCase("")) return Result;
            else return null;
        }
    }
    /**
     * Top_Clause:17
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transTop_Clause(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERNET
    }
    /**
     * Select_List:18
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transSelect_List(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if(subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null) {
                   if (!Result.equalsIgnoreCase(""))
                       Result += ", "+transNodeString;
                   else
                       Result = transNodeString;
               }
           }
           return Result;
       }
       else
           return null;
    }
    /**
     * Table_List:19
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transTable_List(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                  if(Result.equalsIgnoreCase(""))
                      Result=transNodeString;
                  else
                      Result += "," + transNodeString;
               }
           }
           return Result;
       }
       else
           return null;

    }
    /**
     * Expression_List:20
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transExpression_List(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                   if(Result.equalsIgnoreCase(""))
                       Result=transNodeString;
                   else
                     Result += ", "+transNodeString;
               }
           }
           return Result;
        }
        else return null;
    }
    /**
     * Scalar_Expression:21
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transScalar_Expression(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if(subNodes!=null){
           int nodeCount=subNodes.size();
           if (nodeCount==1)
               return transNode(subNodes.get(0),context);
           else if(nodeCount==2)
               return transNode(subNodes.get(0),context)+" as "+transNode(subNodes.get(1),context);
           else
               return null;
       }
       else
           return null;

    }
    /**
     * Expression:22
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transExpression(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null)
                   Result+=transNodeString;
           }
           return Result;
       }
       else
           return null;
    }
    /**
     * Column_Ref:23
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transColumn_Ref(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                   if(Result.equalsIgnoreCase(""))
                       Result=transNodeString;
                   else
                       Result +="."+transNodeString;
               }
           }
           return Result;
       }
       else
           return null;

    }
    /**
     * Function_Ref:24
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Ref(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null){
            int nodeCount=subNodes.size();
            GtNode firstNode=subNodes.get(0);
            if(firstNode.isTerminal()){
                switch(firstNode.getSym()){
                  case sym.ABS:
                  case sym.ACOS:
                  case sym.ASCII:
                  case sym.ASIN:
                  case sym.ATAN:
                  case sym.CEILING:
                  case sym.CHAR:
                  case sym.COS:
                  //case sym.COT:
                  case sym.EXP:
                  case sym.FLOOR:
                  case sym.DATALENGTH:
                  case sym.LOG:
                  case sym.LOWER:
                  case sym.LTRIM:
                  case sym.MAX:
                  case sym.MIN:
                  case sym.RTRIM:
                  case sym.SIGN:
                  case sym.SIN:
                  case sym.SQRT:
                  case sym.STR:
                  case sym.TAN:
                  case sym.UPPER:
                  case sym.TOKEN:
                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+")";
                  case sym.AVG:{
                      String opStr=transNode(subNodes.get(1),null);
                      if (opStr==null)
                          return transNode(firstNode,null)+"("+transNode(subNodes.get(2),null)+")";
                      else
                          return transNode(firstNode,null)+"("+opStr+" "+
                                  transNode(subNodes.get(2),null)+")";
                  }
                  case sym.ATN2:
                  case sym.POWER:
                  case sym.ROUND:
                  case sym.ISNULL:
                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+","+
                              transNode(subNodes.get(2),null)+")";
                  case sym.GETDATE:
                      return transNode(firstNode,null);
                  case sym.ISNUMERIC:
                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+")";
                  case sym.REPLACE:
                  case sym.SUBSTRING:
                      return transNode(firstNode,null)+"("+transNode(subNodes.get(1),null)+","+
                              transNode(subNodes.get(2),null)+","+transNode(subNodes.get(3),null)+")";
                  case sym.DAY://ORALCE&MSSQL DIFFERNT
                  case sym.CHARINDEX://ORALCE&MSSQL DIFFERNT
                  case sym.CONVERT://ORALCE&MSSQL DIFFERNT
                  case sym.DEGREES://ORALCE&MSSQL DIFFERNT
                  case sym.LEFT://ORALCE&MSSQL DIFFERNT
                  case sym.LEN://ORALCE&MSSQL DIFFERNT
                  case sym.LOG10://ORALCE&MSSQL DIFFERNT
                  case sym.LPAD://ORALCE&MSSQL DIFFERNT
                  case sym.MOD://ORALCE&MSSQL DIFFERNT
                  case sym.MONTH://ORALCE&MSSQL DIFFERNT
                  case sym.RADIANS://ORALCE&MSSQL DIFFERNT
                  case sym.REPLICATE://ORALCE&MSSQL DIFFERNT
                  case sym.RIGHT://ORALCE&MSSQL DIFFERNT
                  case sym.SPACE://ORALCE&MSSQL DIFFERNT
                  case sym.STUFF://ORALCE&MSSQL DIFFERNT
                      return null;
                  default: return null;
                }
            }
            else
                return null;
        }
        else return null;
    }
    /**
     * Gt_Function_Ref:25
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transGt_Function_Ref(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null){
            if(subNodes.size()>1){
                GtNode firstNode = subNodes.get(0);
                switch(firstNode.getSym()){
                  case sym.ISZERO:
                      if(subNodes.size()==2)
                          return "(abs("+transNode(subNodes.get(1),context)+")<=0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(2),context).trim());i++)
                              s+="0";
                          return "(abs(" + transNode(subNodes.get(1), context) +
                                  ")<=0." +s+"5)";
                      }
                  case sym.EQ:
                      if(subNodes.size()==3)
                          return "(abs("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+"))<=0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(3),context).trim());i++)
                              s+="0";
                          return "(abs("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+"))<=0."+s+"5)";
                      }
                  case sym.EQT:
                      return null;//MSSQL��ORACLE��ͬ
                  case sym.NE:
                      if(subNodes.size()==3)
                          return "(abs("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+")"
                              +")>0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(3),context).trim());i++)
                              s+="0";
                          return "(abs("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+")"
                              +")>0."+s+"5)";
                      }
                  case sym.GT:
                      if(subNodes.size()==3)
                          return "("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+")>0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(3),context).trim());i++)
                              s+="0";
                          return "("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+")>0."+s+"5)";
                      }
                  case sym.GE:
                      if(subNodes.size()==3)
                          return "("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+")>-0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(3),context).trim());i++)
                              s+="0";
                          return "("+transNode(subNodes.get(1),context)+"-("+
                              transNode(subNodes.get(2),context)+")>-0."+s+"5)";
                      }
                  case sym.LT:
                      if(subNodes.size()==3)
                          return "("+transNode(subNodes.get(2),context)+"-("+
                              transNode(subNodes.get(1),context)+")>0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(3),context).trim());i++)
                              s+="0";
                          return "("+transNode(subNodes.get(2),context)+"-("+
                              transNode(subNodes.get(1),context)+")>0."+s+"5)";
                      }
                  case sym.LE:
                      if(subNodes.size()==3)
                          return "("+transNode(subNodes.get(2),context)+"-("+
                              transNode(subNodes.get(1),context)+")>-0.0005)";
                      else{
                          String s="";
                          for(int i=0;i<Integer.parseInt(transNode(subNodes.get(3),context).trim());i++)
                              s+="0";
                          return "("+transNode(subNodes.get(2),context)+"-("+
                              transNode(subNodes.get(1),context)+")>-0."+s+"5)";
                      }
                  case sym.SUBSTRING2:{
                      throwExceptionMess(node.getLineno(),node.getData(),"<�ݲ�֧��SUBSTRING2����>");
                      return null;
                  }
                  default:
                      return null;
                }
            }
            else
                return null;
        }
        else
          return null;
    }
    /**
     * Comparison:26
     * comparison ::= DENGYU:c {: RESULT = c; :}
     *               |  BUDENGYU:c {: RESULT = c; :}
     *               |  XIAOYU:c {: RESULT = c; :}
     *               |  DAYU:c {: RESULT = c; :}
     *               |  XDENGYU:c {: RESULT = c; :}
     *               |  DDENGYU:c {: RESULT = c; :};
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transComparison(GtNode node,String context) throws Exception{
      //���跭�룬��Ϊ�﷨������ʱ��ֱ�ӷ���������ֹ�����
      return null;
    }
    /**
     * When_List_Normal:27
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transWhen_List_Normal(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Else_Expr:28
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transElse_Expr(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * When_List_Bool:29
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transWhen_List_Bool(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Convert_Data_Type:30
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transConvert_Data_Type(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null && subNodes.size()==1){
            return transNode(subNodes.get(0),context);
        }
        else
            return null;
    }
    /**
     * Datepart:31
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transDatepart(GtNode node,String context) throws Exception{
        return null;//���跭�룬ֱ����transTermial����
    }
    /**
     * Decode_List:32
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transDecode_List(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Decode_Cond_Res_Expr:33
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transDecode_Cond_Res_Expr(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * case_expression_when_list_normal_else:34
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transCase_Expression_When_List_Normal_Else(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Case_When_List_Bool_Else:35
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transCase_When_List_Bool_Else(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Function_Convert_With_Datatype_Length:36
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Convert_With_Datatype_Length(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Function_Count:37
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Count(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes==null)//�޽���ʾcount(*)
            return "count(*)";
        else{
            if(subNodes.size()==2){//�������
                String opr=transNode(subNodes.get(0),null);
                if(opr==null)
                    return "count("+transNode(subNodes.get(1),null)+")";
                else
                    return "count("+opr+" "+transNode(subNodes.get(1),null)+")";
            }
            else return null;
        }
    }
    /**
     * Function_Count:38
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_DateAdd(GtNode node,String context) throws Exception{
        return null;//ORALCE&MSSQL DIFFERENT
    }
    /**
     * Function_Dateiff:39
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_DateDIff(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Function_Datepart:40
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Datepart(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Function_Sum:41
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Sum(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null && subNodes.size()==2){
            String opr=transNode(subNodes.get(0),null);
            if(opr==null || opr.equalsIgnoreCase(""))
                return "sum("+transNode(subNodes.get(1),null)+")";
            else
                return "sum("+opr+" "+transNode(subNodes.get(1),null)+")";
        }
        else return null;
    }
    /**
     * Function_Nullif:42
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Nullif(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Function_Decode:43
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_Decode(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * When_Expression_Then_Expression:44
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transWhen_Expression_Then_Expression(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * When_Expression_Bool_Then_Expression:45
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transWhen_Expression_Bool_Then_Expression(GtNode node,String context) throws Exception{
        return null;//ORACLE&MSSQL DIFFERENT
    }
    /**
     * Function_ParamList:46
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transFunction_ParamList(GtNode node,String context) throws Exception{
      Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                   if(Result.equalsIgnoreCase(""))
                       Result=transNodeString;
                   else
                       Result += ","+transNodeString;
               }
           }
           return Result;
       }
       else
           return null;
    }
    /**
     * Table_Source:
     *     a)table_name
     *     b)table_name TOKEN //��table_name ����
     *     c)derived_table
     *     d)derived_table TOKEN
     *     e)joined_table
     * Table_Source:47
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transTable_Source(GtNode node,String context) throws Exception{
       Vector<GtNode> subNodes=node.getSubNodes();
       if (subNodes!=null){
           String Result="";
           for(int i=0;i<subNodes.size();i++){
               String transNodeString=transNode(subNodes.get(i),context);
               if(transNodeString!=null){
                   if (i==0) Result=transNodeString;
                   else Result +=" "+ transNodeString;
               }
           }
           return Result;
       }
       else
           return null;
    }
    /**
     * Derived_Table:48
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transDerived_Table(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null && subNodes.size()==1){
            DefaultTrans thisTrans;
            if(DBType==1)
                thisTrans=new MSSQLTrans();
            else
                thisTrans=new OracleTrans();
            return "(" + thisTrans.transNode(subNodes.get(0),null) + ")";
        }
        else return null;
    }
    /**
     * Joined_Table:49
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transJoined_Table(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
            int nodeCount=subNodes.size();
            String Result="";
            for (int i=0;i<nodeCount;i++){
                String transNodeString=transNode(subNodes.get(i),null);
                if(transNodeString!=null){
                   if (i==0) Result=transNodeString;
                   else Result +=" "+ transNodeString;
               }
            }
            return Result;
        }
        else return null;

    }
    /**
     * Join_List:50
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transJoin_List(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
            int nodeCount=subNodes.size();
            String Result="";
            for (int i=0;i<nodeCount;i++){
                String transNodeString=transNode(subNodes.get(i),null);
                if(transNodeString!=null){
                   if (i==0) Result=transNodeString;
                   else Result +=" "+ transNodeString;
               }
            }
            return Result;
        }
        else return null;
    }
    /**
     * Join_List_Element:51
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transJoin_List_Element(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
            int nodeCount=subNodes.size();
            if(nodeCount==3){
                return transNode(subNodes.get(0),null)+" join "+transNode(subNodes.get(1),null)
                        +" on "+transNode(subNodes.get(2),null);
            }
            else return null;
        }
        else return null;

    }
    /**
     * OrderBy_List:52
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transOrderBy_List(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if (subNodes!=null){
           int nodeCount=subNodes.size();
           String Result="";
           for(int i=0;i<nodeCount;i++){
               String transNodeString=transNode(subNodes.get(i),null);
               if(transNodeString!=null){
                   if(!Result.equalsIgnoreCase("")) Result+=","+transNodeString;
                   else Result=transNodeString;
               }
           }
           return Result;
        }
        else return null;

    }
    /**
     * OrderBy_Element:53
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transOrderBy_Element(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null){
           int nodeCount=subNodes.size();
           String Result="";
           for(int i=0;i<nodeCount;i++){
               String transNodeString=transNode(subNodes.get(i),null);
               if(transNodeString!=null){
                   if(!Result.equalsIgnoreCase("")) Result+=" "+transNodeString;
                   else Result=transNodeString;
               }
           }
           return Result;
        }
        else return null;
    }
    /**
     * OrderBy_Option:54
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transOrderBy_Option(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null){
           int nodeCount=subNodes.size();
           String Result="";
           for(int i=0;i<nodeCount;i++){
               String transNodeString=transNode(subNodes.get(i),null);
               if(transNodeString!=null){
                   if(!Result.equalsIgnoreCase("")) Result+=" "+transNodeString;
                   else Result=transNodeString;
               }
           }
           return Result;
        }
        else return null;

    }
    /**
     * Update_Element:55
     * @param node GtNode
     * @param context String
     * @return String
     */
    protected String transUpdate_Element(GtNode node,String context) throws Exception{
        Vector<GtNode> subNodes=node.getSubNodes();
        if(subNodes!=null&&subNodes.size()==2){
           return transNode(subNodes.get(0),null)+"="+transNode(subNodes.get(1),null);
        }
        else return null;

    }

}