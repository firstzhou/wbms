package thinkact.framework.util.sql;
import java_cup.runtime.Symbol;

%%

%cup
%line
%unicode
%notunix
%ignorecase
%type thinkact.framework.util.sql.GtSymbol

digit = [0-9]
number = {digit}+
whitespace = [ \t\n\r\f]

%%

"ALL"     {return new GtSymbol(sym.ALL, yytext(), yyline);}
"AND"    {return new GtSymbol(sym.AND, yytext(), yyline);}
"ANY"    {return new GtSymbol(sym.ANY, yytext(), yyline);}
"AS"    {return new GtSymbol(sym.AS, yytext(), yyline);}
"ASC"    {return new GtSymbol(sym.ASC, yytext(), yyline);}
"BETWEEN"    {return new GtSymbol(sym.BETWEEN, yytext(), yyline);}
"BY"    {return new GtSymbol(sym.BY, yytext(), yyline);}
"CASE"    {return new GtSymbol(sym.CASE, yytext(), yyline);}
"CONVERT"    {return new GtSymbol(sym.CONVERT, yytext(), yyline);}
"CROSS"    {return new GtSymbol(sym.CROSS, yytext(), yyline);}
"DELETE"    {return new GtSymbol(sym.DELETE, yytext(), yyline);}
"DESC"    {return new GtSymbol(sym.DESC, yytext(), yyline);}
"DISTINCT"    {return new GtSymbol(sym.DISTINCT, yytext(), yyline);}
"ELSE"    {return new GtSymbol(sym.ELSE, yytext(), yyline);}
"END"    {return new GtSymbol(sym.END, yytext(), yyline);}
"ESCAPE"    {return new GtSymbol(sym.ESCAPE, yytext(), yyline);}
"EXISTS"    {return new GtSymbol(sym.EXISTS, yytext(), yyline);}
"FALSE"    {return new GtSymbol(sym.FALSE, yytext(), yyline);}
"FROM"    {return new GtSymbol(sym.FROM, yytext(), yyline);}
"FULL"    {return new GtSymbol(sym.FULL, yytext(), yyline);}
"GROUP"    {return new GtSymbol(sym.GROUP, yytext(), yyline);}
"HAVING"    {return new GtSymbol(sym.HAVING, yytext(), yyline);}
"IN"    {return new GtSymbol(sym.IN, yytext(), yyline);}
"INNER"    {return new GtSymbol(sym.INNER, yytext(), yyline);}
"INSERT"    {return new GtSymbol(sym.INSERT, yytext(), yyline);}
"INTO"    {return new GtSymbol(sym.INTO, yytext(), yyline);}
"IS"    {return new GtSymbol(sym.IS, yytext(), yyline);}
"JOIN"    {return new GtSymbol(sym.JOIN, yytext(), yyline);}
"LEFT"    {return new GtSymbol(sym.LEFT, yytext(), yyline);}
"LIKE"    {return new GtSymbol(sym.LIKE, yytext(), yyline);}
"NOT"    {return new GtSymbol(sym.NOT, yytext(), yyline);}
"NULL"    {return new GtSymbol(sym.NULL, yytext(), yyline);}
"ON"    {return new GtSymbol(sym.ON, yytext(), yyline);}
"OR"    {return new GtSymbol(sym.OR, yytext(), yyline);}
"ORDER"    {return new GtSymbol(sym.ORDER, yytext(), yyline);}
"RIGHT"    {return new GtSymbol(sym.RIGHT, yytext(), yyline);}
"SELECT"    {return new GtSymbol(sym.SELECT, yytext(), yyline);}
"SET"    {return new GtSymbol(sym.SET, yytext(), yyline);}
"SOME"    {return new GtSymbol(sym.SOME, yytext(), yyline);}
"THEN"    {return new GtSymbol(sym.THEN, yytext(), yyline);}
"TOP"    {return new GtSymbol(sym.TOP, yytext(), yyline);}
"TRUE"    {return new GtSymbol(sym.TRUE, yytext(), yyline);}
"UNION"    {return new GtSymbol(sym.UNION, yytext(), yyline);}
"UPDATE"    {return new GtSymbol(sym.UPDATE, yytext(), yyline);}
"VALUES"    {return new GtSymbol(sym.VALUES, yytext(), yyline);}
"WHEN"    {return new GtSymbol(sym.WHEN, yytext(), yyline);}
"WHERE"    {return new GtSymbol(sym.WHERE, yytext(), yyline);}

"AVG"    {return new GtSymbol(sym.AVG, yytext(), yyline);}
"COUNT"    {return new GtSymbol(sym.COUNT, yytext(), yyline);}
"MAX"    {return new GtSymbol(sym.MAX, yytext(), yyline);}
"MIN"    {return new GtSymbol(sym.MIN, yytext(), yyline);}
"SUM"    {return new GtSymbol(sym.SUM, yytext(), yyline);}

"ABS"    {return new GtSymbol(sym.ABS, yytext(), yyline);}
"ACOS"    {return new GtSymbol(sym.ACOS, yytext(), yyline);}
"ASCII"    {return new GtSymbol(sym.ASCII, yytext(), yyline);}
"ASIN"    {return new GtSymbol(sym.ASIN, yytext(), yyline);}
"ATAN"    {return new GtSymbol(sym.ATAN, yytext(), yyline);}
"ATN2"    {return new GtSymbol(sym.ATN2, yytext(), yyline);}
"CEILING"    {return new GtSymbol(sym.CEILING, yytext(), yyline);}
"CHAR"    {return new GtSymbol(sym.CHAR, yytext(), yyline);}
"CHARINDEX"    {return new GtSymbol(sym.CHARINDEX, yytext(), yyline);}
"CONVERT"    {return new GtSymbol(sym.CONVERT, yytext(), yyline);}
"COS"    {return new GtSymbol(sym.COS, yytext(), yyline);}
"COT"    {return new GtSymbol(sym.COT, yytext(), yyline);}
"DATALENGTH"    {return new GtSymbol(sym.DATALENGTH, yytext(), yyline);}
"DATEADD"    {return new GtSymbol(sym.DATEADD, yytext(), yyline);}
"DATEDIFF"    {return new GtSymbol(sym.DATEDIFF, yytext(), yyline);}
"DATEPART"    {return new GtSymbol(sym.DATEPART, yytext(), yyline);}
"DAY"    {return new GtSymbol(sym.DAY, yytext(), yyline);}
"DECODE"    {return new GtSymbol(sym.DECODE, yytext(), yyline);}
"DEGREES"    {return new GtSymbol(sym.DEGREES, yytext(), yyline);}
"EXP"    {return new GtSymbol(sym.EXP, yytext(), yyline);}
"FLOOR"    {return new GtSymbol(sym.FLOOR, yytext(), yyline);}
"GETDATE"    {return new GtSymbol(sym.GETDATE, yytext(), yyline);}
"ISNULL"    {return new GtSymbol(sym.ISNULL, yytext(), yyline);}
"ISNUMERIC"    {return new GtSymbol(sym.ISNUMERIC, yytext(), yyline);}
"LEN"    {return new GtSymbol(sym.LEN, yytext(), yyline);}
"LOG"    {return new GtSymbol(sym.LOG, yytext(), yyline);}
"LOG10"    {return new GtSymbol(sym.LOG10, yytext(), yyline);}
"LOWER"    {return new GtSymbol(sym.LOWER, yytext(), yyline);}
"LPAD"    {return new GtSymbol(sym.LPAD, yytext(), yyline);}
"LTRIM"    {return new GtSymbol(sym.LTRIM, yytext(), yyline);}
"MOD"    {return new GtSymbol(sym.MOD, yytext(), yyline);}
"MONTH"    {return new GtSymbol(sym.MONTH, yytext(), yyline);}
"NULLIF"    {return new GtSymbol(sym.NULLIF, yytext(), yyline);}
"PI"    {return new GtSymbol(sym.PI, yytext(), yyline);}
"POWER"    {return new GtSymbol(sym.POWER, yytext(), yyline);}
"RADIANS"    {return new GtSymbol(sym.RADIANS, yytext(), yyline);}
"REPLACE"    {return new GtSymbol(sym.REPLACE, yytext(), yyline);}
"REPLICATE"    {return new GtSymbol(sym.REPLICATE, yytext(), yyline);}
"ROUND"    {return new GtSymbol(sym.ROUND, yytext(), yyline);}
"RTRIM"    {return new GtSymbol(sym.RTRIM, yytext(), yyline);}
"SIGN"    {return new GtSymbol(sym.SIGN, yytext(), yyline);}
"SIN"    {return new GtSymbol(sym.SIN, yytext(), yyline);}
"SPACE"    {return new GtSymbol(sym.SPACE, yytext(), yyline);}
"SQRT"    {return new GtSymbol(sym.SQRT, yytext(), yyline);}
"SQUARE"    {return new GtSymbol(sym.SQUARE, yytext(), yyline);}
"STR"    {return new GtSymbol(sym.STR, yytext(), yyline);}
"STUFF"    {return new GtSymbol(sym.STUFF, yytext(), yyline);}
"SUBSTRING"    {return new GtSymbol(sym.SUBSTRING, yytext(), yyline);}
"SUM"    {return new GtSymbol(sym.SUM, yytext(), yyline);}
"TAN"    {return new GtSymbol(sym.TAN, yytext(), yyline);}
"TRUNC"    {return new GtSymbol(sym.TRUNC, yytext(), yyline);}
"UPPER"    {return new GtSymbol(sym.UPPER, yytext(), yyline);}
"YEAR"   {return new GtSymbol(sym.YEAR, yytext(), yyline);}

"ISZERO"    {return new GtSymbol(sym.ISZERO, yytext(), yyline);}
"EQ"    {return new GtSymbol(sym.EQ, yytext(), yyline);}
"GT"    {return new GtSymbol(sym.GT, yytext(), yyline);}
"GE"    {return new GtSymbol(sym.GE, yytext(), yyline);}
"LT"    {return new GtSymbol(sym.LT, yytext(), yyline);}
"LE"    {return new GtSymbol(sym.LE, yytext(), yyline);}
"NE"    {return new GtSymbol(sym.NE, yytext(), yyline);}
"EQT"    {return new GtSymbol(sym.EQT, yytext(), yyline);}
"SUBSTRING2"    {return new GtSymbol(sym.SUBSTRING2, yytext(), yyline);}

[a-zA-Z_][a-zA-Z0-9_]*     {return new GtSymbol(sym.TOKEN, yytext(), yyline);}
:[A-Za-z][A-Za-z0-9_]*     {return new GtSymbol(sym.SQLPARAMETER, yytext(), yyline);}

"+"     {return new GtSymbol(sym.JIA, yytext(), yyline);}
"-"     {return new GtSymbol(sym.JIAN, yytext(), yyline);}
"*"     {return new GtSymbol(sym.CHENG, yytext(), yyline);}
"/"     {return new GtSymbol(sym.CHU, yytext(), yyline);}

"("     {return new GtSymbol(sym.LKH, yytext(), yyline);}
")"     {return new GtSymbol(sym.RKH, yytext(), yyline);}
","     {return new GtSymbol(sym.DOUHAO, yytext(), yyline);}
"."     {return new GtSymbol(sym.DIAN, yytext(), yyline);}

"="     {return new GtSymbol(sym.DENGYU, yytext(), yyline);}
"<>"     {return new GtSymbol(sym.BUDENGYU, yytext(), yyline);}
"<"     {return new GtSymbol(sym.XIAOYU, yytext(), yyline);}
">"     {return new GtSymbol(sym.DAYU, yytext(), yyline);}
"<="     {return new GtSymbol(sym.XDENGYU, yytext(), yyline);}
">="     {return new GtSymbol(sym.DDENGYU, yytext(), yyline);}

"||"     {return new GtSymbol(sym.CONNECT, yytext(), yyline);}

{number} {return new GtSymbol(sym.INTNUMBER, yytext(), yyline);}
({number}[.]{number})|([.]{number}) {return new GtSymbol(sym.FLOATNUMBER, yytext(), yyline);}

'([^\n]*\\')*[^'\n]*' {return new GtSymbol(sym.STRING, yytext(), yyline);}
'([^\n]*\\')*[^'\n]*$  {return new GtSymbol(sym.error, "Bad string:" + yytext(), yyline);}

{whitespace} {}
.   {}
