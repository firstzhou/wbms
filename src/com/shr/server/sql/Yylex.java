package com.shr.server.sql;
import java_cup.runtime.Symbol;


public class Yylex implements java_cup.runtime.Scanner {
	private final int YY_BUFFER_SIZE = 512;
	private final int YY_F = -1;
	private final int YY_NO_STATE = -1;
	private final int YY_NOT_ACCEPT = 0;
	private final int YY_START = 1;
	private final int YY_END = 2;
	private final int YY_NO_ANCHOR = 4;
	private final int YY_BOL = 65536;
	private final int YY_EOF = 65537;
	private java.io.BufferedReader yy_reader;
	private int yy_buffer_index;
	private int yy_buffer_read;
	private int yy_buffer_start;
	private int yy_buffer_end;
	private char yy_buffer[];
	private int yyline;
	private boolean yy_at_bol;
	private int yy_lexical_state;

	Yylex (java.io.Reader reader) {
		this ();
		if (null == reader) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(reader);
	}

	Yylex (java.io.InputStream instream) {
		this ();
		if (null == instream) {
			throw (new Error("Error: Bad input stream initializer."));
		}
		yy_reader = new java.io.BufferedReader(new java.io.InputStreamReader(instream));
	}

	private Yylex () {
		yy_buffer = new char[YY_BUFFER_SIZE];
		yy_buffer_read = 0;
		yy_buffer_index = 0;
		yy_buffer_start = 0;
		yy_buffer_end = 0;
		yyline = 0;
		yy_at_bol = true;
		yy_lexical_state = YYINITIAL;
	}

	private boolean yy_eof_done = false;
	private final int YYINITIAL = 0;
	private final int yy_state_dtrans[] = {
		0
	};
	private void yybegin (int state) {
		yy_lexical_state = state;
	}
	private int yy_advance ()
		throws java.io.IOException {
		int next_read;
		int i;
		int j;

		if (yy_buffer_index < yy_buffer_read) {
			return yy_buffer[yy_buffer_index++];
		}

		if (0 != yy_buffer_start) {
			i = yy_buffer_start;
			j = 0;
			while (i < yy_buffer_read) {
				yy_buffer[j] = yy_buffer[i];
				++i;
				++j;
			}
			yy_buffer_end = yy_buffer_end - yy_buffer_start;
			yy_buffer_start = 0;
			yy_buffer_read = j;
			yy_buffer_index = j;
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}

		while (yy_buffer_index >= yy_buffer_read) {
			if (yy_buffer_index >= yy_buffer.length) {
				yy_buffer = yy_double(yy_buffer);
			}
			next_read = yy_reader.read(yy_buffer,
					yy_buffer_read,
					yy_buffer.length - yy_buffer_read);
			if (-1 == next_read) {
				return YY_EOF;
			}
			yy_buffer_read = yy_buffer_read + next_read;
		}
		return yy_buffer[yy_buffer_index++];
	}
	private void yy_move_end () {
		if (yy_buffer_end > yy_buffer_start &&
		    '\n' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
		if (yy_buffer_end > yy_buffer_start &&
		    '\r' == yy_buffer[yy_buffer_end-1])
			yy_buffer_end--;
	}
	private boolean yy_last_was_cr=false;
	private void yy_mark_start () {
		int i;
		for (i = yy_buffer_start; i < yy_buffer_index; ++i) {
			if ('\n' == yy_buffer[i] && !yy_last_was_cr) {
				++yyline;
			}
			if ('\r' == yy_buffer[i]) {
				++yyline;
				yy_last_was_cr=true;
			} else yy_last_was_cr=false;
		}
		yy_buffer_start = yy_buffer_index;
	}
	private void yy_mark_end () {
		yy_buffer_end = yy_buffer_index;
	}
	private void yy_to_mark () {
		yy_buffer_index = yy_buffer_end;
		yy_at_bol = (yy_buffer_end > yy_buffer_start) &&
		            ('\r' == yy_buffer[yy_buffer_end-1] ||
		             '\n' == yy_buffer[yy_buffer_end-1] ||
		             2028/*LS*/ == yy_buffer[yy_buffer_end-1] ||
		             2029/*PS*/ == yy_buffer[yy_buffer_end-1]);
	}
	private java.lang.String yytext () {
		return (new java.lang.String(yy_buffer,
			yy_buffer_start,
			yy_buffer_end - yy_buffer_start));
	}
	private int yylength () {
		return yy_buffer_end - yy_buffer_start;
	}
	private char[] yy_double (char buf[]) {
		int i;
		char newbuf[];
		newbuf = new char[2*buf.length];
		for (i = 0; i < buf.length; ++i) {
			newbuf[i] = buf[i];
		}
		return newbuf;
	}
	private final int YY_E_INTERNAL = 0;
	private final int YY_E_MATCH = 1;
	private java.lang.String yy_error_string[] = {
		"Error: Internal error.\n",
		"Error: Unmatched input.\n"
	};
	private void yy_error (int code,boolean fatal) {
		java.lang.System.out.print(yy_error_string[code]);
		java.lang.System.out.flush();
		if (fatal) {
			throw new Error("Fatal Error.\n");
		}
	}
	private int[][] unpackFromString(int size1, int size2, String st) {
		int colonIndex = -1;
		String lengthString;
		int sequenceLength = 0;
		int sequenceInteger = 0;

		int commaIndex;
		String workString;

		int res[][] = new int[size1][size2];
		for (int i= 0; i < size1; i++) {
			for (int j= 0; j < size2; j++) {
				if (sequenceLength != 0) {
					res[i][j] = sequenceInteger;
					sequenceLength--;
					continue;
				}
				commaIndex = st.indexOf(',');
				workString = (commaIndex==-1) ? st :
					st.substring(0, commaIndex);
				st = st.substring(commaIndex+1);
				colonIndex = workString.indexOf(':');
				if (colonIndex == -1) {
					res[i][j]=Integer.parseInt(workString);
					continue;
				}
				lengthString =
					workString.substring(colonIndex+1);
				sequenceLength=Integer.parseInt(lengthString);
				workString=workString.substring(0,colonIndex);
				sequenceInteger=Integer.parseInt(workString);
				res[i][j] = sequenceInteger;
				sequenceLength--;
			}
		}
		return res;
	}
	private int yy_acpt[] = {
		/* 0 */ YY_NOT_ACCEPT,
		/* 1 */ YY_NO_ANCHOR,
		/* 2 */ YY_NO_ANCHOR,
		/* 3 */ YY_NO_ANCHOR,
		/* 4 */ YY_NO_ANCHOR,
		/* 5 */ YY_NO_ANCHOR,
		/* 6 */ YY_NO_ANCHOR,
		/* 7 */ YY_NO_ANCHOR,
		/* 8 */ YY_NO_ANCHOR,
		/* 9 */ YY_NO_ANCHOR,
		/* 10 */ YY_NO_ANCHOR,
		/* 11 */ YY_NO_ANCHOR,
		/* 12 */ YY_NO_ANCHOR,
		/* 13 */ YY_NO_ANCHOR,
		/* 14 */ YY_NO_ANCHOR,
		/* 15 */ YY_NO_ANCHOR,
		/* 16 */ YY_NO_ANCHOR,
		/* 17 */ YY_NO_ANCHOR,
		/* 18 */ YY_NO_ANCHOR,
		/* 19 */ YY_NO_ANCHOR,
		/* 20 */ YY_NO_ANCHOR,
		/* 21 */ YY_NO_ANCHOR,
		/* 22 */ YY_NO_ANCHOR,
		/* 23 */ YY_NO_ANCHOR,
		/* 24 */ YY_NO_ANCHOR,
		/* 25 */ YY_NO_ANCHOR,
		/* 26 */ YY_NO_ANCHOR,
		/* 27 */ YY_NO_ANCHOR,
		/* 28 */ YY_NO_ANCHOR,
		/* 29 */ YY_NO_ANCHOR,
		/* 30 */ YY_NO_ANCHOR,
		/* 31 */ YY_NO_ANCHOR,
		/* 32 */ YY_NO_ANCHOR,
		/* 33 */ YY_NO_ANCHOR,
		/* 34 */ YY_NO_ANCHOR,
		/* 35 */ YY_NO_ANCHOR,
		/* 36 */ YY_NO_ANCHOR,
		/* 37 */ YY_END,
		/* 38 */ YY_NO_ANCHOR,
		/* 39 */ YY_NO_ANCHOR,
		/* 40 */ YY_NO_ANCHOR,
		/* 41 */ YY_NO_ANCHOR,
		/* 42 */ YY_NO_ANCHOR,
		/* 43 */ YY_NO_ANCHOR,
		/* 44 */ YY_NO_ANCHOR,
		/* 45 */ YY_NO_ANCHOR,
		/* 46 */ YY_NO_ANCHOR,
		/* 47 */ YY_NO_ANCHOR,
		/* 48 */ YY_NO_ANCHOR,
		/* 49 */ YY_NO_ANCHOR,
		/* 50 */ YY_NO_ANCHOR,
		/* 51 */ YY_NO_ANCHOR,
		/* 52 */ YY_NO_ANCHOR,
		/* 53 */ YY_NO_ANCHOR,
		/* 54 */ YY_NO_ANCHOR,
		/* 55 */ YY_NO_ANCHOR,
		/* 56 */ YY_NO_ANCHOR,
		/* 57 */ YY_NO_ANCHOR,
		/* 58 */ YY_NO_ANCHOR,
		/* 59 */ YY_NO_ANCHOR,
		/* 60 */ YY_NO_ANCHOR,
		/* 61 */ YY_NO_ANCHOR,
		/* 62 */ YY_NO_ANCHOR,
		/* 63 */ YY_NO_ANCHOR,
		/* 64 */ YY_NO_ANCHOR,
		/* 65 */ YY_NO_ANCHOR,
		/* 66 */ YY_NO_ANCHOR,
		/* 67 */ YY_NO_ANCHOR,
		/* 68 */ YY_NO_ANCHOR,
		/* 69 */ YY_NO_ANCHOR,
		/* 70 */ YY_NO_ANCHOR,
		/* 71 */ YY_NO_ANCHOR,
		/* 72 */ YY_NO_ANCHOR,
		/* 73 */ YY_NO_ANCHOR,
		/* 74 */ YY_NO_ANCHOR,
		/* 75 */ YY_NO_ANCHOR,
		/* 76 */ YY_NO_ANCHOR,
		/* 77 */ YY_NO_ANCHOR,
		/* 78 */ YY_NO_ANCHOR,
		/* 79 */ YY_NO_ANCHOR,
		/* 80 */ YY_NO_ANCHOR,
		/* 81 */ YY_NO_ANCHOR,
		/* 82 */ YY_NO_ANCHOR,
		/* 83 */ YY_NO_ANCHOR,
		/* 84 */ YY_NO_ANCHOR,
		/* 85 */ YY_NO_ANCHOR,
		/* 86 */ YY_NO_ANCHOR,
		/* 87 */ YY_NO_ANCHOR,
		/* 88 */ YY_NO_ANCHOR,
		/* 89 */ YY_NO_ANCHOR,
		/* 90 */ YY_NO_ANCHOR,
		/* 91 */ YY_NO_ANCHOR,
		/* 92 */ YY_NO_ANCHOR,
		/* 93 */ YY_NO_ANCHOR,
		/* 94 */ YY_NO_ANCHOR,
		/* 95 */ YY_NO_ANCHOR,
		/* 96 */ YY_NO_ANCHOR,
		/* 97 */ YY_NO_ANCHOR,
		/* 98 */ YY_NO_ANCHOR,
		/* 99 */ YY_NO_ANCHOR,
		/* 100 */ YY_NO_ANCHOR,
		/* 101 */ YY_NO_ANCHOR,
		/* 102 */ YY_NO_ANCHOR,
		/* 103 */ YY_NO_ANCHOR,
		/* 104 */ YY_NO_ANCHOR,
		/* 105 */ YY_NO_ANCHOR,
		/* 106 */ YY_NO_ANCHOR,
		/* 107 */ YY_NO_ANCHOR,
		/* 108 */ YY_NO_ANCHOR,
		/* 109 */ YY_NO_ANCHOR,
		/* 110 */ YY_NO_ANCHOR,
		/* 111 */ YY_NO_ANCHOR,
		/* 112 */ YY_NO_ANCHOR,
		/* 113 */ YY_NO_ANCHOR,
		/* 114 */ YY_NO_ANCHOR,
		/* 115 */ YY_NO_ANCHOR,
		/* 116 */ YY_NO_ANCHOR,
		/* 117 */ YY_NO_ANCHOR,
		/* 118 */ YY_NO_ANCHOR,
		/* 119 */ YY_NO_ANCHOR,
		/* 120 */ YY_NO_ANCHOR,
		/* 121 */ YY_NO_ANCHOR,
		/* 122 */ YY_NO_ANCHOR,
		/* 123 */ YY_NO_ANCHOR,
		/* 124 */ YY_NO_ANCHOR,
		/* 125 */ YY_NO_ANCHOR,
		/* 126 */ YY_NO_ANCHOR,
		/* 127 */ YY_NO_ANCHOR,
		/* 128 */ YY_NO_ANCHOR,
		/* 129 */ YY_NO_ANCHOR,
		/* 130 */ YY_NO_ANCHOR,
		/* 131 */ YY_NO_ANCHOR,
		/* 132 */ YY_NO_ANCHOR,
		/* 133 */ YY_NO_ANCHOR,
		/* 134 */ YY_NO_ANCHOR,
		/* 135 */ YY_NO_ANCHOR,
		/* 136 */ YY_NO_ANCHOR,
		/* 137 */ YY_NOT_ACCEPT,
		/* 138 */ YY_NO_ANCHOR,
		/* 139 */ YY_NO_ANCHOR,
		/* 140 */ YY_NO_ANCHOR,
		/* 141 */ YY_END,
		/* 142 */ YY_NOT_ACCEPT,
		/* 143 */ YY_NO_ANCHOR,
		/* 144 */ YY_NO_ANCHOR,
		/* 145 */ YY_NOT_ACCEPT,
		/* 146 */ YY_NO_ANCHOR,
		/* 147 */ YY_NO_ANCHOR,
		/* 148 */ YY_NOT_ACCEPT,
		/* 149 */ YY_NO_ANCHOR,
		/* 150 */ YY_NOT_ACCEPT,
		/* 151 */ YY_NO_ANCHOR,
		/* 152 */ YY_NO_ANCHOR,
		/* 153 */ YY_NO_ANCHOR,
		/* 154 */ YY_NO_ANCHOR,
		/* 155 */ YY_NO_ANCHOR,
		/* 156 */ YY_NO_ANCHOR,
		/* 157 */ YY_NO_ANCHOR,
		/* 158 */ YY_NO_ANCHOR,
		/* 159 */ YY_NO_ANCHOR,
		/* 160 */ YY_NO_ANCHOR,
		/* 161 */ YY_NO_ANCHOR,
		/* 162 */ YY_NO_ANCHOR,
		/* 163 */ YY_NO_ANCHOR,
		/* 164 */ YY_NO_ANCHOR,
		/* 165 */ YY_NO_ANCHOR,
		/* 166 */ YY_NO_ANCHOR,
		/* 167 */ YY_NO_ANCHOR,
		/* 168 */ YY_NO_ANCHOR,
		/* 169 */ YY_NO_ANCHOR,
		/* 170 */ YY_NO_ANCHOR,
		/* 171 */ YY_NO_ANCHOR,
		/* 172 */ YY_NO_ANCHOR,
		/* 173 */ YY_NO_ANCHOR,
		/* 174 */ YY_NO_ANCHOR,
		/* 175 */ YY_NO_ANCHOR,
		/* 176 */ YY_NO_ANCHOR,
		/* 177 */ YY_NO_ANCHOR,
		/* 178 */ YY_NO_ANCHOR,
		/* 179 */ YY_NO_ANCHOR,
		/* 180 */ YY_NO_ANCHOR,
		/* 181 */ YY_NO_ANCHOR,
		/* 182 */ YY_NO_ANCHOR,
		/* 183 */ YY_NO_ANCHOR,
		/* 184 */ YY_NO_ANCHOR,
		/* 185 */ YY_NO_ANCHOR,
		/* 186 */ YY_NO_ANCHOR,
		/* 187 */ YY_NO_ANCHOR,
		/* 188 */ YY_NO_ANCHOR,
		/* 189 */ YY_NO_ANCHOR,
		/* 190 */ YY_NO_ANCHOR,
		/* 191 */ YY_NO_ANCHOR,
		/* 192 */ YY_NO_ANCHOR,
		/* 193 */ YY_NO_ANCHOR,
		/* 194 */ YY_NO_ANCHOR,
		/* 195 */ YY_NO_ANCHOR,
		/* 196 */ YY_NO_ANCHOR,
		/* 197 */ YY_NO_ANCHOR,
		/* 198 */ YY_NO_ANCHOR,
		/* 199 */ YY_NO_ANCHOR,
		/* 200 */ YY_NO_ANCHOR,
		/* 201 */ YY_NO_ANCHOR,
		/* 202 */ YY_NO_ANCHOR,
		/* 203 */ YY_NO_ANCHOR,
		/* 204 */ YY_NO_ANCHOR,
		/* 205 */ YY_NO_ANCHOR,
		/* 206 */ YY_NO_ANCHOR,
		/* 207 */ YY_NO_ANCHOR,
		/* 208 */ YY_NO_ANCHOR,
		/* 209 */ YY_NO_ANCHOR,
		/* 210 */ YY_NO_ANCHOR,
		/* 211 */ YY_NO_ANCHOR,
		/* 212 */ YY_NO_ANCHOR,
		/* 213 */ YY_NO_ANCHOR,
		/* 214 */ YY_NO_ANCHOR,
		/* 215 */ YY_NO_ANCHOR,
		/* 216 */ YY_NO_ANCHOR,
		/* 217 */ YY_NO_ANCHOR,
		/* 218 */ YY_NO_ANCHOR,
		/* 219 */ YY_NO_ANCHOR,
		/* 220 */ YY_NO_ANCHOR,
		/* 221 */ YY_NO_ANCHOR,
		/* 222 */ YY_NO_ANCHOR,
		/* 223 */ YY_NO_ANCHOR,
		/* 224 */ YY_NO_ANCHOR,
		/* 225 */ YY_NO_ANCHOR,
		/* 226 */ YY_NO_ANCHOR,
		/* 227 */ YY_NO_ANCHOR,
		/* 228 */ YY_NO_ANCHOR,
		/* 229 */ YY_NO_ANCHOR,
		/* 230 */ YY_NO_ANCHOR,
		/* 231 */ YY_NO_ANCHOR,
		/* 232 */ YY_NO_ANCHOR,
		/* 233 */ YY_NO_ANCHOR,
		/* 234 */ YY_NO_ANCHOR,
		/* 235 */ YY_NO_ANCHOR,
		/* 236 */ YY_NO_ANCHOR,
		/* 237 */ YY_NO_ANCHOR,
		/* 238 */ YY_NO_ANCHOR,
		/* 239 */ YY_NO_ANCHOR,
		/* 240 */ YY_NO_ANCHOR,
		/* 241 */ YY_NO_ANCHOR,
		/* 242 */ YY_NO_ANCHOR,
		/* 243 */ YY_NO_ANCHOR,
		/* 244 */ YY_NO_ANCHOR,
		/* 245 */ YY_NO_ANCHOR,
		/* 246 */ YY_NO_ANCHOR,
		/* 247 */ YY_NO_ANCHOR,
		/* 248 */ YY_NO_ANCHOR,
		/* 249 */ YY_NO_ANCHOR,
		/* 250 */ YY_NO_ANCHOR,
		/* 251 */ YY_NO_ANCHOR,
		/* 252 */ YY_NO_ANCHOR,
		/* 253 */ YY_NO_ANCHOR,
		/* 254 */ YY_NO_ANCHOR,
		/* 255 */ YY_NO_ANCHOR,
		/* 256 */ YY_NO_ANCHOR,
		/* 257 */ YY_NO_ANCHOR,
		/* 258 */ YY_NO_ANCHOR,
		/* 259 */ YY_NO_ANCHOR,
		/* 260 */ YY_NO_ANCHOR,
		/* 261 */ YY_NO_ANCHOR,
		/* 262 */ YY_NO_ANCHOR,
		/* 263 */ YY_NO_ANCHOR,
		/* 264 */ YY_NO_ANCHOR,
		/* 265 */ YY_NO_ANCHOR,
		/* 266 */ YY_NO_ANCHOR,
		/* 267 */ YY_NO_ANCHOR,
		/* 268 */ YY_NO_ANCHOR,
		/* 269 */ YY_NO_ANCHOR,
		/* 270 */ YY_NO_ANCHOR,
		/* 271 */ YY_NO_ANCHOR,
		/* 272 */ YY_NO_ANCHOR,
		/* 273 */ YY_NO_ANCHOR,
		/* 274 */ YY_NO_ANCHOR,
		/* 275 */ YY_NO_ANCHOR,
		/* 276 */ YY_NO_ANCHOR,
		/* 277 */ YY_NO_ANCHOR,
		/* 278 */ YY_NO_ANCHOR,
		/* 279 */ YY_NO_ANCHOR,
		/* 280 */ YY_NO_ANCHOR,
		/* 281 */ YY_NO_ANCHOR,
		/* 282 */ YY_NO_ANCHOR,
		/* 283 */ YY_NO_ANCHOR,
		/* 284 */ YY_NO_ANCHOR,
		/* 285 */ YY_NO_ANCHOR,
		/* 286 */ YY_NO_ANCHOR,
		/* 287 */ YY_NO_ANCHOR,
		/* 288 */ YY_NO_ANCHOR,
		/* 289 */ YY_NO_ANCHOR,
		/* 290 */ YY_NO_ANCHOR,
		/* 291 */ YY_NO_ANCHOR,
		/* 292 */ YY_NO_ANCHOR,
		/* 293 */ YY_NO_ANCHOR,
		/* 294 */ YY_NO_ANCHOR,
		/* 295 */ YY_NO_ANCHOR,
		/* 296 */ YY_NO_ANCHOR,
		/* 297 */ YY_NO_ANCHOR,
		/* 298 */ YY_NO_ANCHOR,
		/* 299 */ YY_NO_ANCHOR,
		/* 300 */ YY_NO_ANCHOR,
		/* 301 */ YY_NO_ANCHOR,
		/* 302 */ YY_NO_ANCHOR,
		/* 303 */ YY_NO_ANCHOR,
		/* 304 */ YY_NO_ANCHOR,
		/* 305 */ YY_NO_ANCHOR,
		/* 306 */ YY_NO_ANCHOR,
		/* 307 */ YY_NO_ANCHOR,
		/* 308 */ YY_NO_ANCHOR,
		/* 309 */ YY_NO_ANCHOR,
		/* 310 */ YY_NO_ANCHOR,
		/* 311 */ YY_NO_ANCHOR,
		/* 312 */ YY_NO_ANCHOR,
		/* 313 */ YY_NO_ANCHOR,
		/* 314 */ YY_NO_ANCHOR,
		/* 315 */ YY_NO_ANCHOR,
		/* 316 */ YY_NO_ANCHOR,
		/* 317 */ YY_NO_ANCHOR,
		/* 318 */ YY_NO_ANCHOR,
		/* 319 */ YY_NO_ANCHOR,
		/* 320 */ YY_NO_ANCHOR,
		/* 321 */ YY_NO_ANCHOR,
		/* 322 */ YY_NO_ANCHOR,
		/* 323 */ YY_NO_ANCHOR,
		/* 324 */ YY_NO_ANCHOR,
		/* 325 */ YY_NO_ANCHOR,
		/* 326 */ YY_NO_ANCHOR,
		/* 327 */ YY_NO_ANCHOR,
		/* 328 */ YY_NO_ANCHOR,
		/* 329 */ YY_NO_ANCHOR,
		/* 330 */ YY_NO_ANCHOR,
		/* 331 */ YY_NO_ANCHOR,
		/* 332 */ YY_NO_ANCHOR,
		/* 333 */ YY_NO_ANCHOR,
		/* 334 */ YY_NO_ANCHOR,
		/* 335 */ YY_NO_ANCHOR,
		/* 336 */ YY_NO_ANCHOR,
		/* 337 */ YY_NO_ANCHOR,
		/* 338 */ YY_NO_ANCHOR,
		/* 339 */ YY_NO_ANCHOR,
		/* 340 */ YY_NO_ANCHOR,
		/* 341 */ YY_NO_ANCHOR,
		/* 342 */ YY_NO_ANCHOR,
		/* 343 */ YY_NO_ANCHOR,
		/* 344 */ YY_NO_ANCHOR,
		/* 345 */ YY_NO_ANCHOR,
		/* 346 */ YY_NO_ANCHOR,
		/* 347 */ YY_NO_ANCHOR,
		/* 348 */ YY_NO_ANCHOR,
		/* 349 */ YY_NO_ANCHOR,
		/* 350 */ YY_NO_ANCHOR,
		/* 351 */ YY_NO_ANCHOR,
		/* 352 */ YY_NO_ANCHOR,
		/* 353 */ YY_NO_ANCHOR,
		/* 354 */ YY_NO_ANCHOR,
		/* 355 */ YY_NO_ANCHOR,
		/* 356 */ YY_NO_ANCHOR,
		/* 357 */ YY_NO_ANCHOR,
		/* 358 */ YY_NO_ANCHOR,
		/* 359 */ YY_NO_ANCHOR,
		/* 360 */ YY_NO_ANCHOR,
		/* 361 */ YY_NO_ANCHOR,
		/* 362 */ YY_NO_ANCHOR,
		/* 363 */ YY_NO_ANCHOR,
		/* 364 */ YY_NO_ANCHOR,
		/* 365 */ YY_NO_ANCHOR,
		/* 366 */ YY_NO_ANCHOR,
		/* 367 */ YY_NO_ANCHOR,
		/* 368 */ YY_NO_ANCHOR,
		/* 369 */ YY_NO_ANCHOR,
		/* 370 */ YY_NO_ANCHOR,
		/* 371 */ YY_NO_ANCHOR,
		/* 372 */ YY_NO_ANCHOR,
		/* 373 */ YY_NO_ANCHOR,
		/* 374 */ YY_NO_ANCHOR,
		/* 375 */ YY_NO_ANCHOR,
		/* 376 */ YY_NO_ANCHOR,
		/* 377 */ YY_NO_ANCHOR,
		/* 378 */ YY_NO_ANCHOR,
		/* 379 */ YY_NO_ANCHOR,
		/* 380 */ YY_NO_ANCHOR,
		/* 381 */ YY_NO_ANCHOR,
		/* 382 */ YY_NO_ANCHOR,
		/* 383 */ YY_NO_ANCHOR,
		/* 384 */ YY_NO_ANCHOR,
		/* 385 */ YY_NO_ANCHOR,
		/* 386 */ YY_NO_ANCHOR,
		/* 387 */ YY_NO_ANCHOR,
		/* 388 */ YY_NO_ANCHOR,
		/* 389 */ YY_NO_ANCHOR,
		/* 390 */ YY_NO_ANCHOR,
		/* 391 */ YY_NO_ANCHOR
	};
	private int yy_cmap[] = unpackFromString(1,65538,
"46:9,52,48,46,52,50,46:18,52,46:6,45,37,38,35,33,39,34,40,36,27,26,25,31:7," +
"32,46,42,41,43,46:2,1,8,7,4,9,18,21,22,15,23,24,2,19,3,12,16,28,14,6,10,20," +
"13,11,17,5,29,46,47,46:2,30,46,1,8,7,4,9,18,21,22,15,23,24,2,19,3,12,16,28," +
"14,6,10,20,13,11,17,5,29,46,44,46:1903,49:2,46:63506,0,51")[0];

	private int yy_rmap[] = unpackFromString(1,392,
"0,1,2,3,4,1:7,5,1,6,7,1,8,9,10,11:2,12,11,13,14,15,11,16,11,17,5,1:4,18,1,1" +
"1:3,19,11:3,20,11:23,21,11:6,22,11:54,23,11:5,5,24,25,26:3,27,26,28,29,1,18" +
",30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54" +
",55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79" +
",80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,10" +
"3,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,1" +
"22,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140," +
"141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159" +
",160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,17" +
"8,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,1" +
"97,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215," +
"216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234" +
",235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,25" +
"3,254,255,256,257,258,11,259,260,261,262,263,264,265,266,267,268,269,270,27" +
"1")[0];

	private int yy_nxt[][] = unpackFromString(272,53,
"1,2,138,143,146,308,337,355,149,151,366,372,152,374,376,153,154,378,380,381" +
",382,155,383,384,378,3:3,378:3,3,4,5,6,7,8,9,10,11,12,13,14,15,139,144,147:" +
"2,16,147,16,1,16,-1:54,378,156,157,378:2,17,385,158,378,386,378:2,159,378:1" +
"8,-1:46,3:3,-1:3,3,-1:8,137,-1:13,30:24,-1:3,30:2,-1:48,31:3,-1:3,31,-1:62," +
"32,-1,33,-1:50,34,-1:12,378:6,41,378:7,194,378:16,-1:22,378:2,44,378:14,198" +
",378:13,-1:22,378:13,356,378:17,-1:22,378:31,-1:22,378:9,56,378:21,-1:22,37" +
"8:3,218,378:27,-1:22,378:2,323,378:2,346,378:3,223,378:21,-1:22,378:2,324,3" +
"78:25,360,378:2,-1:22,378:9,228,378:21,-1:22,30:31,-1:22,148:46,150,-1,148:" +
"2,-1,148,-1,378:14,230,378:16,-1:22,378:25,233,378:5,-1:22,378:14,262,378:1" +
"6,-1:22,378:14,365,378:16,-1:22,378:24,136,378:6,-1:22,378:8,18,19,378,160," +
"378:2,387,388,378:15,-1:65,35,-1:9,142:44,36,142,145,37,141:2,37,142,-1,378" +
":8,20,378:2,161,378:7,389,378:11,-1:22,142:44,140,142,145,37,141:2,37,142,-" +
"1,162,378:7,163,378:5,164,378:16,-1:22,378:4,21,378:3,175,378:22,-1:22,148:" +
"44,142,148,150,-1,148:2,-1,148,-1,378,338,176,378:2,177,378:10,178,378:10,2" +
"2,378:3,-1:22,378:2,23,378:10,24,378:17,-1:22,378:2,25,378:2,26,378:25,-1:2" +
"2,378:11,188,378:2,27,378:16,-1:22,378:8,28,29,378:3,357,378:17,-1:22,378,3" +
"8,378:29,-1:22,378:3,39,40,378:26,-1:22,378:5,42,378:25,-1:22,378:20,43,378" +
":10,-1:22,378:10,342,378:9,45,378:10,-1:22,378:9,46,378:21,-1:22,378:4,47,3" +
"78:4,202,378:21,-1:22,378,358,378:3,203,367,378:13,318,378:10,-1:22,378:5,3" +
"13,378:25,-1:22,204,378:30,-1:22,378,368,378:7,48,378:21,-1:22,378:13,49,37" +
"8:5,205,378:11,-1:22,378:18,206,378:12,-1:22,378:2,50,378:17,207,378:10,-1:" +
"22,378:7,208,378:10,51,378:12,-1:22,378:13,209,378:5,210,378:11,-1:22,378:1" +
"4,359,378:16,-1:22,378:2,319,378:2,52,378:3,53,378:9,212,378:11,-1:22,378:1" +
"1,320,378:19,-1:22,378:9,390,378:21,-1:22,378:3,54,378:27,-1:22,378:6,321,3" +
"78:24,-1:22,378:14,391,55,378:15,-1:22,378:2,57,378:28,-1:22,378:15,58,378:" +
"15,-1:22,378:19,215,378:11,-1:22,378:8,216,378:22,-1:22,378,219,378:29,-1:2" +
"2,378:3,220,378:27,-1:22,378:15,221,378:15,-1:22,378:13,325,378:17,-1:22,37" +
"8:20,222,378:10,-1:22,378:10,369,378:20,-1:22,378:16,59,378:14,-1:22,378:2," +
"227,60,378:27,-1:22,378:2,61,378:28,-1:22,378:3,345,378:11,373,378:15,-1:22" +
",378:12,348,378:18,-1:22,378:2,62,378:28,-1:22,378:5,63,378:25,-1:22,378:2," +
"64,378:28,-1:22,378:24,65,378:6,-1:22,378:9,66,378:21,-1:22,378:8,67,378:22" +
",-1:22,378:3,68,378:27,-1:22,378,69,378:29,-1:22,326,378:7,234,378:22,-1:22" +
",378:6,70,378:24,-1:22,378:13,71,378:17,-1:22,378:17,236,378:13,-1:22,378:8" +
",72,378:22,-1:22,378:2,73,378:28,-1:22,378:5,349,378:25,-1:22,378:9,74,378:" +
"21,-1:22,238,378:30,-1:22,378:8,75,378:22,-1:22,378:2,239,378:28,-1:22,378:" +
"13,76,378:17,-1:22,378:8,77,378:22,-1:22,378:2,242,378:5,78,378:22,-1:22,37" +
"8:2,79,378:28,-1:22,378:2,80,378:10,243,378:17,-1:22,378:8,244,378:22,-1:22" +
",378:19,245,378:11,-1:22,378:14,246,378:16,-1:22,378,247,378:29,-1:22,378:2" +
"1,250,378:9,-1:22,378:11,81,378:19,-1:22,378:11,255,378:19,-1:22,378:18,82," +
"378:12,-1:22,378,83,378:29,-1:22,378:9,256,378:21,-1:22,378:3,332,378:27,-1" +
":22,378:2,84,378:28,-1:22,378:14,85,378:16,-1:22,378:18,86,378:12,-1:22,378" +
":13,87,378:17,-1:22,378:26,88,378:4,-1:22,263,378:2,264,378:11,352,378:15,-" +
"1:22,378:6,267,378:24,-1:22,378:17,89,378:13,-1:22,378:8,90,378:22,-1:22,37" +
"8:13,268,378:17,-1:22,378:9,91,378:21,-1:22,378:5,92,378:25,-1:22,378:15,27" +
"0,378:15,-1:22,378:6,93,378:24,-1:22,378:8,94,378:22,-1:22,378:13,95,378:17" +
",-1:22,378:8,272,378:22,-1:22,371,378:30,-1:22,273,378:13,335,378:16,-1:22," +
"378:18,96,378:12,-1:22,378:3,97,378:27,-1:22,378:9,98,378:21,-1:22,378:13,9" +
"9,378:17,-1:22,378,275,378:16,276,378:12,-1:22,378:13,100,378:17,-1:22,378:" +
"8,101,378:22,-1:22,378:13,102,378:17,-1:22,378:21,103,378:9,-1:22,378:2,104" +
",378:28,-1:22,378:9,278,378:21,-1:22,378:13,105,378:17,-1:22,378:15,106,378" +
":15,-1:22,378:2,280,378:28,-1:22,378:17,107,378:13,-1:22,378:3,282,378:27,-" +
"1:22,378:14,283,378:16,-1:22,378:8,108,378:22,-1:22,378:8,109,378:22,-1:22," +
"378:9,110,378:21,-1:22,378:8,111,378:22,-1:22,378:13,287,378:17,-1:22,378:8" +
",112,378:22,-1:22,378:5,113,378:25,-1:22,378:5,114,378:25,-1:22,378:6,291,3" +
"78:24,-1:22,378:9,115,378:21,-1:22,378,116,378:29,-1:22,378:8,293,378:22,-1" +
":22,378:11,117,378:19,-1:22,378:8,118,378:22,-1:22,378:9,294,378:21,-1:22,3" +
"78:20,119,378:10,-1:22,378:2,295,378:28,-1:22,378:3,120,378:27,-1:22,378:17" +
",296,378:13,-1:22,378:5,121,378:25,-1:22,378:14,299,378:16,-1:22,378:20,122" +
",378:10,-1:22,378:9,123,378:21,-1:22,378:3,300,378:27,-1:22,378:2,124,378:2" +
"8,-1:22,378:5,125,378:25,-1:22,378:8,126,378:22,-1:22,301,378:30,-1:22,378:" +
"13,302,378:17,-1:22,378:8,127,378:22,-1:22,378:20,336,378:10,-1:22,378:17,1" +
"28,378:13,-1:22,378:9,129,378:21,-1:22,378:9,130,378:21,-1:22,378:2,303,378" +
":28,-1:22,378:8,304,378:22,-1:22,378:9,305,378:21,-1:22,378:14,306,378:16,-" +
"1:22,378:20,131,378:10,-1:22,378:16,132,378:14,-1:22,378:8,133,378:22,-1:22" +
",378:6,134,378:24,-1:22,378:21,135,378:9,-1:22,378:8,165,378:22,-1:22,378:5" +
",211,378:25,-1:22,314,378:30,-1:22,378:14,327,378:16,-1:22,378:11,224,378:1" +
"9,-1:22,378:9,361,378:21,-1:22,378:6,237,378:24,-1:22,378:19,322,378:11,-1:" +
"22,378:8,217,378:22,-1:22,378,344,378:29,-1:22,378:13,375,378:17,-1:22,378:" +
"12,377,378:18,-1:22,378:5,240,378:25,-1:22,241,378:30,-1:22,378:2,249,378:2" +
"8,-1:22,378:8,251,378:22,-1:22,378:19,252,378:11,-1:22,378:14,248,378:16,-1" +
":22,378,331,378:29,-1:22,378:11,257,378:19,-1:22,378:9,265,378:21,-1:22,378" +
":3,266,378:27,-1:22,378:13,274,378:17,-1:22,378:8,281,378:22,-1:22,279,378:" +
"30,-1:22,378:2,354,378:28,-1:22,378:13,297,378:17,-1:22,378:6,292,378:24,-1" +
":22,378:9,307,378:21,-1:22,378:8,166,167,378,168,378:2,169,310,378:3,170,37" +
"8:7,171,378:3,-1:22,378:5,214,378:25,-1:22,213,378:30,-1:22,378:14,229,378:" +
"16,-1:22,378:11,225,378:19,-1:22,378:8,232,378:22,-1:22,378,226,378:29,-1:2" +
"2,378:5,254,378:25,-1:22,258,378:30,-1:22,378:8,330,378:22,-1:22,378:19,260" +
",378:11,-1:22,378:14,261,378:16,-1:22,378:9,363,378:21,-1:22,378:13,277,378" +
":17,-1:22,378:8,284,378:22,-1:22,334,378:30,-1:22,378:2,286,378:28,-1:22,37" +
"8:6,298,378:24,-1:22,309,378:7,172,378:2,173,378,174,378:7,339,378:9,-1:22," +
"378:14,231,378:16,-1:22,378:11,347,378:19,-1:22,378:8,328,378:22,-1:22,378," +
"370,378:29,-1:22,378:8,350,378:22,-1:22,378:14,333,378:16,-1:22,378:9,271,3" +
"78:21,-1:22,378:13,285,378:17,-1:22,378:8,289,378:22,-1:22,378:2,288,378:28" +
",-1:22,179,378:10,180,378,181,378:7,182,378:9,-1:22,378:11,329,378:19,-1:22" +
",378:8,235,378:22,-1:22,378:8,253,378:22,-1:22,378:14,353,378:16,-1:22,378:" +
"2,290,378:28,-1:22,378:21,316,378:9,-1:22,378:8,259,378:22,-1:22,183,378:30" +
",-1:22,378:8,351,378:22,-1:22,184,378:7,185,186,378,315,378:2,187,378:16,-1" +
":22,378:8,269,378:22,-1:22,378:8,364,378:22,-1:22,317,312,378:11,341,378:5," +
"343,378:11,-1:22,189,378:10,190,378:2,191,378:16,-1:22,378:2,311,378:12,192" +
",378:15,-1:22,193,378:30,-1:22,378:11,340,378:19,-1:22,378:11,195,378:19,-1" +
":22,196,378,197,378:28,-1:22,378:23,199,378:7,-1:22,200,378:30,-1:22,378,20" +
"1,378:29,-1:22,378:10,379,378:20,-1:22,378:5,362,378:25,-1:21");

	public com.shr.server.sql.GtSymbol next_token ()
		throws java.io.IOException {
		int yy_lookahead;
		int yy_anchor = YY_NO_ANCHOR;
		int yy_state = yy_state_dtrans[yy_lexical_state];
		int yy_next_state = YY_NO_STATE;
		int yy_last_accept_state = YY_NO_STATE;
		boolean yy_initial = true;
		int yy_this_accept;

		yy_mark_start();
		yy_this_accept = yy_acpt[yy_state];
		if (YY_NOT_ACCEPT != yy_this_accept) {
			yy_last_accept_state = yy_state;
			yy_mark_end();
		}
		while (true) {
			if (yy_initial && yy_at_bol) yy_lookahead = YY_BOL;
			else yy_lookahead = yy_advance();
			yy_next_state = YY_F;
			yy_next_state = yy_nxt[yy_rmap[yy_state]][yy_cmap[yy_lookahead]];
			if (YY_EOF == yy_lookahead && true == yy_initial) {
				return null;
			}
			if (YY_F != yy_next_state) {
				yy_state = yy_next_state;
				yy_initial = false;
				yy_this_accept = yy_acpt[yy_state];
				if (YY_NOT_ACCEPT != yy_this_accept) {
					yy_last_accept_state = yy_state;
					yy_mark_end();
				}
			}
			else {
				if (YY_NO_STATE == yy_last_accept_state) {
					throw (new Error("Lexical Error: Unmatched Input."));
				}
				else {
					yy_anchor = yy_acpt[yy_last_accept_state];
					if (0 != (YY_END & yy_anchor)) {
						yy_move_end();
					}
					yy_to_mark();
					switch (yy_last_accept_state) {
					case 1:
						
					case -2:
						break;
					case 2:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -3:
						break;
					case 3:
						{return new GtSymbol(sym.INTNUMBER, yytext(), yyline);}
					case -4:
						break;
					case 4:
						{}
					case -5:
						break;
					case 5:
						{return new GtSymbol(sym.JIA, yytext(), yyline);}
					case -6:
						break;
					case 6:
						{return new GtSymbol(sym.JIAN, yytext(), yyline);}
					case -7:
						break;
					case 7:
						{return new GtSymbol(sym.CHENG, yytext(), yyline);}
					case -8:
						break;
					case 8:
						{return new GtSymbol(sym.CHU, yytext(), yyline);}
					case -9:
						break;
					case 9:
						{return new GtSymbol(sym.LKH, yytext(), yyline);}
					case -10:
						break;
					case 10:
						{return new GtSymbol(sym.RKH, yytext(), yyline);}
					case -11:
						break;
					case 11:
						{return new GtSymbol(sym.DOUHAO, yytext(), yyline);}
					case -12:
						break;
					case 12:
						{return new GtSymbol(sym.DIAN, yytext(), yyline);}
					case -13:
						break;
					case 13:
						{return new GtSymbol(sym.DENGYU, yytext(), yyline);}
					case -14:
						break;
					case 14:
						{return new GtSymbol(sym.XIAOYU, yytext(), yyline);}
					case -15:
						break;
					case 15:
						{return new GtSymbol(sym.DAYU, yytext(), yyline);}
					case -16:
						break;
					case 16:
						{}
					case -17:
						break;
					case 17:
						{return new GtSymbol(sym.AS, yytext(), yyline);}
					case -18:
						break;
					case 18:
						{return new GtSymbol(sym.LE, yytext(), yyline);}
					case -19:
						break;
					case 19:
						{return new GtSymbol(sym.LT, yytext(), yyline);}
					case -20:
						break;
					case 20:
						{return new GtSymbol(sym.NE, yytext(), yyline);}
					case -21:
						break;
					case 21:
						{return new GtSymbol(sym.BY, yytext(), yyline);}
					case -22:
						break;
					case 22:
						{return new GtSymbol(sym.EQ, yytext(), yyline);}
					case -23:
						break;
					case 23:
						{return new GtSymbol(sym.ON, yytext(), yyline);}
					case -24:
						break;
					case 24:
						{return new GtSymbol(sym.OR, yytext(), yyline);}
					case -25:
						break;
					case 25:
						{return new GtSymbol(sym.IN, yytext(), yyline);}
					case -26:
						break;
					case 26:
						{return new GtSymbol(sym.IS, yytext(), yyline);}
					case -27:
						break;
					case 27:
						{return new GtSymbol(sym.PI, yytext(), yyline);}
					case -28:
						break;
					case 28:
						{return new GtSymbol(sym.GE, yytext(), yyline);}
					case -29:
						break;
					case 29:
						{return new GtSymbol(sym.GT, yytext(), yyline);}
					case -30:
						break;
					case 30:
						{return new GtSymbol(sym.SQLPARAMETER, yytext(), yyline);}
					case -31:
						break;
					case 31:
						{return new GtSymbol(sym.FLOATNUMBER, yytext(), yyline);}
					case -32:
						break;
					case 32:
						{return new GtSymbol(sym.XDENGYU, yytext(), yyline);}
					case -33:
						break;
					case 33:
						{return new GtSymbol(sym.BUDENGYU, yytext(), yyline);}
					case -34:
						break;
					case 34:
						{return new GtSymbol(sym.DDENGYU, yytext(), yyline);}
					case -35:
						break;
					case 35:
						{return new GtSymbol(sym.CONNECT, yytext(), yyline);}
					case -36:
						break;
					case 36:
						{return new GtSymbol(sym.STRING, yytext(), yyline);}
					case -37:
						break;
					case 37:
						{return new GtSymbol(sym.error, "Bad string:" + yytext(), yyline);}
					case -38:
						break;
					case 38:
						{return new GtSymbol(sym.ALL, yytext(), yyline);}
					case -39:
						break;
					case 39:
						{return new GtSymbol(sym.AND, yytext(), yyline);}
					case -40:
						break;
					case 40:
						{return new GtSymbol(sym.ANY, yytext(), yyline);}
					case -41:
						break;
					case 41:
						{return new GtSymbol(sym.ASC, yytext(), yyline);}
					case -42:
						break;
					case 42:
						{return new GtSymbol(sym.ABS, yytext(), yyline);}
					case -43:
						break;
					case 43:
						{return new GtSymbol(sym.AVG, yytext(), yyline);}
					case -44:
						break;
					case 44:
						{return new GtSymbol(sym.LEN, yytext(), yyline);}
					case -45:
						break;
					case 45:
						{return new GtSymbol(sym.LOG, yytext(), yyline);}
					case -46:
						break;
					case 46:
						{return new GtSymbol(sym.NOT, yytext(), yyline);}
					case -47:
						break;
					case 47:
						{return new GtSymbol(sym.DAY, yytext(), yyline);}
					case -48:
						break;
					case 48:
						{return new GtSymbol(sym.SET, yytext(), yyline);}
					case -49:
						break;
					case 49:
						{return new GtSymbol(sym.STR, yytext(), yyline);}
					case -50:
						break;
					case 50:
						{return new GtSymbol(sym.SIN, yytext(), yyline);}
					case -51:
						break;
					case 51:
						{return new GtSymbol(sym.SUM, yytext(), yyline);}
					case -52:
						break;
					case 52:
						{return new GtSymbol(sym.COS, yytext(), yyline);}
					case -53:
						break;
					case 53:
						{return new GtSymbol(sym.COT, yytext(), yyline);}
					case -54:
						break;
					case 54:
						{return new GtSymbol(sym.END, yytext(), yyline);}
					case -55:
						break;
					case 55:
						{return new GtSymbol(sym.EXP, yytext(), yyline);}
					case -56:
						break;
					case 56:
						{return new GtSymbol(sym.EQT, yytext(), yyline);}
					case -57:
						break;
					case 57:
						{return new GtSymbol(sym.TAN, yytext(), yyline);}
					case -58:
						break;
					case 58:
						{return new GtSymbol(sym.TOP, yytext(), yyline);}
					case -59:
						break;
					case 59:
						{return new GtSymbol(sym.MAX, yytext(), yyline);}
					case -60:
						break;
					case 60:
						{return new GtSymbol(sym.MOD, yytext(), yyline);}
					case -61:
						break;
					case 61:
						{return new GtSymbol(sym.MIN, yytext(), yyline);}
					case -62:
						break;
					case 62:
						{return new GtSymbol(sym.ASIN, yytext(), yyline);}
					case -63:
						break;
					case 63:
						{return new GtSymbol(sym.ACOS, yytext(), yyline);}
					case -64:
						break;
					case 64:
						{return new GtSymbol(sym.ATAN, yytext(), yyline);}
					case -65:
						break;
					case 65:
						{return new GtSymbol(sym.ATN2, yytext(), yyline);}
					case -66:
						break;
					case 66:
						{return new GtSymbol(sym.LEFT, yytext(), yyline);}
					case -67:
						break;
					case 67:
						{return new GtSymbol(sym.LIKE, yytext(), yyline);}
					case -68:
						break;
					case 68:
						{return new GtSymbol(sym.LPAD, yytext(), yyline);}
					case -69:
						break;
					case 69:
						{return new GtSymbol(sym.NULL, yytext(), yyline);}
					case -70:
						break;
					case 70:
						{return new GtSymbol(sym.DESC, yytext(), yyline);}
					case -71:
						break;
					case 71:
						{return new GtSymbol(sym.YEAR, yytext(), yyline);}
					case -72:
						break;
					case 72:
						{return new GtSymbol(sym.SOME, yytext(), yyline);}
					case -73:
						break;
					case 73:
						{return new GtSymbol(sym.SIGN, yytext(), yyline);}
					case -74:
						break;
					case 74:
						{return new GtSymbol(sym.SQRT, yytext(), yyline);}
					case -75:
						break;
					case 75:
						{return new GtSymbol(sym.CASE, yytext(), yyline);}
					case -76:
						break;
					case 76:
						{return new GtSymbol(sym.CHAR, yytext(), yyline);}
					case -77:
						break;
					case 77:
						{return new GtSymbol(sym.ELSE, yytext(), yyline);}
					case -78:
						break;
					case 78:
						{return new GtSymbol(sym.TRUE, yytext(), yyline);}
					case -79:
						break;
					case 79:
						{return new GtSymbol(sym.THEN, yytext(), yyline);}
					case -80:
						break;
					case 80:
						{return new GtSymbol(sym.WHEN, yytext(), yyline);}
					case -81:
						break;
					case 81:
						{return new GtSymbol(sym.INTO, yytext(), yyline);}
					case -82:
						break;
					case 82:
						{return new GtSymbol(sym.FROM, yytext(), yyline);}
					case -83:
						break;
					case 83:
						{return new GtSymbol(sym.FULL, yytext(), yyline);}
					case -84:
						break;
					case 84:
						{return new GtSymbol(sym.JOIN, yytext(), yyline);}
					case -85:
						break;
					case 85:
						{return new GtSymbol(sym.ASCII, yytext(), yyline);}
					case -86:
						break;
					case 86:
						{return new GtSymbol(sym.LTRIM, yytext(), yyline);}
					case -87:
						break;
					case 87:
						{return new GtSymbol(sym.LOWER, yytext(), yyline);}
					case -88:
						break;
					case 88:
						{return new GtSymbol(sym.LOG10, yytext(), yyline);}
					case -89:
						break;
					case 89:
						{return new GtSymbol(sym.STUFF, yytext(), yyline);}
					case -90:
						break;
					case 90:
						{return new GtSymbol(sym.SPACE, yytext(), yyline);}
					case -91:
						break;
					case 91:
						{return new GtSymbol(sym.COUNT, yytext(), yyline);}
					case -92:
						break;
					case 92:
						{return new GtSymbol(sym.CROSS, yytext(), yyline);}
					case -93:
						break;
					case 93:
						{return new GtSymbol(sym.TRUNC, yytext(), yyline);}
					case -94:
						break;
					case 94:
						{return new GtSymbol(sym.WHERE, yytext(), yyline);}
					case -95:
						break;
					case 95:
						{return new GtSymbol(sym.ORDER, yytext(), yyline);}
					case -96:
						break;
					case 96:
						{return new GtSymbol(sym.RTRIM, yytext(), yyline);}
					case -97:
						break;
					case 97:
						{return new GtSymbol(sym.ROUND, yytext(), yyline);}
					case -98:
						break;
					case 98:
						{return new GtSymbol(sym.RIGHT, yytext(), yyline);}
					case -99:
						break;
					case 99:
						{return new GtSymbol(sym.INNER, yytext(), yyline);}
					case -100:
						break;
					case 100:
						{return new GtSymbol(sym.POWER, yytext(), yyline);}
					case -101:
						break;
					case 101:
						{return new GtSymbol(sym.FALSE, yytext(), yyline);}
					case -102:
						break;
					case 102:
						{return new GtSymbol(sym.FLOOR, yytext(), yyline);}
					case -103:
						break;
					case 103:
						{return new GtSymbol(sym.MONTH, yytext(), yyline);}
					case -104:
						break;
					case 104:
						{return new GtSymbol(sym.UNION, yytext(), yyline);}
					case -105:
						break;
					case 105:
						{return new GtSymbol(sym.UPPER, yytext(), yyline);}
					case -106:
						break;
					case 106:
						{return new GtSymbol(sym.GROUP, yytext(), yyline);}
					case -107:
						break;
					case 107:
						{return new GtSymbol(sym.NULLIF, yytext(), yyline);}
					case -108:
						break;
					case 108:
						{return new GtSymbol(sym.DELETE, yytext(), yyline);}
					case -109:
						break;
					case 109:
						{return new GtSymbol(sym.DECODE, yytext(), yyline);}
					case -110:
						break;
					case 110:
						{return new GtSymbol(sym.SELECT, yytext(), yyline);}
					case -111:
						break;
					case 111:
						{return new GtSymbol(sym.SQUARE, yytext(), yyline);}
					case -112:
						break;
					case 112:
						{return new GtSymbol(sym.ESCAPE, yytext(), yyline);}
					case -113:
						break;
					case 113:
						{return new GtSymbol(sym.EXISTS, yytext(), yyline);}
					case -114:
						break;
					case 114:
						{return new GtSymbol(sym.VALUES, yytext(), yyline);}
					case -115:
						break;
					case 115:
						{return new GtSymbol(sym.INSERT, yytext(), yyline);}
					case -116:
						break;
					case 116:
						{return new GtSymbol(sym.ISNULL, yytext(), yyline);}
					case -117:
						break;
					case 117:
						{return new GtSymbol(sym.ISZERO, yytext(), yyline);}
					case -118:
						break;
					case 118:
						{return new GtSymbol(sym.UPDATE, yytext(), yyline);}
					case -119:
						break;
					case 119:
						{return new GtSymbol(sym.HAVING, yytext(), yyline);}
					case -120:
						break;
					case 120:
						{return new GtSymbol(sym.DATEADD, yytext(), yyline);}
					case -121:
						break;
					case 121:
						{return new GtSymbol(sym.DEGREES, yytext(), yyline);}
					case -122:
						break;
					case 122:
						{return new GtSymbol(sym.CEILING, yytext(), yyline);}
					case -123:
						break;
					case 123:
						{return new GtSymbol(sym.CONVERT, yytext(), yyline);}
					case -124:
						break;
					case 124:
						{return new GtSymbol(sym.BETWEEN, yytext(), yyline);}
					case -125:
						break;
					case 125:
						{return new GtSymbol(sym.RADIANS, yytext(), yyline);}
					case -126:
						break;
					case 126:
						{return new GtSymbol(sym.REPLACE, yytext(), yyline);}
					case -127:
						break;
					case 127:
						{return new GtSymbol(sym.GETDATE, yytext(), yyline);}
					case -128:
						break;
					case 128:
						{return new GtSymbol(sym.DATEDIFF, yytext(), yyline);}
					case -129:
						break;
					case 129:
						{return new GtSymbol(sym.DATEPART, yytext(), yyline);}
					case -130:
						break;
					case 130:
						{return new GtSymbol(sym.DISTINCT, yytext(), yyline);}
					case -131:
						break;
					case 131:
						{return new GtSymbol(sym.SUBSTRING, yytext(), yyline);}
					case -132:
						break;
					case 132:
						{return new GtSymbol(sym.CHARINDEX, yytext(), yyline);}
					case -133:
						break;
					case 133:
						{return new GtSymbol(sym.REPLICATE, yytext(), yyline);}
					case -134:
						break;
					case 134:
						{return new GtSymbol(sym.ISNUMERIC, yytext(), yyline);}
					case -135:
						break;
					case 135:
						{return new GtSymbol(sym.DATALENGTH, yytext(), yyline);}
					case -136:
						break;
					case 136:
						{return new GtSymbol(sym.SUBSTRING2, yytext(), yyline);}
					case -137:
						break;
					case 138:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -138:
						break;
					case 139:
						{}
					case -139:
						break;
					case 140:
						{return new GtSymbol(sym.STRING, yytext(), yyline);}
					case -140:
						break;
					case 141:
						{return new GtSymbol(sym.error, "Bad string:" + yytext(), yyline);}
					case -141:
						break;
					case 143:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -142:
						break;
					case 144:
						{}
					case -143:
						break;
					case 146:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -144:
						break;
					case 147:
						{}
					case -145:
						break;
					case 149:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -146:
						break;
					case 151:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -147:
						break;
					case 152:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -148:
						break;
					case 153:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -149:
						break;
					case 154:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -150:
						break;
					case 155:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -151:
						break;
					case 156:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -152:
						break;
					case 157:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -153:
						break;
					case 158:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -154:
						break;
					case 159:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -155:
						break;
					case 160:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -156:
						break;
					case 161:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -157:
						break;
					case 162:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -158:
						break;
					case 163:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -159:
						break;
					case 164:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -160:
						break;
					case 165:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -161:
						break;
					case 166:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -162:
						break;
					case 167:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -163:
						break;
					case 168:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -164:
						break;
					case 169:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -165:
						break;
					case 170:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -166:
						break;
					case 171:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -167:
						break;
					case 172:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -168:
						break;
					case 173:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -169:
						break;
					case 174:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -170:
						break;
					case 175:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -171:
						break;
					case 176:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -172:
						break;
					case 177:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -173:
						break;
					case 178:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -174:
						break;
					case 179:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -175:
						break;
					case 180:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -176:
						break;
					case 181:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -177:
						break;
					case 182:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -178:
						break;
					case 183:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -179:
						break;
					case 184:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -180:
						break;
					case 185:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -181:
						break;
					case 186:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -182:
						break;
					case 187:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -183:
						break;
					case 188:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -184:
						break;
					case 189:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -185:
						break;
					case 190:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -186:
						break;
					case 191:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -187:
						break;
					case 192:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -188:
						break;
					case 193:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -189:
						break;
					case 194:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -190:
						break;
					case 195:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -191:
						break;
					case 196:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -192:
						break;
					case 197:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -193:
						break;
					case 198:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -194:
						break;
					case 199:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -195:
						break;
					case 200:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -196:
						break;
					case 201:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -197:
						break;
					case 202:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -198:
						break;
					case 203:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -199:
						break;
					case 204:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -200:
						break;
					case 205:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -201:
						break;
					case 206:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -202:
						break;
					case 207:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -203:
						break;
					case 208:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -204:
						break;
					case 209:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -205:
						break;
					case 210:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -206:
						break;
					case 211:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -207:
						break;
					case 212:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -208:
						break;
					case 213:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -209:
						break;
					case 214:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -210:
						break;
					case 215:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -211:
						break;
					case 216:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -212:
						break;
					case 217:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -213:
						break;
					case 218:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -214:
						break;
					case 219:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -215:
						break;
					case 220:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -216:
						break;
					case 221:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -217:
						break;
					case 222:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -218:
						break;
					case 223:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -219:
						break;
					case 224:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -220:
						break;
					case 225:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -221:
						break;
					case 226:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -222:
						break;
					case 227:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -223:
						break;
					case 228:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -224:
						break;
					case 229:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -225:
						break;
					case 230:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -226:
						break;
					case 231:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -227:
						break;
					case 232:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -228:
						break;
					case 233:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -229:
						break;
					case 234:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -230:
						break;
					case 235:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -231:
						break;
					case 236:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -232:
						break;
					case 237:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -233:
						break;
					case 238:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -234:
						break;
					case 239:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -235:
						break;
					case 240:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -236:
						break;
					case 241:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -237:
						break;
					case 242:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -238:
						break;
					case 243:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -239:
						break;
					case 244:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -240:
						break;
					case 245:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -241:
						break;
					case 246:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -242:
						break;
					case 247:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -243:
						break;
					case 248:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -244:
						break;
					case 249:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -245:
						break;
					case 250:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -246:
						break;
					case 251:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -247:
						break;
					case 252:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -248:
						break;
					case 253:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -249:
						break;
					case 254:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -250:
						break;
					case 255:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -251:
						break;
					case 256:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -252:
						break;
					case 257:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -253:
						break;
					case 258:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -254:
						break;
					case 259:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -255:
						break;
					case 260:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -256:
						break;
					case 261:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -257:
						break;
					case 262:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -258:
						break;
					case 263:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -259:
						break;
					case 264:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -260:
						break;
					case 265:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -261:
						break;
					case 266:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -262:
						break;
					case 267:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -263:
						break;
					case 268:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -264:
						break;
					case 269:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -265:
						break;
					case 270:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -266:
						break;
					case 271:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -267:
						break;
					case 272:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -268:
						break;
					case 273:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -269:
						break;
					case 274:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -270:
						break;
					case 275:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -271:
						break;
					case 276:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -272:
						break;
					case 277:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -273:
						break;
					case 278:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -274:
						break;
					case 279:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -275:
						break;
					case 280:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -276:
						break;
					case 281:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -277:
						break;
					case 282:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -278:
						break;
					case 283:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -279:
						break;
					case 284:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -280:
						break;
					case 285:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -281:
						break;
					case 286:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -282:
						break;
					case 287:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -283:
						break;
					case 288:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -284:
						break;
					case 289:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -285:
						break;
					case 290:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -286:
						break;
					case 291:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -287:
						break;
					case 292:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -288:
						break;
					case 293:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -289:
						break;
					case 294:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -290:
						break;
					case 295:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -291:
						break;
					case 296:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -292:
						break;
					case 297:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -293:
						break;
					case 298:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -294:
						break;
					case 299:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -295:
						break;
					case 300:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -296:
						break;
					case 301:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -297:
						break;
					case 302:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -298:
						break;
					case 303:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -299:
						break;
					case 304:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -300:
						break;
					case 305:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -301:
						break;
					case 306:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -302:
						break;
					case 307:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -303:
						break;
					case 308:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -304:
						break;
					case 309:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -305:
						break;
					case 310:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -306:
						break;
					case 311:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -307:
						break;
					case 312:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -308:
						break;
					case 313:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -309:
						break;
					case 314:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -310:
						break;
					case 315:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -311:
						break;
					case 316:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -312:
						break;
					case 317:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -313:
						break;
					case 318:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -314:
						break;
					case 319:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -315:
						break;
					case 320:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -316:
						break;
					case 321:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -317:
						break;
					case 322:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -318:
						break;
					case 323:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -319:
						break;
					case 324:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -320:
						break;
					case 325:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -321:
						break;
					case 326:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -322:
						break;
					case 327:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -323:
						break;
					case 328:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -324:
						break;
					case 329:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -325:
						break;
					case 330:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -326:
						break;
					case 331:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -327:
						break;
					case 332:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -328:
						break;
					case 333:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -329:
						break;
					case 334:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -330:
						break;
					case 335:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -331:
						break;
					case 336:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -332:
						break;
					case 337:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -333:
						break;
					case 338:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -334:
						break;
					case 339:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -335:
						break;
					case 340:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -336:
						break;
					case 341:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -337:
						break;
					case 342:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -338:
						break;
					case 343:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -339:
						break;
					case 344:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -340:
						break;
					case 345:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -341:
						break;
					case 346:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -342:
						break;
					case 347:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -343:
						break;
					case 348:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -344:
						break;
					case 349:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -345:
						break;
					case 350:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -346:
						break;
					case 351:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -347:
						break;
					case 352:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -348:
						break;
					case 353:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -349:
						break;
					case 354:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -350:
						break;
					case 355:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -351:
						break;
					case 356:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -352:
						break;
					case 357:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -353:
						break;
					case 358:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -354:
						break;
					case 359:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -355:
						break;
					case 360:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -356:
						break;
					case 361:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -357:
						break;
					case 362:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -358:
						break;
					case 363:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -359:
						break;
					case 364:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -360:
						break;
					case 365:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -361:
						break;
					case 366:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -362:
						break;
					case 367:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -363:
						break;
					case 368:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -364:
						break;
					case 369:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -365:
						break;
					case 370:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -366:
						break;
					case 371:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -367:
						break;
					case 372:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -368:
						break;
					case 373:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -369:
						break;
					case 374:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -370:
						break;
					case 375:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -371:
						break;
					case 376:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -372:
						break;
					case 377:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -373:
						break;
					case 378:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -374:
						break;
					case 379:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -375:
						break;
					case 380:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -376:
						break;
					case 381:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -377:
						break;
					case 382:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -378:
						break;
					case 383:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -379:
						break;
					case 384:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -380:
						break;
					case 385:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -381:
						break;
					case 386:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -382:
						break;
					case 387:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -383:
						break;
					case 388:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -384:
						break;
					case 389:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -385:
						break;
					case 390:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -386:
						break;
					case 391:
						{return new GtSymbol(sym.TOKEN, yytext(), yyline);}
					case -387:
						break;
					default:
						yy_error(YY_E_INTERNAL,false);
					case -1:
					}
					yy_initial = true;
					yy_state = yy_state_dtrans[yy_lexical_state];
					yy_next_state = YY_NO_STATE;
					yy_last_accept_state = YY_NO_STATE;
					yy_mark_start();
					yy_this_accept = yy_acpt[yy_state];
					if (YY_NOT_ACCEPT != yy_this_accept) {
						yy_last_accept_state = yy_state;
						yy_mark_end();
					}
				}
			}
		}
	}
}
