echo off
echo －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
echo 使用Lex工具JLex根据gtSQL词法定义文件gtSQL.lex生成词法扫描工具类源代码gtSQL.lex.java
echo gtSQL.lex.java中的类Yylex实现了java_cup.runtime.Scanner接口
echo －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

java -classpath "JLex.jar"  JLex.Main gtSQL.lex

echo －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
echo 使用Yacc工具Cup根据gtSQL语法定义文件gtSQL.cup生成语法分析工具类源代码parser.java和符号常量定义源代码sym.java
echo gtSQL.lex.java需要引用sym.java
echo parser.java中的类parser有一个以java_cup.runtime.Scanner为参数的构造函数
echo －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

java -classpath "java-cup-11a.jar" java_cup.Main -nonterms -interface -nopositions gtSQL.cup

echo －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
echo 本程序输出的三个源代码文件将和gtSQL的其他源代码一起编译
echo 本程序、JLex工具、Cup工具、gtSQL.lex和gtSQL.cup编译时不再需要
