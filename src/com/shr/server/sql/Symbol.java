﻿package com.shr.server.sql;

import java.util.Vector;

/**
 * <p>Title: 词法分析和语法分析结果的类型</p>
 *
 * <p>Description: 词法分析JLex返回此类型的终结符
 *                 语法分析Cup的工作栈上的终结符和非终结符都是此类型
 * </p>
 *
 * <p>Copyright: Copyright (c) 2007</p>
 *
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0 
 */
public class Symbol extends java_cup.runtime.Symbol{
    /**
     * 构造终结符
     *
     * @param sym int
     * @param data String
     * @param lineno int
     */
    public Symbol(int sym, String data, int lineno){
        super(sym,new GtNode(sym,data,lineno));
    }
}
