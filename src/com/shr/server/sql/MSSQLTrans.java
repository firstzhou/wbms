package com.shr.server.sql;

import java.util.Vector;

public class MSSQLTrans extends DefaultTrans {
	// ==============================================================================
	/**
	 * BEGIN TOP...END TOP���־��˷���TopClause(TOP INTNUMBER:i1 INTNUMBER:i2)ʹ��
	 * ����ط��������ʹ�� �����б�����TOP_A TOP_B rn_��Ϊ����ʹ�ã������в���ʹ��������Ϊ����ʹ��
	 */
	// BEGIN TOP
	/**
	 *������¼orderby_clause������
	 */
	private String Top_Order1;
	/**
	 * ������¼orderby_clause������,������
	 */
	private String Top_Order2;

	// END TOP
	// ==============================================================================
	public MSSQLTrans() {
		super();
		DBType = 1;
	}

	/**
	 * ������ֹ��
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transTerminal(GtNode node, String context)
			throws Exception {
		if (node != null) {
			switch (node.getSym()) {
			case sym.CONNECT:
				return "+";
			case sym.CHAR:
				return "char";
			case sym.CHARINDEX: // 58
				return "charIndex";
			case sym.DATALENGTH:// 63
				return "dataLength";
			case sym.PI:// 85
				return "PI()";
			case sym.LEFT:
				return "left";
			case sym.LEN:
				return "len";
			case sym.LOG:
				return "log";
			case sym.LOG10:
				return "log10";
			case sym.SPACE:
				return "space";
			case sym.SQUARE:
				return "square";
			case sym.STR:
				return "str";
			case sym.STUFF:
				return "stuff";
			case sym.SUBSTRING:
				return "subString";
			case sym.GETDATE:
				return "getDate()";
			case sym.ATN2:
				return "ATN2";
			case sym.ISNULL:
				return "IsNull";
			case sym.CEILING:
				return "ceiling";
			case sym.DEGREES:
				return "degrees";
			default:
				return super.transTerminal(node, context);
			}
		} else
			return null;
	}

	protected String transSelect_Statement(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			int nodeCount = subNodes.size();
			if (nodeCount != 6)
				return super.transSelect_Statement(node, context);
			else {
				String Result = "";
				for (int i = 0; i < nodeCount; i++) {
					String transNodeString = transNode(subNodes.get(i), null);
					if (transNodeString != null) {
						if (i != 0)
							Result += "\n" + transNodeString;
						else
							Result = transNodeString;
					}
				}
				if (!TopTwo)
					return Result;
				else {
					return "select top 100 percent * " + "\r\n"
							+ "from (select top " + TopNumber1 + " " + TOP_A
							+ ".* " + "\r\n" + "      from (" + Result + ") "
							+ TOP_A + "\r\n" + "order by " + Top_Order2
							+ "\r\n" + "     ) " + TOP_B + "\r\n" + Top_Order1;

				}
			}
		} else
			return null;
	}

	/**
	 * Select_Clause:10
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transSelect_Clause(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			String SelectList = "";
			for (int i = 0; i < subNodes.size(); i++) {
				String transNodeString = transNode(subNodes.get(i), context);
				if (transNodeString != null) {
					if (i != 0)
						SelectList += " " + transNodeString;
					else
						SelectList = transNodeString;
				}
			}
			return "select " + SelectList;
		} else
			return null;

	}

	protected String transOrderBy_Clause(GtNode node, String context)
			throws Exception {
		String orderBy = super.transOrderBy_Clause(node, context);
		if (TopTwo)
			Top_Order1 = orderBy;
		return orderBy;
	}

	/**
	 * Top_Clause:17 a)TOP INTNUMBER:i b)TOP INTNUMBER:i1 INTNUMBER:i2
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transTop_Clause(GtNode node, String context)
			throws Exception {
		String DataString = node.getData();
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes == null)
			return null;
		else {
			int nodeCount = subNodes.size();
			if (nodeCount == 1) {
				return "top " + transNode(subNodes.get(0), context);
			} else if (nodeCount == 2) {
				TopTwo = true;
				TopNumber1 = transNode(subNodes.get(0), null);
				TopNumber2 = transNode(subNodes.get(1), null);
				return "top " + TopNumber2;
			} else
				return null;
		}
	}

	/**
	 * Function_Ref:24
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_Ref(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			int nodeCount = subNodes.size();
			GtNode firstNode = subNodes.get(0);
			if (firstNode.isTerminal()) {
				switch (firstNode.getSym()) {
				case sym.DAY:
				case sym.LEN:
				case sym.LOG10:
				case sym.MONTH:
				case sym.YEAR:
				case sym.RADIANS:
				case sym.SPACE:
				case sym.SQUARE:
				case sym.DEGREES:
					return transNode(firstNode, null) + "("
							+ transNode(subNodes.get(1), null) + ")";
				case sym.CONVERT:
				case sym.LEFT:
				case sym.REPLICATE:
				case sym.RIGHT:
					return transNode(firstNode, null) + "("
							+ transNode(subNodes.get(1), null) + ","
							+ transNode(subNodes.get(2), null) + ")";
				case sym.CHARINDEX: {
					if (nodeCount == 3)
						return transNode(firstNode, null) + "("
								+ transNode(subNodes.get(1), null) + ","
								+ transNode(subNodes.get(2), null) + ")";
					else if (nodeCount == 4)
						return transNode(firstNode, null) + "("
								+ transNode(subNodes.get(1), null) + ","
								+ transNode(subNodes.get(2), null) + ","
								+ transNode(subNodes.get(3), null) + ")";
					else
						return null;
				}
				case sym.LPAD:
					return "replicate(" + transNode(subNodes.get(3), null)
							+ "," + transNode(subNodes.get(2), null)
							+ "-dataLength(" + transNode(subNodes.get(1), null)
							+ "))+" + transNode(subNodes.get(1), null);
				case sym.MOD:
					return "(" + transNode(subNodes.get(1), null) + " % "
							+ transNode(subNodes.get(2), null) + ")";
				case sym.STUFF:
					return transNode(firstNode, null) + "("
							+ transNode(subNodes.get(1), null) + ","
							+ transNode(subNodes.get(2), null) + ","
							+ transNode(subNodes.get(3), null) + ","
							+ transNode(subNodes.get(4), null) + ")";
				case sym.TRUNC:
					return "round(" + transNode(subNodes.get(1), null)
							+ ",0,1)";
				default:
					return super.transFunction_Ref(node, context);
				}
			} else
				return null;
		} else
			return null;

	}

	/**
	 * Gt_Function_Ref:25
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transGt_Function_Ref(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			if (subNodes.size() > 1) {
				GtNode firstNode = subNodes.get(0);
				if (firstNode.getSym() != sym.EQT)
					return super.transGt_Function_Ref(node, context);
				else {// EQT�����Ƚ�����ʱ����
					return "(Round(convert(float, convert(datetime, "
							+ transNode(subNodes.get(1), context)
							+ ")) * 24 * 3600, 0, 1)="
							+ "Round(convert(float, convert(datetime, ("
							+ transNode(subNodes.get(2), context)
							+ "))) * 24 * 3600, 0, 1)  )";
				}
			} else
				return null;
		} else
			return null;
	}

	/**
	 * When_List_Normal:27
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transWhen_List_Normal(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			String Result = "";
			for (int i = 0; i < subNodes.size(); i++) {
				String transNodeString = transNode(subNodes.get(i), context);
				if (transNodeString != null) {
					if (Result.equalsIgnoreCase(""))
						Result = transNodeString;
					else
						Result += " " + transNodeString;
				}
			}
			return Result;
		} else
			return null;
	}

	/**
	 * Else_Expr:28
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transElse_Expr(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 1) {
			return "else " + transNode(subNodes.get(0), null);
		} else
			return null;
	}

	/**
	 * When_List_Bool:29
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transWhen_List_Bool(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			String Result = "";
			for (int i = 0; i < subNodes.size(); i++) {
				String transNodeString = transNode(subNodes.get(i), context);
				if (transNodeString != null) {
					if (Result.equalsIgnoreCase(""))
						Result = transNodeString;
					else
						Result += " " + transNodeString;
				}
			}
			return Result;
		} else
			return null;
	}

	/**
	 * Decode_List:32
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transDecode_List(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			String Result = "";
			for (int i = 0; i < subNodes.size(); i++) {
				String transNodeString = transNode(subNodes.get(i), context);
				if (transNodeString != null)
					Result += " " + transNodeString;
			}
			return Result;
		} else
			return null;
	}

	/**
	 * Decode_Cond_Res_Expr:33
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transDecode_Cond_Res_Expr(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 2) {
			return "when " + transNode(subNodes.get(0), null) + " then "
					+ transNode(subNodes.get(1), null);
		} else
			return null;
	}

	/**
	 * case_expression_when_list_normal_else:34
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transCase_Expression_When_List_Normal_Else(GtNode node,
			String context) throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 3) {
			String NodeStr1 = transNode(subNodes.get(0), null);
			String NodeStr2 = transNode(subNodes.get(1), null);
			String NodeStr3 = transNode(subNodes.get(2), null);
			if (NodeStr3 != null)
				return "case " + NodeStr1 + " " + NodeStr2 + " " + NodeStr3
						+ " end";
			else
				return "case " + NodeStr1 + " " + NodeStr2 + " end";
		} else
			return null;
	}

	/**
	 * Case_When_List_Bool_Else:35
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transCase_When_List_Bool_Else(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 2) {
			String NodeStr1 = transNode(subNodes.get(0), null);
			String NodeStr2 = transNode(subNodes.get(1), null);
			if (NodeStr2 != null)
				return "case " + NodeStr1 + " " + NodeStr2 + " end";
			else
				return "case " + NodeStr1 + " end";
		} else
			return null;

	}

	/**
	 * Function_Convert_With_Datatype_Length:36
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_Convert_With_Datatype_Length(GtNode node,
			String context) throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 3) {
			String nodeStr1 = transNode(subNodes.get(0), null);
			if (nodeStr1.equalsIgnoreCase("char")
					|| nodeStr1.equalsIgnoreCase("varchar")) {
				return "convert(" + nodeStr1 + "("
						+ transNode(subNodes.get(1), null) + "),"
						+ transNode(subNodes.get(2), null) + ")";
			} else {
				throwExceptionMess(node.getLineno(), node.getData(),
						"convert ����֧�� " + nodeStr1 + "("
								+ transNode(subNodes.get(1), null) + ") ����");
				return "<convert ����֧�� " + nodeStr1 + "("
						+ transNode(subNodes.get(1), null) + ") ����>";
			}

		} else
			return null;
	}

	/**
	 * Function_Count:38
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_DateAdd(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 3) {
			GtNode firstNode = subNodes.get(0);
			String firstNodeStr = transNode(firstNode, null);
			if (firstNodeStr.equalsIgnoreCase("HOUR")
					|| firstNodeStr.equalsIgnoreCase("MINUTE")
					|| firstNodeStr.equalsIgnoreCase("SECOND")
					|| firstNodeStr.equalsIgnoreCase("DAY")
					|| firstNodeStr.equalsIgnoreCase("WEEK")
					|| firstNodeStr.equalsIgnoreCase("MONTH")
					|| firstNodeStr.equalsIgnoreCase("QUARTER")
					|| firstNodeStr.equalsIgnoreCase("YEAR")) {
				return "dateAdd(" + firstNodeStr + ","
						+ transNode(subNodes.get(1), null) + ","
						+ transNode(subNodes.get(2), null) + ")";
			} else {
				throwExceptionMess(firstNode.getLineno(), firstNode.getData(),
						"DateAdd��֧�� " + firstNodeStr + " ����");
				return "<DateAdd��֧�� " + firstNodeStr + " ����>";
			}
		} else
			return null;
	}

	/**
	 * Function_Dateiff:39
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_DateDIff(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 3) {
			GtNode firstNode = subNodes.get(0);
			String firstNodeStr = transNode(firstNode, null);
			// year, month, quarter, week, day, second
			if (firstNodeStr.equalsIgnoreCase("SECOND")
					|| firstNodeStr.equalsIgnoreCase("DAY")
					|| firstNodeStr.equalsIgnoreCase("WEEK")
					|| firstNodeStr.equalsIgnoreCase("MONTH")
					|| firstNodeStr.equalsIgnoreCase("QUARTER")
					|| firstNodeStr.equalsIgnoreCase("YEAR")) {
				return "dateDiff(" + firstNodeStr + ","
						+ transNode(subNodes.get(1), null) + ","
						+ transNode(subNodes.get(2), null) + ")";
			} else {
				throwExceptionMess(firstNode.getLineno(), firstNode.getData(),
						"DateDiff��֧�� " + firstNodeStr + " ����");
				return "<DateDiff��֧�� " + firstNodeStr + " ����>";
			}
		} else
			return null;
	}

	/**
	 * Function_Datepart:40
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_Datepart(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 2) {
			GtNode firstNode = subNodes.get(0);
			String firstNodeStr = transNode(firstNode, null);
			// year�� quarter�� month�� week�� day�� weekday�� hour�� minute�� second
			if (firstNodeStr.equalsIgnoreCase("MINUTE")
					|| firstNodeStr.equalsIgnoreCase("HOUR")
					|| firstNodeStr.equalsIgnoreCase("SECOND")
					|| firstNodeStr.equalsIgnoreCase("DAY")
					|| firstNodeStr.equalsIgnoreCase("WEEKDAY")
					|| firstNodeStr.equalsIgnoreCase("WEEK")
					|| firstNodeStr.equalsIgnoreCase("MONTH")
					|| firstNodeStr.equalsIgnoreCase("QUARTER")
					|| firstNodeStr.equalsIgnoreCase("YEAR")) {
				return "datepart(" + firstNodeStr + ","
						+ transNode(subNodes.get(1), null) + ")";
			} else {
				throwExceptionMess(firstNode.getLineno(), firstNode.getData(),
						"Datepart��֧�� " + firstNodeStr + " ����");
				return "<Datepart��֧�� " + firstNodeStr + " ����>";
			}
		} else
			return null;

	}

	/**
	 * Function_Nullif:42
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_Nullif(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 2) {
			return "nullIf(" + transNode(subNodes.get(0), null) + ","
					+ transNode(subNodes.get(1), null) + ")";
		} else
			return null;
	}

	/**
	 * Function_Decode:43
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transFunction_Decode(GtNode node, String context)
			throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null) {
			int NodeCount = subNodes.size();
			if (NodeCount == 2) {
				return "(case " + transNode(subNodes.get(0), null) + " "
						+ transNode(subNodes.get(1), null) + " end)";
			} else if (NodeCount == 3) {
				return "(case " + transNode(subNodes.get(0), null) + " "
						+ transNode(subNodes.get(1), null) + " else "
						+ transNode(subNodes.get(2), null) + " end)";
			} else {
				throwExceptionMess(node.getLineno(), node.getData(),
						"decode�������");
				return "<decode �������>";
			}
		} else
			return null;
	}

	/**
	 * When_Expression_Then_Expression:44
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transWhen_Expression_Then_Expression(GtNode node,
			String context) throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 2) {
			return "when " + transNode(subNodes.get(0), null) + " then "
					+ transNode(subNodes.get(1), null);
		} else
			return null;
	}

	/**
	 * When_Expression_Bool_Then_Expression:45
	 * 
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transWhen_Expression_Bool_Then_Expression(GtNode node,
			String context) throws Exception {
		Vector<GtNode> subNodes = node.getSubNodes();
		if (subNodes != null && subNodes.size() == 2) {
			return "when " + transNode(subNodes.get(0), null) + " then "
					+ transNode(subNodes.get(1), null);
		} else
			return null;

	}

	/**
	 * @param node
	 *            GtNode
	 * @param context
	 *            String
	 * @return String
	 */
	protected String transOrderBy_Element(GtNode node, String context)
			throws Exception {
		if (TopTwo) {
			Vector<GtNode> subNodes = node.getSubNodes();
			if (subNodes != null && subNodes.size() == 2) {
				String firstNode = transNode(subNodes.get(0), null);
				if (Top_Order2 != null && !Top_Order2.equalsIgnoreCase("")) {
					Top_Order2 += "," + firstNode;
				} else {
					Top_Order2 = firstNode;
				}
				String OrderStr = transNode(subNodes.get(1), null);
				if (OrderStr != null) {
					if (OrderStr.equalsIgnoreCase("desc")) {
						Top_Order2 += " asc";
					} else {
						Top_Order2 += " desc";
					}
				} else
					Top_Order2 += " desc";
			}
		}
		return super.transOrderBy_Element(node, context);
	}
}
