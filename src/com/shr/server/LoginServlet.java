package com.shr.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shr.server.db.TAConnection;
import com.shr.server.system.PublicServer;
import com.shr.shared.MD5;

public class LoginServlet extends HttpServlet
{

    public void destroy()
    {
        super.destroy(); // Just puts "destroy" string in log
        // Put your code here
    }

    public void init() throws ServletException
    {
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	response.setCharacterEncoding("GB2312");
        String usrid = request.getParameter("usrid");
        String password = request.getParameter("password");
        String action = request.getParameter("action");
        String codesvr = request.getParameter("codesvr");
        ServerApp.ZHANG_TAO = request.getParameter("yearSelectId");
        String zhangTao = "/"+ServerApp.ZHANG_TAO;
        zhangTao = "";
        String sScript = "";
        TAConnection conn = new TAConnection();
        ResultSet rs = null;
        try
        {
        	if(PublicServer.isReginst())
        	{
        		throw new Exception("系统已过期！");
        	}
            if ("Login".equals(action))
            {
                MD5 md5 = new MD5();

                conn.SQL().clear();
                conn.SQL().Add("select USRNAME,USRID,USRDEPTNAME,BM_MC, isnull(USRCHECK,'') USRCHECK, isnull(USRDBID,'') USRDBID  from SYS_USERS,SYS_BM ");
                conn.SQL().Add(" where 1=1 AND SYS_USERS.USRDEPTNAME = SYS_BM.BM_BMID ");
                conn.SQL().Add(" and ISNULL(USRHOLDED,'N') ='N'  ");
                conn.SQL().Add(" and USRID=:pUSRID  ");
                conn.SQL().Add(" and USRPASSWORD=:pPASSWORD  ");

                conn.SQL().setString("pUSRID", usrid);
                conn.SQL().setString("pPASSWORD", md5.getMD5ofStr(password));

                rs = conn.executeQuery();
                if (rs.next())
                {// 登录成功
                    request.getSession().setAttribute("usrid", usrid);
                    request.getSession().setAttribute("userId", usrid); //以前有一部分是这个名字
                    request.getSession().setAttribute("usrname", rs.getString("USRNAME").trim());
                    request.getSession().setAttribute("USRDEPTNAME", rs.getString("USRDEPTNAME").trim());
                    request.getSession().setAttribute("BM_MC", rs.getString("BM_MC").trim());
                    request.getSession().setAttribute("curUserDataRight", rs.getString("USRCHECK").trim());
                    request.getSession().setAttribute("curUserRightContent", rs.getString("USRDBID").trim());
                    ServerApp.DB_TYPE = conn.getDbType();

                    PrintWriter writer = response.getWriter();
                    if (Tools.isNullOrEmpty(codesvr) || codesvr.equals("null"))
                    {
                        sScript = "<script>top.location.href=\""+zhangTao+"/shr.jsp\";</script>";
                    }
                    else
                    {
                        sScript = "<script>top.location.href=\""+zhangTao+"/shr.jsp?gwt.codesvr=" + codesvr + "\";</script>";
                    }
                    writer.print(sScript);
                    response.flushBuffer();
                }
                else
                {
                    System.out.println("登录失败");
                    PrintWriter writer = response.getWriter();
                    writer.print("<script>alert('用户名密码错误!');top.location.href=\""+zhangTao+"/index.jsp\";</script>");
                }

            }
            else
            {
                request.getSession().removeAttribute("usrid");
            }

        }
        catch (Exception e)
        {
            e.printStackTrace();
            response.getWriter().print(e.getMessage());
            // 可以跳转出错页面
        }
        finally
        {
            try
            {
                if (rs != null)
                {
                    rs.close();
                }
            }
            catch (SQLException e)
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            conn.close();
        }
    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        doPost(req, resp);
    }
}
