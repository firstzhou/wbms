package com.shr.server;

import javax.servlet.http.HttpServletRequest;

import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.datasource.DataSource;

public class BaseDocServerDMI extends BaseServerDMI {

	public DSResponse doOperation(DSRequest dsRequest,
			HttpServletRequest servletRequest) throws Exception {
		
		if (DataSource.isAdd(dsRequest.getOperationType()) || DataSource.isUpdate(dsRequest.getOperationType())) 
		{
			saveFile(dsRequest, servletRequest);
		} 
		return super.doOperation(dsRequest, servletRequest);
	}
	
	/**
	 * 保存文件
	 * @param dsRequest
	 * @param servletRequest
	 * @throws Exception
	 */
	private void saveFile(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception
	{
	 	String filename = (String)dsRequest.getFieldValue("DEF1");
	 	if(filename == null)
	 	{
	 		return;
	 	}
	 	filename = filename.substring(filename.lastIndexOf("\\")+1);
	 	dsRequest.setFieldValue("image_filename", filename);
		dsRequest.setFieldValue("DEF1", filename);
	}
}
