package com.shr.server;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.isomorphic.datasource.DSField;
import com.isomorphic.datasource.DSRequest;
import com.isomorphic.datasource.DSResponse;
import com.isomorphic.datasource.DataSource;

public class BaseServerDMI {
	private String userId = "";
	private String userName = "";

	public DSResponse doOperation(DSRequest dsRequest, HttpServletRequest servletRequest) throws Exception 
	{

		userId = servletRequest.getSession().getAttribute("usrid").toString();
		userName = servletRequest.getSession().getAttribute("usrname").toString();
		init();
		String sTableName = dsRequest.getDataSourceName();
		if (DataSource.isAdd(dsRequest.getOperationType())) 
		{
			boolean booleanGuid = true;
			List<DSField> f = dsRequest.getDataSource().getFields();
			for (int i = 0; i < f.size(); i++) 
			{ // 如果是非标准的名称，标准PK名字为PK_表名
				DSField dsField = f.get(i);
				String guid = (String)dsField.get("guid");
				if(guid != null && !"".equals(guid))
				{
					booleanGuid = Boolean.parseBoolean(guid);
				}
				
				if ("true".equals(guid)) 
				{
					String uuid = Tools.getTimeMillisSequence();
					dsRequest.setFieldValue(dsField.getName(), uuid.toString());
					i = f.size();
				}
			}
			if(booleanGuid)
			{
				String uuid = Tools.getTimeMillisSequence();
				dsRequest.setFieldValue("PK_" + sTableName, uuid.toString());
			}
			dsRequest.setFieldValue(sTableName + "_WHR", userName);
			dsRequest.setFieldValue(sTableName + "_WHRID", userId);
			dsRequest.setFieldValue(sTableName + "_WHSJ", new Date());
			dsRequest.setFieldValue(sTableName.substring(4) + "_WHR", userName);
			dsRequest.setFieldValue(sTableName.substring(4) + "_WHRID", userId);
			dsRequest.setFieldValue(sTableName.substring(4) + "_WHSJ", new Date());
			dsRequest.setFieldValue("WHR", userName);
			dsRequest.setFieldValue("WHRID", userId);
			dsRequest.setFieldValue("WHSJ", new Date());
			beforeAdd(dsRequest, servletRequest);
		} 
		else if (DataSource.isUpdate(dsRequest.getOperationType())) 
		{
			dsRequest.setFieldValue(sTableName + "_WHR", userName);
			dsRequest.setFieldValue(sTableName + "_WHRID", userId);
			dsRequest.setFieldValue(sTableName + "_WHSJ", new Date());
			dsRequest.setFieldValue("WHR", userName);
			dsRequest.setFieldValue("WHRID", userId);
			dsRequest.setFieldValue("WHSJ", new Date());
			beforeUpdate(dsRequest, servletRequest);
		}
		return dsRequest.execute();
	}
	
	/**
	 * 服务端在ds新增保存之前操作
	 * @param dsRequest
	 * @param servletRequest
	 */
	public void beforeAdd(DSRequest dsRequest, HttpServletRequest servletRequest) 
	{

	}

	public void init() {

	}

	/**
	 * 服务端在ds编辑保存之前操作
	 * @param dsRequest
	 * @param servletRequest
	 */
	public void beforeUpdate(DSRequest dsRequest, HttpServletRequest servletRequest) 
	{

	}
}
