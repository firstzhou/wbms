package com.shr.server.excel;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.shr.server.excel.xml.Mapping;
import com.shr.server.excel.xml.Table;
import com.shr.server.excel.xml.XmlUtil;

public class DataImport extends HttpServlet 
{
	private static final long serialVersionUID = 7440302204266787092L;
	String path = "UploadFiles";
	String uploadPath = ""; // 用于存放上传文件的目录
	File tempPath ;
	
	XlsDataImport importServer;
	
	String userid = null;
	
	String username = null;
	
	public DataImport() {
		super();
		// System.out.println("文件上传启动");
	}

	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void init() throws ServletException {
		System.out.println("文件上传初始化!");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException 
	{
		String impResult = null;
		try {
			uploadPath = request.getSession().getServletContext().getRealPath("UploadFiles");

			String type = request.getParameter("types");
			String impKey = request.getParameter("mappingID");
			String replaceData = request.getParameter("replaceData");
			userid = (String)request.getSession().getAttribute("usrid");
			username = (String)request.getSession().getAttribute("usrname");
			//保存自定义参数
			String buildContent = request.getParameter("buildContent");
			if(null != buildContent)
			{
				XmlUtil.buildContent(buildContent);
			}
			
			if(userid == null)
			{
				response.getWriter().print("打开页面时间过长，请重新登陆！");
				return;
			}
			DiskFileItemFactory factory = new DiskFileItemFactory();
	        // set the size threshold, above which content will be stored on disk
	        factory.setSizeThreshold(1 * 1024 * 1024); // 1 MB
	        
			ServletFileUpload fu = new ServletFileUpload(factory);

			List<?> fileItems = fu.parseRequest(request); // 得到所有的文件：
			Iterator<?> i = fileItems.iterator();
			// 依次处理每一个文件：
			while (i.hasNext()) 
			{ 
				FileItem fi = (FileItem) i.next();
				String fileName = fi.getName();// 获得文件名，这个文件名包括路径：
				if(fileName == null)
				{
					continue;
				}
				if(fileName.endsWith(".xls")) 
				{
					String extfile = fileName.substring(fileName
							.lastIndexOf("."));
					Timestamp now = new Timestamp(
							(new java.util.Date()).getTime());
					SimpleDateFormat fmt = new SimpleDateFormat(
							"yyyyMMddHHmmssSSS");
					String pfileName = fmt.format(now).toString().trim();
					fi.write(new File(uploadPath + "/" + pfileName + extfile));
					if("ExcelImp".equals(type))
					{
						impResult = excutImp(impKey, uploadPath + "/" + pfileName + extfile, replaceData);
					}
					response.setContentType("text/html;charset=utf-8");
					if(impResult == null || impResult.equals(""))
					{
						impResult = "导入成功!";
					}
					response.getWriter().print("导入信息：<br>" +impResult+ "<br>");
				} 
				else 
				{
					response.setContentType("text/html;charset=utf-8");
					response.getWriter().print("导入信息：<br>错误信息：文件类型不符，必须是xls文件。");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			response.getWriter().print("{success:flase,message:'失败'}");
		}
	}
	
	/**
	 * 执行导入
	 * @param mappingKey
	 * @param filePath
	 */
	private String excutImp(String mappingKey, String filePath, String replaceData)
	{
		Map<String, Mapping> warpMapping = null;
		try
		{
			warpMapping = XmlUtil.warpMappingBean();
		}
		catch(Exception e)
		{
			return e.getMessage();
		}
		Mapping map = warpMapping.get(mappingKey);
		
		//控制导入时候是否覆盖数据
		for(Table t : map.getTables())
		{
			if("replace".equals(t.getImpType()) && "false".equals(replaceData))
			{
				t.setImpType("");
			}
		}
		//代理类
		String strClass = map.getProxy();
		if(strClass != null)
		{
			Class<?> imp = null;
			try
			{
				imp = Class.forName(strClass);
				importServer = (XlsDataImport)imp.newInstance();
			}
			catch(Exception e)
			{
				return "配置的代理类有问题！";
			}
		}
		else
		{
			importServer = new XlsDataImport();
		}
		importServer.defaultValue.put(XmlUtil.DEFAULT_VALUE_USERID, userid);
		importServer.defaultValue.put(XmlUtil.DEFAULT_VALUE_USERNAME, username);
		return importServer.excutImpExcel(map, filePath);
	}
}
