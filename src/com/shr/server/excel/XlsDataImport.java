package com.shr.server.excel;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import com.shr.server.ServerApp;
import com.shr.server.Tools;
import com.shr.server.excel.dao.XlsDataImportDao;
import com.shr.server.excel.xml.CellValue;
import com.shr.server.excel.xml.Filed;
import com.shr.server.excel.xml.Mapping;
import com.shr.server.excel.xml.Table;
import com.shr.server.excel.xml.XmlUtil;

public class XlsDataImport 
{
	
	//导入的结果
	StringBuffer impResult = new StringBuffer();
	
	//存储外键的值
	Map<String, Map<String, String>> forKeyValueMap = new HashMap<String, Map<String, String>>();
	
	//数据库操作
	XlsDataImportDao importDao = new XlsDataImportDao();
	
	//存储导入的默认值
	public Map<String, String> defaultValue = new HashMap<String, String>();

	/**
	 * 执行导入的主方法
	 * @param mappingKey
	 * @param filePath
	 * @return
	 */
	public String excutImpExcel(Mapping map, String filePath)
	{
		try
		{
			Map<String, List<List<CellValue>>> tables = readExcelMapping(map, filePath);
			if(tables == null || tables.isEmpty()) 
			{
				throw new Exception("不能导入空表格，请检查各页签表格。");
			}
			Map<String, Table> tabelMap = new HashMap<String, Table>();
			for(Table table : map.getTables())
			{
				String tableName = table.getName();
				tabelMap.put(tableName, table);
			}
			//如果数据校验有重复的先解决重复问题再解决别的问题
			if(!checkXlsRepeat(tables, tabelMap))
			{
				List<String> sqlList = buildSQL(tables, tabelMap);
				if(sqlList != null && sqlList.size()>0)
				{
					//返回错误消息为空才执行插入
					if(impResult == null || impResult.length() == 0)
					{
						importDao.exeSQLs(sqlList.toArray(new String[]{}));
						String re = doImportAfter();
						if(re != null)
						{
							addImpResult(re);
						}
					}
				}
				else
				{
					addImpResult("生成了0条记录");
				}
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			addImpResult(e.getMessage());
		}
		return impResult.toString();
	}
	
	/**
	 * 子类重写处理后续事情
	 */
	public String doImportAfter()
	{
		return null;
	}
	
	/**
	 * 校验excel中的值是否有重复的
	 * @param tables
	 * @param tabelMap
	 * @return
	 */
	public boolean checkXlsRepeat(Map<String, List<List<CellValue>>> tables, Map<String, Table> tabelMap)
	{
		boolean result = false;
		for(String tableName : tables.keySet())
		{
			Table tableConf = tabelMap.get(tableName);
			String xlsRepeatFiled = tableConf.getXlsRepeatFiled();
			List<String> repeatFileds = new ArrayList<String>();
			if(xlsRepeatFiled != null && !"".equals(xlsRepeatFiled))
			{
				String[] rfs = xlsRepeatFiled.split(XmlUtil.KEY_FILED_SPLIT);
				for(String filed : rfs)
				{
					repeatFileds.add(filed);
				}
			}
			else
			{
				continue;
			}
			List<List<CellValue>> tableValue = tables.get(tableName);
			List<String> checkList = new ArrayList<String>();
			for(List<CellValue> cells : tableValue)
			{
				int row = 0;
				String tempStr = "";
				for(CellValue cell : cells)
				{
					if(repeatFileds.contains(cell.getFiledName()))
					{
						tempStr += cell.getValue();
						row = cell.getRow();
					}
				}
				if(checkList.contains(tempStr))
				{
					addImpResult("第"+(row+1)+"行数据在文件中重复");
					result = true;
				}
				else
				{
					checkList.add(tempStr);
				}
			}
		}
		return result;
	}
	/**
	 * 根据配置文件读取excel文件
	 * 每行值放入一个List，一个表有多个行即多个list再放入一个List
	 * @return Map<表名,List<List<CellValue>>>
	 * @throws Exception 
	 */
	public Map<String, List<List<CellValue>>> readExcelMapping(Mapping map, String filePath) throws Exception
	{
		Map<String, List<List<CellValue>>> excelResult = new HashMap<String, List<List<CellValue>>>();
		//表格数据
		List<Table> tables = map.getTables();
		try
		{
			// 打开文件
			File excel = new File(filePath);
			Workbook book = Workbook.getWorkbook(excel);
			// 至取得第一个sheet
			Sheet sheets[] = book.getSheets();
			String tableName = null;
			List<List<CellValue>> listFileds = new ArrayList<List<CellValue>>();
			for(Sheet sheet : sheets)
			{
				String sheetName = sheet.getName();
				int rows = sheet.getRows();
				
				if(rows == 0 || sheet.isHidden()) 
				{
					continue;
				}
				for(Table table : tables)
				{
					String sheetValidateName = table.getSheetName();
					if(sheetValidateName != null && !"".equals(sheetValidateName))
					{
						//正则表达式校验sheet名称
						Pattern pa = Pattern.compile(sheetValidateName);
						Matcher matcher = pa.matcher(sheetName);
						boolean b= matcher.matches();
						if(!b)
						{
							continue;
						}
					}
					tableName = table.getName();
					
					int beginRow = table.getBeginRow();
					int endRow = table.getEndRow();
					List<Filed> fileds = table.getFileds();
					while(beginRow <= rows)
					{
						//每行数据存入一个List
						List<CellValue> filedValue = new ArrayList<CellValue>();
						
						Cell[] cells = sheet.getRow(beginRow-1);
						if(isRowEmpty(cells))
						{
							break;
						}
						for(Filed filed : fileds)
						{
							CellValue cellValue = getFiledValue(sheetName, cells, filed);
							cellValue.setRow(beginRow-1);
							filedValue.add(cellValue);
						}
						beginRow++;
						listFileds.add(filedValue);
					}
					if(endRow < listFileds.size())
					{
						throw new Exception("一次最多支持导入"+endRow+"条数据！");
					}
				}
			}
			excelResult.put(tableName, listFileds);
			// 关闭文件
			book.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			throw e;
		}
		return excelResult;
	}
	
	/**
	 * 校验是否是空行，如遇到空行则结束导入
	 * @param row
	 * @return
	 */
	private boolean isRowEmpty(Cell[] row)
	{
		boolean result = true;
		if(row == null)
		{
			return true;
		}
		for(Cell cell : row)
		{
			String content = cell.getContents();
			if(content != null && !"".equals(content.trim()))
			{
				result = false;
				break;
			}
		}
		return result;
	}
	
	/**
	 * 将excle表结构转换为sql语句
	 * @param tables
	 * @param impType 导入类型（replace，error）
	 * @return keyFiled 标识列，可以多个
	 */
	public List<String> buildSQL(Map<String, List<List<CellValue>>> tables, Map<String, Table> tabelMap)
	{
		List<String> sqlList = new ArrayList<String>();
		for(String table : tables.keySet())
		{
			Table tableEle = tabelMap.get(table);
			String impType = tableEle.getImpType();
			String keyFiled = tableEle.getKeyFiled();
			List<Filed> xmlFields = tableEle.getFileds();
			List<String> primaryKeys = getPrimaryKeys(xmlFields);
			
			List<List<CellValue>> fields = tables.get(table);
			if(XmlUtil.IMP_TYPE_REPLACE.equals(impType))
			{
				if(keyFiled != null && !"".equals(keyFiled))
				{
					sqlList.add(getDeleteSql(table, fields.get(0),keyFiled));
				}
				else
				{
					sqlList.add("delete from "+table);
				}
			}
			for(List<CellValue> field : fields)
			{
				if(XmlUtil.IMP_TYPE_REPLACE.equals(impType))
				{
					sqlList.add(getInsertSql(table, field));
				}
				else if(XmlUtil.IMP_TYPE_ERROR.equals(impType))
				{
					if(!checkRepeat(table, field, keyFiled))
					{
						sqlList.add(getInsertSql(table, field));
					}
					else
					{
						addImpResult("第"+(field.get(0).getRow()+1)+"行数据已在数据库中存在！");
					}
				}
				else if(XmlUtil.IMP_TYPE_UPDATE.equals(impType))
				{
					if(checkRepeat(table, field, keyFiled))
					{
						
						sqlList.add(getUpdateSql(table, field, keyFiled, primaryKeys));
					}
					else
					{
						sqlList.add(getInsertSql(table, field));
					}
				}
				else
				{
					sqlList.add(getInsertSql(table, field));
				}
			}
		}
		return sqlList;
	}
	
	/**
	 * 组装update 语句
	 * @param table
	 * @param field
	 * @return
	 */
	public String getDeleteSql(String table, List<CellValue> fields, String keyFiled)
	{
		String[] keyFileds = keyFiled.split(XmlUtil.KEY_FILED_SPLIT);
		String updateSql2 = "";
		//构建where语句
		for(String keyf : keyFileds)
		{
			Object value = null;
			for(CellValue field : fields)
			{
				String fn = field.getFiledName();
				if(keyf.equals(fn))
				{
					value = field.getValue();
					break;
				}
			}
			if(value == null)
			{
				continue;
			}
			
			if(value instanceof String)
			{
				updateSql2 += keyf + "='" + value + "' and ";
			}
			else if(value instanceof Date)
			{
				updateSql2 += keyf + "='" + value + "' and ";
			}
			else
			{
				updateSql2 += keyf + "=" + value + " and ";
			}
		}
		updateSql2 = updateSql2.substring(0, updateSql2.length()-4);
		String sql = "delete from "+table +" where " + updateSql2 + "; ";
		return sql;
	}
	
	/**
	 * 获得表的主键字段
	 * @param xmlFields
	 * @return
	 */
	private List<String> getPrimaryKeys(List<Filed> xmlFields)
	{
		List<String> result = new ArrayList<String>();
		for(Filed filed : xmlFields)
		{
			if(filed.isPrimaryKey())
			{
				result.add(filed.getName());
			}
		}
		return result;
	}
	
	/**
	 * 组装insert 语句
	 * @param table
	 * @param field
	 * @return
	 */
	public String getInsertSql(String table, List<CellValue> fields)
	{
		String insertSql1 = "";
		String insertSql2 = "";
		for(CellValue field : fields)
		{
			String fieldName = field.getFiledName();
			Object value = field.getValue();
			if(value == null)
			{
				continue;
			}
			insertSql1 += fieldName + ",";
			if(value instanceof String)
			{
				insertSql2 += "'" + value + "',";
			}
			else if(value instanceof Date)
			{
				if(ServerApp.DB_TYPE.equals("ORACLE"))
				{
					insertSql2 += "to_date('" + XmlUtil.getImpTimeStr(((Date)value)) + "','yyyy-mm-dd hh24:mi:ss'),";
				}
				else if(ServerApp.DB_TYPE.equals("MSSQL"))
				{
					insertSql2 += "'" + XmlUtil.getImpTimeStr(((Date)value)) + "',";
				}
			}
			else
			{
				insertSql2 += value + ",";
			}
		}
		insertSql1 = insertSql1.substring(0, insertSql1.length()-1);
		insertSql2 = insertSql2.substring(0, insertSql2.length()-1);
		String sql = "insert into "+table +"(" + insertSql1 + ") values (" + insertSql2 + "); ";
		return sql;
	}
	
	/**
	 * 组装update 语句
	 * @param table
	 * @param field
	 * @return
	 */
	public String getUpdateSql(String table, List<CellValue> fields, String keyFiled, List<String> pks)
	{
		String[] keyFileds = keyFiled.split(XmlUtil.KEY_FILED_SPLIT);
		String updateSql1 = "";
		String updateSql2 = "";
		//构建update语句
		for(CellValue field : fields)
		{
			String fieldName = field.getFiledName();
			Object value = field.getValue();
			if(value == null)
			{
				continue;
			}
			
			//主键不做更新
			if(pks.contains(fieldName))
			{
				continue;
			}
			
			if(value instanceof String)
			{
				updateSql1 += fieldName + "='" + value + "',";
			}
			else if(value instanceof Date)
			{
				if(ServerApp.DB_TYPE.equals("ORACLE"))
				{
					updateSql1 += fieldName + "=" + "to_date('" + XmlUtil.getImpTimeStr(((Date)value)) + "','yyyy-mm-dd hh24:mi:ss'),";
				}
				else if(ServerApp.DB_TYPE.equals("MSSQL"))
				{
					updateSql1 += fieldName + "='" + XmlUtil.getImpTimeStr(((Date)value)) + "',";
				}
			}
			else
			{
				updateSql1 += fieldName + "=" + value + ",";
			}
		}
		//构建where语句
		for(String keyf : keyFileds)
		{
			Object value = null;
			for(CellValue field : fields)
			{
				String fn = field.getFiledName();
				if(keyf.equals(fn))
				{
					value = field.getValue();
					break;
				}
			}
			if(value == null)
			{
				continue;
			}
			
			if(value instanceof String)
			{
				updateSql2 += keyf + "='" + value + "' and ";
			}
			else if(value instanceof Date)
			{
				updateSql2 += keyf + "='" + value + "' and ";
			}
			else
			{
				updateSql2 += keyf + "=" + value + " and ";
			}
		}
		updateSql1 = updateSql1.substring(0, updateSql1.length()-1);
		updateSql2 = updateSql2.substring(0, updateSql2.length()-4);
		String sql = "update "+table +" set " + updateSql1 + " where " + updateSql2 + "; ";
		return sql;
	}
	
	
	/**
	 * 校验数据库中是否已经存在值
	 * @param table
	 * @param fields
	 * @param keyFiled
	 * @return
	 */
	public boolean checkRepeat(String table, List<CellValue> fields, String keyFiled)
	{
		boolean result = false;
		String[] keyFileds = keyFiled.split(XmlUtil.KEY_FILED_SPLIT);
		String whereStr = "";
		for(String keyf : keyFileds)
		{
			Object value = null;
			for(CellValue field : fields)
			{
				String fn = field.getFiledName();
				if(keyf.equals(fn))
				{
					value = field.getValue();
					break;
				}
			}
			if(value == null)
			{
				continue;
			}
			
			if(value instanceof String)
			{
				whereStr += keyf + "='" + value + "' and ";
			}
			else if(value instanceof Date)
			{
				whereStr += keyf + "='" + value + "' and ";
				if(ServerApp.DB_TYPE.equals("ORACLE"))
				{
					whereStr += keyf + "=" + "to_date('" + XmlUtil.getImpTimeStr(((Date)value)) + "','yyyy-mm-dd hh24:mi:ss')" + " and ";
				}
				else if(ServerApp.DB_TYPE.equals("MSSQL"))
				{
					whereStr += keyf + "='" + XmlUtil.getImpTimeStr(((Date)value)) + "' and ";
				}
			}
			else
			{
				whereStr += keyf + "=" + value + " and ";
			}
		}
		whereStr = whereStr.substring(0, whereStr.length()-4);
		String sql = "select * from " + table + " where " + whereStr;
		try
		{
			result = importDao.existSelect(sql);
		}
		catch(Exception e)
		{
			addImpResult(sql+"执行失败");
		}
		return result;
	}
	/**
	 * 获得单元格的值对应关系
	 * @param cells
	 * @param filed
	 * @return
	 */
	public CellValue getFiledValue(String sheetName, Cell[] cells, Filed filed)
	{
		CellValue cellValueObj = new CellValue();
		int colnum = filed.getColnum();
		cellValueObj.setColumn(colnum);
		
		String cellValue = null;
		if(colnum > 0 && cells.length >= colnum)
		{
			cellValue = cells[colnum-1].getContents();
			if(cellValue != null)
			{
				//去除excle中的换行
				cellValue = cellValue.replace("\n", " ").trim();
			}
			cellValueObj.setRow(cells[colnum-1].getRow());
		}
		String filedName = filed.getName();
		cellValueObj.setFiledName(filedName);
		String foreignKey = filed.getForeignKey();
		String viewField = filed.getViewField();
		String filedType = filed.getType();
		String pattern = filed.getPattern();
		String validate = filed.getValidate();
		
		String defaultValue = getDiyDefaultValue(filed.getDefaultValue());
		//设置默认值
		if(cellValue == null)
		{
			cellValue = defaultValue;
		}
		
		//如果是隐藏列直接取默认值
		if(filed.isHidden())
		{
			if(XmlUtil.DEFAULT_VALUE_CURDATE.equals(filed.getDefaultValue()))
			{
				cellValueObj.setValue(new Date());
			}
			else if(XmlUtil.DEFAULT_VALUE_CURTIME.equals(filed.getDefaultValue()))
			{
				cellValueObj.setValue(new Date());
			}
			else
			{
				cellValueObj.setValue(defaultValue);
			}
			return cellValueObj;
		}
		
		//判断是否必填
		if(filed.isRequired() && (cellValue == null || "".equals(cellValue)))
		{
			addImpResult(sheetName, cellValueObj, "为必填字段");
			return cellValueObj;
		}
		
		//如果该属性允许为空，且值为空则直接返回空值
		if(!filed.isRequired() && (cellValue == null || "".equals(cellValue)))
		{
			return cellValueObj;
		}
		
		//根据valueMap取值
		if(filed.getValueMap() != null && !filed.getValueMap().isEmpty())
		{
			cellValue = filed.getValueMap().get(cellValue);
		}
		
		if(foreignKey != null && viewField != null)
		{
			String[] foreigns = foreignKey.split("\\.");
			if(foreigns == null || foreigns.length != 2)
			{
				addImpResult("foreignKey设置错误，请检查配置文件");
			}
			else
			{
				buildValueMap(foreigns[0], foreigns[1], viewField);
				if(forKeyValueMap.get(foreigns[0]+foreigns[1]+viewField) == null)
				{
					addImpResult("foreignKey配置的表或字段不存在，请检查配置文件");
				}
				else
				{
					String dbValue = forKeyValueMap.get(foreigns[0]+foreigns[1]+viewField).get(cellValue);
					if(dbValue == null)
					{
						addImpResult(sheetName, cellValueObj, "所填数据在系统（" + foreigns[0] + "）中不存在！");
					}
					cellValueObj.setValue(dbValue);
				}
			}
		}
		else
		{
			Object value = null;
			if(cellValue != null)
			{
				try
				{
					if(filedType == null){filedType="text";}
					if("text".equals(filedType))
					{
						value = String.valueOf(cellValue);
					}
					else if("integer".equals(filedType))
					{
						value = Integer.parseInt(cellValue);
					}
					else if("float".equals(filedType))
					{
						value = Float.parseFloat(cellValue);
					}
					else if("double".equals(filedType))
					{
						value = Double.parseDouble(cellValue);
					}
					else if("date".equals(filedType))
					{
						value = XmlUtil.getImpDate(cellValue, pattern);
					}
					else if("time".equals(filedType))
					{
						value = XmlUtil.getImpTime(cellValue, pattern);
					}
					else
					{
						addImpResult(sheetName, cellValueObj, "数据类型配置错误");
					}
					
					if(validate != null)
					{
						//正则表达式校验值
						Pattern pa = Pattern.compile(validate);
						Matcher matcher = pa.matcher(cellValue);
						boolean b= matcher.matches();
						if(!b)
						{
							addImpResult(sheetName, cellValueObj, "数据格式校验不通过："+validate);
						}
					}
				}
				catch(Exception e)
				{
					e.printStackTrace();
					addImpResult(sheetName, cellValueObj, "数据类型或格式错误");
				}
			}
			
			cellValueObj.setValue(value);
		}
		return cellValueObj;
	}
	
	
	public void addImpResult(String msg)
	{
		addImpResult(null, null, msg);
	}
	
	/**
	 * 添加提示信息，能定位到那个单元格数据
	 * @param cell
	 * @param msg
	 */
	public void addImpResult(String sheetName, CellValue cell, String msg)
	{
		if(sheetName != null)
		{
			impResult.append("[");
			impResult.append(sheetName);
			impResult.append("]");
		}
		if(cell != null)
		{
			impResult.append("第");
			impResult.append(cell.getRow()+1);
			impResult.append("行");
			impResult.append(cell.getColumn());
			impResult.append("列，");
			if(cell.getValue() != null && !"".equals(cell.getValue()))
			{
				impResult.append("值：");
				impResult.append(cell.getValue());
				impResult.append("，");
			}
			impResult.append("错误信息：");
			impResult.append(msg);
			impResult.append("<br>");
		}
		else
		{
			impResult.append("错误信息：");
			impResult.append(msg);
			impResult.append("<br>");
		}
	}
	
	/**
	 * 获得公用的默认值
	 * 子类可以重写该方法加入自己的默认值
	 * @param keyStr
	 * @return
	 */
	public String getDiyDefaultValue(String keyStr)
	{
		if(XmlUtil.DEFAULT_VALUE_USERID.equals(keyStr))
		{
			return this.defaultValue.get(XmlUtil.DEFAULT_VALUE_USERID);
		}
		else if(XmlUtil.DEFAULT_VALUE_USERNAME.equals(keyStr))
		{
			return this.defaultValue.get(XmlUtil.DEFAULT_VALUE_USERNAME);
		}
		else if(XmlUtil.DEFAULT_VALUE_CURDATE.equals(keyStr))
		{
			SimpleDateFormat format = new SimpleDateFormat(XmlUtil.IMP_DATA_TYPE);
			return format.format(new Date());
		}
		else if(XmlUtil.DEFAULT_VALUE_CURTIME.equals(keyStr))
		{
			SimpleDateFormat fmt = new SimpleDateFormat(XmlUtil.IMP_TIME_TYPE);
			return fmt.format(new Date());
		}
		else if(XmlUtil.DEFAULT_VALUE_GUID.equals(keyStr))
		{
			return Tools.getTimeMillisSequence();
		}
			
		return keyStr;
	}
	
	/**
	 * 创建键值对
	 * @param tableKey 表名
	 * @param forKey 外键名
	 * @param disKey 显示名称
	 */
	public void buildValueMap(String tableKey, String forKey, String disKey)
	{
		if(!forKeyValueMap.containsKey(tableKey + forKey + disKey))
		{
			Map<String, String> valeKey = null;
			try
			{
				valeKey = importDao.getValueKey(tableKey, forKey, disKey);
			}
			catch(Exception e)
			{
				addImpResult("importDao.getValueKey("+ tableKey + ","+ forKey + "," + disKey + ")"+ "执行失败");
			}
			forKeyValueMap.put(tableKey + forKey + disKey, valeKey);
		}
	}
}
