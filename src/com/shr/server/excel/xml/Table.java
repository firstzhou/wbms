package com.shr.server.excel.xml;

import java.util.List;

public class Table 
{
	String name ;
	
	int beginRow;
	
	int endRow;
	
	String impType;
	
	String keyFiled;
	
	String whereClause;
	
	String sheetName;
	
	List<Filed> fileds;
	
	String xlsRepeatFiled;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBeginRow() {
		return beginRow;
	}

	public void setBeginRow(int beginRow) {
		this.beginRow = beginRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public List<Filed> getFileds() {
		return fileds;
	}

	public void setFileds(List<Filed> fileds) {
		this.fileds = fileds;
	}

	public String getImpType() {
		return impType;
	}

	public void setImpType(String impType) {
		this.impType = impType;
	}

	public String getKeyFiled() {
		return keyFiled;
	}

	public void setKeyFiled(String keyFiled) {
		this.keyFiled = keyFiled;
	}

	public String getXlsRepeatFiled() {
		return xlsRepeatFiled;
	}

	public void setXlsRepeatFiled(String xlsRepeatFiled) {
		this.xlsRepeatFiled = xlsRepeatFiled;
	}

	public String getWhereClause() {
		return whereClause;
	}

	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}
	
	
}
