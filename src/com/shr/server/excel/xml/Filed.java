package com.shr.server.excel.xml;

import java.util.Map;

public class Filed 
{
	//字段名称
	String name ;
	
	//第几列
	int colnum;
	
	//数据类型
	String type ;
	
	boolean required;
	
	//校验表达式
	String validate;
	
	//关联键
	String foreignKey;
	
	//显示的值
	String viewField;
	
	//默认值
	String defaultValue;
	
	//是否需要在excel上填写
	boolean hidden;
	
	//是否是主键
	boolean primaryKey;
	
	//日期转换格式
	String pattern;

	//键值对
	Map<String, String> valueMap;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getColnum() {
		return colnum;
	}

	public void setColnum(int colnum) {
		this.colnum = colnum;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValidate() {
		return validate;
	}

	public void setValidate(String validate) {
		this.validate = validate;
	}

	public String getForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(String foreignKey) {
		this.foreignKey = foreignKey;
	}

	public String getViewField() {
		return viewField;
	}

	public void setViewField(String viewField) {
		this.viewField = viewField;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public String getDefaultValue() {
		return defaultValue;
	}

	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public Map<String, String> getValueMap() {
		return valueMap;
	}

	public void setValueMap(Map<String, String> valueMap) {
		this.valueMap = valueMap;
	}

	public boolean isPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(boolean primaryKey) {
		this.primaryKey = primaryKey;
	}

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

}
