package com.shr.server.excel.xml;

import java.util.List;

public class Mapping 
{
	//关键字
	String id;
	
	//代理类
	String proxy;
	
	//涉及到的表
	List<Table> tables;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProxy() {
		return proxy;
	}

	public void setProxy(String proxy) {
		this.proxy = proxy;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}
	
	
}
