/*
 * Huawei Software Technologies Co., Ltd. Copyright 1998-2009, All rights reserved.
 * 文件名  :XmlUtil.java
 * 创建人  :Administrator
 * 创建时间:2013-5-16
 */

package com.shr.server.excel.xml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * [简要描述]:xml文件操作工具<br/>
 * [详细描述]:<br/>
 * 
 * @author Administrator
 * @version 1.0, 2013-5-16
 * @since IPTV CMS V100R003C02B620
 */
public class XmlUtil
{
	
	public static String IMP_TYPE_REPLACE = "replace";
	public static String IMP_TYPE_ERROR = "error";
	public static String IMP_TYPE_UPDATE = "update";
	
	public static String KEY_FILED_SPLIT = ";";
	
	public static String DEFAULT_VALUE_USERID = "$userid";
	public static String DEFAULT_VALUE_USERNAME = "$username";
	public static String DEFAULT_VALUE_CURDATE = "$curdate";
	public static String DEFAULT_VALUE_CURTIME = "$curtime";
	public static String DEFAULT_VALUE_GUID = "$guid";
	
	public static String IMP_DATA_TYPE = "yyyy-MM-dd";
	
	public static String IMP_TIME_TYPE = "yyyy-MM-dd HH:mm:ss";
	
	//存储自定义的数据，一般为前台传过来的数据
	public static Map<String, String> BUILD_CONTENTS = new HashMap<String, String>();
	
	/**
	 * 获得导入支持的日期格式
	 * 导入时格式之支持这种
	 * @param date
	 * @return
	 */
	public static String getImpDateStr(Date date)
	{
		return getImpDateStr(date, null);
	}
	
	/**
	 * 获得导入支持的日期格式
	 * 导入时格式之支持这种
	 * @param date
	 * @return
	 */
	public static String getImpDateStr(Date date, String pattern)
	{
		SimpleDateFormat format = null;
		if(null == pattern)
		{
			format = new SimpleDateFormat(IMP_DATA_TYPE);
		}
		else
		{
			format = new SimpleDateFormat(pattern);
		}
		return format.format(date);
	}
	
	/**
	 * 获得导入支持的日期格式
	 * 导入时格式之支持这种
	 * @param date
	 * @return
	 */
	public static String getImpTimeStr(Date date)
	{
		return getImpTimeStr(date, null);
	}
	
	/**
	 * 获得导入支持的日期格式
	 * 导入时格式之支持这种
	 * @param date
	 * @return
	 */
	public static String getImpTimeStr(Date date, String pattern)
	{
		SimpleDateFormat format = null;
		if(null == pattern)
		{
			format = new SimpleDateFormat(IMP_TIME_TYPE);
		}
		else
		{
			format = new SimpleDateFormat(pattern);
		}
		return format.format(date);
	}
	
	/**
	 * 字符窜转换为日期格式
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static Date getImpDate(String date) throws ParseException
	{
		return getImpDate(date, null);
	}
	
	/**
	 * 字符窜转换为日期格式
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static Date getImpDate(String date, String pattern) throws ParseException
	{
		SimpleDateFormat format = null;
		
		if(null == pattern)
		{
			format = new SimpleDateFormat(IMP_DATA_TYPE);
		}
		else
		{
			format = new SimpleDateFormat(pattern);
		}
		return format.parse(date);
	}
	
	/**
	 * 字符窜转换为日期格式
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static Date getImpTime(String date, String pattern) throws ParseException
	{
		SimpleDateFormat format = null;
		
		if(null == pattern)
		{
			format = new SimpleDateFormat(IMP_TIME_TYPE);
		}
		else
		{
			format = new SimpleDateFormat(pattern);
		}
		return format.parse(date);
	}
	
	/**
	 * 字符窜转换为日期格式
	 * @param date
	 * @return
	 * @throws ParseException 
	 */
	public static Date getImpTime(String date) throws ParseException
	{
		return getImpTime(date, null);
	}
	
    /**
     * [简要描述]:获取Document<br/>
     * [详细描述]:<br/>
     * 
     * @author Administrator
     * @return Document Document
     */
    public static Document getDocument()
    {
        SAXReader reader = new SAXReader();
        Document doc = null;
        try
        {
            doc = reader.read(Thread.currentThread().getContextClassLoader().getResource(
                    "com/shr/server/excel/xml/excel-db-mapping.xml"));
        }
        catch (DocumentException e)
        {
            e.printStackTrace();
        }

        return doc;
    }

    /**
     * [简要描述]:封装列映射数据<br/>
     * [详细描述]:<br/>
     * 
     * @author Administrator
     * @return Mapping
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Mapping> warpMappingBean() throws Exception
    {
        // 获取document
        Document document = getDocument();

        Element mappingsEle = (Element) document.selectObject("mappings");

        // mapping节点
        List<Element> mappchilds = mappingsEle.elements();
        
        Map<String, Mapping> mappings = null;

        if (mappchilds != null && !mappchilds.isEmpty())
        {
            mappings = new HashMap<String, Mapping>();

            Mapping mapping = null;
            for (Element mappEle : mappchilds)
            {
                mapping = new Mapping();
                // 获取key
                String id = mappEle.attributeValue("id");
                if(id == null || mappings.keySet().contains(id))
                {
                	throw new Exception("mapping的id为必设属性，且不能重复！");
                }
                mapping.setId(id);
                String proxy = mappEle.attributeValue("proxy");
                mapping.setProxy(proxy);

                // table节点
                List<Element> tableChilds = mappEle.elements();
                List<Table> tables = new ArrayList<Table>();
                for (Element tableEle : tableChilds)
                {
                    Table table = new Table();

                    // 表名
                    String tname = tableEle.attributeValue("name");
                    if(tname == null)
                    {
                    	throw new Exception("表名必设属性！");
                    }
                    table.setName(tname);

                    // sheet名称正则表达式
                    String sheetName = tableEle.attributeValue("sheetName");
                    table.setSheetName(sheetName);
                    
                    // 开始行
                    String tbegin = tableEle.attributeValue("beginRow");
                    if(tbegin == null)
                    {
                    	throw new Exception("表的beginRow属性必须设置！");
                    }
                    table.setBeginRow(Integer.valueOf(tbegin));

                    // 结束行
                    String tend = tableEle.attributeValue("endRow");
                    if(tend == null || "".equals(tend))
                    {
                    	tend = "2000"; //最大导入2000行
                    }
                    table.setEndRow(Integer.valueOf(tend));
                    
                    //导入类型
                    String impType = tableEle.attributeValue("impType");
                    table.setImpType(impType);
                    
                    //wher条件
                    String whereClause = tableEle.attributeValue("whereClause");
                    table.setWhereClause(whereClause);
                    
                    //标识列
                    String keyFiled = tableEle.attributeValue("keyFiled");
                    table.setKeyFiled(keyFiled);
                    
                    //设置导入文件中不允许重复的列
                    String xlsRepeat = tableEle.attributeValue("xlsRepeatFiled");
                    table.setXlsRepeatFiled(xlsRepeat);
                    
                    if("update".equals(impType) && keyFiled == null)
                    {
                    	throw new Exception("表的impType属性为update时必须设置keyFiled属性！");
                    }

                    // col节点的子节点
                    List<Element> colChild = tableEle.elements();
                    if (colChild != null && !colChild.isEmpty())
                    {
                        Filed filed = null;
                        
                        List<Filed> fileds = new ArrayList<Filed>();

                        for (Element tabEle : colChild)
                        {
                        	filed = new Filed();
                            // 表名
                            String fname = tabEle.attributeValue("name");
                            if(fname == null)
                            {
                            	throw new Exception("字段的name属性必须设置！");
                            }
                            filed.setName(fname);
                            
                            //是否不需要填写
                            String hidden = tabEle.attributeValue("hidden");
                            if(hidden == null) hidden="false";
                            filed.setHidden(Boolean.parseBoolean(hidden));
                            
                            // 第几列
                            String tdbcol = tabEle.attributeValue("colnum");
                            if(tdbcol == null && !Boolean.parseBoolean(hidden))
                            {
                            	throw new Exception("hidden字段为true时colnum属性必须设置！");
                            }
                            else
                            {
                            	if(!Boolean.parseBoolean(hidden))
                            	{
                            		filed.setColnum(Integer.valueOf(tdbcol));
                            	}
                            }

                            //字段类型
                            String fType = tabEle.attributeValue("type");
                            filed.setType(fType);
                            
                            //日期转换格式
                            String pattern = tabEle.attributeValue("pattern");
                            if(null != pattern && !"".equals(pattern))
                            {
                            	filed.setPattern(pattern);
                            }

                            //是否必填
                            String fReq = tabEle.attributeValue("required");
                            if(fReq == null)
                            {
                            	fReq = "false";
                            }
                            filed.setRequired(Boolean.valueOf(fReq));
                            
                            //校验
                            String fValidate = tabEle.attributeValue("validate");
                            filed.setValidate(fValidate);
                            
                            //外键
                            String ffk = tabEle.attributeValue("foreignKey");
                            filed.setForeignKey(ffk);
                            
                            String pk = tabEle.attributeValue("primaryKey");
                            if(pk == null)
                            {
                            	pk = "false";
                            }
                            filed.setPrimaryKey(Boolean.valueOf(pk));
                            
                            //显示列
                            String fvf = tabEle.attributeValue("viewField");
                            filed.setViewField(fvf);
                            
                            if(ffk != null && fvf == null)
                            {
                            	throw new Exception("字段设置foreignKey属性必须设置viewField属性！");
                            }
                            //默认值
                            String defaultValue = tabEle.attributeValue("defaultValue");
                            filed.setDefaultValue(defaultValue);
                            
                            //设置键值对的属性
                            List<Element> valueMap = tabEle.elements();
                            Map<String, String> viewKeyMap = null;
                            if(valueMap != null && !valueMap.isEmpty())
                            {
                            	viewKeyMap = new HashMap<String, String>();
                            	Element values = valueMap.get(0);
                            	List<Element> vals = values.elements();
                            	for(Element v : vals)
                            	{
                            		viewKeyMap.put(v.getText(), v.attributeValue("ID"));
                            	}
                            }
                            filed.setValueMap(viewKeyMap);
                            
                            fileds.add(filed);
                        }
                        table.setFileds(fileds);
                    }
                    tables.add(table);
                }
                mapping.setTables(tables);

                // 放入内存
                mappings.put(id, mapping);
            }
        }

        return mappings;
    }
    
    /**
     * 存储自定义数据
     * 即一般为前台传过来的数据
     * @param content 格式必须为XXXX=XXXX;YYYY=YYYY;
     */
    public static void buildContent(String content)
    {
    	String[] contents = content.split(";");
    	if(contents != null)
    	{
    		for(String c : contents)
    		{
    			BUILD_CONTENTS.put(c.split(":")[0], c.split(":")[1]);
    		}
    	}
    }
    
    public static void main(String[] args) throws Exception
    {
        //XmlUtil.warpMappingBean();
      System.out.println(getImpDate("2015-01-22"));
    //正则表达式校验sheet名称
		Pattern pa = Pattern.compile("^国药准字[A-Za-z0-9]+$");
		Matcher matcher = pa.matcher("国药准字A");
		boolean b= matcher.matches();
		System.out.println(b);
    }
}
