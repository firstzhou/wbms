package com.shr.server.excel.xml;

/**
 * 单元格的值类
 * @author zw
 *
 */
public class CellValue 
{
	//列明
	String filedName;
	
	//行
	int row;
	
	//列
	int column;
	
	//值
	Object value;

	public String getFiledName() {
		return filedName;
	}

	public void setFiledName(String filedName) {
		this.filedName = filedName;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = value;
	}
	
}
