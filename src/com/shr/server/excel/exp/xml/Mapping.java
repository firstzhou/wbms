package com.shr.server.excel.exp.xml;

import java.util.List;

public class Mapping 
{
	//关键字
	String id;
	
	//代理类
	String proxy;
	
	int pageSize;
	
	boolean moveRow;
	
	String protect;
	
	//涉及到的表
	List<Table> tables;
	
	String showPageNum;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getProxy() {
		return proxy;
	}

	public void setProxy(String proxy) {
		this.proxy = proxy;
	}

	public List<Table> getTables() {
		return tables;
	}

	public void setTables(List<Table> tables) {
		this.tables = tables;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public boolean isMoveRow() {
		return moveRow;
	}

	public void setMoveRow(boolean moveRow) {
		this.moveRow = moveRow;
	}

	public String getShowPageNum() {
		return showPageNum;
	}

	public void setShowPageNum(String showPageNum) {
		this.showPageNum = showPageNum;
	}

	public String getProtect() {
		return protect;
	}

	public void setProtect(String protect) {
		this.protect = protect;
	}

	
}
