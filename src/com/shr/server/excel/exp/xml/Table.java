package com.shr.server.excel.exp.xml;

import java.util.List;

public class Table 
{
	String name ;
	
	int beginRow;
	
	int endRow;
	
	String expType;
	
	List<Filed> fileds;
	
	String customSQL;
	
	int viewPage;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBeginRow() {
		return beginRow;
	}

	public void setBeginRow(int beginRow) {
		this.beginRow = beginRow;
	}

	public int getEndRow() {
		return endRow;
	}

	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}

	public List<Filed> getFileds() {
		return fileds;
	}

	public void setFileds(List<Filed> fileds) {
		this.fileds = fileds;
	}

	public String getExpType() {
		return expType;
	}

	public void setExpType(String expType) {
		this.expType = expType;
	}

	public String getCustomSQL() {
		return customSQL;
	}

	public void setCustomSQL(String customSQL) {
		this.customSQL = customSQL;
	}

	public int getViewPage() {
		return viewPage;
	}

	public void setViewPage(int viewPage) {
		this.viewPage = viewPage;
	}

	
}
