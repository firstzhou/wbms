
package com.shr.server.excel.exp.xml;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

/**
 * [简要描述]:xml文件操作工具<br/>
 * [详细描述]:<br/>
 * 
 * @author Administrator
 * @version 1.0, 2013-5-16
 * @since IPTV CMS V100R003C02B620
 */
public class XmlUtil
{
	
	public static String EXP_TYPE_SINGLE = "single";
	public static String IMP_TYPE_MORE = "more";
	
	
    /**
     * [简要描述]:获取Document<br/>
     * [详细描述]:<br/>
     * 
     * @author Administrator
     * @return Document Document
     */
    public static Document getDocument()
    {
        SAXReader reader = new SAXReader();
        Document doc = null;
        try
        {
            doc = reader.read(Thread.currentThread().getContextClassLoader().getResource(
                    "com/shr/server/excel/exp/xml/excel-exp-mapping.xml"));
        }
        catch (DocumentException e)
        {
            e.printStackTrace();
        }

        return doc;
    }

    /**
     * [简要描述]:封装列映射数据<br/>
     * [详细描述]:<br/>
     * 
     * @author Administrator
     * @return Mapping
     */
    @SuppressWarnings("unchecked")
    public static Map<String, Mapping> warpMappingBean() throws Exception
    {
        // 获取document
        Document document = getDocument();

        Element mappingsEle = (Element) document.selectObject("mappings");

        // mapping节点
        List<Element> mappchilds = mappingsEle.elements();
        
        Map<String, Mapping> mappings = null;

        if (mappchilds != null && !mappchilds.isEmpty())
        {
            mappings = new HashMap<String, Mapping>();

            Mapping mapping = null;
            for (Element mappEle : mappchilds)
            {
                mapping = new Mapping();
                // 获取key
                String id = mappEle.attributeValue("id");
                if(id == null || mappings.keySet().contains(id))
                {
                	throw new Exception("mapping的id为必设属性，且不能重复！");
                }
                mapping.setId(id);
                String proxy = mappEle.attributeValue("proxy");
                mapping.setProxy(proxy);
                
                // 每页显示的条数
                String pageSize = mappEle.attributeValue("pageSize");
                if(pageSize != null)
                {
                	mapping.setPageSize(Integer.parseInt(pageSize));
                }
                else
                {
                	//没设值pageSize则设置一个足够大的值，控制在一页显示
                	mapping.setPageSize(999999);
                }
                
                // 是否移动
                String moveRow = mappEle.attributeValue("moveRow");
                if(moveRow != null)
                {
                	mapping.setMoveRow(Boolean.getBoolean(moveRow));
                }
                else
                {
                	mapping.setMoveRow(true);
                }
                
                // 是否保护工作簿
                String protect = mappEle.attributeValue("protect");
                if(protect != null)
                {
                	mapping.setProtect(protect);
                }
                
                String showPageNum = mappEle.attributeValue("showPageNum");
                mapping.setShowPageNum(showPageNum);
                
                // table节点
                List<Element> tableChilds = mappEle.elements();
                List<Table> tables = new ArrayList<Table>();
                for (Element tableEle : tableChilds)
                {
                    Table table = new Table();

                    // 表名
                    String tname = tableEle.attributeValue("name");
                    if(tname == null)
                    {
                    	throw new Exception("表名必设属性！");
                    }
                    table.setName(tname);
                    
                    // 开始行
                    String tbegin = tableEle.attributeValue("beginRow");
                    if(tbegin == null)
                    {
                    	throw new Exception("表的beginRow属性必须设置！");
                    }
                    table.setBeginRow(Integer.valueOf(tbegin));

                    // 结束行
                    String tend = tableEle.attributeValue("endRow");
                    if(tend == null || "".equals(tend))
                    {
                    	tend = "2000"; //最大导入2000行
                    }
                    table.setEndRow(Integer.valueOf(tend));
                    
                    //导出类型
                    String expType = tableEle.attributeValue("expType");
                    table.setExpType(expType);
                    
                    //设置数据显示在那页上
                    String viewPage = tableEle.attributeValue("viewPage");
                    if(null == viewPage)
                    {
                    	table.setViewPage(0);
                    }
                    else
                    {
                    	table.setViewPage(Integer.valueOf(viewPage));
                    }
                    // col节点的子节点
                    List<Element> colChild = tableEle.elements();
                    if (colChild != null && !colChild.isEmpty())
                    {
                        Filed filed = null;
                        
                        List<Filed> fileds = new ArrayList<Filed>();

                        for (Element tabEle : colChild)
                        {
                        	if("customSQL".equals(tabEle.getName()))
                        	{
                        		table.setCustomSQL(String.valueOf(tabEle.getData()));
                        	}
                        	else
                        	{
	                        	filed = new Filed();
	                            // 表名
	                            String fname = tabEle.attributeValue("name");
	                            if(fname == null)
	                            {
	                            	throw new Exception("字段的name属性必须设置！");
	                            }
	                            filed.setName(fname);
	                            
	                            // 第几列
	                            String tdbcol = tabEle.attributeValue("colnum");
	                            filed.setColnum(Integer.valueOf(tdbcol));
	                            
	                            // 第几行
	                            String tdbrow = tabEle.attributeValue("row");
	                            if(tdbrow != null)
	                            {
	                            	filed.setRow(Integer.valueOf(tdbrow));
	                            }
	                            
	                            String type = tabEle.attributeValue("type");
	                            filed.setType(type);
	                            
	                            fileds.add(filed);
                        	}
                        	
                        }
                        table.setFileds(fileds);
                    }
                    tables.add(table);
                }
                mapping.setTables(tables);

                // 放入内存
                mappings.put(id, mapping);
            }
        }

        return mappings;
    }
    
    public static void main(String[] args) throws Exception
    {
        XmlUtil.warpMappingBean();
    }
}
