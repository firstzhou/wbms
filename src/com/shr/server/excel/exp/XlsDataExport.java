package com.shr.server.excel.exp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.WritableWorkbook;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.shr.server.dao.BaseServerDao;
import com.shr.server.excel.exp.xml.Filed;
import com.shr.server.excel.exp.xml.Mapping;
import com.shr.server.excel.exp.xml.Table;
import com.shr.server.excel.exp.xml.XmlUtil;
import com.shr.server.system.PublicServer;

public class XlsDataExport {
	
	
	public String dowPath = "DownloadFiles";
	
	public String upPath = "UploadFiles";
	
	//返回增加的sheet数
	private int sheetSize = 1;
	/**
	 * 执行导出
	 * @param params
	 * @return
	 */
	public String excutExpExcel(Mapping map, String path, String params)
	{
		
		String id = map.getId();
		
		String filePath = path+dowPath+"\\"+id+".xls";
		
		String tempFile = id+String.valueOf(new Date().getTime())+".xls";
		String newFilePath = path+upPath+"\\"+tempFile;
		
		createWorkbook(filePath, newFilePath);
		
		sheetSize = createDataSheet(sheetSize,"sheetn", newFilePath, map, params);
		
		removeSheet(newFilePath, 0);
		return tempFile;
	}
	
	public void setSheetSize(int sheetSize)
	{
		this.sheetSize = sheetSize;
	}
	
	public int getSheetSize()
	{
		return this.sheetSize;
	}
	/**
	 * 根据配置构建数据
	 * @param map
	 * @return
	 */
	public static Map<String,List<Map<String, Object>>> buildData(Mapping map, String params)
	{
		Map<String,List<Map<String, Object>>> cellValues = new HashMap<String,List<Map<String, Object>>>();
		
		for(Table table : map.getTables())
		{
			String name = table.getName();
			String customSql = table.getCustomSQL();
			customSql = customSql.replace("$whereClause", params);
			List<Filed> fileds = table.getFileds();
			List<String> fName = new ArrayList<String>();
			for(Filed f : fileds)
			{
				fName.add(f.getName());
			}
			List<Map<String, Object>> valus = BaseServerDao.getValuesBySQL(customSql, fName.toArray(new String[]{}));
			cellValues.put(name, valus);
		}
		
		return cellValues;
	}
	
	
	/**
	 * 根据模板创建文件
	 * @param mapping
	 * @param newFile
	 * @return
	 */
	public static boolean createWorkbook(String mapping, String newFile)
	{
		try
		{
			File excel = new File(mapping);
			Workbook book = Workbook.getWorkbook(excel);
			
			WritableWorkbook newWb = Workbook.createWorkbook(new File(newFile), book); 
			newWb.write();
			book.close();
			newWb.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	
	/**
	 * 删除sheet
	 * @param newFile
	 * @param index
	 */
	public static void removeSheet(String newFile, int index)
	{
		HSSFWorkbook workbook = null;
        FileInputStream workbookInput = null;
        FileOutputStream fileOut = null;
        try 
        {
            workbookInput = new FileInputStream(newFile);
            workbook = new HSSFWorkbook(workbookInput);
            try
            {
	            if(workbook.getSheetAt(index) != null)
	            {
	            	workbook.removeSheetAt(index);
	            }
            }
            catch(IllegalArgumentException e)
            {
            	//删除完了
            }
            fileOut = new FileOutputStream(newFile);
            workbook.write(fileOut);
            fileOut.flush();
        }
        catch(Exception e)
        {
        	e.printStackTrace();
        }
        finally
        {
            try
            {
                if(workbookInput != null)
                {
                    workbookInput.close();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                if(fileOut != null)
                {
                    fileOut.close();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
	}
	
	/**
	 * 从模板复制sheet到新文件
	 * @param to 复制到第几个sheet 
	 * @param sheetName shet名字
	 * @param newFile 文件路径
	 * @param map 配置文件的mapping
	 * @param params查询数据的条件
	 * @param newFile
	 * @return
	 */
	public static int createDataSheet(int to, String sheetName, String newFile, Mapping map, String params)
	{
		HSSFWorkbook workbook = null;
        FileInputStream workbookInput = null;
        FileOutputStream fileOut = null;
        try 
        {
            workbookInput = new FileInputStream(newFile);
            workbook = new HSSFWorkbook(workbookInput);
            try
            {
	            while(workbook.getSheetAt(to) != null)
	            {
	            	workbook.removeSheetAt(to);
	            }
            }
            catch(IllegalArgumentException e)
            {
            	//删除完了
            }
            boolean moveRow = map.isMoveRow();
        	int pageSize = map.getPageSize();
        	String protect = map.getProtect();
        	String showPageNum = map.getShowPageNum(); //控制显示页码
        	int pageNumRow = -1;
        	int pageNumCol = -1;
        	if(null != showPageNum && !"".equals(showPageNum))
        	{
        		try
        		{
	        		pageNumRow = Integer.parseInt(showPageNum.split(":")[0]);
	        		pageNumCol = Integer.parseInt(showPageNum.split(":")[1]);
        		}
        		catch(Exception e)
        		{
        			e.printStackTrace();
        			showPageNum = null;
        		}
        	}
	        //根据maping从数据库中获得数据
	        Map<String,List<Map<String, Object>>> cellValues = buildData(map, params);
	        String tableID = null;
	        //获取table里扩展的表
	        for(Table table : map.getTables())
			{
	        	String type = table.getExpType();
	        	if("more".equals(type))
	        	{
	        		tableID = table.getName();
	        	}
			}
	        
	        int moreSize = 1;
	        //获取more的数据行数
	        List<Map<String, Object>> dataList = cellValues.get(tableID);
	        if(dataList != null)
	        {
	        	moreSize = dataList.size();
	        }
	        if(moreSize%pageSize > 0)
	        {
	        	moreSize = (moreSize / pageSize) + 1;
	        }
	        else
	        {
	        	moreSize = (moreSize / pageSize);
	        }
	        //实现分页效果
	        for(int i=0; i<moreSize; i++)
	        {
	        	int rowCount = 0;
	        	//String sname = sheetName + "_" + i;
	        	String sname = sheetName;
	        	//创建一个新的sheet
	            HSSFSheet newSheet = createNewSheet(workbook, to++, sname, protect);
	            
		        for(Table table : map.getTables())
				{
					String type = table.getExpType();
					
					int pagen = table.getViewPage();//显示在第几页
					
					List<Filed> fileds = table.getFileds();
					List<Map<String, Object>> valueMap = cellValues.get(table.getName());
					
					//固定输出
					if("single".equals(type) 
							&& (pagen==0 || pagen==i+1 || (pagen==-1 && i==moreSize-1)))//0表示每页输出，-1表示是最后一页
					{
						Map<String, Object> mapv = valueMap.get(0);
						for(Filed f : fileds)
						{
							HSSFRow row = newSheet.getRow(f.getRow()-1+rowCount);
							if(row == null)
							{
								row = newSheet.createRow(f.getRow()-1+rowCount);
							}
							HSSFCell cell = row.getCell(f.getColnum()-1);
							if(cell == null)
							{
								cell = row.createCell(f.getColnum()-1);
							}
							String cellType = f.getType();
							if("number".equals(cellType))
							{
								try
								{
									cell.setCellValue(Double.valueOf(String.valueOf(mapv.get(f.getName()))));
								}
								catch(NumberFormatException e)
								{
									cell.setCellValue(0);
								}
							}
							else
							{
								cell.setCellValue(String.valueOf(mapv.get(f.getName())));
							}
						}
					}
					else if("more".equals(type))
					{
						int row = table.getBeginRow() + rowCount-1;

						//循环，一行一行写数据
						for(int index=i*pageSize; index < valueMap.size() && index < (i+1)*pageSize; index++)
						{
							Map<String, Object> mv = valueMap.get(index);
							if(moveRow)//如果是活动的行，则插入一行，如果是固定的行，则不插
							{
								insertRow(newSheet, row, 1);
								rowCount ++;
							}
							for(Filed f : fileds)
							{
								HSSFRow hrow = newSheet.getRow(row);
								if(hrow == null)
								{
									hrow = newSheet.createRow(row);
								}
								HSSFCell cell = hrow.getCell(f.getColnum()-1);
								if(cell == null)
								{
									cell = hrow.createCell(f.getColnum()-1);
								}
								String cellType = f.getType();
								if("number".equals(cellType))
								{
									try
									{
										cell.setCellValue(Double.valueOf(String.valueOf(mv.get(f.getName()))));
									}
									catch(NumberFormatException e)
									{
										cell.setCellValue(0);
									}
								}
								else
								{
									cell.setCellValue(String.valueOf(mv.get(f.getName())));
								}
							}
							
							row ++;
						}
					}
				}
		        
		        //设置页码
		        if(showPageNum != null)
		        {
		        	HSSFRow pageNRow = newSheet.getRow(pageNumRow-1);
		        	if(pageNRow == null)
		        	{
		        		pageNRow = newSheet.createRow(pageNumRow-1);
		        	}
		        	HSSFCell cell = pageNRow.getCell(pageNumCol-1);
		        	if(cell == null)
		        	{
		        		cell = pageNRow.createCell(pageNumCol-1);
		        	}
		        	cell.setCellValue((i+1)+"/"+moreSize);
		        }
		        
		        newSheet.setForceFormulaRecalculation(true);
	        }
            fileOut = new FileOutputStream(newFile);
            workbook.write(fileOut);
            fileOut.flush();
            return to;
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally
        {
            try
            {
                if(workbookInput != null)
                {
                    workbookInput.close();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
            try
            {
                if(fileOut != null)
                {
                    fileOut.close();
                }
            }
            catch(Exception e)
            {
                e.printStackTrace();
            }
        }
        return to;
	}
	
	/**
	 * 创建一个新的sheet
	 * @param workbook
	 * @param index
	 * @param sheetName
	 * @return
	 */
	public static HSSFSheet createNewSheet(HSSFWorkbook workbook, int index, String sheetName, String protect)
	{
		HSSFSheet newSheet = workbook.cloneSheet(0);
		if(protect != null)
		{
			newSheet.protectSheet(protect);
		}
		
        workbook.setSheetName(index, sheetName);
        
        return newSheet;
	}
	
	/**
	 * 复制一行
	 * @param sheet
	 * @param startRow 从哪行开始
	 * @param rows 复制几行
	 * @throws IOException
	 */
	public static void insertRow(HSSFSheet sheet, int startRow, int rows) throws IOException 
	{
		int starRow = startRow;
		sheet.shiftRows(starRow + 1, sheet.getLastRowNum(), rows, true, false);
		starRow = starRow - 1;
		for (int i = 0; i < rows; i++) 
		{
			HSSFRow sourceRow = null;
			HSSFRow targetRow = null;
			HSSFCell sourceCell = null;
			HSSFCell targetCell = null;
			short m;
			starRow = starRow + 1;
			sourceRow = sheet.getRow(starRow);
			targetRow = sheet.createRow(starRow + 1);
			targetRow.setHeight(sourceRow.getHeight());

			for (m = sourceRow.getFirstCellNum(); m < sourceRow
					.getLastCellNum(); m++) 
			{
				sourceCell = sourceRow.getCell(m);
				targetCell = targetRow.createCell(m);
				
				// targetCell.setEncoding(sourceCell.getEncoding());
				targetCell.setCellStyle(sourceCell.getCellStyle());
				targetCell.setCellType(sourceCell.getCellType());
				//targetCell.setCellValue(i);// 設置值
			}
		}
	}
	
	public static void main(String[] str)
	{
//		try {
//			insertRow();
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		createDataShet(1,  "E:/12.xls");
		//copyShet("E:/1.xls", "E:/12.xls");
		
		Map<String, Mapping> maping = null;
		try {
			maping = XmlUtil.warpMappingBean();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		createWorkbook("E:/1.xls", "E:/13.xls");
		
		createDataSheet(1,"22", "E:/13.xls", maping.get("Customer"),"");
	}
}
