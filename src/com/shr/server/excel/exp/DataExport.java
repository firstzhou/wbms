package com.shr.server.excel.exp;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shr.server.ServerApp;
import com.shr.server.excel.exp.xml.Mapping;
import com.shr.server.excel.exp.xml.XmlUtil;


public class DataExport extends HttpServlet 
{

	private static final long serialVersionUID = 21L;
	
	XlsDataExport expServer;
	
	public void init() throws ServletException {
		System.out.println("文件上传初始化!");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException 
	{
		ServerApp.WORK_DIR = request.getSession().getServletContext().getRealPath("");
		response.setCharacterEncoding("UTF-8");
		String expKey = request.getParameter("mappingID");
		String expParameters = request.getParameter("expParameters");
		
		String message = excutExp(expKey, expParameters);
		String download = "<a href='/UploadFiles/"+message+"'>Download the file</a>";
		PrintWriter writer = response.getWriter();
		writer.write(download);
		writer.flush();
		response.flushBuffer();
		writer.close();
		
	}
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException 
	{
		doPost(request, response);
	}
	
	/**
	 * 执行导入
	 * @param mappingKey
	 * @param filePath
	 */
	public String excutExp(String mappingKey, String params)
	{
		Map<String, Mapping> warpMapping = null;
		try
		{
			warpMapping = XmlUtil.warpMappingBean();
		}
		catch(Exception e)
		{
			return e.getMessage();
		}
		Mapping map = warpMapping.get(mappingKey);
		//代理类
		String strClass = map.getProxy();
		if(strClass != null)
		{
			Class<?> exp = null;
			try
			{
				exp = Class.forName(strClass);
				expServer = (XlsDataExport)exp.newInstance();
			}
			catch(Exception e)
			{
				return "配置的代理类有问题！";
			}
		}
		else
		{
			expServer = new XlsDataExport();
		}
		return expServer.excutExpExcel(map, ServerApp.WORK_DIR, params);
	}
}
