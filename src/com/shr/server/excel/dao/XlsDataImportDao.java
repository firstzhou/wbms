package com.shr.server.excel.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import com.shr.server.db.TAConnection;

public class XlsDataImportDao 
{
	
	/**
	 * 获得值键对
	 * @param table
	 * @param keyFiled
	 * @param viewFiled
	 * @return
	 * @throws SQLException 
	 */
	public Map<String, String> getValueKey(String table, String keyFiled, String viewFiled) throws SQLException 
	{
		Map<String, String> keyValues = new HashMap<String, String>();
		TAConnection conn = null;
		ResultSet rs = null;
		String querySQL = "select "+ keyFiled + ", " + viewFiled + " from " + table ;
		try
		{
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(querySQL);
			rs = conn.executeQuery();
			while(rs.next()) 
			{
				keyValues.put(rs.getString(viewFiled), rs.getString(keyFiled));
			}
			rs.close();
		}
		catch(SQLException e)
		{
			 e.printStackTrace();
			 throw e;
		}
		finally
		{
			if(conn != null)
			{
				conn.close();
			}
		}
		return keyValues;
	}
	
	/**
	 * 校验数据库中是否存在
	 * @param querySQL
	 * @return
	 * @throws SQLException 
	 */
	public boolean existSelect(String querySQL) throws SQLException 
	{
		boolean result = false;
		TAConnection conn = null;
		ResultSet rs = null;
		try{
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(querySQL);
			rs = conn.executeQuery();
			if(rs.next()) 
			{
				result = true;
			}
			rs.close();
		}
		catch(SQLException e)
		{
			 e.printStackTrace();
			 throw e;
		}
		finally
		{
			if(conn != null)
			{
				conn.close();
			}
		}
		return result;
	}
	
	/**
	 * 批量执行sql
	 * @param sSQLS
	 * @return
	 * @throws Exception
	 */
	public String exeSQLs(String[] sSQLS) throws Exception 
	{
		TAConnection conn = null;
		try 
		{
			conn = new TAConnection();
			conn.setAutoCommit(false);
			for (String sSQL : sSQLS) 
			{
				conn.SQL().clear();
				conn.SQL().Add(sSQL);
//				System.out.println(sSQL);
				conn.execute();
			}
			conn.commit();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			throw e;
		} 
		finally 
		{
			if(conn != null)
			{
				conn.close();
			}
		}
		return null;
	}
}
