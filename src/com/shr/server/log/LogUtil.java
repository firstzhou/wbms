package com.shr.server.log;

import org.apache.log4j.Logger;

public class LogUtil 
{
	public static String LEVEL_INFO = "info";
	
	public static String LEVEL_WARN = "warn";
	
	public static String LEVEL_ERROR = "wrror";
	
	public static String LEVEL_DEBUG = "debug";
	
	static Logger logger;
	
	public static void log(Object className, String type, String message)
	{
		logger = Logger.getLogger(className.getClass());
		
		if(LEVEL_INFO.equals(type))
		{
			logger.info(message);
		}
		else if(LEVEL_WARN.equals(type))
		{
			logger.warn(message);
		}
		else if(LEVEL_ERROR.equals(type))
		{
			logger.error(message);
		}
		else if(LEVEL_DEBUG.equals(type))
		{
			logger.debug(message);
		}
		else
		{
			logger.info(message);
		}
	}
	
}
