package com.shr.server;

import com.isomorphic.base.Config;
import com.isomorphic.base.Reflection;
import com.isomorphic.datasource.DSFileSpec;
import com.isomorphic.log.Logger;
import com.isomorphic.util.DataTools;
import com.isomorphic.util.IOUtil;
import isc.org.apache.oro.text.GlobCompiler;
import isc.org.apache.oro.text.perl.Perl5Util;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.vfs.FileContent;
import org.apache.commons.vfs.FileName;
import org.apache.commons.vfs.FileObject;
import org.apache.commons.vfs.FileSystemManager;
import org.apache.commons.vfs.FileType;
import org.apache.commons.vfs.VFS;

public class ISCFile {
	private static Logger log = new Logger(ISCFile.class.getName());
	public static final String SHUTDOWN = "; shutdown=true";
	public static ServletContext servletContext = null;
	private static String fallBackRootDir;
	String canonicalPath;
	String filename;
	URL url;
	File file;
	FileObject fileObject;
	DSFileSpec dsFileSpec;
	private static Boolean forceCloseConnection = null;
	public static final String CONTAINER_IO_PREFIX = "__USE_CONTAINER__";

	public ISCFile(URL url) {
		this.url = url;
	}

	public ISCFile(File file) {
		this.file = file;
	}

	public ISCFile(FileObject fileObject) {
		this.fileObject = fileObject;
	}

	public ISCFile(DSFileSpec dsFileSpec) {
		this.dsFileSpec = dsFileSpec;
		this.filename = dsFileSpec.toString();
	}

	public ISCFile(String filename) throws IOException {
		this.filename = filename;

		if (filename.startsWith("{CLASSPATH}")) {
			filename = StringUtils.substringAfter(filename, "{CLASSPATH}");
			this.url = ISCFile.class.getResource(canonicalizePath(filename));
		} else if (filename.startsWith("ds:/")) {
			this.dsFileSpec = new DSFileSpec(filename);
		} else if ((filename.startsWith("ram://"))
				|| (filename.startsWith("res://"))) {
			this.fileObject = VFS.getManager().resolveFile(filename);
		} else if (isContainerIOPath(filename)) {
			filename = canonicalizePath(stripContainerIOPrefix(filename));

			if (!filename.startsWith("/")) {
				filename = "/" + filename;
			}

			if (servletContext == null) {
				if (fallBackRootDir != null) {
					this.file = new File(canonicalizePath(fallBackRootDir
							+ filename));
					return;
				}

				throw new IOException(
						"Configured for containerIO, but servletContext not available!  You need to install the Init servlet");
			}

			this.url = servletContext.getResource(filename);
		} else {
			if (filename.startsWith("file:")) {
				filename = filename.substring("file:".length());
			}

			if (DataTools.isURI(filename)) {
				this.url = new URL(filename);
			} else
				this.file = new File(canonicalizePath(filename));
		}
	}

	public boolean delete() throws IOException {
		if (this.file != null)
			return this.file.delete();
		if (this.fileObject != null)
			return this.fileObject.delete();
		if (this.dsFileSpec != null) {
			return this.dsFileSpec.remove();
		}

		throw new IOException("delete() operation not supported for filename: "
				+ this.filename);
	}

	public boolean exists() throws IOException {
		if (this.file != null) {
			return DataTools.caseSensitiveFileExists(this.filename, this.file);
		}

		if (this.fileObject != null) {
			return this.fileObject.exists();
		}

		if (this.dsFileSpec != null) {
			return this.dsFileSpec.exists();
		}

		if (this.url == null) {
			return false;
		}

		InputStream is = null;
		try {
			is = getInputStream();

			if (is != null)
				return true;
		} catch (IOException ioe) {
		} finally {
			try {
				if (is != null)
					is.close();
			} catch (Exception ignored) {
			}
		}
		return false;
	}

	public String getParent() {
		if (this.fileObject != null) {
			try {
				return this.fileObject.getParent().getName().getURI();
			} catch (Exception e) {
				return null;
			}
		}
		return new File(this.filename).getParent();
	}

	public String getCanonicalPath() throws IOException {
		if (this.canonicalPath == null) {
			if (this.file != null) {
				this.canonicalPath = this.file.getCanonicalPath();
			} else if (this.url != null) {
				if ((this.filename != null)
						&& (isContainerIOPath(this.filename))) {
					return this.filename;
				}
				this.canonicalPath = this.url.toExternalForm();
			}

		}

		if (this.canonicalPath != null) {
			return this.canonicalPath;
		}

		return this.filename;
	}

	public boolean canRead() throws IOException {
		if (this.file != null)
			return this.file.canRead();
		if (this.fileObject != null)
			return this.fileObject.isReadable();
		if (this.url != null)
			return true;
		if (this.dsFileSpec != null)
			return true;
		return false;
	}

	public boolean canWrite() throws IOException {
		if (this.file != null)
			return this.file.canWrite();
		if (this.fileObject != null)
			return this.fileObject.isWriteable();
		if (this.dsFileSpec != null)
			return true;
		return false;
	}

	public boolean mkdir() throws IOException {
		if (this.file != null)
			return this.file.mkdir();
		if (this.fileObject != null) {
			try {
				this.fileObject.createFolder();
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		throw new IOException("mkdir() not supported for filename: "
				+ this.filename);
	}

	public boolean mkdirs() throws IOException {
		if (this.file != null)
			return this.file.mkdirs();
		if (this.fileObject != null) {
			try {
				this.fileObject.createFolder();
				return true;
			} catch (Exception e) {
				return false;
			}
		}
		throw new IOException("mkdirs() not supported for filename: "
				+ this.filename);
	}

	public String getPath() throws IOException {
		return getCanonicalPath();
	}

	public String getName() throws IOException {
		String name = canonicalizePath(getPath());
		int slashIndex = name.lastIndexOf("/");
		if (slashIndex != -1)
			name = name.substring(slashIndex + 1);
		return name;
	}

	public Object getNativeHandler() {
		if (this.file != null)
			return this.file;
		if (this.fileObject != null)
			return this.fileObject;
		return this.url;
	}

	public Reader getReader() throws IOException {
		return getReader(Charset.forName("UTF-8"));
		// return getReader(null);
	}

	public Reader getReader(Charset charset) throws IOException {
		if (this.dsFileSpec != null) {
			return this.dsFileSpec.getReader();
		}

		InputStream is = getInputStream();
		if (charset == null) {
			return new InputStreamReader(is);
		}
		return new InputStreamReader(is, charset);
	}

	public String getAsString() throws IOException {
		StringWriter sw = new StringWriter();
		Reader reader = getReader();
		IOUtil.copyCharacterStreams(reader, sw);
		reader.close();
		return sw.toString();
	}

	public InputStream getInputStream() throws IOException {
		if (this.file != null)
			return new FileInputStream(this.file);
		if (this.fileObject != null)
			return this.fileObject.getContent().getInputStream();
		if (this.dsFileSpec != null)
			return this.dsFileSpec.getInputStream();
		return this.url.openConnection().getInputStream();
	}

	public OutputStream getOutputStream() throws IOException {
		if (this.file != null)
			return new FileOutputStream(this.file);
		if (this.fileObject != null)
			return this.fileObject.getContent().getOutputStream();
		throw new IOException("getOutputStream not supported for filename: "
				+ this.filename);
	}

	public Writer getWriter() throws IOException {
		return new OutputStreamWriter(getOutputStream());
	}

	public boolean isURL() {
		try {
			if ((this.url != null) && (this.url.openConnection() != null))
				return true;
		} catch (IOException e) {
			return false;
		}

		return false;
	}

	public boolean isJarURL() {
		if (!isURL()) {
			return false;
		}

		InputStream inputStream = null;
		try {
			URLConnection connection;
			URL url = new URL(this.url.toString());
			connection = url.openConnection();

			inputStream = connection.getInputStream();

			if ((connection instanceof JarURLConnection))
				return true;
		} catch (IOException e) {

			return false;
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (Exception ignored) {
				}
			}
		}
		return false;
	}

	public long length() throws IOException {
		if (!exists())
			return 0L;

		if (this.file != null)
			return this.file.length();
		if (this.fileObject != null)
			return this.fileObject.getContent().getSize();
		if (this.dsFileSpec != null)
			return this.dsFileSpec.getSize();

		return this.url.openConnection().getContentLength();
	}

	public long lastModified() throws IOException {
		if (this.file != null)
			return this.file.lastModified();
		if (this.dsFileSpec != null)
			return this.dsFileSpec.getLastModified();
		if (this.fileObject != null) {
			if (exists()) {
				long lastModified = this.fileObject.getContent()
						.getLastModifiedTime();

				lastModified = (long) Math.floor(lastModified / 1000L) * 1000L;
				return lastModified;
			}
			return 0L;
		}

		if (isJarURL()) {
			Config config = Config.getGlobal();
			if ((config != null)
					&& (config.getBoolean("file.permacacheJarURLs", false)))
				return 0L;

		}

		URLConnection urlConnection = this.url.openConnection();
		long lastModified = urlConnection.getLastModified();

		if (forceCloseConnection()) {
			try {
				urlConnection.getInputStream().close();
			} catch (Exception e) {
				log.warn("Exception during file connection closure", e);
			}
		}

		return lastModified;
	}

	public boolean setLastModified(long time) {
		if (this.dsFileSpec != null) {
			return this.dsFileSpec.setLastModified(time);
		}

		return (this.file != null) && (this.file.setLastModified(time));
	}

	private boolean forceCloseConnection() {
		if (forceCloseConnection == null) {
			synchronized (ISCFile.class) {
				if (forceCloseConnection == null) {
					Boolean flag = null;
					if (Config.getGlobal() != null) {
						flag = Config.getGlobal().getBoolean(
								"file.forceCloseURLConnection");
					}
					if (flag == null) {
						Class wlVersion = null;
						try {
							wlVersion = Reflection
									.classForName("weblogic.common.internal.Version");
						} catch (ClassNotFoundException ignored) {
						}
						if (wlVersion != null) {
							forceCloseConnection = Boolean.TRUE;
						} else
							forceCloseConnection = Boolean.FALSE;
					} else {
						forceCloseConnection = flag;
					}

				}

			}

		}

		return forceCloseConnection.booleanValue();
	}

	public static boolean isContainerIOPath(String path) {
		Config config = Config.getGlobal(false);

		if (config == null) {
			return false;
		}

		List prefixes = config.getCommaSeparatedList("containerIOPrefixes");

		if (prefixes == null) {
			return false;
		}

		for (Object prefix : prefixes) {
			if (path.contains(prefix.toString())) {
				return true;
			}
		}

		return false;
	}

	public static String stripContainerIOPrefix(String path) {
		Config config = Config.getGlobal();

		if (config == null) {
			return path;
		}

		List prefixes = config.getCommaSeparatedList("containerIOPrefixes");

		if (prefixes == null) {
			return path;
		}

		for (Object prefix : prefixes) {
			int containerIOIndex = path.indexOf(prefix.toString());

			if (containerIOIndex != -1) {
				path = path.substring(containerIOIndex
						+ prefix.toString().length());
				break;
			}
		}

		return path;
	}

	public static String canonicalizePath(String path) {
		if (path == null) {
			return null;
		}

		path = path.trim();

		StringWriter sw = new StringWriter();

		int copiedFrom = 0;
		int length = path.length();
		for (int ii = 0; ii < length; ii++) {
			char currChar = path.charAt(ii);
			if ((currChar == '\\') || (currChar == '/')) {
				sw.write(path.substring(copiedFrom, ii));
				sw.write(47);
				while (ii + 1 < length) {
					char nextChar = path.charAt(ii + 1);
					if ((nextChar != '/') && (nextChar != '\\'))
						break;
					if ((ii - 1 >= 0) && (path.charAt(ii - 1) == ':'))
						sw.write(47);
					ii++;
				}

				copiedFrom = ii + 1;
			}
		}

		sw.write(path.substring(copiedFrom, length));

		path = sw.getBuffer().toString();
		length = path.length();

		if (length > 1) {
			char lastChar = path.charAt(length - 1);

			if ((lastChar == '/') || (lastChar == '\\')) {
				path = path.substring(0, length - 1);
			}

		}

		return path;
	}

	public static boolean inContainerIOMode() {
		Config config = Config.getGlobal();
		return isContainerIOPath(config.getPath("webRoot"));
	}

	public static List<String> list(String path) {
		if (servletContext != null) {
			Set files = servletContext.getResourcePaths(path);

			if (files == null) {
				return null;
			}

			return new ArrayList(files);
		}
		File f = new File(path);

		if (!f.exists()) {
			return null;
		}

		String[] files = f.list();

		if (files == null) {
			return null;
		}

		List results = new ArrayList();

		for (String file : files) {
			String fileName = canonicalizePath(path + "/" + file);
			File x = new File(fileName);

			if (x.isDirectory())
				results.add(fileName + "/");
			else {
				results.add(fileName);
			}
		}
		return results;
	}

	public static List<String> list(String path, String regex) {
		Set<String> files = servletContext.getResourcePaths(path);

		if (files == null) {
			return null;
		}

		if ((!regex.startsWith("/")) || (!regex.endsWith("/"))) {
			regex = "/" + regex + "/";
		}

		Perl5Util perl5 = new Perl5Util();
		List<String> matchingFiles = new ArrayList();

		for (String file : files) {
			if (perl5.match(regex, file)) {
				matchingFiles.add(file);
			}
		}

		return matchingFiles;
	}

	public static List<String> find(String basePath, boolean recurse,
			String glob) {
		String regex = GlobCompiler.globToPerl5(glob.toCharArray(), 0);

		regex = "^" + regex + "(\\/)?$";

		return ffind(basePath, recurse, regex);
	}

	public static List<String> ffind(String basePath, boolean recurse,
			String regex) {
		if (basePath == null) {
			return null;
		}

		List<String> filesAtBasePath = list(basePath);

		if (filesAtBasePath == null) {
			return null;
		}

		if ((!regex.startsWith("/")) || (!regex.endsWith("/"))) {
			regex = "/" + regex + "/";
		}

		List matchingFiles = new ArrayList();
		Perl5Util perl5 = new Perl5Util();

		for (String path : filesAtBasePath) {
			if ((recurse) && (isDirectory(path))) {
				List matchingFilesInDir = ffind(path, true, regex);

				if (matchingFilesInDir != null) {
					matchingFiles.addAll(matchingFilesInDir);
				}

			}

			if (perl5.match(regex, path)) {
				matchingFiles.add(path);
			}
		}

		return matchingFiles;
	}

	public boolean isDirectory() throws IOException {
		if (this.file != null) {
			return this.file.isDirectory();
		}

		if (this.fileObject != null) {
			return this.fileObject.getType().hasChildren();
		}

		return this.filename.endsWith("/");
	}

	public static boolean isDirectory(String path) {
		return path.endsWith("/");
	}

	public static void setFallBackRootDir(String fallBackRootDir) {
		fallBackRootDir = fallBackRootDir;
	}

	public static boolean isUnresolvedContainer(String path) {
		return path.contains("__USE_CONTAINER__");
	}

	public static String replaceUnresolvedContainer(String path) {
		if (fallBackRootDir == null) {
			return path;
		}

		return path.replaceFirst("__USE_CONTAINER__", fallBackRootDir);
	}

	public static File getCanonicalFile(String filename) throws IOException {
		return new ISCFile(filename).file;
	}
}