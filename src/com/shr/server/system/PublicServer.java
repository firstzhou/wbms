package com.shr.server.system;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shr.server.ServerApp;
import com.shr.server.dao.BaseServerDao;
import com.shr.server.db.TAConnection;
import com.shr.shared.MD5;

public class PublicServer 
{

	public boolean setUsersPassword(String sPassword, HttpServletRequest servletRequest) throws Exception 
	{
		String sUser = servletRequest.getSession().getAttribute("usrid").toString();
		TAConnection conn = null;
		try 
		{
			conn = new TAConnection();
			conn.SQL().clear();
			MD5 md5 = new MD5();
			conn.SQL().Add(	"update SYS_USERS SET USRPASSWORD='" + md5.getMD5ofStr(sPassword) + "' WHERE usrid='"
							+ sUser + "'");
			conn.execute();
			return true;
		} 
		catch (Exception e) 
		{
			throw new Exception("密码修改失败！");

		} 
		finally 
		{
			conn.close();
		}
	}
	
	
	/**
	 * 判断系统是否过期
	 * @return
	 * @throws Exception
	 */
	public static boolean isReginst() throws Exception
	{
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		
		String sql = "select * from sys_reginst where out_date <= '"+format.format(new Date())+"'";
		if(BaseServerDao.isExit(sql))
		{
			sql = "update sys_reginst set out_date='19010101'";
			BaseServerDao.exeSQL(sql);
			return true;
		}
		else
		{
			return false;
		}
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public LinkedHashMap getCurrentUser(HttpServletRequest servletRequest) 
	{
		LinkedHashMap map = new LinkedHashMap();
		String userId = String.valueOf(servletRequest.getSession()
				.getAttribute("usrid"));
		String usrName = String.valueOf(servletRequest.getSession()
				.getAttribute("usrname"));
		String userRole = String.valueOf(servletRequest.getSession()
				.getAttribute("userrole"));
		String curUserDepartName = String.valueOf(servletRequest.getSession()
				.getAttribute("BM_MC"));
		String curUserDepartID = String.valueOf(servletRequest.getSession()
				.getAttribute("USRDEPTNAME"));
		String curUserDataRight = String.valueOf(servletRequest.getSession()
				.getAttribute("curUserDataRight"));
		String curUserRightContent = String.valueOf(servletRequest.getSession()
				.getAttribute("curUserRightContent"));
		//设置工作目录
		ServerApp.WORK_DIR = servletRequest.getSession().getServletContext().getRealPath("");
		Map<String, String> userRight = null;
		try 
		{
			userRight = getUserRight(userId);
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		map.put("userId", userId);
		map.put("userName", usrName);
		map.put("userBM", curUserDepartID);
		map.put("userRole", userRole);
		map.put("curUserDepartName", curUserDepartName);
		map.put("userRight", userRight);
		map.put("zhangtao", ServerApp.ZHANG_TAO);
		map.put("version", ServerApp.getVersion());
		map.put("curUserDataRight", curUserDataRight);
		map.put("curUserRightContent", curUserRightContent);
		return map;
	}

	/**
	 * 获得用户的权限
	 * @param userid
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> getUserRight(String userid) throws Exception {
		TAConnection conn = null;
		String sSQL = "";
		ResultSet rs = null;
		Map<String, String> userRight = new HashMap<String, String>();
		try {
			conn = new TAConnection();
			if ("MSSQL".equals(conn.getDbType())) {
				sSQL = " select ORMDID, " + " 'butNew=' + isnull(ORCANINSERT,'N') + "
						+ " ';butEdit=' + isnull(ORCANEDIT,'N') + "
						+ " ';butDel=' + isnull(ORCANDELETE,'N') + "
						+ " ';butExport=' + isnull(ORCANEXP,'N') + "
						+ " ';butImport=' + isnull(ORCANIMP,'N') + "
						+ " ';' + isnull(ORUSEROPTION,'') " + " USRRIGHT "
						+ " from SYS_OPRIGHT where ORID='" + userid + "'";
			} else {
				sSQL = " select ORMDID, " + " 'butNew=' || ORCANINSERT || "
						+ " ';butEdit=' || ORCANEDIT || "
						+ " ';butDel=' || ORCANDELETE || "
						+ " ';butExport=' || ORCANEXP || "
						+ " ';butImport=' || ORCANIMP || "
						+ " ';' || ORUSEROPTION " + " USRRIGHT "
						+ " from SYS_OPRIGHT where ORID='" + userid + "'";
			}
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			while (rs.next()) {
				userRight.put(rs.getString("ORMDID"), rs.getString("USRRIGHT"));
			}
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally {
			if (rs != null) {
				rs.close();
			}
			conn.close();
		}
		return userRight;
	}
	
	/**
	 * 根据用户获得用户的菜单权限
	 * @param userid
	 * @return
	 * @throws Exception 
	 */
	public Map<String, List<Map<String, String>>> getUserMenu(String userid) throws Exception
	{
		TAConnection conn = null;
		String sSQL = "";
		ResultSet rs = null;
		Map<String, List<Map<String, String>>> userRightConver = new LinkedHashMap<String, List<Map<String, String>>>();
		Map<String, List<Map<String, String>>> userRight = new LinkedHashMap<String, List<Map<String, String>>>();
		List<Map<String, String>> cmenu = null;
		Map<String, String> fatherMap = new LinkedHashMap<String, String>();
		try 
		{
			conn = new TAConnection();

			sSQL = "  select AMTEMPLET, AMPARAM, ORMDID, AMNAME, AMTYPE, OMFATHERID";
			sSQL += " FROM SYS_OPRIGHT, SYS_ACTIVEMODULE query_user_right,SYS_ORGMODEL,SYS_USERS";
			sSQL += " where 1=1";
			sSQL += " and query_user_right.AMID = ORMDID";
			sSQL += " and ((ORID=USRROLEID and USRRIGHT='N')";
			sSQL += " or (ORID = USRID and  ORUSERORROLE='U' and USRRIGHT='Y')";
			sSQL += " or (ORID=USRID and isNull(USRROLEID,' ')=' ' and ORUSERORROLE='U' and USRRIGHT<>'Y'))";
			sSQL += " and isnull(amholded,'N') <> 'Y'";
			sSQL += " and isNull(ORADJUST,' ') <> '-'		";		 
			sSQL += " and OMCHILDID = ORMDID ";
			sSQL += " and USRID = '"+userid+"'";
			sSQL += " and OMFATHERID is not null";
			sSQL += " and (AMTYPE='Y')";
			sSQL += " order by case when AMTEMPLET='vf' then 0 else 1 end, AMORDER";
			
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			while (rs.next()) 
			{
				String mType = rs.getString("AMTEMPLET");
				String mName = rs.getString("AMNAME");
				String mCode = rs.getString("ORMDID");
				String fName = rs.getString("OMFATHERID");
				if("vf".equals(mType))
				{
					fatherMap.put(mCode, mName);
					if(userRight.get(mCode) == null)
					{
						cmenu = new ArrayList<Map<String, String>>();
						userRight.put(mCode, cmenu);
					}
				}
				else
				{
					cmenu = userRight.get(fName);
					if(cmenu == null)
					{
						continue;
					}
					Map<String, String> cmap = new LinkedHashMap<String, String>();
					cmap.put("AMTEMPLET", rs.getString("AMTEMPLET"));
					cmap.put("AMPARAM", rs.getString("AMPARAM"));
					cmap.put("ORMDID", rs.getString("ORMDID"));
					cmap.put("AMNAME", rs.getString("AMNAME"));
					cmap.put("AMTYPE", rs.getString("AMTYPE"));
					cmap.put("OMFATHERID", rs.getString("OMFATHERID"));
					cmenu.add(cmap);
				}
			}
			
			//将父节点的编码转换为名称
			for(String fcode : userRight.keySet())
			{
				userRightConver.put(fatherMap.get(fcode), userRight.get(fcode));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally {
			if (rs != null) {
				rs.close();
			}
			conn.close();
		}
		return userRightConver;
	}

	public Double getMenuId(HttpServletRequest servletRequest) throws Exception {
		TAConnection conn = null;
		String sSQL = "";
		ResultSet rs = null;
		Double MenuId = (double) 0;
		try {
			conn = new TAConnection();
			sSQL = "select max(AMID)+1 from SYS_ACTIVEMODULE where AMID>9000000";
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				MenuId = rs.getDouble(1);
			} else {
				MenuId = (double) 9000001;
			}

		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally {
			if (rs != null) {
				rs.close();
			}
			conn.close();
		}
		return MenuId;

	}

	static public String getMaxId(String tableName, String idName,
			TAConnection connection) 
	{
		TAConnection conn = null;
		String sSQL = "select max(" + idName + ") from " + tableName
				+ " where " + idName + " like '0%'";
		ResultSet rs = null;
		int maxValue = 0;
		try {
			if (null != connection) {
				conn = connection;
			} else {
				conn = new TAConnection();
			}
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				maxValue = rs.getInt(1) + 1;
			} else {
				maxValue = 1;
			}
			rs.close();
			conn.close();
			return (String.format("%1$08d", maxValue));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	static public String getGG() {
		TAConnection conn = null;
		String sSQL = "select top 1 REPLACE(GGMEMO, CHAR(10), '<br>') from SYS_GG order by GG_WHSJ desc";
		ResultSet rs = null;
		String maxValue = null;
		try {
			
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				maxValue = rs.getString(1);
			}
			rs.close();
			conn.close();
			return maxValue;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * 查询预警信息
	 * @return
	 */
	static public String getYJ() {
		
		TAConnection conn = null;
		String sSQL = "select datediff( dd,XM_HTJS ,getdate()) as dateNum,XM_HTJS,XM_BM,XM_MC from BEIYE_XM where  XM_HTJS is  not  null and XM_TY ='0' and XM_HTJS < dateadd(dd,30,XM_HTJS)";
		ResultSet rs = null;
		try {
			
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			rs = conn.executeQuery();
			
			StringBuffer sb = new StringBuffer();
			StringBuffer sb2 = new StringBuffer();
			while (rs.next()) {
				
				String dateNum = rs.getString("dateNum");
				String XM_BM = rs.getString("XM_BM");
				String XM_MC = rs.getString("XM_MC");
				
				if(Integer.parseInt(dateNum) > 0) {
					sb.append("<font color=\"red\">项目 "+XM_BM+" "+XM_MC+" 已到期；</font>");
				}else if(Integer.parseInt(dateNum) <= 0 && Integer.parseInt(dateNum) >= -30){
					sb2.append("<font color=\"#FF8000\">项目 "+XM_BM+" "+XM_MC+" 还有"+(0-Integer.parseInt(dateNum))+"天到期</font>；");
				}
			}
			
			rs.close();
			conn.close();
			return "即将到期<br/>"+sb2.toString()+"<br/>　已到期<br/>"+sb.toString();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "";
	}
	
	
	public void logout(HttpServletRequest servletRequest,
			HttpServletResponse servletResponse) throws IOException {
		servletRequest.removeAttribute("usrid");
	}

	
	static public String getMaxValueBySQL(String querySQL, String columnName,
			TAConnection connection) {
		String configSql = "select CONFIG_KEY from SYS_CONFIG where CONFIG_KEY='AUTO_GET_CODE' and CONFIG_VALUE=0";
		
		try
		{
			//如果配置不自动生成编码则返回NOT_AUTO
			if(BaseServerDao.isExit(configSql))
			{
				return "NOT_AUTO";
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		TAConnection conn = null;
		ResultSet rs = null;
		try {
			if (null != connection) {
				conn = connection;
			} else {
				conn = new TAConnection();
			}
			conn.SQL().clear();
			conn.SQL().Add(querySQL);
			rs = conn.executeQuery();
			if (rs.next()) {
				return rs.getString(columnName);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "";
	}
	
	/**
	 * 获得系统配置
	 * @param configKey
	 * @return
	 */
	public static String getSystemConfig(String configKey)
	{
		String sql = "select CONFIG_VALUE from SYS_CONFIG where CONFIG_STATE=0 and CONFIG_KEY='"+configKey+"'";
		
		String value = BaseServerDao.getValueBySQL(sql, "CONFIG_VALUE", null);
		
		return value;
	}
	
	@Deprecated
	public static void exeSQL(String sSQL)
			throws Exception {
		TAConnection conn = null;
		try {
			conn = new TAConnection();
			conn.SQL().clear();
			conn.SQL().Add(sSQL);
			conn.execute();

		} catch (Exception e) {
			// TODO: handle exception
			throw new Exception(sSQL + "执行失败！");

		} finally {
			conn.close();
		}

	}
	
	@Deprecated
	public static String exeSQLs(String[] sSQLS)
			throws Exception {
		return exePSQL(sSQLS, null);
	}
	
	@Deprecated
	public static String exePSQL(String[] sSQLS, String connName)
			throws Exception {
		TAConnection conn = null;
		try {
			if(connName == null)
			{
				conn = new TAConnection();
			}
			else
			{
				conn = new TAConnection(connName);
			}
			conn.setAutoCommit(false);
			for (String sSQL : sSQLS) {
				if(sSQL == null){continue;}
				conn.SQL().clear();
				conn.SQL().Add(sSQL);
				conn.execute();
			}
			conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			conn.rollback();
			throw e;
		} finally {
			conn.close();
			return ("设置成功");

		}

	}
}
