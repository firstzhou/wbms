package com.shr.server.system.task;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TaskConfigUtil 
{
	private Properties prop = null;

    public TaskConfigUtil()
    {
        prop = new Properties();
        
        InputStream in = TaskConfigUtil.class.getResourceAsStream("/task_config.properties");
        try
        {
            prop.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
    }
    
    public String getValue(String key)
    {
        return prop.getProperty(key);
    }
}
