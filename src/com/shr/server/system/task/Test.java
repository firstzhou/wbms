package com.shr.server.system.task;
import java.util.HashMap;
import java.util.Map;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;


public class Test {
     public static void main(String[] args) {
		String jobname = "F:\\apache-tomcat-7.0.41\\bin\\kettles\\ZBWCQK.kjb";
		//String jobname = "F:\\workspace_new\\think_xinxiongdi\\war\\kettles\\JOB_数据汇总和维度表.kjb";
		
//		String[] params = new String[2];
//		params[0]="'1002H3100000000000VV'";
//		params[1]="'2017-12-04'";
//		
//	    runJob(params,jobname);
		Map<String, String> params = new HashMap<String, String>();
		params.put("ND", "2018");
		params.put("YF", "10");
		try {
			//KettleRun.runTransfer(null, jobname);
			KettleRun.runJob(null, jobname);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	 
	}	
	 /* java 调用 kettle 的job
		 * 
		 * @param jobname
		 *            如： String fName= "D:\\kettle\\informix_to_am_4.ktr";
		 */
		public static void runJob(String[] params, String jobPath) {
			try {
				KettleEnvironment.init();
				// jobname 是Job脚本的路径及名称
				JobMeta jobMeta = new JobMeta(jobPath, null);
				Job job = new Job(null, jobMeta);
				// 向Job 脚本传递参数，脚本中获取参数值：${参数名}
				// job.setVariable(paraname, paravalue);
				job.start();
				job.waitUntilFinished();
				if (job.getErrors() > 0) {
					throw new Exception(
							"There are errors during job exception!(执行job发生异常)");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
}