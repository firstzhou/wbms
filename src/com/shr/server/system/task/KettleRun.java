package com.shr.server.system.task;

import java.util.HashMap;
import java.util.Map;

import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.pentaho.di.trans.Trans;
import org.pentaho.di.trans.TransMeta;

public class KettleRun 
{
	/**  
     * 运行转换文件方法 
     * @param params 多个参数变量值 
     * @param ktrPath 转换文件的路径，后缀ktr
	 * @throws Exception 
     */  
    public static void runTransfer(String[] params, String ktrPath) throws Exception 
    {  
        Trans trans = null;  
        // // 初始化  
        // 转换元对象  
        KettleEnvironment.init();// 初始化  
        EnvUtil.environmentInit();  
        TransMeta transMeta = new TransMeta(ktrPath);  
        // 转换  
        trans = new Trans(transMeta);  
          
        // 执行转换  
        trans.execute(params);  
        // 等待转换执行结束  
        trans.waitUntilFinished();  
        // 抛出异常  
        if (trans.getErrors() > 0) 
        {  
            throw new Exception("There are errors during transformation exception!(传输过程中发生异常)");  
        }  
    }  
  
    /** 
     * java 调用 kettle 的job 
     *  
     * @param jobname 
     *            如： String fName= "D:\\kettle\\informix_to_am_4.ktr"; 
     * @throws Exception 
     */  
    public static void runJob(Map<String, String> params, String jobPath) throws Exception 
    {  
        KettleEnvironment.init();  
        // jobname 是Job脚本的路径及名称  
        JobMeta jobMeta = new JobMeta(jobPath, null);  
        Job job = new Job(null, jobMeta);  
        // 向Job 脚本传递参数，脚本中获取参数值：${参数名}  
        // job.setVariable(paraname, paravalue);  
        if(params != null)
        {
            for(String key : params.keySet())
            {
            	job.setVariable(key, params.get(key));  
            }
        }
        job.start();  
        job.waitUntilFinished();  
        if (job.getErrors() > 0) {  
            throw new Exception(  
                    "There are errors during job exception!(执行job发生异常)");  
        }  
    }  
    
    public static void main(String[] str)
    {
    	Map<String, String> params = new HashMap<String, String>();
    	params.put("CJBM", "'1002H3100000000000VV'");
    	params.put("TJRQ", "'2017-12-04'");
    	//runJob(params, "D:\\workspace\\think_dongming\\war\\kettles\\东明石化\\JOB_车间报表.kjb");
    }
}
