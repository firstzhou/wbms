package com.shr.server.system.task;

import java.util.Calendar;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import com.shr.server.log.LogUtil;

public class SystemTaskInit 
{
	static final String[] orders = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N"};
	static final String kettleDir = "kettles";
	
	public static void start()
	{
		TaskConfigUtil config = new TaskConfigUtil();
		//循环配置文件，获取所有定时的配置，从A-Z表示启动的先后顺序
		for(String order : orders)
		{
			try
			{
				//获取该任务是否有效
				String enable = config.getValue("task."+order+".enable");
				if(enable == null || "".equals(enable) || "0".equals(enable))
				{
					continue;
				}
				//定时任务类型，java|kettle
				final String type = config.getValue("task."+order+".type");
				//几点开始执行，如果为负数则不考虑几点，系统启动后10分钟执行
				String start = config.getValue("task."+order+".start");
				//执行周期
				String cycle = config.getValue("task."+order+".cycle");
				//执行kettle文件
				final String file = config.getValue("task."+order+".file");
				
				TimerTask task = new TimerTask()
				{
					@Override
					public void run() 
					{
						if("kettle".equals(type))
						{
							String path1 = kettleDir + "\\" + file;
							LogUtil.log(this, LogUtil.LEVEL_INFO, "begin kettle task " + path1);
							try {
								KettleRun.runTransfer(new String[]{}, path1);
							} catch (Exception e) {
								LogUtil.log(this, LogUtil.LEVEL_ERROR, e.getMessage());
								e.printStackTrace();
							}
							LogUtil.log(this, LogUtil.LEVEL_INFO, "end kettle task " + path1);
						}
						else if("java".equals(type))
						{
							try 
							{
								Class taskClass = Class.forName(file);
								Object task = taskClass.newInstance();
								if(task instanceof ISystemTask)
								{
									ISystemTask t = (ISystemTask)task;
									LogUtil.log(t, LogUtil.LEVEL_INFO, "begin java task " + file);
									t.run();
									LogUtil.log(t, LogUtil.LEVEL_INFO, "end java task " + file);
								}
							} 
							catch (Exception e) 
							{
								e.printStackTrace();
							}
						}
					}
				};
				Timer timer = new Timer();
				if(Integer.parseInt(start) >= 0)
				{
					int h = new Date().getHours();
					
					Calendar calendar = Calendar.getInstance();  
					if(h >= Integer.parseInt(start)) //下一天
					{
						calendar.set(Calendar.DATE, calendar.get(Calendar.DATE) + 1);
					}
			        calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(start)); // 控制时  
			        calendar.set(Calendar.MINUTE, 0);       // 控制分  
			        calendar.set(Calendar.SECOND, 0);       // 控制秒  
			        
			        Date time = calendar.getTime();           
			        timer.scheduleAtFixedRate(task, time, Long.valueOf(cycle) * 60 * 1000l);
				}
				else
				{
					timer.schedule(task, 10*60*1000l, Long.valueOf(cycle) * 60 * 1000l);
				}
			}
			catch(Exception e)
			{
				e.printStackTrace();
				continue;
			}
		}
	}
}
