package com.shr.server.system;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.shr.server.Tools;
import com.shr.server.dao.BaseServerDao;
import com.shr.server.system.task.ISystemTask;

/**
 * 自动备份数据库
 * @author samzhuwei
 *
 */
public class AutoBackDB implements ISystemTask
{
	
	@Override
	public void run() 
	{
		String[] parameString = getDir();
		String filePath = parameString[0];
		String dbName = parameString[1];
		
		doBackDB(filePath, dbName);
		doRemoveFile(filePath);
	}
	
	/**
	 * 备份数据
	 */
	private void doBackDB(String backPath, String dbName)
	{
		String uuid = Tools.getToday();
		
		String bakSql = "BACKUP DATABASE["+dbName+"] TO DISK=N'"+backPath+"\\"+dbName+uuid+".bak"+"' WITH NOFORMAT, NOINIT, NAME=N'Full Database Backup', SKIP, NOREWIND, NOUNLOAD, STATS=10";
		try
		{
			BaseServerDao.exeSQL(bakSql);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * 删除7天前备份
	 * @param filePath
	 */
	private void doRemoveFile(String filePath)
	{
		File dir = new File(filePath);
		File[] files = dir.listFiles();
		for(File file : files)
		{
			if(file.isDirectory() || !file.getName().endsWith(".bak"))
			{
				continue;
			}
	        long time = file.lastModified();  
	        long curtime = System.currentTimeMillis();
	        if (((curtime-time)/(1000*60*60*24))>10) 
	        {
	        	boolean b = file.delete();
	        }
		}
	}
	
	/**
	 * 获得备份目录
	 * @return
	 */
	private String[] getDir()
	{
		Properties prop = new Properties();
        
        InputStream in = AutoBackDB.class.getResourceAsStream("/server.properties");
        try
        {
            prop.load(in);
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
		 
		String dir = prop.getProperty("db.backup.dir");
		String dbName = prop.getProperty("db.backup.dbname");
		return new String[]{dir, dbName};
	}
	
	  public static void main(String[] args) {
		  AutoBackDB a = new AutoBackDB();
		  a.run();
	  }
}
