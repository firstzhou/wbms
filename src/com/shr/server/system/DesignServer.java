package com.shr.server.system;

import java.io.FileWriter;
import java.util.List;
import java.util.Map;

import com.shr.server.dao.BaseServerDao;

public class DesignServer 
{

	/**
	 * 创建功能
	 * @param mid
	 * @return
	 */
	public String doCreateModel(String type, String mid)
	{
		String sqlDesign = " select PK_DESIGN, DESIGN_TITLE, DESIGN_NAME, DESIGN_TBABLENAME, "
				+ "                 DESIGN_PATH, DESIGN_DESC, DESIGN_IMP "
				+ "          from SYS_DESIGN "
				+ "          where PK_DESIGN='"+mid+"'";
		List<Map<String, Object>> designList = BaseServerDao.getValuesBySQL(sqlDesign, 
				new String[]{"DESIGN_TITLE","DESIGN_NAME", "DESIGN_TBABLENAME", "DESIGN_PATH", "DESIGN_DESC", "DESIGN_IMP"});
		Map<String, Object> designMap = designList.get(0);
		
		String sqlDesignMX = " select PK_DESIGN_MX, PK_DESIGN, DESIGN_MX_COLUMN, DESIGN_MX_TYPE, DESIGN_MX_TITLE, DESIGN_MX_REQ, DESIGN_MX_VIEW, DESIGN_MX_WIDTH, DESIGN_MX_PX, DESIGN_MX_PK, DESIGN_MX_WHR, DESIGN_MX_WHRID, DESIGN_MX_WHSJ, DESIGN_MX_LENGTH "
				+ "          from SYS_DESIGN_MX "
				+ "          where PK_DESIGN='"+mid+"' order by DESIGN_MX_PX";
		List<Map<String, Object>> designMXList = BaseServerDao.getValuesBySQL(sqlDesignMX, 
				new String[]{"DESIGN_MX_COLUMN","DESIGN_MX_TYPE", "DESIGN_MX_TITLE", "DESIGN_MX_REQ", "DESIGN_MX_VIEW", "DESIGN_MX_WIDTH", "DESIGN_MX_PK", "DESIGN_MX_LENGTH"});
		
		if("TABLE".equals(type))
		{
			String createTable = doBuildTable(String.valueOf(designMap.get("DESIGN_TBABLENAME")), designMXList);
			if(createTable != null)
			{
				return createTable;
			}
		}
		
		if("DS".equals(type))
		{
			String createDataSource = doBuildDataSource(designList, designMXList);
			if(createDataSource != null)
			{
				return createDataSource;
			}
		}
		
		if("UI".equals(type))
		{
			String createUI = doBuildUI(designList, designMXList);
			if(createUI != null)
			{
				return createUI;
			}
		}
		return null;
	}
	
	/**
	 * 创建表
	 * @param tableName
	 * @param clomns
	 * @return
	 */
	public String doBuildTable(String tableName, List<Map<String, Object>> clomns)
	{
		StringBuffer buildTable = new StringBuffer("");
		buildTable.append(" create table " + tableName);
		buildTable.append("(");
		for(Map<String, Object> column : clomns)
		{
			String columnName = String.valueOf(column.get("DESIGN_MX_COLUMN"));
			String columnType = String.valueOf(column.get("DESIGN_MX_TYPE"));
			String columnLength = String.valueOf(column.get("DESIGN_MX_LENGTH"));
			buildTable.append( columnName );
			buildTable.append( " " );
			/**
			<value ID="1">字符</value>
			<value ID="2">浮点</value>
			<value ID="3">整型</value>
			<value ID="4">日期</value>
			<value ID="5">时间</value>
			 */
			if("1".equals(columnType))
			{
				buildTable.append( "varchar" );
				buildTable.append( "(" +columnLength+")"  );
			}
			else if("2".equals(columnType))
			{
				buildTable.append( "float" );
			}
			else if("3".equals(columnType))
			{
				buildTable.append( "int" );
			}
			else if("4".equals(columnType))
			{
				buildTable.append( "datetime" );
			}
			else if("5".equals(columnType))
			{
				buildTable.append( "datetime" );
			}
			buildTable.append( " " );
			buildTable.append( "NULL," );
		}
		buildTable.deleteCharAt(buildTable.length()-1); //去掉最后逗号
		buildTable.append( ")" );
		
		try
		{
			BaseServerDao.exeSQL("drop table " + tableName);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		try
		{
			BaseServerDao.exeSQL(buildTable.toString());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}
	
	/**
	 * 创建DS文件
	 * @param tableName
	 * @param clomns
	 * @return
	 */
	public String doBuildDataSource(List<Map<String, Object>> ds, List<Map<String, Object>> clomns)
	{
		Map<String, Object> designMap = ds.get(0);
		String fielName = String.valueOf(designMap.get("DESIGN_NAME"));
		String fielPath = String.valueOf(designMap.get("DESIGN_PATH"));
		StringBuffer strDS = new StringBuffer("");
		strDS.append("<DataSource dbName=\"SQLServer\"	tableName=\""+designMap.get("DESIGN_TBABLENAME")+"\"	ID=\""+designMap.get("DESIGN_TBABLENAME")+"\"	serverType=\"sql\">");
		strDS.append("\r\n");
		strDS.append("	<fields>");
		strDS.append("\r\n");
		for(Map<String, Object> column : clomns)
		{
			strDS.append("		<field 	");
			strDS.append("name=\"");
			strDS.append(column.get("DESIGN_MX_COLUMN"));
			strDS.append("\"");
			strDS.append(" ");
			
			strDS.append("title=\"");
			strDS.append(column.get("DESIGN_MX_TITLE"));
			strDS.append("\"");
			strDS.append(" ");
			
			String columnType = String.valueOf(column.get("DESIGN_MX_TYPE"));
			if("1".equals(columnType))
			{
				strDS.append("type=\"");
				strDS.append("text");
				strDS.append("\"");
				strDS.append(" ");
				
				strDS.append("length=\"");
				strDS.append(column.get("DESIGN_MX_LENGTH"));
				strDS.append("\"");
				strDS.append(" ");
			}
			else if("2".equals(columnType))
			{
				strDS.append("type=\"");
				strDS.append("text");
				strDS.append("\"");
				strDS.append(" ");
			}
			else if("3".equals(columnType))
			{
				strDS.append("type=\"");
				strDS.append("text");
				strDS.append("\"");
				strDS.append(" ");
			}
			else if("4".equals(columnType))
			{
				strDS.append("type=\"");
				strDS.append("date");
				strDS.append("\"");
				strDS.append(" ");
			}
			else if("5".equals(columnType))
			{
				strDS.append("type=\"");
				strDS.append("datetime");
				strDS.append("\"");
				strDS.append(" ");
			}
			
			if(!"true".equals(column.get("DESIGN_MX_VIEW")))
			{
				strDS.append("hidden=\"");
				strDS.append("true");
				strDS.append("\"");
				strDS.append(" ");
			}
			
			if("true".equals(column.get("DESIGN_MX_PK")))
			{
				strDS.append("guid=\"");
				strDS.append("true");
				strDS.append("\"");
				strDS.append(" ");
				
				strDS.append("primaryKey=\"");
				strDS.append("true");
				strDS.append("\"");
				strDS.append(" ");
			}
			
			if("true".equals(column.get("DESIGN_MX_REQ")))
			{
				strDS.append("required=\"");
				strDS.append("true");
				strDS.append("\"");
				strDS.append(" ");
			}
			
			strDS.append("> </field>");
			strDS.append("\r\n");
		}
		strDS.append("	</fields>");
		strDS.append("\r\n");
		strDS.append("	<serverObject className=\"com.shr.server.BaseServerDMI\"	methodName=\"doOperation\" />");
		strDS.append("\r\n");
		strDS.append("	<operationBindings>");
		strDS.append("\r\n");
		strDS.append("		<operationBinding operationType=\"fetch\" > ");
		strDS.append("\r\n");
		strDS.append("			<whereClause><![CDATA[ $defaultWhereClause ]]> </whereClause> ");
		strDS.append("\r\n");
		strDS.append("		</operationBinding>");
		strDS.append("\r\n");
		strDS.append(" </operationBindings>");
		strDS.append("\r\n");
		strDS.append("</DataSource>");
		try
		{
			FileWriter fw = new FileWriter(fielPath+"\\" + fielName + ".ds.xml");
			fw.write(strDS.toString());
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}
	
	/**
	 * 创建UI文件
	 * @param tableName
	 * @param clomns
	 * @return
	 */
	public String doBuildUI(List<Map<String, Object>> ds, List<Map<String, Object>> clomns)
	{
		Map<String, Object> designMap = ds.get(0);
		String fielName = String.valueOf(designMap.get("DESIGN_NAME"));
		String fielPath = String.valueOf(designMap.get("DESIGN_PATH"));
		StringBuffer strList = new StringBuffer("");
		strList.append("<ListGrid dataSource=\""+designMap.get("DESIGN_TBABLENAME")+"\" autoFetchData=\"true\" ID=\"baseListGrid\" autoDraw=\"false\" width=\"100%\" height=\"100%\">");
		strList.append("\r\n");
		strList.append("	<fields>");
		strList.append("\r\n");
		for(Map<String, Object> column : clomns)
		{
			if(!"true".equals(column.get("DESIGN_MX_VIEW")))
			{
				continue;
			}
			strList.append("		<field 	");
			strList.append("name=\"");
			strList.append(column.get("DESIGN_MX_COLUMN"));
			strList.append("\"");
			strList.append(" ");
			
			strList.append("width=\"");
			strList.append(column.get("DESIGN_MX_WIDTH"));
			strList.append("\"");
			strList.append(" ");
			
			strList.append("> </field>");
			strList.append("\r\n");
		}
		strList.append("	</fields>");
		strList.append("\r\n");
		strList.append("	<leaveScrollbarGap>false</leaveScrollbarGap>");
		strList.append("\r\n");
		strList.append("	<autoFitFieldWidths>true</autoFitFieldWidths>");
		strList.append("\r\n");
		strList.append("	<showFilterEditor>true</showFilterEditor>");
		strList.append("\r\n");
		
		strList.append("</ListGrid>");
		try
		{
			FileWriter fw = new FileWriter(fielPath+"\\" + fielName + "List.ui.xml");
			fw.write(strList.toString());
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return e.getMessage();
		}
		
		StringBuffer strForm = new StringBuffer("");
		strForm.append("<DynamicForm numCols=\"2\" dataSource=\""+designMap.get("DESIGN_TBABLENAME")+"\" ID=\"baseFormEdit\" autoDraw=\"false\">");
		strForm.append("\r\n");
		strForm.append("	<fields>");
		strForm.append("\r\n");
		for(Map<String, Object> column : clomns)
		{
			String columnType = String.valueOf(column.get("DESIGN_MX_TYPE"));
			if("1".equals(columnType))
			{
				strForm.append("		<TextItem ");
				
				strForm.append("name=\"");
				strForm.append(column.get("DESIGN_MX_COLUMN"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("width=\"");
				strForm.append("300");
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append(">");
				if(!"true".equals(column.get("DESIGN_MX_VIEW")))
				{
					strForm.append("\r\n");
					strForm.append("			<visible>false</visible>");
					strForm.append("\r\n");
				}
				strForm.append("		</TextItem>");
			}
			else if("2".equals(columnType))
			{
				strForm.append("		<TextItem ");
				
				strForm.append("name=\"");
				strForm.append(column.get("DESIGN_MX_COLUMN"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("width=\"");
				strForm.append(column.get("DESIGN_MX_WIDTH"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append(">");
				if(!"true".equals(column.get("DESIGN_MX_VIEW")))
				{
					strForm.append("\r\n");
					strForm.append("			<visible>false</visible>");
					strForm.append("\r\n");
				}
				strForm.append("		</TextItem>");
			}
			else if("3".equals(columnType))
			{
				strForm.append("		<TextItem ");
				
				strForm.append("name=\"");
				strForm.append(column.get("DESIGN_MX_COLUMN"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("width=\"");
				strForm.append(column.get("DESIGN_MX_WIDTH"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append(">");
				if(!"true".equals(column.get("DESIGN_MX_VIEW")))
				{
					strForm.append("\r\n");
					strForm.append("			<visible>false</visible>");
					strForm.append("\r\n");
				}
				strForm.append("		</TextItem>");
			}
			else if("4".equals(columnType))
			{
				strForm.append("		<DateItem ");
				
				strForm.append("name=\"");
				strForm.append(column.get("DESIGN_MX_COLUMN"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("width=\"");
				strForm.append(column.get("DESIGN_MX_WIDTH"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("useTextField=\"");
				strForm.append("true");
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append(">");
				if(!"true".equals(column.get("DESIGN_MX_VIEW")))
				{
					strForm.append("\r\n");
					strForm.append("			<visible>false</visible>");
					strForm.append("\r\n");
				}
				strForm.append("		</DateItem>");
			}
			else if("5".equals(columnType))
			{
				strForm.append("		<DateTimeItem ");
				
				strForm.append("name=\"");
				strForm.append(column.get("DESIGN_MX_COLUMN"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("width=\"");
				strForm.append(column.get("DESIGN_MX_WIDTH"));
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append("useTextField=\"");
				strForm.append("true");
				strForm.append("\"");
				strForm.append(" ");
				
				strForm.append(">");
				if(!"true".equals(column.get("DESIGN_MX_VIEW")))
				{
					strForm.append("\r\n");
					strForm.append("			<visible>false</visible>");
					strForm.append("\r\n");
				}
				strForm.append("		</DateTimeItem>");
			}
			
			strForm.append("\r\n");
		}
		strForm.append("	</fields>");
		strForm.append("\r\n");
		strForm.append("	<titleWidth>120</titleWidth>");
		strForm.append("\r\n");
		strForm.append("</DynamicForm>");
		try
		{
			FileWriter fw = new FileWriter(fielPath+"\\" + fielName + "Form.ui.xml");
			fw.write(strForm.toString());
			fw.flush();
			fw.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return e.getMessage();
		}
		return null;
	}
}
