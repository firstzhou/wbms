package com.shr.server.system;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import com.isomorphic.datasource.DataSource;
import com.isomorphic.datasource.DataSourceManager;
import com.shr.server.Tools;
import com.shr.server.db.TAConnection;

public class DataImport extends HttpServlet {
	/**
	 *   
	 */
	private static final long serialVersionUID = 7440302204266787092L;
	String path = "UploadFiles";
	String uploadPath = ""; // 用于存放上传文件的目录
	File tempPath ;

	public DataImport() {
		super();
		// System.out.println("文件上传启动");
	}

	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void init() throws ServletException {
		System.out.println("文件上传初始化!");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		try {
			uploadPath = request.getRealPath(path);

			String type = request.getParameter("types");
			
			DiskFileItemFactory factory = new DiskFileItemFactory();
	        // set the size threshold, above which content will be stored on disk
	        factory.setSizeThreshold(4 * 1024 * 1024); // 1 MB
	        
			ServletFileUpload fu = new ServletFileUpload(factory);

			List<?> fileItems = fu.parseRequest(request); // 得到所有的文件：
			Iterator<?> i = fileItems.iterator();
			// 依次处理每一个文件：
			while (i.hasNext()) { 
				FileItem fi = (FileItem) i.next();
				String fileName = fi.getName();// 获得文件名，这个文件名包括路径：
				if (fileName != null) {
					String extfile = fileName.substring(fileName
							.lastIndexOf("."));
					Timestamp now = new Timestamp(
							(new java.util.Date()).getTime());
					SimpleDateFormat fmt = new SimpleDateFormat(
							"yyyyMMddHHmmssSSS");
					String pfileName = fmt.format(now).toString().trim();
					System.out.println(uploadPath + "/" + pfileName + extfile);
					fi.write(new File(uploadPath + "/" + pfileName + extfile));
					readExcel(uploadPath + "/" + pfileName + extfile);
					response.setContentType("text/html;charset=utf-8");
					response.getWriter().print(
							"{success:true,msg:'" + path + "/" + pfileName
									+ extfile + "'}");
				} else {
					response.setContentType("text/html;charset=utf-8");
					response.getWriter().print("{success:false,msg:'文件类型不符'}");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
			response.getWriter().print("{success:flase,message:'失败'}");
		}

	}

	public void readExcel(String pathname) {
		TAConnection conn = null;
		String sBMID;
		String sND;
		String sXMID;
		String sRYID;
		String sBB;
		String sYSJE;
		String sYF;
		int i = 0;
		int j;
		int iYF;
		int iND;
		DataSource ds;

		try {
			// 打开文件
			File excel = new File(pathname);
			ds = DataSourceManager.get("U_FYBX_FYYS");
			Workbook book = Workbook.getWorkbook(excel);
			// 取得第一个sheet
			Sheet[] sheets = book.getSheets();

			for (Sheet sheet : sheets) {
				// 取得行数

				sND = sheet.getRow(0)[1].getContents();
				sBMID = sheet.getRow(0)[3].getContents();
				i = 2;
				while (true) {
					if (i >= sheet.getRows()
							|| sheet.getRow(i) == null
							|| Tools.isNullOrEmpty(sheet.getRow(i)[0]
									.getContents())) {
						break;// 项目编码为空，直接退出
					}
					sXMID = sheet.getRow(i)[0].getContents();
					sRYID = sheet.getRow(i)[2].getContents();
					sBB = sheet.getRow(i)[4].getContents();
					Map row = new HashMap();

					for (j = 1; j <= 12; j++) {
						sYSJE = sheet.getRow(i)[j + 4].getContents();
						if (!Tools.isNullOrEmpty(sYSJE)) {
							if (j <= 11) {
								iYF = j + 1;
								iND = Integer.parseInt(sND);
							} else {
								iYF = 1;
								iND = Integer.parseInt(sND) + 1;
							}

							row.put("年度", iND);
							row.put("月份", iYF);
							row.put("部门编码", sBMID);
							row.put("人员编码", sRYID);
							row.put("项目编码", sXMID);
							row.put("版本", sBB);
							row.put("调整日期", new Date());
							row.put("费用预算", sYSJE);
							if (sBB.equals("调整")) {
								row.put("备注", Tools.getNow() + "调整" + ";系统导入");
							} else {
								row.put("备注", "系统导入");
							}
							ds.add(row);
						}
					}
					i++;
				}

			}

			// 关闭文件
			book.close();
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
		}
	}

}
