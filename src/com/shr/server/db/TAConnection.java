package com.shr.server.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Vector;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.isomorphic.sql.SQLConnectionManager;
import com.shr.server.log.LogUtil;
import com.shr.server.sql.SQL;
import com.shr.server.sql.SqlParameter;
import com.shr.server.sql.SqlParameters;

public class TAConnection
{
    private Connection connection;

    private PreparedStatement prepareStatement;

    private String dbType = "";

    private SQL sql;

    /**
     * [简要描述]:事务
     * @author zhangfanghai
     */
    private Statement statement;

    public SQL SQL()
    {
        return sql;
    }

    private void setParameters() throws SQLException
    {
        Vector<String> paramsMap = sql.getParamsMap();
        SqlParameters params = sql.getSqlParameters();
        SqlParameter param;

        prepareStatement.clearParameters();
        for (int i = 0; i < paramsMap.size(); i++)
        {
            param = params.getSqlParameter(paramsMap.elementAt(i));
            if (param == null)
            {
                throw new java.sql.SQLException("参数值没有指定" + paramsMap.elementAt(i));
            }
            if (param.getValue() == null)
            {
                prepareStatement.setNull(i + 1, java.sql.Types.DATE);
            }
            else
            {
                prepareStatement.setObject(i + 1, param.getValue());
            }
        }
    }

    // //////////////////////////////////////////////////////////////////
    /**
     * ִ 执行更新操作
     * 
     * @return int
     * @throws SQLException
     */

    public int executeUpdate() throws SQLException
    {
        if (prepareStatement != null)
        {
            prepareStatement.close();
        }
        prepareStatement = connection.prepareStatement(sql.getJdbcSql());
        setParameters();
        return prepareStatement.executeUpdate();
    }

    /**
     * 执行增删改等操作，不返回结果集
     * 
     * @return boolean
     * @throws SQLException
     */

    public boolean execute() throws SQLException
    {
        if (prepareStatement != null)
        {
            prepareStatement.close();
        }
        prepareStatement = connection.prepareStatement(sql.getJdbcSql());
        setParameters();
        return prepareStatement.execute();
    }

    /**
     * 执行查询条件
     * 
     * @return ResultSet
     * @throws SQLException
     */

    public ResultSet executeQueryThrowException() throws SQLException
    {
        if (prepareStatement != null)
        {
            prepareStatement.close();
        }
        String runSql = sql.getJdbcSql();
        LogUtil.log(this, LogUtil.LEVEL_INFO, runSql);
        prepareStatement = connection.prepareStatement(runSql, ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);
        setParameters();
        return prepareStatement.executeQuery();
    }

    /**
     * 执行查询
     * @return
     * @throws SQLException 
     */
    public ResultSet executeQuery() throws SQLException
    {
    	String runSql = null;
        if (prepareStatement != null)
        {
            prepareStatement.close();
        }
        runSql = sql.getJdbcSql();
        LogUtil.log(this, LogUtil.LEVEL_INFO, runSql);
        prepareStatement = connection.prepareStatement(runSql, ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);
        setParameters();
        return prepareStatement.executeQuery();
    }

    /**
     * ִ执行查询操作，且查询结果可以直接编辑
     * 
     * @return ResultSet
     * @throws SQLException 
     */

    public ResultSet executeQueryEditable() throws SQLException
    {
        if (prepareStatement != null)
        {
            prepareStatement.close();
        }
        String runSql = sql.getJdbcSql();
        prepareStatement = connection.prepareStatement(runSql, ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_UPDATABLE);
        setParameters();
        return prepareStatement.executeQuery();
    }

    public boolean close()
    {
        try
        {
            SQLConnectionManager.free(connection);
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return false;
        }

        //		if (prepareStatement != null) {
        //			try {
        //				prepareStatement.close();
        //			} catch (SQLException ex) {
        //				return false;
        //			}
        //		}
        //
        //		try {
        //			connection.close();
        //		} catch (SQLException ex) {
        //			return false;
        //		}

        return true;
    }


    /**
     * 得到 PrepareStatement
     * 
     * @return PreparedStatement
     */
    public PreparedStatement getPrepareStatement()
    {
        return this.prepareStatement;
    }

    public void setAutoCommit(boolean autoCommit) throws SQLException
    {
        this.connection.setAutoCommit(autoCommit);
    }

    public void commit() throws SQLException
    {
        this.connection.commit();
    }

    public void rollback() throws SQLException
    {
        this.connection.rollback();
    }

    /**
     * 根据DS名称构建JgtConnection对象
     * 
     * @param dbName
     *            String
     * @return JgtConnection
     */
    public TAConnection()
    {
        String url = "";
        try
        {
            this.connection = SQLConnectionManager.getConnection();

            url = this.connection.getMetaData().getURL();
            LogUtil.log(this, LogUtil.LEVEL_INFO, "数据库url:" + url);
            if (url.indexOf("sqlserver") > 0)
            {
                dbType = "MSSQL";
            }
            else if (url.indexOf("oracle") > 0)
            {
                dbType = "ORACLE";
            }
            sql = new SQL(dbType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    /**
     * 根据DS名称构建JgtConnection对象
     * 
     * @param dbName
     *            String
     * @return JgtConnection
     */
    public TAConnection(String dsName)
    {
        String url = "";
        try
        {
            this.connection = SQLConnectionManager.getConnection(dsName);
            url = this.connection.getMetaData().getURL();
            if (url.indexOf("sqlserver") > 0)
            {
                dbType = "MSSQL";
            }
            else if (url.indexOf("oracle") > 0)
            {
                dbType = "ORACLE";
            }
            sql = new SQL(dbType);
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }

    }

    private Connection getConnection(DataSource ds)
    {
        try
        {
            return ds.getConnection();
        }
        catch (SQLException e)
        {
            // TODO Auto-generated catch block
            // e.printStackTrace();
            return null;
        }
    }

    /**
     * [简要描述]:添加sql脚本<br/>
     * [详细描述]:<br/>
     * 
     * @author zhangfanghai
     * @param sql
     */
    public void addBatch(String sql)
    {
        try
        {
            if(statement == null)
            {
                statement = this.connection.createStatement();
            }
            statement.addBatch(sql);
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    /**
     * [简要描述]:批量执行<br/>
     * [详细描述]:<br/>
     * 
     * @author zhangfanghai
     */
    public void batchExe()
    {
        try
        {
            statement.executeBatch();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
        finally
        {
            try
            {
                statement.close();
            }
            catch (SQLException e)
            {
                e.printStackTrace();

            }
        }
    }

    public TAConnection(String dbName, HttpSession session)
    {
        try
        {
            if (dbName.equals("DBSYS"))
            {
                this.connection = getDataSourceByName(dbName).getConnection();
            }
            else
            {
                String sDBID = session.getAttribute("userDBID").toString().trim();
                if (sDBID != null && !sDBID.equals(""))
                {
                    this.connection = getDataSourceByName(sDBID).getConnection();
                }
                else
                {
                    this.connection = getDataSourceByName("DBERP3").getConnection();
                }
            }
        }
        catch (SQLException ex)
        {
            ex.printStackTrace();
        }
        String url = "";
        try
        {
            url = this.connection.getMetaData().getURL();
        }
        catch (SQLException ex)
        {
        }
        if (url.indexOf("sqlserver") > 0)
        {
            dbType = "MSSQL";
        }
        else if (url.indexOf("oracle") > 0)
        {
            dbType = "ORACLE";
        }
        sql = new SQL(dbType);
    }

    //	/**
    //	 * 得到默认“DBERP”的JgtConnection对象
    //	 * 
    //	 * @return JgtConnection
    //	 */
    //
    //	public TAConnection() {
    //		this("DBERP");
    //	}

    /**
     * 获取DataSource
     * 
     * @param dbName
     *            String
     * @return DataSource
     */

    private DataSource getDataSourceByName(String dbName)
    {
        Context ct = null;
        try
        {
            ct = new InitialContext();
        }
        catch (NamingException ex)
        {
        }
        DataSource datasource = null;
        try
        {
            datasource = (DataSource) ct.lookup("java:comp/env/" + dbName);
        }
        catch (NamingException ex1)
        {
        }
        return datasource;
    }
    
    public String getDbType() {
		return dbType;
	}

	public void setDbType(String dbType) {
		this.dbType = dbType;
	}
}
