package com.shr.server;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.shr.server.system.task.SystemTaskInit;

public class ServerInit extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	public void destroy()
    {
        super.destroy(); 
    }

    public void init() throws ServletException
    {
    	SystemTaskInit.start(); //启动kettle定时任务
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
    	
    }
    
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        doPost(req, resp);
    }
}
