package com.shr.client;

import java.util.HashMap;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Img;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;

public class MainTopPanel extends ToolStrip {
	/**
	 * 
	 */
	public MainTopPanel() {
		setWidth100();
		setHeight(30);
		//this.setBackgroundColor("#C0C0C0");
		final Img img = new Img();  
        img.setLeft(10);  
        img.setTop(2);  
        img.setWidth(500);  
        img.setHeight(40);
        img.setSrc("menuicon/ylogo.png");  
        addChild(img);
        
		addFill();
		final Label label = new Label();
		label.setWidth(80);  
		label.setHeight(30); 
		//label.setTop(20);  
		//label.setRight(60);
		
		DMI.call("shr", "PublicServer",
				"getCurrentUser", new RPCCallback() {
					@Override
					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {
						HashMap user = (HashMap) response.getDataAsMap();
						String userName = "<br/>" + (String) user.get("userName") + "";
						label.setContents(userName);
						label.setTitle(userName);
					}
				}, null);
		addMember(label);
		ToolStripButton btnSetting = new ToolStripButton();
		btnSetting.setIcon("menuicon/UserSetting.png");
		btnSetting.setIconSize(25);
		btnSetting.setSize("25", "25");
		btnSetting.setTooltip("修改密码");
		addButton(btnSetting);
		btnSetting.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {

				final Window winModal = new Window();
				winModal.setWidth(360);
				winModal.setHeight(115);
				winModal.setTitle("密码修改");
				winModal.setShowMinimizeButton(false);
				winModal.setIsModal(true);
				winModal.setShowModalMask(true);
				winModal.centerInPage();
				winModal.addCloseClickHandler(new CloseClickHandler() {
					public void onCloseClick(CloseClickEvent event) {
						winModal.destroy();
					}
				});
				DynamicForm form = new DynamicForm();
				form.setHeight100();
				form.setWidth100();
				form.setPadding(5);
				form.setLayoutAlign(VerticalAlignment.BOTTOM);
				final TextItem txtPassword = new TextItem("sPassword");
				IButton btnSave = new IButton("保存");

				txtPassword.setTitle("新的密码");
				form.setFields(txtPassword);
				winModal.addItem(form);
				winModal.addItem(btnSave);
				winModal.show();

				btnSave.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {

						DMI.call("shr",	"PublicServer",	"setUsersPassword", new RPCCallback() {

									public void execute(RPCResponse response,
											Object rawData, RPCRequest request) {
										SC.say("密码设置成功！");
										winModal.destroy();
									}
								}, new Object[] { txtPassword.getValue() });
					}
				});

			}
		});

		ToolStripButton btnLogout = new ToolStripButton();
		btnLogout.setTitle("");
		btnLogout.setIcon("menuicon/Logout.png");
		btnLogout.setIconSize(25);
		btnLogout.setSize("25", "25");
		btnLogout.setTooltip("注销登录");
		btnLogout.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				DMI.call("shr", "PublicServer",	"logout", new RPCCallback() {

							public void execute(RPCResponse response,
									Object rawData, RPCRequest request) {
//								redirect("../bms/index.jsp");
								redirect("../index.jsp");
							}
						}, null);
			}
		});
		addButton(btnLogout);
		
		ToolStripButton btnDesign = new ToolStripButton();
		btnDesign.setTitle("");
		btnDesign.setIcon("menuicon/Setting.png");
		btnDesign.setIconSize(25);
		btnDesign.setSize("25", "25");
		btnDesign.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				String strWindowFeatures = "menubar=yes,location=yes,resizable=yes,scrollbars=yes,status=yes";
				com.google.gwt.user.client.Window.open(GWT.getModuleBaseURL()
						+ "tools/visualBuilder/index.jsp?skin=Tahoe", "vb_blank",
						strWindowFeatures);
			}
		});
		addButton(btnDesign);

	}

	protected static native void redirect(String url)/*-{
		$wnd.location = url;
	}-*/;
}
