package com.shr.client;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.client.model.UREPORTPanel;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ShrMenu  extends Canvas
{
	private TabSet mainTabSet;
	
	HLayout main;
	
	/**
	 * 
	 * @param mainTabSet
	 */
	public ShrMenu(TabSet mainTabSet)
	{
		this.mainTabSet = mainTabSet;
		
		main = new HLayout();
		RPCManager.cacheScreens(new String[]{"ThinkMenu"}, new Function() {
			@Override
			public void execute() {
				Canvas menu = RPCManager.createScreen("ThinkMenu");
				//需要将HLayout0作为主面板，否则出现不了滚动条
				HLayout mainHL = (HLayout)menu.getByLocalId("HLayout0");
				//存放菜单的面板
				VLayout vlayout7 = (VLayout)menu.getByLocalId("VLayout7");
				Canvas canvas0 = (Canvas)menu.getByLocalId("Canvas0");
				canvas0.setContents("   此处为公告内容！");
				Canvas canvasYJ = (Canvas)menu.getByLocalId("CanvasYJ");
				canvas0.setContents("   此处为预警内容！");
				buildGG(canvas0);
				buildYJ(canvasYJ);
				buildRightMenu(vlayout7);
				main.addMember(mainHL);
				main.setWidth100();
				main.setHeight100();
			}
		});
		
		this.setWidth100();
		this.setHeight100();
		this.addChild(main);
	}
	
	/**
	 * 根据权限添加菜单
	 */
	private void buildRightMenu(final VLayout vlayout)
	{
		
		DMI.call("shr", "PublicServer",
				"getUserMenu", new RPCCallback() {
					@Override
					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {

						HashMap<String, List<Map<String, String>>> userMenu = (HashMap) response.getDataAsMap();
						
						for(String fName : userMenu.keySet())
						{
							vlayout.addMember(buildMenuCanvas(fName, userMenu.get(fName)));
						}
						main.redraw();
					}
		}, new Object[]{App.curUserId});
	}
	
	/**
	 * 根据权限添加菜单
	 */
	private void buildGG(final Canvas vlayout)
	{
		
		DMI.call("shr", "PublicServer",
				"getGG", new RPCCallback() {
					@Override
					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {

						String userMenu = (String) response.getDataAsString();
						
						vlayout.setContents("&nbsp;&nbsp;&nbsp;&nbsp;"+userMenu);
					}
		}, null);
	}
	
	private void buildYJ(final Canvas vlayout)
	{
		
		DMI.call("shr", "PublicServer",
				"getYJ", new RPCCallback() {
					@Override
					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {

						String userMenu = (String) response.getDataAsString();
						
						vlayout.setContents("&nbsp;&nbsp;&nbsp;&nbsp;"+userMenu);
					}
		}, null);
	}

	
	private Canvas buildMenuCanvas(String title, List<Map<String, String>> menu)
	{
		VLayout vl = new VLayout();
		vl.setHeight(70);
		
		Label titleLabel = new Label();
		titleLabel.setContents("<font color=\"#6E8BB0\">"+title+"</font>");
		titleLabel.setHeight(30);
		
		VLayout line = new VLayout();
		line.setHeight(1);
		line.setBackgroundColor("#6E8BB0");
		
		VLayout lineB = new VLayout();
		lineB.setHeight(15);
		
		vl.addMember(titleLabel);
		int fbl = this.getInnerWidth(); //浏览器内部宽度
		int compw = 100; //两边去掉100
		HLayout hl = null;
		for(final Map<String, String> m : menu)
		{
			if(compw == 100)
			{
				hl = new HLayout();
				hl.setHeight(60);
				vl.addMember(hl);
				
				HLayout temph = new HLayout();
				temph.setHeight(5);
				vl.addMember(temph);
			}
			
			IButton m1 = new IButton();
			m1.setWidth(120);
			m1.setTitle(m.get("AMNAME"));
			m1.setHeight(50);
			m1.setTooltip(m.get("AMNAME"));
			m1.addClickHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					openMenu(m.get("AMTEMPLET"), m.get("AMPARAM"), m.get("AMNAME"));
				}
			});
			Label l = new Label();
			l.setWidth(30);
			
			hl.addMember(m1);
			hl.addMember(l);
			
			compw += 150;
			
			if(compw+150 > fbl)
			{
				compw = 100;
			}
		}
		vl.addMember(line);
		vl.addMember(lineB);
		return vl;
	}
	
	/**
	 * 打开菜单
	 * @param type
	 * @param param
	 * @param menuName
	 * @param AMID
	 */
	private void openMenu(String type, String param, String menuName) 
	{
		//autologin();
		Tab[] tabs = mainTabSet.getTabs();
		boolean found = false;
		for (int i = 0; i < tabs.length; i++) 
		{
			if (tabs[i].getTitle().equals(menuName)) {
				found = true;
				mainTabSet.selectTab(tabs[i].getID());
			}
		}
		if (!found) 
		{
			// 如果未找到标签，新建一个

			Tab tab = new Tab();
			tab.setCanClose(true);
			tab.setTitle(menuName);

			if (type.equals("UREPORTPanel")) 
			{
				UREPORTPanel reportPanel = new UREPORTPanel(param);
				tab.setPane(reportPanel);
			} 
			else 
			{
				Canvas panel = ModuleFactor.createModule(param);
				panel.setWidth100();
				tab.setPane(panel);
			}
			mainTabSet.addTab(tab);
			mainTabSet.selectTab(tab.getID());
		}
	}
	
	/** 查询代办事宜
	 select * from 
				(
				select DQCLR, MAX(PK_BusinessFYSQ_Workflow) pk, tableName,DQ_mState
				from Workflow where 1=1
				group by SYB_mState,DQCLR,tableName,DQ_mState
				) a where DQ_mState != '2'
	 */
}
