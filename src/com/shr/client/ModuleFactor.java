package com.shr.client;

import com.shr.client.beiye.jcsj.CBDJClient;
import com.shr.client.beiye.jcsj.CKClient;
import com.shr.client.beiye.jcsj.CKXXClient;
import com.shr.client.beiye.jcsj.FYLXClient;
import com.shr.client.beiye.jcsj.GOODSClient;
import com.shr.client.beiye.jcsj.GSClient;
import com.shr.client.beiye.jcsj.GSZDClient;
import com.shr.client.beiye.jcsj.GYSClient;
import com.shr.client.beiye.jcsj.HTClient;
import com.shr.client.beiye.jcsj.HTXXClient;
import com.shr.client.beiye.jcsj.KPTTClient;
import com.shr.client.beiye.jcsj.QYClient;
import com.shr.client.beiye.jcsj.SYBClient;
import com.shr.client.beiye.jcsj.XMClient;
import com.shr.client.beiye.jcsj.XMGSClient;
import com.shr.client.bms.CBHCFClient;
import com.shr.client.bms.CBTZClient;
import com.shr.client.bms.CCFClient;
import com.shr.client.bms.CZFClient;
import com.shr.client.bms.DDCLFClient;
import com.shr.client.bms.GLFClient;
import com.shr.client.bms.JCFClient;
import com.shr.client.bms.JTPFClient;
import com.shr.client.bms.KPXXClient;
import com.shr.client.bms.KQMJClient;
import com.shr.client.bms.LJZFClient;
import com.shr.client.bms.LZFClient;
import com.shr.client.bms.QDFClient;
import com.shr.client.bms.QTFClient;
import com.shr.client.bms.SDFClient;
import com.shr.client.bms.SMFClient;
import com.shr.client.bms.SRSCClient;
import com.shr.client.bms.TBDRSJClient;
import com.shr.client.bms.TBFClient;
import com.shr.client.bms.TXFClient;
import com.shr.client.bms.XMZYClient;
import com.shr.client.bms.YCFClient;
import com.shr.client.bms.ZDCBClient;
import com.shr.client.bms.ZDSCClient;
import com.shr.client.bms.ZHXFClient;
import com.shr.client.bms.ZJFClient;
import com.shr.client.bms.ZXFClient;
import com.shr.client.bms.ZXLWFClient;
import com.shr.client.system.BMClient;
import com.shr.client.system.CGGClient;
import com.shr.client.system.DICCATEGORYClient;
import com.shr.client.system.MenuDesign;
import com.shr.client.system.RoleClient;
import com.shr.client.system.SYS_DESIGNClient;
import com.shr.client.system.SystemConfigClient;
import com.shr.client.system.USERSClient;
import com.shr.client.wbms.CKDDXX_HClient;
import com.shr.client.wbms.CKDDXX_H_EDClient;
import com.shr.client.wbms.CKSJClient;
import com.shr.client.wbms.HWXXClient;
import com.shr.client.wbms.HZSJClient;
import com.shr.client.wbms.JFGZ_HClient;
import com.shr.client.wbms.KCKZClient;
import com.shr.client.wbms.KCKZ_EDClient;
import com.shr.client.wbms.KQXXClient;
import com.shr.client.wbms.OrderStatusClient;
import com.shr.client.wbms.OrderTypeClient;
import com.shr.client.wbms.RKDDSJHClient;
import com.shr.client.wbms.RKDDSJH_EDClient;
import com.shr.client.wbms.SPDWZHClient;
import com.shr.client.wbms.StockStatusClient;
import com.shr.client.wf.WFClient;
import com.smartgwt.client.widgets.Canvas;

public class ModuleFactor
{

	/**
	 * 系统功能
	 * @param AMID
	 * @return
	 */
    public static Canvas createModule(String AMID)
    {
        if (AMID.equals("9723006")) //部门
        {
            return new BMClient();
        }
        else if (AMID.equals("9723004")) //菜单设计
        {
            return new MenuDesign();
        }
        else if (AMID.equals("9723005")) //用户管理
        {
            return new USERSClient();
        }
        else if (AMID.equals("9723007")) //数据字典
        {
            return new DICCATEGORYClient();
        }
        else if (AMID.equals("9723011")) //角色管理
        {
            return new RoleClient();
        }
  		else if(AMID.equals("1605291145")) //工作流设置
  		{
  			return new WFClient();
  		}
  		else if (AMID.equals("1603301545")) //公告
		{
			return new CGGClient();
		}
  		else if(AMID.equals("1603302310")) //系统配置
  		{
  			
  			return new SystemConfigClient();
  		}
  		else if(AMID.equals("1802201546")) //功能配置
  		{
  			
  			return new SYS_DESIGNClient();
  		}
        Canvas canvas1 = createProjectModule1(AMID);
        if(canvas1 != null)
        {
        	return canvas1;
        }
        Canvas canvas2 = createProjectModule2(AMID);
        if(canvas2 != null)
        {
        	return canvas2;
        }
        Canvas canvas3 = createProjectModule3(AMID);
        if(canvas3 != null)
        {
        	return canvas3;
        } 
        
        return null;
    }
    
    /**
     * 具体项目的功能
     * @param AMID
     * @return
     */
    public static Canvas createProjectModule1(String AMID)
    {
    	//贝业
  		if(AMID.equals("1802071536")) //事业部
  		{
  			
  			return new SYBClient();
  		}
  		else if(AMID.equals("1803011713")) //区域
  		{
  			
  			return new QYClient();
  		}
  		else if(AMID.equals("1803012047")) //项目
  		{
  			
  			return new XMClient();
  		}
  		else if(AMID.equals("1808160944")) //仓库
  		{
  			
  			return new CKClient();
  		}
  		else if(AMID.equals("1803012048")) //项目归属
  		{
  			
  			return new XMGSClient();
  		}
  		else if(AMID.equals("1807112205")) //费用类型
  		{
  			
  			return new FYLXClient();
  		}
  		else if(AMID.equals("1807231847")) //合同信息
  		{
  			
  			return new HTXXClient();
  		}
  		else if(AMID.equals("1808022128")) //仓库信息
  		{
  			
  			return new CKXXClient();
  		}
  		else if(AMID.equals("1809121557")) //供应商信息
  		{
  			
  			return new GYSClient();
  		}
  		else if(AMID.equals("1908190001")) //开票抬头档案
  		{
  			
  			return new KPTTClient();
  		}
  		else if(AMID.equals("1908190002")) //公司档案
  		{
  			
  			return new GSClient();
  		}
  		else if(AMID.equals("1908190003")) //合同档案
  		{
  			
  			return new HTClient();
  		}
  		else if(AMID.equals("2019082001")) //商品数据    
  		{
  			
  			return new GOODSClient();
  		}
  		else if(AMID.equals("2019082002")) //商品单位
  		{
  			
  			return new SPDWZHClient();
  		}
  		else if(AMID.equals("2019082003")) //订单类型
  		{
  			
  			return new OrderTypeClient();
  		}
  		else if(AMID.equals("2019082004")) //订单状态
  		{
  			
  			return new OrderStatusClient();
  		}
  		else if(AMID.equals("2019082005")) //库存状态
  		{
  			
  			return new StockStatusClient();
  		}
  		else if(AMID.equals("2019082101")) //货主数据
  		{
  			
  			return new HZSJClient();
  		}
  		else if(AMID.equals("2019082102")) //仓库数据
  		{
  			
  			return new CKSJClient();
  		}
  		else if(AMID.equals("2019082103")) //库区信息
  		{
  			
  			return new KQXXClient();
  		}
  		else if(AMID.equals("2019082104")) //库存快照
  		{
  			
  			return new 	KCKZClient();
  		}
  		else if(AMID.equals("2019082603")) //库存快照 确认表
        {
  			
  			return new 	KCKZ_EDClient();
  		}
  		else if(AMID.equals("2019082105")) //货位信息
  		{
  			
  			return new HWXXClient();
  		}
  		else if(AMID.equals("2019082201")) //收货明细     
  		{
  			
  			return new RKDDSJHClient();
  		}
  		else if(AMID.equals("2019082202")) //出库明细     
  		{
  			
  			return new CKDDXX_HClient();
  		}
  		else if(AMID.equals("2019082601")) //收货明细  确认表   
  		{
  			
  			return new RKDDSJH_EDClient();
  		}
  		else if(AMID.equals("2019082602")) //出库明细  确认表   
  		{
  			
  			return new CKDDXX_H_EDClient();
  		}
  		else if(AMID.equals("2019090501")) //计费规则
  		{
  			
  			return new JFGZ_HClient();
  		}
  		else if(AMID.equals("2019091201")) //公式字段
  		{
  			
  			return new GSZDClient();
  		}
        return null;
    }
    
    /**
     * 具体项目的功能
     * @param AMID
     * @return
     */
    public static Canvas createProjectModule2(String AMID)
    {
    	
    	//贝业
  		if(AMID.equals("1807112232")) //BMS计算
  		{
  			
  			return new TBDRSJClient();
  		}
  		else if(AMID.equals("1807112328")) //仓储费
  		{
  			
  			return new CCFClient();
  		}
  		else if(AMID.equals("1808011446")) //管理费
  		{
  			
  			return new GLFClient();
  		}
  		else if(AMID.equals("1807121016")) //进出费
  		{
  			
  			return new JCFClient();
  		}
  		else if(AMID.equals("1807121048")) //装卸费
  		{
  			
  			return new ZXFClient();
  		}
  		else if(AMID.equals("1807121055")) //贴标费
  		{
  			
  			return new TBFClient();
  		}
  		else if(AMID.equals("1807121118")) //清点费
  		{
  			
  			return new QDFClient();
  		}
  		else if(AMID.equals("1807121128")) //扫码费
  		{
  			
  			return new SMFClient();
  		}
  		else if(AMID.equals("1807121139")) //订单处理费
  		{
  			
  			return new DDCLFClient();
  		}
  		else if(AMID.equals("1807121147")) //08零支分拣费用
  		{
  			
  			return new LZFClient();
  		}
  		else if(AMID.equals("1807172145")) //重包耗材费用
  		{
  			
  			return new CBHCFClient();
  		}
  		else if(AMID.equals("1807121156")) //09理货费
  		{
  			
  			return new LJZFClient();
  		}
  		else if(AMID.equals("1807172156")) //加托盘费表模
  		{
  			
  			return new JTPFClient();
  		}
  		else if(AMID.equals("1807171151")) //装箱费用
  		{
  			
  			return new ZHXFClient();
  		}
  		else if(AMID.equals("1807121348")) //称重
  		{
  			
  			return new CZFClient();
  		}
  		else if(AMID.equals("1807121415")) //掏箱
  		{
  			
  			return new TXFClient();
  		}
  		else if(AMID.equals("1807122306")) //其他
  		{
  			
  			return new QTFClient();
  		}
  		else if(AMID.equals("1808011538")) //异常费
  		{
  			
  			return new YCFClient();
  		}
  		else if(AMID.equals("1809141937")) //收入差额调整
  		{
  			
  			return new SRSCClient();
  		}
  		else if(AMID.equals("1901080933")) //成本差额调整
  		{
  			
  			return new CBTZClient();
  		}
  		else if(AMID.equals("1808102153")) //c成本装卸劳务费
  		{
  			return new ZXLWFClient();
  		}
  		else if(AMID.equals("1808061437")) //c成本水电费
  		{
  			return new SDFClient();
  		}
  		else if(AMID.equals("1808061604")) //c成本租金费
  		{
  			return new ZJFClient();
  		}
  		else if(AMID.equals("1808130839")) //成本定价
  		{
  			
  			return new CBDJClient();
  		}
  		else if(AMID.equals("1808172214")) //库区面积
  		{
  			
  			return new KQMJClient();
  		}
  		else if(AMID.equals("1808172234")) //项目占用
  		{
  			
  			return new XMZYClient();
  		}
  		else if(AMID.equals("1808029523")) //收入开票维护
  		{
  			
  			return new KPXXClient();
  		}
  		else if(AMID.equals("1811212055")) //生成账单
  		{
  			
  			return new ZDSCClient();
  		}
  		else if(AMID.equals("1903230905")) //生成成本账单
  		{
  			
  			return new ZDCBClient();
  		}
        return null;
    }
    
    /**
     * 具体项目的功能
     * @param AMID
     * @return
     */
    public static Canvas createProjectModule3(String AMID)
    {
        return null;
    }
}
