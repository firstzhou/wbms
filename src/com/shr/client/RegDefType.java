package com.shr.client;

import com.google.gwt.i18n.client.NumberFormat;
import com.smartgwt.client.core.DataClass;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.data.SimpleType;
import com.smartgwt.client.data.SimpleTypeFormatter;
import com.smartgwt.client.types.FieldType;
import com.smartgwt.client.widgets.DataBoundComponent;

public class RegDefType {
	static public void reg() {
		final NumberFormat SNumberFormat = NumberFormat	.getFormat("###,###,##0.00");
		SimpleType SNumber = new SimpleType("SNumber", FieldType.FLOAT);
		SNumber.setNormalDisplayFormatter(new SimpleTypeFormatter() {

			@Override
			public String format(Object value, DataClass field,
					DataBoundComponent component, Record record) {
				try
				{
					if (value != null && !"".equals(value) && !"&nbsp;".equals(value)) {
						return SNumberFormat.format(Double.valueOf(value.toString()));
					} else {
						return "";
					}
				}catch(Exception e)
				{
					e.printStackTrace();
					return "";
				}
			}
		});
		SNumber.setShortDisplayFormatter(new SimpleTypeFormatter() {

			@Override
			public String format(Object value, DataClass field,
					DataBoundComponent component, Record record) {
				if (value != null) {
					return SNumberFormat.format(Double.valueOf(value.toString()));
				} else {
					return "";
				}
			}
		});
		SNumber.register();

	}
}
