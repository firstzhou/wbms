package com.shr.client;

import java.util.HashMap;
import java.util.Map;

public class App {
	public static String curUserId;
	public static String curUserName;
	public static String curUserDepartName;
	public static String curUserDepart;
	public static String curUserRole;
	public static String curUserAttribute;	
	public static String zhangtao;
	public static String curUserDataRight;
	public static String curUserRightContent;
	/**
	 * <AMDI,right>
	 * right : btnNew=Y;btnEdit=Y...
	 */
	public static Map<String, String> curUserRight = new HashMap<String, String>();
	
	/**
	 * 存储客户端临时属性
	 */
	public static Map<String, Object> curSessionMap = new HashMap<String, Object>();
	
	/**
	 * 用版本控制客户端代码有没有更新
	 * @return
	 */
	static public String getVersion()
	{
		return "V201902171917";
	}
}
