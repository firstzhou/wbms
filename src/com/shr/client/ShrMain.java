package com.shr.client;

import java.util.HashMap;

import com.shr.client.model.ThirdPartyPanel;
import com.shr.client.model.UREPORTPanel;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;  
  
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.Layout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tree.TreeNode;
import com.smartgwt.client.widgets.tree.events.LeafClickEvent;
import com.smartgwt.client.widgets.tree.events.LeafClickHandler;
import com.google.gwt.core.client.EntryPoint;  
  
public class ShrMain implements EntryPoint 
{  
	private TabSet mainTabSet;
	private Tab mainTab;
	
    public void onModuleLoad() 
    {  
    	VLayout main = new VLayout();
    	Canvas.resizeFonts(2); //设置页面字体
        Canvas.resizeControls(6); //设置控件大小
        main.setWidth100();
		main.setHeight100();
		main.setLayoutMargin(3);
		
		HLayout hLayout1 = new HLayout();
		hLayout1.setWidth100();
		hLayout1.setHeight100();

		final VLayout sideNavLayout = new VLayout();
		sideNavLayout.setHeight100();
		sideNavLayout.setWidth(200);
		sideNavLayout.setShowResizeBar(true);
		//sideNavLayout.setVisible(false);

		SideNavTree sideNav = new SideNavTree();
		sideNav.addLeafClickHandler(new LeafClickHandler() {

			public void onLeafClick(LeafClickEvent event) {
				// TODO Auto-generated method stub
				TreeNode treeNode = event.getLeaf();
				openMenu(treeNode.getAttribute("AMTEMPLET"),
						treeNode.getAttribute("AMPARAM"),
						treeNode.getAttribute("AMNAME"));
			}
		});
		sideNavLayout.addMember(sideNav);

		hLayout1.addMember(sideNavLayout);

		mainTabSet = new TabSet();
		mainTabSet.setWidth100();
		mainTabSet.setHeight100();
		mainTabSet.setID("Frame_MainTabSet");

		Layout paneContainerProperties = new Layout();
		paneContainerProperties.setLayoutMargin(0);
		paneContainerProperties.setLayoutTopMargin(1);
		mainTabSet.setPaneContainerProperties(paneContainerProperties);

		mainTab = new Tab();
		mainTab.setTitle("首页");
		mainTab.setIcon("menuicon/windows.png");
		mainTabSet.addTab(mainTab);

		Canvas canvas = new Canvas();
		canvas.setBackgroundImage("[SKIN]/shared/background.gif");
		canvas.setWidth100();
		canvas.setHeight100();
		canvas.addChild(mainTabSet);

		hLayout1.addMember(canvas);
		MainTopPanel mainTopPanel = new MainTopPanel();
		
		main.addMember(mainTopPanel);

		main.addMember(hLayout1);

		RegDefType.reg();
		//设置超时时间
      	RPCManager.setDefaultTimeout(20000000);
		DMI.call("shr", "PublicServer",	"getCurrentUser", new RPCCallback() 
		{
			@Override
			public void execute(RPCResponse response, Object rawData,
					RPCRequest request) 
			{
				HashMap user = (HashMap) response.getDataAsMap();
				
				String version = (String) user.get("version");
				if(!App.getVersion().equals(version))
				{
					SC.showPrompt("客户端代码版本过旧，请按ctr+F5刷新页面。");
				}
				App.curUserId = (String) user.get("userId");
				App.curUserName = (String) user.get("userName");
				App.curUserRole = (String) user.get("userRole");
				App.curUserDepartName = (String)user.get("curUserDepartName");	
				App.curUserDepart = (String) user.get("userBM");
				App.curUserRight = (HashMap<String, String>)user.get("userRight");
				App.zhangtao = (String) user.get("zhangtao");
				App.curUserDataRight = (String) user.get("curUserDataRight");
				App.curUserRightContent = (String) user.get("curUserRightContent");
				//显示首页页面
				mainTab.setPane(new ShrMenu(mainTabSet));
			}
		}, null);
		main.draw();
    }  
    
    /**
	 * 打开菜单
	 * @param type
	 * @param param
	 * @param menuName
	 * @param AMID
	 */
	private void openMenu(String type, String param, String menuName) 
	{
		Tab[] tabs = mainTabSet.getTabs();
		boolean found = false;
		for (int i = 0; i < tabs.length; i++) 
		{
			if (tabs[i].getTitle().equals(menuName)) 
			{
				found = true;
				mainTabSet.selectTab(tabs[i].getID());
			}
		}
		if (!found) 
		{
			// 如果未找到标签，新建一个

			Tab tab = new Tab();
			tab.setCanClose(true);
			tab.setTitle(menuName);
			if  (type.equals("ThirdParty")) 
			{
				ThirdPartyPanel baseDocPanel = new ThirdPartyPanel(param);
				tab.setPane(baseDocPanel);
			} 
			else if (type.equals("UREPORTPanel")) 
			{
				UREPORTPanel reportPanel = new UREPORTPanel(param);
				tab.setPane(reportPanel);
			} 
			else 
			{
				Canvas panel = ModuleFactor.createModule(param);
				panel.setWidth100();
				tab.setPane(panel);
			}
			mainTabSet.addTab(tab);
			mainTabSet.selectTab(tab.getID());
		}
	}
}  
