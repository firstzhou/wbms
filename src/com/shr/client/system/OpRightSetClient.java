package com.shr.client.system;

import java.util.ArrayList;
import java.util.List;

import com.shr.shared.ClientTools;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.events.DataArrivedEvent;
import com.smartgwt.client.widgets.tree.events.DataArrivedHandler;

public class OpRightSetClient extends VLayout {
	private TreeGrid tree;
	private DynamicForm boundForm;
	private DataSource dsOpRight;
	TabSet tabSetList;
	Tab tabList, tabEdit;
	VLayout mainLayout;
	HLayout bottomLayout;
	Window currentWin;
	String operateType = "";
	String userId = "";
	public OpRightSetClient() {
		dsOpRight = DataSource.getDataSource("Q_OPRIGHT");
		mainLayout = new VLayout();
		bottomLayout = new HLayout();
		
		tabSetList = new TabSet();
		tabList = new Tab("用户权限菜单");
		tree = new TreeGrid();
		tree.setCanEdit(true);
		tree.setCanGroupBy(true);
		tree.setShowFilterEditor(false);
		tree.setAutoFetchData(true);
		tree.setWidth100();
		//设置为false，否则末级节点也会显示成父级的样式
		tree.setLoadDataOnDemand(false);
		tree.setDataSource(dsOpRight);
		tree.setSelectionAppearance(SelectionAppearance.CHECKBOX); 
		tree.setShowPartialSelection(false);
		//设置自动保存编辑为false,保存使用insert语句插入OPRIGHT
		tree.setAutoSaveEdits(false);
//	    tree.setSelectionType(SelectionStyle.SIMPLE); 
	    //设置checkBoxTree的选中状态对应字段
		tree.setSelectionProperty("SELECTED");
		tree.setEditByCell(true);
		//该属性与selectionProperty冲突，不知为哈，所以用selectionChanged事件代替
		//tree.setCascadeSelection(true);
		tree.addDataArrivedHandler(new DataArrivedHandler() {  
            public void onDataArrived(DataArrivedEvent event) {  
            	tree.getData().openAll();  
            }  
        }); 
		tree.addSelectionChangedHandler(new SelectionChangedHandler(){

            @Override
            public void onSelectionChanged(SelectionEvent event)
            {
                // TODO Auto-generated method stub
                Record record = event.getRecord();
                String strFid = record.getAttribute("OMFATHERID");
                String strCid = record.getAttribute("OMCHILDID");
                boolean selected = record.getAttributeAsBoolean("SELECTED");
                //选择父节点
                selectAllFather(strFid, selected);
                //选择说有的子节点
                selectAllChildren(strCid, selected);
                tree.redraw();
            }
            
        });
		
		tree.setFields(new TreeGridField("OMFATHERID") {
			{
				setWidth100();
				setTreeField(true);
				setTitle("用户权限");
				setDisplayField("AMNAME");
				setCanEdit(false);
			}
		}, new TreeGridField("OMCHILDID") {
			{
				setTreeField(true);
				setHidden(true);
			}
		}, new TreeGridField("AMNAME") {
			{
				setTreeField(true);
				setHidden(true);
			}
		}, new TreeGridField("AMID") {
			{
				setTreeField(true);
				setHidden(true);
			}
		}
		/**
		, new TreeGridField("ORCANVIEW") { 
			{
				setTreeField(true);
				setTitle("显示权限");
			}
		}
		**/
		,new TreeGridField("ORCANINSERT") { 
			{
				setTreeField(true);
				setTitle("新增权限");
			}
		}, new TreeGridField("ORCANEDIT") { 
			{
				setTreeField(true);
				setTitle("编辑权限");
			}
		}, new TreeGridField("ORCANDELETE") {  
			{
				setTreeField(true);
				setTitle("删除权限");
			}
		}, new TreeGridField("ORCANIMP") { 
			{
				setTreeField(true);
				setTitle("导入权限");
			}
		}, new TreeGridField("ORCANEXP") { 
			{
				setTreeField(true);
				setTitle("导出权限");
			}
		}, new TreeGridField("ORUSEROPTION") {  
			{
				setTreeField(true);
				setTitle("用户权限配置");
			}
		}, new TreeGridField("SELECTED") {  
			{
				setTreeField(true);
				setTitle("选中状态");
				setHidden(true);
			}
		});
		
		tabList.setPane(tree);
		tabSetList.addTab(tabList);
		tabSetList.setHeight("90%");
		mainLayout.addMember(tabSetList);
		
		IButton ensureButton = new IButton("确定");
		ensureButton.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				//收起的节点检索不到，必须要展开，不知为啥
				tree.getData().openAll();
				String delSql = "delete from SYS_OPRIGHT where ORID = '"+userId+"';";
				//ClientTools.exeSQL(delSql);
				ListGridRecord[] rs = tree.getRecords();
				List<ListGridRecord> listRecord = new ArrayList<ListGridRecord>();
				for(ListGridRecord record : rs)
				{
					String selected = record.getAttribute("SELECTED");
					if("true".equals(selected))
					{
						listRecord.add(record);
					}
				}
				String insertSql = "";
				String[] opRightSqls = new String[listRecord.size()+4];
				opRightSqls[0] = delSql;
				for(int i=1; i<listRecord.size()+1; i++) {
					insertSql = "insert into SYS_OPRIGHT (ORUSERORROLE,ORID,ORMDID,ORCANVIEW,ORCANINSERT," +
							"ORCANEDIT,ORCANDELETE,ORCANEXP,ORCANIMP,ORUSEROPTION) values('U','"+userId +"','"+
							listRecord.get(i-1).getAttribute("AMID")+"','"+getEditValue(listRecord.get(i-1),"ORCANVIEW")+"','"
							+getEditValue(listRecord.get(i-1),"ORCANINSERT")+"','"+
							getEditValue(listRecord.get(i-1),"ORCANEDIT")+"','"+getEditValue(listRecord.get(i-1),"ORCANDELETE")+"','"+
							getEditValue(listRecord.get(i-1),"ORCANEXP")+"','" +
							getEditValue(listRecord.get(i-1),"ORCANIMP")+"','"+ClientTools.trim(getEditValue(listRecord.get(i-1),"ORUSEROPTION"))+"');";	
					opRightSqls[i] = insertSql;
				}
				
				//把角色的权限加上
				opRightSqls[listRecord.size()+1] = "insert into SYS_OPRIGHT (ORUSERORROLE,ORID,ORMDID,ORCANVIEW,ORCANINSERT,ORCANEDIT,ORCANDELETE,ORCANEXP,ORCANIMP,ORUSEROPTION)" +
				"            select ORUSERORROLE,'"+userId+"',ORMDID,ORCANVIEW,ORCANINSERT,ORCANEDIT,ORCANDELETE,ORCANEXP,ORCANIMP,ORUSEROPTION from SYS_OPRIGHT " +
				"            where 1=1" +
				"            and ORID in (select USRROLEID from SYS_USERS where usrid = '"+userId+"') " +
				"            and ORMDID not in (select ORMDID from SYS_OPRIGHT where ORID='"+userId+"')" ;
				
				//角色修改了，刷新角色下的用户权限
				String deletRight = "delete from SYS_OPRIGHT where ORID in (select USRID from SYS_USERS where USRROLEID='"+userId+"')";
				String instRight = "insert into SYS_OPRIGHT (ORUSERORROLE,ORID,ORMDID,ORCANVIEW,ORCANINSERT,ORCANEDIT,ORCANDELETE,ORCANEXP,ORCANIMP,ORUSEROPTION)" + 
								   "select ORUSERORROLE,USRID,ORMDID,ORCANVIEW,ORCANINSERT,ORCANEDIT,ORCANDELETE,ORCANEXP,ORCANIMP,ORUSEROPTION " +
						           "from SYS_OPRIGHT, SYS_USERS where ORID = '"+userId+"' and USRROLEID='"+userId+"'";
				opRightSqls[listRecord.size()+2] = deletRight;
				opRightSqls[listRecord.size()+3] = instRight;
				ClientTools.exeSQLs(opRightSqls);
				SC.say("权限设置成功！");
			}
		});
		bottomLayout.addMember(ensureButton);
		
		IButton cancelButton = new IButton("关闭");
		cancelButton.addClickHandler(new ClickHandler(){
			public void onClick(ClickEvent event) {
				currentWin.destroy();
			}
		});
		bottomLayout.addMember(cancelButton);
		mainLayout.addMember(bottomLayout);
		bottomLayout.setHeight(30);
		bottomLayout.setAlign(Alignment.CENTER);
		bottomLayout.setMembersMargin(60);
		mainLayout.setMembersMargin(30);
		this.addMember(mainLayout);
		
	}

	public void init(String getUserId, String getUserName, Window getCurrentWindow) {
		currentWin = getCurrentWindow;
		userId = getUserId;
		tabList.setTitle(getUserName + "——权限设置");
		Criteria c = new Criteria("ORID",getUserId);
		tree.fetchData(c);

	}
	
	public String getEditValue(ListGridRecord editRecord, String fieldName) {
		if(!"".equals(ClientTools.trim(tree.getEditValues(editRecord).get(fieldName)))){
			return tree.getEditValues(editRecord).get(fieldName).toString();
		}else {
			return editRecord.getAttribute(fieldName);
		}
	}
	
	/**
     * 选择下级所有节点
     * @param
     */
    private void selectAllFather(String faterNodeID, boolean selected)
    {
        if(selected)
        {
            Record[] recordList = tree.getRecordList().findAll("OMCHILDID", faterNodeID);
            if(recordList != null)
            {
                for(Record record : recordList)
                {
                    record.setAttribute("SELECTED", selected);
                    String fnodeid = record.getAttribute("OMFATHERID");
                    if(null != fnodeid)
                    {
                        selectAllFather(fnodeid, selected);
                    }
                }
            }
        }
        else
        {
            Record[] recordList = tree.getRecordList().findAll("OMCHILDID", faterNodeID);
            if(recordList == null)
            {
            	return;
            }
            for(Record record : recordList)
            {
                String thisnodeid = record.getAttribute("OMCHILDID");
                Record[] fatherRecordList = tree.getRecordList().findAll("OMFATHERID", thisnodeid);
                if(null != fatherRecordList)
                {
                    for(Record tempRecord : fatherRecordList)
                    {
                        if(tempRecord.getAttributeAsBoolean("SELECTED"))
                        {
                            return;
                        }
                    }
                    record.setAttribute("SELECTED", selected);
                }
                String fnodeid = record.getAttribute("OMFATHERID");
                if(null != fnodeid)
                {
                    selectAllFather(fnodeid, selected);
                }
            }
        }
    }
    
    /**
     * 选择下级所有节点
     * @param
     */
    private void selectAllChildren(String nodeID, boolean selected)
    {
        Record[] recordList = tree.getRecordList().findAll("OMFATHERID", nodeID);
        if(null != recordList)
        {
            for(Record record : recordList)
            {
                record.setAttribute("SELECTED", selected);
                String childID = record.getAttribute("OMCHILDID");
                selectAllChildren(childID, selected);
            }
        }
    }
}
