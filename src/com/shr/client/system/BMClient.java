package com.shr.client.system;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SortArrow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public class BMClient extends VLayout {
	private DynamicForm boundForm;
	private ToolStripButton newBtn, editBtn, delBtn, saveBtn;
	private ToolStrip tollStrip;
	DataSource dsBM;
	TreeGrid tree;
	
	String selectBMID;
	
	String selectSJBM;

	public BMClient() {
		dsBM = DataSource.get("SYS_BM");

		boundForm = new DynamicForm();
		VLayout mainLayout = new VLayout();

		tollStrip = new ToolStrip();
		
		newBtn = new ToolStripButton("新增同级");
		newBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(tree.getSelectedRecord() != null)
				{
					boundForm.editNewRecord();
					
					//当前选中节点下新建部门，则从树结构节点下获取部门ID作为表单中新建部门的上级部门ID
					boundForm.setValue("BM_SJBM", selectSJBM == null ? "" : selectSJBM);
					//所有公司都加在跟节点
					//boundForm.setValue("BM_SJBM", "");
					saveBtn.enable();
				}
				else
				{
					SC.warn("请选择同级部门！");
				}
				
			}
		});
		tollStrip.addMember(newBtn);
		
		newBtn = new ToolStripButton("新增下级");
		newBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) 
			{
				if(tree.getSelectedRecord() != null)
				{
					boundForm.editNewRecord();
					//当前选中节点下新建部门，则从树结构节点下获取部门ID作为表单中新建部门的上级部门ID
					boundForm.setValue("BM_SJBM", selectBMID == null ? "" : selectBMID);
					//所有公司都加在跟节点
					//boundForm.setValue("BM_SJBM", "");
					saveBtn.enable();
				}
				else
				{
					SC.warn("请选择上级部门！");
				}
			}
		});
		tollStrip.addMember(newBtn);

		editBtn = new ToolStripButton("编辑");
		editBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundForm.editRecord(tree.getSelectedRecord());
				saveBtn.enable();
			}
		});
		tollStrip.addMember(editBtn);

		delBtn = new ToolStripButton("删除");
		delBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.confirm("确认删除吗？删除后所有的模块信息和权限信息都将无法恢复，请谨慎操作！",
						new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null && value) {
									dsBM.removeData(tree.getSelectedRecord());
								}
							}
						});
			}
		});
		tollStrip.addMember(delBtn);

		saveBtn = new ToolStripButton("保存");
		saveBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
//				 boundForm.setValue("BM_SJBM", ClientTools.trim(tree
//				 .getSelectedRecord() == null ? "" : tree
//				 .getSelectedRecord().getAttribute("BM_BMID")));
				if (boundForm.isNewRecord()) 
				{
					//检查数据是否重复
					String BMID = String.valueOf(boundForm.getValue("BM_BMID"));
					Criteria criteria = new Criteria();
					criteria.addCriteria("BM_BMID", BMID);
					dsBM.fetchData(criteria, new DSCallback()
					{
						public void execute(DSResponse response, Object rawData, DSRequest request) 
						{
							if(response.getData() != null && response.getData().length != 0)
							{
								SC.warn("该功部门编码已经存在，请使用其他编码。");
							}
							else
							{
								boundForm.saveData();
								tree.fetchData();
								if (!boundForm.hasErrors()) 
								{
									boundForm.clearValues();
									saveBtn.disable();
								}
							}
						}
					});
				}
				else
				{
					boundForm.saveData();
					tree.fetchData();
					if (!boundForm.hasErrors()) 
					{
						boundForm.clearValues();
						saveBtn.disable();
					}
				}
			}
		});
		tollStrip.addMember(saveBtn);

		mainLayout.addMember(tollStrip);
		HLayout hLayout = new HLayout();

		createLeftTree();
		hLayout.addMember(tree);

		boundForm.setNumCols(2);
		boundForm.setWidth100();
		boundForm.setAutoFocus(false);
		boundForm.setDataSource(dsBM);

		hLayout.addMember(boundForm);

		mainLayout.addMember(hLayout);

		this.addMember(mainLayout);
	}

	public void createLeftTree() {

		tree = new TreeGrid();
		tree.setWidth(370);
		tree.setHeight100();
		tree.setShowHeader(false);
		tree.setAnimateFolderTime(100);
		tree.setAnimateFolders(true);
		tree.setAnimateFolderSpeed(1000);
		tree.setShowSortArrow(SortArrow.CORNER);
		tree.setLoadDataOnDemand(false);
		tree.setCanReorderRecords(true);  
		tree.setCanAcceptDroppedRecords(true); 
		tree.setCanSort(false);
		tree.setLeaveScrollbarGap(false);
		tree.setDataSource(dsBM);
		tree.setFields(new TreeGridField("BM_MC") {
			{
				setTreeField(true);
			}
		});

		tree.addRecordClickHandler(new RecordClickHandler() {

			/*
			 * (non-Javadoc)
			 * 
			 * @see com.smartgwt.client.widgets.grid.events.RecordClickHandler#
			 * onRecordClick
			 * (com.smartgwt.client.widgets.grid.events.RecordClickEvent)
			 */
			public void onRecordClick(RecordClickEvent event) {
				// TODO Auto-generated method stub
				boundForm.editRecord(event.getRecord());
				selectBMID = tree.getSelectedRecord().getAttribute("BM_BMID");
				selectSJBM = tree.getSelectedRecord().getAttribute("BM_SJBM");
			}
		});

		tree.setAutoFetchData(true);

	}
}
