package com.shr.client.system;

import com.shr.shared.ClientTools;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.SortArrow;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.tree.events.LeafClickEvent;
import com.smartgwt.client.widgets.tree.events.LeafClickHandler;

public class MenuDesign extends VLayout {
	private DynamicForm boundForm;
	private ToolStrip tollStrip;
	DataSource dsORGMODEL;
	DataSource dsActiveModle;
	DataSource dsOpright;
	TreeGrid tree;
	ToolStripButton saveBtn;
	int newModel = 0;

	public MenuDesign() {
		dsORGMODEL = DataSource.get("SYS_ORGMODEL");
		dsActiveModle = DataSource.get("SYS_ACTIVEMODULE");
		dsOpright = DataSource.get("SYS_OPRIGHT");

		tollStrip = new ToolStrip();

		ToolStripButton newOBtn = new ToolStripButton("新增同级");
		newOBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundForm.editNewRecord();
				boundForm.getField("AMID").enable();
				saveBtn.enable();
				newModel = 1;
			}
		});

		tollStrip.addMember(newOBtn);
		ToolStripButton newCBtn = new ToolStripButton("新增下级");
		newCBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundForm.editNewRecord();
				boundForm.getField("AMID").enable();
				saveBtn.enable();
				newModel = 2;
			}
		});
		tollStrip.addMember(newCBtn);

		ToolStripButton editBtn = new ToolStripButton("编辑");
		editBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				String AMID = tree.getSelectedRecord()
						.getAttribute("OMCHILDID");
				Criteria criteria = new Criteria();
				criteria.addCriteria("AMID", AMID);

				dsActiveModle.fetchData(criteria, new DSCallback() {

					public void execute(DSResponse response, Object rawData,
							DSRequest request) {
						boundForm.editRecord(response.getData()[0]);
						boundForm.getField("AMID").disable();
						saveBtn.enable();
					}
				});
				saveBtn.enable();
			}
		});
		tollStrip.addMember(editBtn);
		ToolStripButton delBtn = new ToolStripButton("删除");
		delBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				SC.confirm("确认删除吗？删除后所有的模块信息和权限信息都将无法恢复，请谨慎操作！",
						new BooleanCallback() {
							public void execute(Boolean value) {
								if (value != null && value) {
									// labelAnswer.setContents("OK");
									Record record = new Record();
									String sOMCHILDID = tree
											.getSelectedRecord().getAttribute("OMCHILDID");// 获取同级父节点
									record.setAttribute("OMCHILDID", sOMCHILDID);
									dsORGMODEL.removeData(record);

									record = new Record();
									record.setAttribute("ORMDID", sOMCHILDID);
									dsOpright.removeData(record);

									record = new Record();
									record.setAttribute("AMID", sOMCHILDID);
									dsActiveModle.removeData(record);

								}
							}
						});

			}
		});
		tollStrip.addMember(delBtn);

		saveBtn = new ToolStripButton("保存");
		saveBtn.addClickHandler(new ClickHandler() 
		{
			public void onClick(ClickEvent event) 
			{
				if (boundForm.isNewRecord()) 
				{
					//检查数据是否重复
					String AMID = String.valueOf(boundForm.getValue("AMID"));
					Criteria criteria = new Criteria();
					criteria.addCriteria("AMID", AMID);
					dsActiveModle.fetchData(criteria, new DSCallback()
					{
						public void execute(DSResponse response, Object rawData, DSRequest request) 
						{
							if(response.getData() != null && response.getData().length != 0)
							{
								SC.warn("该功能模块编码已经存在，请修改其他模块编码。");
							}
							else
							{
								boundForm.saveData(new DSCallback() 
								{
									@Override
									public void execute(DSResponse response, Object rawData,
											DSRequest request) {
										// 如果是新建一条记录，则往
										// OrgModule中插入一条记录
										String fOMFATHERID = "0";
										Record record = new Record();
										if (newModel == 1) 
										{ // 同级
											String s = ClientTools.trim(tree
													.getSelectedRecord()
													.getAttributeAsString("OMFATHERID"));
											if (!"".equals(s)) 
											{
												fOMFATHERID = s;// 获取同级父节点
											}
										} 
										else 
										{
											fOMFATHERID = tree.getSelectedRecord()
													.getAttributeAsString("OMCHILDID");
										}
										record.setAttribute("OMFATHERID", fOMFATHERID);
										record.setAttribute("OMORDER", -1);
										record.setAttribute("OMCHILDID", boundForm.getValue("AMID"));
										record.setAttribute("AMNAME", boundForm.getValue("AMNAME"));
										tree.addData(record);

										// 默认增加权限
										record = new Record();
										record.setAttribute("ORUSERORROLE", "U");
										record.setAttribute("ORID", "SUPERVISOR");
										record.setAttribute("ORMDID", boundForm.getValue("AMID"));
										record.setAttribute("ORCANINSERT", "Y");
										record.setAttribute("ORCANEDIT", "Y");
										record.setAttribute("ORCANDELETE", "Y");
										record.setAttribute("ORCANPRINT", "Y");
										record.setAttribute("ORUSEROPTION", "");
										record.setAttribute("ORCUSTOMUI", "");

										dsOpright.addData(record);
									}
								});
								if (!boundForm.hasErrors()) 
								{
									saveBtn.disable();
								}
							}
						}
					});
				}
				else
				{
					boundForm.saveData();
					if (!boundForm.hasErrors()) 
					{
						saveBtn.disable();
					}
				}
			}
		});
		tollStrip.addMember(saveBtn);

		VLayout mainLayout = new VLayout();
		mainLayout.addMember(tollStrip);
		HLayout hLayout = new HLayout();

		createLeftTree();
		hLayout.addMember(tree);

		boundForm = new DynamicForm();
		boundForm.setNumCols(2);
		boundForm.setWidth100();
		boundForm.setAutoFocus(false);
		boundForm.setDataSource(dsActiveModle);
		hLayout.addMember(boundForm);

		mainLayout.addMember(hLayout);

		this.addMember(mainLayout);
	}

	public void createLeftTree() {

		tree = new TreeGrid();
		tree.setWidth(290);
		tree.setHeight100();
		tree.setShowHeader(false);
		tree.setCustomIconProperty("AMICON");
		tree.setAnimateFolderTime(100);
		tree.setAnimateFolders(true);
		tree.setAnimateFolderSpeed(1000);
		tree.setShowSortArrow(SortArrow.CORNER);
		tree.setLoadDataOnDemand(false);
		tree.setCanSort(false);
		tree.setLeaveScrollbarGap(false);
		tree.setDataSource(dsORGMODEL);
		tree.setFields(new TreeGridField("AMNAME") {
			{
				setTreeField(true);
			}
		});

		tree.addLeafClickHandler(new LeafClickHandler() 
		{
			public void onLeafClick(LeafClickEvent event) 
			{
				String AMID = event.getLeaf().getAttribute("OMCHILDID");
				Criteria criteria = new Criteria();
				criteria.addCriteria("AMID", AMID);

				dsActiveModle.fetchData(criteria, new DSCallback() 
				{
					public void execute(DSResponse response, Object rawData, DSRequest request) 
					{
						boundForm.editRecord(response.getData()[0]);
						boundForm.getField("AMID").disable();
						saveBtn.enable();
					}
				});
			}
		});

		tree.setAutoFetchData(true);
	}
}
