package com.shr.client.system;

import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class RoleClient extends VLayout {

	private ListGrid boundList;
	private DynamicForm boundForm;
	private ToolStripButton saveBtn;
	private ToolStripButton newBtn;
	private ToolStripButton delBtn;
	public ToolStrip tollStrip;

	public RoleClient() {
		init();
	}

	private void init() {
		final DataSource ds = DataSource.get("SYS_USERGROUP");

		HLayout mainLayout = new HLayout();
		mainLayout.setWidth100();
		mainLayout.setLayoutMargin(5);

		tollStrip = new ToolStrip();
		TabSet tabSet = new TabSet();
		tabSet.setWidth(500);
		
		Tab tabEdit = new Tab("编辑");
		tabSet.addTab(tabEdit);

		newBtn = new ToolStripButton("新建角色");
		newBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundForm.editNewRecord();
				saveBtn.enable();
			}
		});
		tollStrip.addMember(newBtn);
		
		delBtn = new ToolStripButton("删除角色");
		delBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (null != boundList.getSelectedRecord())
		        {
		            SC.confirm("确认删除这<font color='red'>【<strong>" + boundList.getSelectedRecords().length
		                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！", new BooleanCallback()
		            {
		                public void execute(Boolean value)
		                {
		                    if (value != null && value)
		                    {
		                    	boundList.removeSelectedData();
		                    	boundForm.clearValues();
		                    }
		                }
		            });
		        }
		        else
		        {
		            SC.say("请选择需要删除的数据！");
		        }
			}
		});
		tollStrip.addMember(delBtn);

		saveBtn = new ToolStripButton("保存");
		saveBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundForm.validate(false);
				if (!boundForm.hasErrors()) {
					if (boundForm.isNewRecord()) 
					{
						//检查数据是否重复
						String UGID = String.valueOf(boundForm.getValue("UGID"));
						Criteria criteria = new Criteria();
						criteria.addCriteria("UGID", UGID);
						ds.fetchData(criteria, new DSCallback()
						{
							public void execute(DSResponse response, Object rawData, DSRequest request) 
							{
								if(response.getData() != null && response.getData().length != 0)
								{
									SC.warn("该功角色编码已经存在，请使用其他编码。");
								}
								else
								{
									boundForm.saveData(new DSCallback(){
										@Override
										public void execute(DSResponse dsResponse, Object data,
												DSRequest dsRequest) {
											boundForm.clearValues();
											saveBtn.disable();
										}
									});
								}
							}
						});
					}
					else
					{
						boundForm.saveData(new DSCallback(){
							@Override
							public void execute(DSResponse dsResponse, Object data,
									DSRequest dsRequest) {
								boundForm.clearValues();
								saveBtn.disable();
							}
						});
					}
				}
			}
		});
		tollStrip.addMember(saveBtn);

//		ToolStripButton clearBtn = new ToolStripButton("清空");
//		clearBtn.addClickHandler(new ClickHandler() {
//			public void onClick(ClickEvent event) {
//				boundForm.clearValues();
//				saveBtn.disable();
//			}
//		});
//		tollStrip.addMember(clearBtn);

		ToolStripButton filterBtn = new ToolStripButton("刷新");
		filterBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundList.invalidateCache();
				boundList.fetchData();
			}
		});
		tollStrip.addMember(filterBtn);

		ToolStripButton btnOpright = new ToolStripButton("设置权限");
		btnOpright.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(null != boundList.getSelectedRecord()) {
					String userId = boundList.getSelectedRecord().getAttributeAsString("UGID");
					String userName = boundList.getSelectedRecord().getAttributeAsString("UGNAME");
					final Window winModal = new Window();
					winModal.setWidth100();
					winModal.setHeight100();
					winModal.setTitle("权限设置");
					winModal.setShowMinimizeButton(true);
					winModal.setShowCloseButton(true);
					winModal.setIsModal(true);
					winModal.setShowModalMask(true);
					winModal.centerInPage();
					OpRightSetClient opRightSetClient = new OpRightSetClient();
					opRightSetClient.setHeight100();
					opRightSetClient.setWidth100();
					winModal.addItem(opRightSetClient);
					winModal.show();
					opRightSetClient.init(userId, userName, winModal);
				}else{
					SC.say("请先选择需要设置的角色");
				}
			}
		});
		tollStrip.addMember(btnOpright);

		this.addMember(tollStrip);

		boundList = new ListGrid();
		boundList.setWidth100();
		boundList.setCanEdit(true);
		boundList.setShowFilterEditor(true);

		boundList.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				boundForm.editRecord(record);
				saveBtn.enable();
			}
		});
		
		mainLayout.addMember(boundList);
		mainLayout.addMember(tabSet);

		boundForm = new DynamicForm();
		boundForm.setNumCols(2);
		boundForm.setAutoFocus(false);
		boundForm.setDataSource(ds);
		tabEdit.setPane(boundForm);

		this.addMember(mainLayout);
		boundList.setDataSource(ds);
		boundList.fetchData();
		saveBtn.disable();

	}
}
