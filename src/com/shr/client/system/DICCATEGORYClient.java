package com.shr.client.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.client.model.xml.XmlFiliationEditPanel;
import com.shr.client.model.xml.XmlSingleEditPanel;
import com.shr.client.model.xml.util.XmlBaseStr;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.Canvas;

public class DICCATEGORYClient extends XmlFiliationEditPanel {

	@Override
	public List<Map<String, Object>> buildChildContent() {
		List<Map<String, Object>> childList = new ArrayList<Map<String, Object>>();
		XmlFiliationEditPanel dicValuePanel = new XmlFiliationEditPanel() {
			@Override
			public void initUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub

			}

			@Override
			public void initFormUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub

			}

			@Override
			public String createToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createPanelTitle() {
				// TODO Auto-generated method stub
				return "数据字典值";
			}

			@Override
			public String createFormToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createEditFormUI() {
				// TODO Auto-generated method stub
				return "DICVALUEForm";
			}

			@Override
			public String createQueryFormUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createListGridUI() {
				// TODO Auto-generated method stub
				return "DICVALUEList";
			}

			@Override
			public String[] createDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] createFormDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String initPanelType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Criteria getDefaultCriteria() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String importKeyStr() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected DataSource createDataSource() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected String getAMID() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public List<Map<String, Object>> buildChildContent() {
				// TODO Auto-generated method stub
				List<Map<String, Object>> childList = new ArrayList<Map<String, Object>>();
				XmlSingleEditPanel dicValuePanel = new XmlSingleEditPanel() {
					@Override
					public void initUIObject(Map<String, Canvas> canvas) {
						// TODO Auto-generated method stub

					}

					@Override
					public void initFormUIObject(Map<String, Canvas> canvas) {
						// TODO Auto-generated method stub

					}

					@Override
					public String createToolStripUI() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public String createPanelTitle() {
						// TODO Auto-generated method stub
						return "数据字典值";
					}

					@Override
					public String createFormToolStripUI() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public String createEditFormUI() {
						// TODO Auto-generated method stub
						return "DICVALUEForm";
					}

					@Override
					public String createQueryFormUI() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public String createListGridUI() {
						// TODO Auto-generated method stub
						return "DICVALUEList";
					}

					@Override
					public String[] createDiyUI() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public String[] createFormDiyUI() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public String initPanelType() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public Criteria getDefaultCriteria() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					public String importKeyStr() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					protected DataSource createDataSource() {
						// TODO Auto-generated method stub
						return null;
					}

					@Override
					protected String getAMID() {
						// TODO Auto-generated method stub
						return null;
					}

				};

				HashMap<String, String> dicValueFields = new HashMap<String, String>();
				dicValueFields.put("DCNO", "DICKEY");

				Map<String, Object> childDicValueMap = new HashMap<String, Object>();
				childDicValueMap.put(XmlBaseStr.BASE_CHILD_TITLE, "数据字典值");
				childDicValueMap.put(XmlBaseStr.BASE_CHILD_PANEL, dicValuePanel);
				childDicValueMap.put(XmlBaseStr.BASE_CHILD_KEYMAP, dicValueFields);
				childList.add(childDicValueMap);

				return childList;
			}

		};

		HashMap<String, String> dicValueFields = new HashMap<String, String>();
		dicValueFields.put("DCNO", "CODE");

		Map<String, Object> childDicValueMap = new HashMap<String, Object>();
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_TITLE, "数据字典值");
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_PANEL, dicValuePanel);
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_KEYMAP, dicValueFields);
		childList.add(childDicValueMap);

		return childList;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub

	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		// return "DICCATEGORYToolStrip";
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return "数据字典";
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "DICCATEGORYForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "DICCATEGORYList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return null;
	}

}
