package com.shr.client.system;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.client.model.xml.XmlFiliationEditPanel;
import com.shr.client.model.xml.XmlSingleEditPanel;
import com.shr.client.model.xml.util.XmlBaseStr;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class SYS_DESIGNClient extends XmlFiliationEditPanel{

	@Override
	public List<Map<String, Object>> buildChildContent() {
		// TODO Auto-generated method stub
		List<Map<String, Object>> childList = new ArrayList<Map<String, Object>>();
		XmlSingleEditPanel dicValuePanel = new XmlSingleEditPanel() {
			@Override
			public void initUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub

			}

			@Override
			public void initFormUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub

			}

			@Override
			public String createToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createPanelTitle() {
				// TODO Auto-generated method stub
				return "功能明细";
			}

			@Override
			public String createFormToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createEditFormUI() {
				// TODO Auto-generated method stub
				return "SYS_DESIGN_MXForm";
			}

			@Override
			public String createQueryFormUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createListGridUI() {
				// TODO Auto-generated method stub
				return "SYS_DESIGN_MXList";
			}

			@Override
			public String[] createDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] createFormDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String initPanelType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Criteria getDefaultCriteria() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String importKeyStr() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected DataSource createDataSource() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected String getAMID() {
				// TODO Auto-generated method stub
				return null;
			}

		};

		HashMap<String, String> dicValueFields = new HashMap<String, String>();
		dicValueFields.put("PK_DESIGN", "PK_DESIGN");

		Map<String, Object> childDicValueMap = new HashMap<String, Object>();
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_TITLE, "功能明细");
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_PANEL, dicValuePanel);
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_KEYMAP, dicValueFields);
		childList.add(childDicValueMap);

		return childList;
	}

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return "1802201546";
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		Canvas canva = canvas.get("SYS_DESIGNToolStrip");
		ToolStripButton butCreateTable = (ToolStripButton)canva.getByLocalId("butCreateTable");
		butCreateTable.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) 
			{
				ListGridRecord record = baseListGrid.getSelectedRecord();
				if(record == null)
				{
					SC.say("必须选择一条数据。");
					return;
				}
				String pk = record.getAttribute("PK_DESIGN");
				SC.showPrompt("正在处理。。。");
				DMI.call("shr", "DesignServer", "doCreateModel", new RPCCallback() {
					public void execute(RPCResponse response, Object rawData, RPCRequest request) 
					{
						SC.clearPrompt();
						SC.say("生成成功。");
					}
				}, new Object[] { "TABLE", pk });
			}
		});
		
		ToolStripButton butCreateDS = (ToolStripButton)canva.getByLocalId("butCreateDS");
		butCreateDS.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) 
			{
				ListGridRecord record = baseListGrid.getSelectedRecord();
				if(record == null)
				{
					SC.say("必须选择一条数据。");
					return;
				}
				String pk = record.getAttribute("PK_DESIGN");
				SC.showPrompt("正在处理。。。");
				DMI.call("shr", "DesignServer", "doCreateModel", new RPCCallback() {
					public void execute(RPCResponse response, Object rawData, RPCRequest request) 
					{
						SC.clearPrompt();
						SC.say("生成成功。");
					}
				}, new Object[] { "DS", pk });
			}
		});
		
		ToolStripButton butCreateUI = (ToolStripButton)canva.getByLocalId("butCreateUI");
		butCreateUI.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) 
			{
				ListGridRecord record = baseListGrid.getSelectedRecord();
				if(record == null)
				{
					SC.say("必须选择一条数据。");
					return;
				}
				String pk = record.getAttribute("PK_DESIGN");
				SC.showPrompt("正在处理。。。");
				DMI.call("shr", "DesignServer", "doCreateModel", new RPCCallback() {
					public void execute(RPCResponse response, Object rawData, RPCRequest request) 
					{
						SC.clearPrompt();
						SC.say("生成成功。");
					}
				}, new Object[] { "UI", pk });
			}
		});
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		return "SYS_DESIGNToolStrip";
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "SYS_DESIGNForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "SYS_DESIGNList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return null;
	}

}
