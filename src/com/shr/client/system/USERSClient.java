package com.shr.client.system;

import com.shr.shared.ClientTools;
import com.shr.shared.MD5;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.types.SortArrow;
import com.smartgwt.client.types.VerticalAlignment;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Button;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.FocusEvent;
import com.smartgwt.client.widgets.form.fields.events.FocusHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;
import com.smartgwt.client.widgets.viewer.DetailViewer;

public class USERSClient extends VLayout {

	private ListGrid boundList;
	private DynamicForm boundForm;
	private ToolStripButton saveBtn;
	private DetailViewer boundViewer;
	private ToolStripButton newBtn;
	private ToolStripButton delBtn;
	public ToolStrip tollStrip;
	DataSource dsDepartmentInfor = null;
	
	FormItem bmItem = null;

	public USERSClient() {
		init();
	}

	private void init() {
		final DataSource ds = DataSource.get("SYS_USERS");
		DataSource dsGroup = DataSource.get("SYS_USERGROUP");
		dsDepartmentInfor = DataSource.get("SYS_BM");
		HLayout mainLayout = new HLayout();
		mainLayout.setWidth100();
		mainLayout.setLayoutMargin(5);

		tollStrip = new ToolStrip();
		TabSet tabSet = new TabSet();
		tabSet.setWidth(500);
		
		Tab tabEdit = new Tab("编辑");
		Tab tabGroupMember = new Tab("角色管理");
		tabSet.addTab(tabEdit);
		//隐藏角色
		//tabSet.addTab(tabGroupMember);

		newBtn = new ToolStripButton("新建用户");
		newBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				MD5 md5 = new MD5();
				boundForm.editNewRecord();
				boundForm.setValue("USRPASSWORD", md5.getMD5ofStr("123456"));
				saveBtn.enable();
			}
		});
		tollStrip.addMember(newBtn);
		
		delBtn = new ToolStripButton("删除用户");
		delBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if (null != boundList.getSelectedRecord())
		        {
		            SC.confirm("确认删除这<font color='red'>【<strong>" + boundList.getSelectedRecords().length
		                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！", new BooleanCallback()
		            {
		                public void execute(Boolean value)
		                {
		                    if (value != null && value)
		                    {
		                    	boundList.removeSelectedData();
		                    	boundForm.clearValues();
		                    }
		                }
		            });
		        }
		        else
		        {
		            SC.say("请选择需要删除的数据！");
		        }
			}
		});
		tollStrip.addMember(delBtn);

		saveBtn = new ToolStripButton("保存");
		saveBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				final String usrid = boundForm.getValueAsString("USRID");
				final String rolid = boundForm.getValueAsString("USRROLEID");
				boundForm.validate(false);
				if (!boundForm.hasErrors()) {
					if (boundForm.isNewRecord()) 
					{
						//检查数据是否重复
						String USRID = String.valueOf(boundForm.getValue("USRID"));
						Criteria criteria = new Criteria();
						criteria.addCriteria("USRID", USRID);
						ds.fetchData(criteria, new DSCallback()
						{
							public void execute(DSResponse response, Object rawData, DSRequest request) 
							{
								if(response.getData() != null && response.getData().length != 0)
								{
									SC.warn("该功用户编码已经存在，请使用其他编码。");
								}
								else
								{
									boundForm.saveData(new DSCallback(){
										@Override
										public void execute(DSResponse dsResponse, Object data,
												DSRequest dsRequest) {
											// TODO Auto-generated method stub
											if(rolid != null && !"".equals(rolid))
											{
												setRightByRole(usrid, rolid);
											}
											boundForm.clearValues();
											saveBtn.disable();
										}
									});
								}
							}
						});
					}
					else
					{
						boundForm.saveData(new DSCallback(){
							@Override
							public void execute(DSResponse dsResponse, Object data,
									DSRequest dsRequest) {
								// TODO Auto-generated method stub
								if(rolid != null && !"".equals(rolid))
								{
									setRightByRole(usrid, rolid);
								}
								boundForm.clearValues();
								saveBtn.disable();
							}
						});
					}
				}
			}
		});
		tollStrip.addMember(saveBtn);

		ToolStripButton clearBtn = new ToolStripButton("清空");
		clearBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundForm.clearValues();
				saveBtn.disable();
			}
		});
//		tollStrip.addMember(clearBtn);

		ToolStripButton filterBtn = new ToolStripButton("刷新");
		filterBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				boundList.invalidateCache();
				boundList.fetchData();
			}
		});
		tollStrip.addMember(filterBtn);

		ToolStripButton expBtn = new ToolStripButton("导出");
		expBtn.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				DSRequest dsRequestProperties = new DSRequest();  
				//以xls格式导出
		        dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
		        dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
		        boundList.exportClientData(dsRequestProperties);
		        //boundList.exportData(dsRequestProperties);
			}
		});
		tollStrip.addMember(expBtn);

		ToolStripButton btnSetPassword = new ToolStripButton("设置密码");
		btnSetPassword.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				ListGridRecord record = boundList.getSelectedRecord();
				if (record != null) { // 选中了

					final Window winModal = new Window();
					winModal.setWidth(360);
					winModal.setHeight(115);
					winModal.setTitle("Modal Window");
					winModal.setShowMinimizeButton(false);
					winModal.setIsModal(true);
					winModal.setShowModalMask(true);
					winModal.centerInPage();
					
					

					DynamicForm form = new DynamicForm();
					form.setHeight100();
					form.setWidth100();
					form.setPadding(5);
					form.setLayoutAlign(VerticalAlignment.BOTTOM);
					final TextItem textItem = new TextItem();
					textItem.setTitle("新密码");

					Button btnSave = new Button("保存密码");
					btnSave.addClickHandler(new ClickHandler() {

						public void onClick(ClickEvent event) {
							// TODO Auto-generated method stub
							MD5 md5 = new MD5();
							boundForm.setValue("USRPASSWORD", md5
									.getMD5ofStr(textItem.getValueAsString()));
							boundForm.saveData();
						}
					});

					form.setFields(textItem);
					winModal.addItem(form);
					winModal.addItem(btnSave);
					winModal.show();
				}
			}
		});
		tollStrip.addMember(btnSetPassword);
		
		
		ToolStripButton btnOpright = new ToolStripButton("设置权限");
		btnOpright.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				if(null != boundList.getSelectedRecord()) {
					String userId = boundList.getSelectedRecord().getAttributeAsString("USRID");
					String userName = boundList.getSelectedRecord().getAttributeAsString("USRNAME");
					final Window winModal = new Window();
					winModal.setWidth100();
					winModal.setHeight100();
					winModal.setTitle("用户权限设置");
					winModal.setShowMinimizeButton(true);
					winModal.setShowCloseButton(true);
					winModal.setIsModal(true);
					winModal.setShowModalMask(true);
					winModal.centerInPage();
					OpRightSetClient opRightSetClient = new OpRightSetClient();
					opRightSetClient.setHeight100();
					opRightSetClient.setWidth100();
					winModal.addItem(opRightSetClient);
					winModal.show();
					opRightSetClient.init(userId, userName, winModal);
				}else{
					SC.say("请先选择需要设置的用户");
				}
			}
		});
		//tollStrip.addMember(btnOpright);

		this.addMember(tollStrip);


		boundList = new ListGrid();
		boundList.setWidth100();
		boundList.setCanEdit(true);
		boundList.setShowFilterEditor(true);
		boundList.setShowAllRecords(true);
		boundList.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				Record record = event.getRecord();
				boundForm.editRecord(record);
				freshRightComp(record.getAttribute("USRCHECK"));
				saveBtn.enable();

				Criteria criteria = new Criteria();
				criteria.addCriteria("GMMEMBERID", record.getAttribute("USRID")
						.toString());
			}
		});
		mainLayout.addMember(boundList);
		mainLayout.addMember(tabSet);

		boundForm = new DynamicForm();
		boundForm.setNumCols(2);
		boundForm.setAutoFocus(false);
		boundForm.setDataSource(ds);
		
		bmItem = boundForm.getField("USRDEPTNAME");
		
		bmItem.addKeyUpHandler(new KeyUpHandler(){

			@Override
			public void onKeyUp(KeyUpEvent event) {
				bmItem.setValue("");
				showPopWin();
			}
		});
		
		PickerIcon dwItemPicker = new PickerIcon(PickerIcon.SEARCH, new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				// TODO Auto-generated method stub
				showPopWin();
			}
		});
		bmItem.setIcons(dwItemPicker);
		
		final SelectItem nextType = (SelectItem)boundForm.getField("USRDBID");
		final SelectItem dataRight = (SelectItem)boundForm.getField("USRCHECK");
		
		dataRight.addChangedHandler(new ChangedHandler(){
			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				nextType.setValue("");
				freshRightComp(value);
			}
		});
		
		tabEdit.setPane(boundForm);
		

		this.addMember(mainLayout);
		
		boundList.setDataSource(ds);
		boundList.fetchData();
		saveBtn.disable();

	}
	
	/**
	 * 刷新授权控件
	 * @param value
	 */
	private void freshRightComp(String value)
	{
		final SelectItem nextType = (SelectItem)boundForm.getField("USRDBID");
		
		if("QY".equals(value)) //区域选择
		{
			nextType.setDisabled(false);
			nextType.setOptionDataSource(DataSource.get("BEIYE_QY"));
			nextType.setDisplayField("QY_MC");
			nextType.setValueField("QY_MC");
			nextType.fetchData();
		}
		else if("SYB".equals(value)) //事业部
		{
			nextType.setDisabled(false);
			nextType.setOptionDataSource(DataSource.get("BEIYE_SYB"));
			nextType.setDisplayField("SYB_MC");
			nextType.setValueField("SYB_MC");
			nextType.fetchData();
		}
		else if("XM".equals(value)) //项目
		{
			nextType.setDisabled(false);
			nextType.setOptionDataSource(DataSource.get("BEIYE_XM"));
			nextType.setOptionOperationId("USER_XM");
			nextType.setDisplayField("XM_MC");
			nextType.setValueField("XM_MC");
			nextType.fetchData();
		}
		else
		{
			nextType.setDisabled(true);
		}
	}
	
	/**
	 * 根据角色生成权限
	 * @param rolID
	 */
	private void setRightByRole(String usrid, String rolID)
	{
		String insertSql = "";
		insertSql = "insert into SYS_OPRIGHT (ORUSERORROLE,ORID,ORMDID,ORCANVIEW,ORCANINSERT,ORCANEDIT,ORCANDELETE,ORCANPRINT,ORUSEROPTION)" +
		"            select ORUSERORROLE,'"+usrid+"',ORMDID,ORCANVIEW,ORCANINSERT,ORCANEDIT,ORCANDELETE,ORCANPRINT,ORUSEROPTION from SYS_OPRIGHT " +
		"            where 1=1" +
		"            and ORID = '" +rolID+ "' " +
		"            and ORMDID not in (select ORMDID from SYS_OPRIGHT where ORID='"+usrid+"')" ;
		
		ClientTools.exeSQL(insertSql);
	}
	
	boolean viewWin = false;
	private void showPopWin()
	{
		if(viewWin)
		{
			return;
		}
		else
		{
			viewWin = true;
		}
		VLayout pane = new VLayout();
    	final Window popWindow = new Window();
    	ToolStripButton configBtn = new ToolStripButton("确定");
    	final TextItem queryText = new TextItem();
    	queryText.setShowTitle(false);
    	queryText.setWidth(200);
    	queryText.setTooltip("模糊匹配，不同关键字用空格隔开，点击查询。");
    	ToolStripButton queryBtn = new ToolStripButton("查询");
    	
    	ToolStrip baseToolStrip = new ToolStrip();
    	baseToolStrip.addFormItem(queryText);
    	baseToolStrip.addButton(queryBtn);
        baseToolStrip.addButton(configBtn);
        
        pane.addMember(baseToolStrip);
        final TreeGrid list = new TreeGrid();
        list.setWidth("100%");
        list.setHeight100();
        list.setShowHeader(false);
        list.setAnimateFolderTime(100);
        list.setAnimateFolders(true);
        list.setAnimateFolderSpeed(1000);
        list.setShowSortArrow(SortArrow.CORNER);
        list.setLoadDataOnDemand(false);
        list.setCanReorderRecords(true);  
        list.setCanAcceptDroppedRecords(true); 
        list.setCanSort(false);
        list.setLeaveScrollbarGap(false);
        list.setFields(new TreeGridField("BM_MC") {
			{
				setTreeField(true);
			}
		});
        list.setDataSource(dsDepartmentInfor);
        list.setFetchOperation("enable");
        list.setAutoFetchData(true);
        
        queryText.addKeyUpHandler(new KeyUpHandler(){

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if("Enter".equals(event.getKeyName()))
				{
					String qStr = queryText.getValueAsString();
					if(qStr != null)
					{
						qStr = qStr.replace(" ", "%");
						AdvancedCriteria acri = new AdvancedCriteria();
						acri.addCriteria("BM_MC", OperatorId.CONTAINS, qStr);
						acri.addCriteria("BM_TYBZ", OperatorId.EQUALS, "N");
						list.fetchData(acri);
					}
					else
					{
						list.fetchData(new Criteria("BM_TYBZ","N"));
					}
				}
			}
    	});
        
        queryBtn.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr != null)
				{
					qStr = qStr.replace(" ", "%");
					AdvancedCriteria acri = new AdvancedCriteria();
					acri.addCriteria("BM_MC", OperatorId.CONTAINS, qStr);
					acri.addCriteria("BM_TYBZ", OperatorId.EQUALS, "N");
					list.fetchData(acri);
				}
				else
				{
					list.fetchData(new Criteria("BM_TYBZ","N"));
				}
			}
        });
        
        configBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ListGridRecord record = list.getSelectedRecord();
				if(record == null)
				{
					SC.warn("请先选择一个部门！");
					return;
				}
				String pkc = record.getAttribute("BM_BMID");
				bmItem.setValue(pkc);
				viewWin = false;
				popWindow.close();
			}
		});
        
        list.addRecordDoubleClickHandler(new RecordDoubleClickHandler(){

			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				ListGridRecord record = list.getSelectedRecord();
				if(record == null)
				{
					SC.warn("请先选择一个部门！");
					return;
				}
				String pkc = record.getAttribute("BM_BMID");
				bmItem.setValue(pkc);
				viewWin = false;
				popWindow.close();
			}
        });
        
        popWindow.addCloseClickHandler(new CloseClickHandler(){
			@Override
			public void onCloseClick(CloseClickEvent event) {
				// TODO Auto-generated method stub
				viewWin = false;
				popWindow.close();
			}
        });
        
        pane.addMember(list);
        
        popWindow.setTitle("请选择部门");
        popWindow.setIsModal(true);
        popWindow.setShowModalMask(true); 
        popWindow.setWidth(420);
        popWindow.setHeight("85%");
        popWindow.centerInPage();
        popWindow.addItem(pane);
        popWindow.show();
	}
}
