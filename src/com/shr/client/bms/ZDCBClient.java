package com.shr.client.bms;

import java.util.Map;

import com.shr.client.model.xml.XmlBasePanel;
import com.shr.client.tool.ClientTool;
import com.shr.client.tool.DialogCallback;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Criterion;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Hilite;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.ValueCallback;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class ZDCBClient extends XmlBasePanel 
{
	
	//删除按钮
	protected ToolStripButton baseBtnDel;
	
	//明细按钮
	protected ToolStripButton baseBtnMsg;
	
	//刷新按钮
	protected ToolStripButton baseBtnRefresh;
		
	//生成账单按钮
	protected ToolStripButton baseBtnCreate;
	
	//上传账单按钮
	protected ToolStripButton baseBtnSubmit;
	
	//账单表格
	protected ListGrid baseList;
		
	@Override
	public String[] canvasUI()
	{
		return new String[]{"bms/BMS_ZDCBPanel"};
	}
	
	@Override
	public void initChild(Map<String,Canvas> baseCanvas)
	{
		super.initChild(baseCanvas);
		Canvas baseCanva = baseCanvas.get("bms/BMS_ZDCBPanel");
		baseBtnDel = (ToolStripButton)baseCanva.getByLocalId("ToolStripButton3");
		baseBtnDel.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				doDelete();
			}
		});
		
		baseBtnMsg = (ToolStripButton)baseCanva.getByLocalId("ToolStripButton2");
		baseBtnMsg.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				doViewMesge();
			}
		});
		
		baseBtnRefresh = (ToolStripButton)baseCanva.getByLocalId("ToolStripButton1");
		baseBtnRefresh.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				doRefresh();
			}
		});
		
		baseBtnCreate = (ToolStripButton)baseCanva.getByLocalId("ToolStripButton0");
		baseBtnCreate.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				doCreateZD();
			}
		});
		
		baseBtnSubmit = (ToolStripButton)baseCanva.getByLocalId("ToolStripButton4");
		baseBtnSubmit.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				doSubmitZD();
			}
		});
		
		baseList = (ListGrid)baseCanva.getByLocalId("ListGrid0");
	}
	
	/**
	 * 刷新
	 */
	public void doRefresh()
	{
		baseList.invalidateCache();
		baseList.fetchData();
	}
	
	/**
	 * 删除
	 */
	public void doDelete()
	{
		if (null != baseList.getSelectedRecord())
        {
			String state = baseList.getSelectedRecord().getAttribute("ZDCB_ZT");
			if(!state.startsWith("0"))
			{
				SC.say("正在处理不能删除。");
				return;
			}
            SC.confirm("确认删除这<font color='red'>【<strong>" + baseList.getSelectedRecords().length
                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！", new BooleanCallback()
            {
                public void execute(Boolean value)
                {
                    if (value != null && value)
                    {
                    	baseList.removeSelectedData();
                    }
                }
            });
        }
        else
        {
            SC.say("请选择需要删除的数据！");
        }
	}
	
	/**
	 * 查看明细数据
	 */
	public void doViewMesge()
	{
		if (null != baseList.getSelectedRecord())
        {
			ListGridRecord sRecord = baseList.getSelectedRecord();
			String zdh = sRecord.getAttribute("ZDCB_ZDH");
			
			VLayout vl = new VLayout();
			ToolStrip ts = new ToolStrip();
			ToolStripButton butExp = new ToolStripButton();
			butExp.setTitle("导出明细");
			ToolStripButton butBack = new ToolStripButton();
			butBack.setTitle("撤回");
			ts.addButton(butExp);
			ts.addButton(butBack);
			
			final ListGrid msgList = new ListGrid();
	    	msgList.setDataSource(DataSource.get("BMS_ZDCBMX"));
	    	msgList.setShowFilterEditor(true);
	    	msgList.fetchData(new Criteria("ZDCBMX_ZDH",zdh));
	    	
	    	Hilite[] hilites = new Hilite[] {
					new Hilite() {
						{
							Criterion cri = new Criterion();
							cri.addCriteria(new Criterion("ZDCBMX_SEND", OperatorId.EQUALS, "0"));
							cri.addCriteria(new Criterion("ZDCBMX_SEND", OperatorId.EQUALS, "3"));
							cri.addCriteria(new Criterion("ZDCBMX_SEND", OperatorId.EQUALS, "4"));
							//待处理显示红色
							setCriteria(cri);
							//setBackgroundColor("#8080ff");
							setTextColor("#FF0000");
						}
					}
				};
	    	msgList.setHilites(hilites);
	    	
	    	butExp.addClickHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					DSRequest dsRequestProperties = new DSRequest();  
					//以xls格式导出
			        dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
			        dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
			        //baseListGrid.exportClientData(dsRequestProperties);
			        msgList.exportData(dsRequestProperties);
				}
			});
	    	//撤回
	    	butBack.addClickHandler(new ClickHandler(){
				@Override
				public void onClick(ClickEvent event) {
					SC.askforValue("确定要撤回选择明细？","撤回原因", new ValueCallback(){

						@Override
						public void execute(final String value) {
							if(value != null)
							{
								doBack(msgList, value);
							}
						}
					});
				}
			});
	    	
	    	vl.setMembers(ts, msgList);
	    	ClientTool.showWindow("账单详细信息", vl, "98%", "95%");
        }
		else
        {
            SC.say("请选择需要查看的数据！");
        }
	}
	
	/**
	 * 生成账单
	 */
	public void doCreateZD()
	{
		VLayout vl = new VLayout();
		
		DynamicForm form = new DynamicForm();
		final TextItem nyItem = new TextItem();
		nyItem.setName("NY");
		nyItem.setTitle("年月");
		nyItem.setTooltip("yyyymm");
		
		final SelectItem xmItem = new SelectItem();
		xmItem.setName("XM");
		xmItem.setTitle("项目");
		xmItem.setMultiple(true);
		xmItem.setOptionDataSource(DataSource.get("BEIYE_XM"));
		xmItem.setOptionOperationId("KPXM");
		xmItem.setDisplayField("XM_MC");
		xmItem.setValueField("XM_MC");
		
		ListGrid pickListProperties = new ListGrid();
		pickListProperties.setShowFilterEditor(true);
		pickListProperties.setFilterOnKeypress(false);
		ListGridField xmmcField = new ListGridField("XM_MC");
		xmItem.setPickListFields(xmmcField);
		xmItem.setPickListProperties(pickListProperties);
		
		form.setItems(nyItem, xmItem);
		vl.addMember(form);
		
		ClientTool.showYesNoDialog("生成账单", vl, "400", "160", new DialogCallback(){

			@Override
			public void executeYes() {
				String ny = nyItem.getValueAsString();
				String xm = xmItem.getValueAsString();
				
				SC.showPrompt("请稍等。。。");
				DMI.call("bms", "com.shr.server.bms.ZDCBServer",
						"doCreateZD", new RPCCallback() 
				{
					public void execute(RPCResponse response, Object rawData, RPCRequest request) 
					{
						SC.clearPrompt();
						final String result = String.valueOf(rawData);
						if(result.startsWith("ERROR:"))
						{
							SC.warn(result);
						}
						else
						{
							baseList.refreshData();
							SC.say(result);
						}
					}
				},new Object[] { ny, xm});
			}

			@Override
			public void executeCancel() {
				
			}
		});
		
	}
	
	/**
	 * 上传账单
	 */
	public void doSubmitZD()
	{
		Record[] selectedRecord = baseList.getSelectedRecords();
		
		if(selectedRecord != null && selectedRecord.length > 0)
		{
			String zdhs = ""; //多个账单用英文逗号隔开
			for(Record r : selectedRecord)
			{
				String zdzt = r.getAttribute("ZDCB_ZT");
				if(zdzt.startsWith("0"))
				{
					zdhs += r.getAttribute("ZDCB_ZDH");
					zdhs += ",";
				}
			}
			if("".equals(zdhs))
			{
				SC.say("没有需要上传的账单（必须是初始或撤回状态账单才能上传）。");
				return;
			}
			SC.showPrompt("请稍等。。。");
			DMI.call("bms", "com.shr.server.bms.ZDCBServer",
					"doSubmitZD", new RPCCallback() 
			{
				public void execute(RPCResponse response, Object rawData, RPCRequest request) 
				{
					SC.clearPrompt();
					final String result = String.valueOf(rawData);
					if(result.startsWith("ERROR:"))
					{
						SC.warn(result);
					}
					else
					{
						baseList.refreshData();
						SC.say("上传完成。");
					}
				}
			},new Object[] { zdhs});
		}
		else
		{
			SC.say("请选择需要上传的账单。");
		}
	}
	
	/**
	 * 撤回账单
	 */
	public void doBack(final ListGrid zdmxList, String msg)
	{

		Record[] selectedRecord = zdmxList.getSelectedRecords();
		
		if(selectedRecord != null && selectedRecord.length > 0)
		{
			String zdhs = ""; //多个账单用英文逗号隔开
			for(Record r : selectedRecord)
			{
				String zdzt = r.getAttribute("ZDCBMX_SEND");
				if(zdzt.startsWith("20"))
				{
					zdhs += r.getAttribute("PK_ZDCBMX");
					zdhs += ",";
				}
			}
			if("".equals(zdhs))
			{
				SC.say("没有需要撤回的账单（必须是已上传状态账单才能撤回）。");
				return;
			}
			SC.showPrompt("请稍等。。。");
			DMI.call("bms", "com.shr.server.bms.ZDCBServer",
					"doBackZDMX", new RPCCallback() 
			{
				public void execute(RPCResponse response, Object rawData, RPCRequest request) 
				{
					SC.clearPrompt();
					final String result = String.valueOf(rawData);
					if(result.startsWith("ERROR:"))
					{
						SC.warn(result);
					}
					else
					{
						baseList.refreshData();
						zdmxList.refreshData();
						SC.say("撤回完成。");
					}
				}
			},new Object[] { zdhs, msg});
		}
		else
		{
			SC.say("请选择需要撤回的账单。");
		}
	}
}
