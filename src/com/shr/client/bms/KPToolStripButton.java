package com.shr.client.bms;

import com.shr.client.tool.ClientTool;
import com.shr.client.tool.DialogCallback;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class KPToolStripButton extends ToolStripButton
{
	public KPToolStripButton(final String key, final ListGrid grid)
	{

		this.setTitle("设置开票");
		this.setVisible(false);
		this.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) 
			{
				VLayout vl = new VLayout();
				
				DynamicForm form = new DynamicForm();
				final TextItem nyItem = new TextItem();
				nyItem.setName("NY");
				nyItem.setTitle("年月");
				nyItem.setTooltip("yyyymm");
				
				final SelectItem xmItem = new SelectItem();
				xmItem.setName("XM");
				xmItem.setTitle("项目");
				xmItem.setOptionDataSource(DataSource.get("BEIYE_XM"));
				xmItem.setOptionOperationId("KPXM");
				xmItem.setDisplayField("XM_MC");
				xmItem.setValueField("XM_MC");
				ListGrid pickListProperties = new ListGrid();
				pickListProperties.setShowFilterEditor(true);
				pickListProperties.setFilterOnKeypress(false);
				ListGridField xmmcField = new ListGridField("XM_MC");
				xmItem.setPickListFields(xmmcField);
				xmItem.setPickListProperties(pickListProperties);
				
				final SelectItem kpItem = new SelectItem();
				kpItem.setName("KP");
				kpItem.setTitle("是否开票");
				kpItem.setValueMap("是","否");
				kpItem.setValue("是");
				
				form.setItems(nyItem, xmItem, kpItem);
				vl.addMember(form);
				
				ClientTool.showYesNoDialog("设置开票", vl, "400", "230", new DialogCallback(){

					@Override
					public void executeYes() {
						String ny = nyItem.getValueAsString();
						String xm = xmItem.getValueAsString();
						String kp = kpItem.getValueAsString();
						
						SC.showPrompt("请稍等。。。");
						DMI.call("bms", "com.shr.server.bms.TBDRSJServer",
								"doSetKP", new RPCCallback() 
						{
							public void execute(RPCResponse response, Object rawData, RPCRequest request) 
							{
								SC.clearPrompt();
								grid.refreshData();
								SC.say("设置完成。");
							}
						},new Object[] { ny, xm, kp, key});
					}

					@Override
					public void executeCancel() {
						
					}
				});
				
			}
		});
		
		//this.setVisible(true);
	}
	
}
