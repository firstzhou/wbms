package com.shr.client.bms;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.client.App;
import com.shr.client.model.SingleQueryPanel;
import com.shr.client.model.util.BasePanelType;
import com.shr.client.tool.ClientTool;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.Criterion;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Hilite;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class TBDRSJClient extends SingleQueryPanel 
{
	
	DynamicForm queryParemForm = null;
	
	
	//删除按钮
	protected ToolStripButton baseBtnDel;
	
	//详细按钮
	protected ToolStripButton baseBtnMsg;
	
	//查询按钮
	protected ToolStripButton baseBtnSyn;
	
	public TBDRSJClient()
	{
		this.init();
	}
	
	private void init()
	{
		baseBtnDel = new ToolStripButton("删除");
		baseBtnDel.setIcon("icons/delete.png");
		baseToolStrip.addMember(baseBtnDel);
		
		baseBtnDel.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event) 
			{
				doBaseDelete();
			}
		});
		
		baseBtnSyn = new ToolStripButton("计算数据");
		baseBtnSyn.setIcon("icons/accept.png");
		baseBtnSyn.setTooltip("选择年度与月份计算该月数据。");
		baseToolStrip.addMember(baseBtnSyn);
		
		baseBtnSyn.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event) 
			{
				Map defMap = baseQueryForm.getValues();
				if(defMap.get("ND") == null || defMap.get("YF") == null)
				{
					SC.say("年度和月份必须选择。");
					return;
				}
				String ny = "";
				String nd = String.valueOf(defMap.get("ND"));
				//String yf = String.valueOf(defMap.get("YF"));
				List<Object> yf = (List<Object>)defMap.get("YF");
				
				for(Object yue : yf)
				{
					ny = ny + nd + yue + ",";
				}
				
				Map<String, String> params = new HashMap<String, String>();
				params.put("1", App.curUserName);
				params.put("2", ny);
				
				SC.showPrompt("已提交处理，请等候...");
				DMI.call("bms", "com.shr.server.bms.TBDRSJServer",
						"doSynData", new RPCCallback() 
				{
					public void execute(RPCResponse response, Object rawData, RPCRequest request) {
						// TODO Auto-generated method stub
						SC.clearPrompt();
						SC.say("处理完成！");
						baseListGrid.invalidateCache();
						baseListGrid.fetchData();
					}
				},new Object[] { params});
			}
		});
		
		baseToolStrip.addFill();
		baseBtnMsg = new ToolStripButton("详细日志");
		baseBtnMsg.setIcon("icons/application_form_magnify.png");
		baseToolStrip.addMember(baseBtnMsg);
		
		baseBtnMsg.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event) 
			{
				doViewMsg();
			}
		});
	}
	
	/**
	 * 获得默认查询条件
	 * @return
	 */
	public Criteria getDefaultCriteria()
	{
		return null;
	}
	
	protected String initSinglePanelType(){
		return BasePanelType.BASE_PANEL_MORE;
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return DataSource.get("BMS_TBDRSJ");
	}
	
	@Override
	protected DynamicForm createQueryForm(){
		
		queryParemForm = new DynamicForm();
		queryParemForm.setNumCols(6);
		queryParemForm.setWidth(700);
		
//		SelectItem textBIAO = new SelectItem("BIAO");
//		textBIAO.setTitle("同步类型");
//		textBIAO.setWidth(100);
//		textBIAO.setValueMap(new String[] {"勤哲填报数据","多维表一到表七", "税收表", "思后行全部数据", "NC客商明细"});
//		textBIAO.setDefaultValue("勤哲填报数据");
        
		SelectItem textNY = new SelectItem("ND");
		textNY.setRequired(true);
        textNY.setTitle("年度");
        textNY.setValueMap(new String[] {"2017", "2018", "2019", "2020", "2021", "2022", "2023", "2024",
                "2025", "2026", "2027"});
        textNY.setWidth(150);

        SelectItem textYF = new SelectItem("YF");
        textYF.setRequired(true);
        textYF.setTitle("月份");
        textYF.setWidth(150);
        textYF.setMultiple(true);
        textYF.setValueMap(new String[] {"01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"});
        
		final SelectItem xmItem = new SelectItem();
		xmItem.setName("XM");
		xmItem.setTitle("项目");
//		xmItem.setRequired(true);
		xmItem.setWidth(150);
		xmItem.setMultiple(true);
		xmItem.setOptionDataSource(DataSource.get("BEIYE_XM"));
		xmItem.setOptionOperationId("KPXM");
		xmItem.setDisplayField("XM_MC");
		xmItem.setValueField("XM_MC");
		
		ListGrid pickListProperties = new ListGrid();
		pickListProperties.setShowFilterEditor(true);
		pickListProperties.setFilterOnKeypress(false);
		ListGridField xmmcField = new ListGridField("XM_MC");
		xmItem.setPickListFields(xmmcField);
		xmItem.setPickListProperties(pickListProperties);
 
        //queryParemForm.setFields(textBIAO, dwItem, textNY, textYF);
        queryParemForm.setFields(textNY, textYF,xmItem);
		return queryParemForm;
	}
	
	
	
	/**
     * 模板删除按钮方法
     */
    @Override
    public void doBaseDelete()
    {
        if (null != baseListGrid.getSelectedRecord())
        {
            SC.confirm("确认删除这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！", new BooleanCallback()
            {
                public void execute(Boolean value)
                {
                    if (value != null && value)
                    {
                        baseListGrid.removeSelectedData();
                    }
                }
            });
        }
        else
        {
            SC.say("请选择需要删除的数据！");
        }
    }
    
    private void doViewMsg()
    {
    	ListGrid msgList = new ListGrid();
    	msgList.setDataSource(DataSource.get("BMS_CALLOG"));
    	msgList.setAutoFetchData(true);
    	msgList.setShowFilterEditor(true);
    	msgList.setCanEdit(true);
    	
    	Hilite[] hilites = new Hilite[] {
				new Hilite() {
					{
						//待处理显示红色
						setCriteria(new Criterion("CALLOG_PARAM", OperatorId.EQUALS, "1"));
						//setBackgroundColor("#8080ff");
						setTextColor("#FF0000");
					}
				}
			};
    	msgList.setHilites(hilites);
		
    	ClientTool.showWindow("计算详细信息", msgList, "1000", "600");
    }
}
