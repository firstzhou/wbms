package com.shr.client.bms;

import java.util.Map;

import com.shr.client.model.xml.XmlSingleEditPanel;
import com.shr.client.tool.ClientTool;
import com.shr.client.tool.DialogCallback;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class KPXXClient extends XmlSingleEditPanel{

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return "1808029523";
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		
		ToolStripButton butKP = buildCPButton(baseListGrid);
		baseToolStrip.addFill();
		baseToolStrip.addButton(butKP);
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_KPXXForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_KPXXList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 构建开票刷新按钮
	 * @param key
	 * @param grid
	 * @return
	 */
	public ToolStripButton buildCPButton(final ListGrid grid)
	{
		ToolStripButton butKP = new ToolStripButton();
		butKP.setID("butKP");
		butKP.setTitle("刷新开票信息");
		
		butKP.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) 
			{
				VLayout vl = new VLayout();
				
				DynamicForm form = new DynamicForm();
				final TextItem nyItem = new TextItem();
				nyItem.setName("NY");
				nyItem.setTitle("年月");
				nyItem.setTooltip("yyyymm");
				
				final SelectItem xmItem = new SelectItem();
				xmItem.setName("XM");
				xmItem.setTitle("项目");
				xmItem.setOptionDataSource(DataSource.get("BEIYE_XM"));
				xmItem.setOptionOperationId("KPXM");
				xmItem.setDisplayField("XM_MC");
				xmItem.setValueField("XM_MC");
				ListGrid pickListProperties = new ListGrid();
				pickListProperties.setShowFilterEditor(true);
				pickListProperties.setFilterOnKeypress(false);
				ListGridField xmmcField = new ListGridField("XM_MC");
				xmItem.setPickListFields(xmmcField);
				xmItem.setPickListProperties(pickListProperties);
				
				form.setItems(nyItem, xmItem);
				vl.addMember(form);
				
				ClientTool.showYesNoDialog("刷新时将先删除符合条件的数据", vl, "400", "180", new DialogCallback(){

					@Override
					public void executeYes() {
						String ny = nyItem.getValueAsString();
						String xm = xmItem.getValueAsString();
						
						SC.showPrompt("请稍等。。。");
						DMI.call("bms", "com.shr.server.bms.TBDRSJServer",
								"doRebuildKP", new RPCCallback() 
						{
							public void execute(RPCResponse response, Object rawData, RPCRequest request) 
							{
								SC.clearPrompt();
								grid.refreshData();
								SC.say("刷新完成。");
							}
						},new Object[] { ny, xm});
					}

					@Override
					public void executeCancel() {
						
					}
				});
				
			}
		});
		return butKP;
	}
}
