package com.shr.client.bms;

import java.util.Map;

import com.shr.client.model.xml.XmlSingleEditPanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class TBFClient extends XmlSingleEditPanel{

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return "1807121055";
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		ToolStripButton butKP = new KPToolStripButton("BMS_TBF", baseListGrid);
		baseToolStrip.addFill();
		baseToolStrip.addButton(butKP);
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_TBFForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_TBFList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return "BMS_TBF";
	}

}
