package com.shr.client.wf;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.shr.client.model.xml.XmlFiliationEditPanel;
import com.shr.client.model.xml.XmlSingleEditPanel;
import com.shr.client.model.xml.util.XmlBaseStr;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;

public class WFClient extends XmlFiliationEditPanel {

	@Override
	public List<Map<String, Object>> buildChildContent() 
	{
		List<Map<String, Object>> childList = new ArrayList<Map<String, Object>>();
		XmlSingleEditPanel dicValuePanel = new XmlSingleEditPanel() {
			@Override
			public void initUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub

			}

			@Override
			public void initFormUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub
				DynamicForm form = (DynamicForm)canvas.get("WF_MXForm");
				final SelectItem nextType = (SelectItem)form.getField("NEXTTYPE");
				final SelectItem nextUsr = (SelectItem)form.getField("NEXTUSER");
				
				nextType.addChangedHandler(new ChangedHandler(){
					@Override
					public void onChanged(ChangedEvent event) {
						String value = String.valueOf(event.getValue());
						nextUsr.setValue("");
						
						if("1".equals(value)) //人员选择
						{
							nextUsr.setDisabled(false);
							nextUsr.setOptionDataSource(DataSource.get("USERS"));
							nextUsr.setDisplayField("USRNAME");
							nextUsr.setValueField("USRID");
							nextUsr.fetchData();
						}
						else if("2".equals(value)) //角色
						{
							nextUsr.setDisabled(false);
							nextUsr.setOptionDataSource(DataSource.get("USERGROUP"));
							nextUsr.setDisplayField("UGNAME");
							nextUsr.setValueField("UGID");
							nextUsr.fetchData();
						}
						else
						{
							nextUsr.setDisabled(true);
						}
					}
				});
			}

			@Override
			public String createToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createPanelTitle() {
				// TODO Auto-generated method stub
				return "工作流";
			}

			@Override
			public String createFormToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createEditFormUI() {
				// TODO Auto-generated method stub
				return "WF_MXForm";
			}

			@Override
			public String createQueryFormUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createListGridUI() {
				// TODO Auto-generated method stub
				return "WF_MXList";
			}

			@Override
			public String[] createDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] createFormDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String initPanelType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Criteria getDefaultCriteria() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String importKeyStr() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected DataSource createDataSource() {
				// TODO Auto-generated method stub
				return DataSource.get("SYS_WF_MX");
			}

			@Override
			protected String getAMID() {
				// TODO Auto-generated method stub
				return null;
			}
		};
		
		HashMap<String, String> dicValueFields = new HashMap<String, String>();
		dicValueFields.put("PK_SYS_WF", "PK_SYS_WF");

		Map<String, Object> childDicValueMap = new HashMap<String, Object>();
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_TITLE, "工作流步骤");
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_PANEL, dicValuePanel);
		childDicValueMap.put(XmlBaseStr.BASE_CHILD_KEYMAP, dicValueFields);
		childList.add(childDicValueMap);

		return childList;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub

	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		// return "DICCATEGORYToolStrip";
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return "工作流设置";
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "WFForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "WFList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return DataSource.get("SYS_WF");
	}

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return null;
	}

}
