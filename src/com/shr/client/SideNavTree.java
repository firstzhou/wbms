package com.shr.client;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.SortArrow;
import com.smartgwt.client.types.TreeModelType;
import com.smartgwt.client.widgets.tree.Tree;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public class SideNavTree extends TreeGrid {
    private String idSuffix = "";

    public SideNavTree() {
        setWidth100();
        setHeight100();
        setCustomIconProperty("icon");
        setAnimateFolderTime(100);
        setAnimateFolders(true);
        setAnimateFolderSpeed(1000);        
        setShowSortArrow(SortArrow.CORNER);
        setLoadDataOnDemand(false);
        setCanSort(false);
        setLeaveScrollbarGap(false);
        setDataSource(DataSource.get("query_user_right"));        
        setFields(new TreeGridField("AMNAME") {{ setTreeField(true);setTitle("功能列表"); }});      
        setAutoFetchData(true);  
    }
}
