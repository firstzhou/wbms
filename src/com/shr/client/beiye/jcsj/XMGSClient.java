package com.shr.client.beiye.jcsj;

import java.util.Map;

import com.shr.client.model.xml.XmlBasePanel;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class XMGSClient extends XmlBasePanel{
    
	ListGrid baseListGrid =null;
	
	ToolStripButton butEdit = null;
	
	ToolStripButton butSave = null;
	
	ToolStripButton butExp = null;
	
	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	@Override
	public String[] canvasUI() {
		
		return new String [] {"beiye/BEIYE_XMGS"}  ;
	}


	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	@Override
	public void initChild(Map<String, Canvas> baseCanvas) {
		super.initChild(baseCanvas);
	    Canvas  canvas =  baseCanvas.get("beiye/BEIYE_XMGS");
	    
	    baseListGrid = (ListGrid)canvas.getByLocalId("baseListGrid"); 
	    
	    butEdit = (ToolStripButton)canvas.getByLocalId("butEdit"); 
	    
	    butSave = (ToolStripButton)canvas.getByLocalId("btnSave"); 
	    
	    butExp = (ToolStripButton)canvas.getByLocalId("btnExp"); 
	    
	    
	    //编辑
	    butEdit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				baseListGrid.setCanEdit(true);
				baseListGrid.startEditing();
			}
		});
	    
	    //编辑
	    butSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				baseListGrid.saveAllEdits();
				baseListGrid.setCanEdit(false);
			}
		});
	    
	    //导出
	    butExp.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				DSRequest dsRequestProperties = new DSRequest();  
				//以xls格式导出
		        dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
		        dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
		        //baseListGrid.exportClientData(dsRequestProperties);
		        baseListGrid.exportData(dsRequestProperties);
			}
		});
	}
}
