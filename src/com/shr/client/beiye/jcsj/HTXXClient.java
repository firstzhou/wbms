package com.shr.client.beiye.jcsj;

import java.util.Map;

import com.google.gwt.dom.client.BRElement;
import com.google.gwt.user.client.DOM;
import com.shr.client.model.xml.XmlSingleEditPanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class HTXXClient extends XmlSingleEditPanel{

	
	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return "1807231847";
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub
		final DynamicForm from = (DynamicForm)canvas.get("bms/BMS_HTXXForm");
		final FormItem jffs = (FormItem)from.getItem("HTXX_JFFS");//计费方式
		final FormItem dj = (FormItem)from.getItem("HTXX_DJ");//单价
		final FormItem djgs = (FormItem)from.getItem("HTXX_DJGS");//单价公式
		final FormItem fygs = (FormItem)from.getItem("HTXX_FYGS");//费用公式
		final FormItem writeType = (FormItem)from.getItem("writeType");//书写公式方式（单价公式）
		final FormItem feeType = (FormItem)from.getItem("feeType");//书写公式方式（单价公式）
		
		changeSelect(from);
		
		jffs.addChangedHandler(new ChangedHandler(){
			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				if("1".equals(value))
				{
					dj.setVisible(true);
					djgs.setVisible(false);
					fygs.setVisible(true);
					writeType.setVisible(false);
					feeType.setVisible(true);
				}
				else if("2".equals(value))
				{
					dj.setVisible(false);
					djgs.setVisible(true);
					fygs.setVisible(true);
					writeType.setVisible(true);
					feeType.setVisible(true);
				}
				else if("3".equals(value))
				{
					dj.setVisible(false);
					djgs.setVisible(false);
					fygs.setVisible(false);
					writeType.setVisible(false);
					feeType.setVisible(false);
				}
				from.redraw();
			}
		});
	}
	
	@Override
	public void doFormDataLoadAction(DynamicForm editForm)
	{
		FormItem jffs = (FormItem)editForm.getItem("HTXX_JFFS");
		final FormItem dj = (FormItem)editForm.getItem("HTXX_DJ");
		final FormItem djgs = (FormItem)editForm.getItem("HTXX_DJGS");
		final FormItem fygs = (FormItem)editForm.getItem("HTXX_FYGS");
		final FormItem writeType = (FormItem)editForm.getItem("writeType");
		final FormItem feeType = (FormItem)editForm.getItem("feeType");
		
		if("1".equals(jffs.getValue()))
		{
			dj.setVisible(true);
			djgs.setVisible(false);
			writeType.setVisible(false);
			feeType.setVisible(true);
		}
		else if("2".equals(jffs.getValue()))
		{
			dj.setVisible(false);
			djgs.setVisible(true);
			writeType.setVisible(true);
			feeType.setVisible(true);
		}
		else if("3".equals(jffs.getValue()))
		{
			dj.setVisible(false);
			djgs.setVisible(false);
			fygs.setVisible(false);
			writeType.setVisible(false);
			feeType.setVisible(false);
		}
	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_HTXXForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_HTXXList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return "BMS_HTXX";
	}
	
	
	/**
	 * 项目编码下拉框
	 */
	public void changeSelect(DynamicForm boundForm) {
		
	    SelectItem ckItem = (SelectItem)boundForm.getItem("writeType");
	    SelectItem feeType = (SelectItem)boundForm.getItem("feeType");
	 
	    //单价公式
		ckItem.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				if(value.equals("2")) {
					showPopWin(boundForm,1);
				}
				
			}
		});
		
		//费用公式
		feeType.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				if(value.equals("2")) {
					showPopWin(boundForm,2);
				}
			}
		});
		
	}
	
	boolean viewWin = false;
	private void showPopWin(DynamicForm boundForm,int st)
	{
		if(viewWin)
		{
			return;
		}
		else
		{
			viewWin = true;
		}
		
		VLayout pane = new VLayout();
    	final Window popWindow = new Window();
    	
    	ToolStripButton add = new ToolStripButton("加");
    	ToolStripButton sub = new ToolStripButton("减");
    	ToolStripButton mul = new ToolStripButton("乘");
    	ToolStripButton div = new ToolStripButton("除");
    	ToolStripButton bracketsLeft = new ToolStripButton("（ ");
    	ToolStripButton bracketsRight = new ToolStripButton(" ）");
    	ToolStripButton greater = new ToolStripButton("大于");
    	ToolStripButton less = new ToolStripButton("小于");
    	ToolStripButton equal = new ToolStripButton("等于");
    	ToolStripButton ifis = new ToolStripButton("如果");
    	ToolStripButton tobe = new ToolStripButton("则");
    	ToolStripButton other = new ToolStripButton("其他");
    	ToolStripButton reset = new ToolStripButton("重置");
    	ToolStripButton end = new ToolStripButton("end");
    	ToolStripButton lead = new ToolStripButton("引入字段");
    	
    	ToolStripButton configBtn = new ToolStripButton("确定");
    	
    	final TextAreaItem queryText = new TextAreaItem();
    	queryText.setShowTitle(false);
    	queryText.setWidth(695);
    	queryText.setHeight(200);
    	queryText.setTooltip(" 请填写公式或按enter键查看公式语句");
    	
    	SelectItem ckItem = new SelectItem();
    	ckItem.setShowTitle(false);
		ListGrid pickListProperties = new ListGrid();
		pickListProperties.setShowFilterEditor(true);
		ListGridField ZJMField = new ListGridField("SOURCE_NAME");
		ZJMField.setWidth(100);
		ListGridField bmmcField = new ListGridField("FIELD_INFO");
		bmmcField.setWidth(100);
		ckItem.setValueField("FIELD");
		ckItem.setAllowEmptyValue(true);
		ckItem.setPickListWidth(216);
		ckItem.setPickListProperties(pickListProperties);
		ckItem.setPickListFields(ZJMField, bmmcField);
		ckItem.setOptionDataSource(DataSource.get("GS_ZD"));
		
    	ToolStrip baseToolStrip = new ToolStrip();
    	ToolStrip baseToolStrip2 = new ToolStrip();
    	
    	baseToolStrip.addButton(add);
        baseToolStrip.addButton(sub);
        baseToolStrip.addButton(mul);
        baseToolStrip.addButton(div);
        baseToolStrip.addButton(bracketsLeft);
        baseToolStrip.addButton(bracketsRight);
        baseToolStrip.addButton(greater);
        baseToolStrip.addButton(less);
        baseToolStrip.addButton(equal);
        baseToolStrip.addButton(ifis);
        baseToolStrip.addButton(tobe);
        baseToolStrip.addButton(other);
        baseToolStrip.addButton(end);
        baseToolStrip.addSpacer(10);
        baseToolStrip.addButton(reset);
        baseToolStrip.addFormItem(ckItem);
        baseToolStrip.addButton(lead);
//      baseToolStrip.addButton(configBtn);
//      baseToolStrip.addSpacer(11);
//      baseToolStrip.addResizer();
        
        configBtn.getElement().appendChild(DOM.createElement(BRElement.TAG)); 
        baseToolStrip2.getElement().appendChild(DOM.createElement(BRElement.TAG)); 
       
        baseToolStrip2.addFormItem(queryText);
        baseToolStrip2.addButton(configBtn);
        
        pane.addMember(baseToolStrip);
        pane.addMember(baseToolStrip2);
        
        queryText.addKeyUpHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if("Enter".equals(event.getKeyName()))
				{
					String qStr = queryText.getValueAsString();
			    	StringBuffer sb = new StringBuffer();
			    	
			    	if(qStr != "" && qStr != null) {
			    		qStr = qStr.toLowerCase();
			    		
			    		String[] xmArray = qStr.split(" ");
			    		for(String xmTemp : xmArray)
						{
			    			if(xmTemp == "" || xmTemp == null) {
								continue;
							}
			    			if(xmTemp.equals("case")) {
								continue;
							}
							if(xmTemp.equals("when")) {
								sb.append("如果字段 ");
								continue;
							}else
							if(xmTemp.equals("=")) {
								sb.append(" 等于 ");
								continue;
							}else
							if(xmTemp.equals("then")) {
								sb.append(" 则结果为 ");
								continue;
							}else
							if(xmTemp.equals("then")) {
								sb.append(" 结果为 ");
								continue;
							}else
							if(xmTemp.equals("+")) {
								sb.append(" 加上 ");
								continue;
							}else
							if(xmTemp.equals("-")) {
								sb.append(" 减 去 ");
								continue;
							}else
							if(xmTemp.equals("*")) {
								sb.append(" 乘以 ");
								continue;
							}else
							if(xmTemp.equals("/")) {
								sb.append(" 除以 ");
								continue;
							}else
							if(xmTemp.equals("(")) {
								sb.append(" （ ");
								continue;
							}else
							if(xmTemp.equals(")")) {
								sb.append(" ） ");
								continue;
							}else
							if(xmTemp.equals("<")) {
								sb.append(" 小于 ");
								continue;
							}else
							if(xmTemp.equals(">")) {
								sb.append(" 大于 ");
								continue;
							}else
							if(xmTemp.equals("else")) {
								sb.append(" 其他则结果为 ");
								continue;
							}else
							if(xmTemp.equals("end")) {
								sb.append(" 。 ");
//								continue;
							}else {
								sb.append(" "+xmTemp+" ");
							}
						}
			    		
//			    		queryText.setTooltip(sb.toString());
			    		SC.say("公式语句",sb.toString());
			    	} 
			    	
				}
			}
    	});
        
        //加
        add.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" + ");
					return;
				}
				queryText.setValue(qStr+" + ");
			}
        });
        
        //减
        sub.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" - ");
					return;
				}
				queryText.setValue(qStr+" - ");
			}
        });
        
        //乘
        mul.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" * ");
					return;
				}
				queryText.setValue(qStr+" * ");
			}
        });
        
        //除
        div.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" / ");
					return;
				}
				queryText.setValue(qStr+" / ");
			}
        });
        
        //括号
        bracketsLeft.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" (   ");
					return;
				}
				queryText.setValue(qStr+" (   ");
			}
        });
        
        //括号
        bracketsRight.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue("  ) ");
					return;
				}
				queryText.setValue(qStr+"  ) ");
			}
        });
        
        //大于
        greater.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" > ");
					return;
				}
				queryText.setValue(qStr+" > ");
			}
        });

        //小于
        less.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" < ");
					return;
				}
				queryText.setValue(qStr+" < ");
			}
        });

        //等于
        equal.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" = ");
					return;
				}
				queryText.setValue(qStr+" = ");
			}
        });
        
        //如果
        ifis.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" CASE WHEN ");
					return;
				}else {
					if(qStr.trim() == null || qStr.trim() == "") {
						queryText.setValue(" CASE WHEN ");
						return;
					}
					int num = count(qStr,"(");
					int num2 = count(qStr,")");
					if(num > num2) {
						queryText.setValue(qStr+" CASE WHEN ");
						return;
					}
				}
				queryText.setValue(qStr+" WHEN ");
			}
        });
        
        //则
        tobe.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("语句不合法");
					return;
				}
				queryText.setValue(qStr+" THEN ");
			}
        });
        
        //其他
        other.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("语句不合法");
					return;
				}
				queryText.setValue(qStr+" ELSE ");
			}
        });
        
        //end
        end.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("语句不合法");
					return;
				}
				queryText.setValue(qStr+" END ");
			}
        });
        
        //重置
        reset.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					return;
				}
				queryText.setValue("");
			}
        });
        
        //引入
        lead.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				String sel = ckItem.getValueAsString();
				if( sel == null || sel == "") {
					return;
				}
				if( qStr == null || qStr == "") {
					queryText.setValue(" "+sel+" ");
					return;
				}
				
				queryText.setValue(qStr+" "+sel+" ");
			}
        });
        //确定
        configBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				 
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("请填写内容！");
					return;
				}
				
//		        boolean status = qStr.toLowerCase().contains("case");  
//		        if(status){  
//		        	boundForm.setValue("HTXX_DJGS", qStr+" END");
//					boundForm.setValue("HTXX_FYGS", qStr+" END");
//		        }else{  
				
				if(st == 1) {
					boundForm.setValue("HTXX_DJGS", qStr);
				}
				if(st == 2) {
					boundForm.setValue("HTXX_FYGS", qStr);
				}	
				
//		        }  
				SelectItem ckItem = (SelectItem)boundForm.getItem("writeType");
				SelectItem feeType = (SelectItem)boundForm.getItem("feeType");
				ckItem.setValue("1");
				feeType.setValue("1");
				viewWin = false;
				popWindow.close();
			}
		});
        
        popWindow.addCloseClickHandler(new CloseClickHandler(){
			@Override
			public void onCloseClick(CloseClickEvent event) {
				// TODO Auto-generated method stub
				viewWin = false;
				popWindow.close();
			}
        });
        
        
        popWindow.setTitle("智能输入");
        popWindow.setIsModal(true);
        popWindow.setShowModalMask(true); 
        popWindow.setWidth("55%");
        popWindow.setHeight("55%");
        popWindow.centerInPage();
        popWindow.addItem(pane);
        popWindow.show();
	}
	
	
	/**
	 * 查看字符sub在字符text中出现了几次
	 * @param text
	 * @param sub
	 * @return
	 */
	 public static int count(String text,String sub){
         int count =0, start =0;
         while((start=text.indexOf(sub,start))>=0){
             start += sub.length();
             count ++;
         }
         return count;
     }
}
