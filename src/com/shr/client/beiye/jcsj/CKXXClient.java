package com.shr.client.beiye.jcsj;

import java.util.Map;

import com.google.gwt.dom.client.BRElement;
import com.google.gwt.user.client.DOM;
import com.shr.client.model.xml.XmlSingleEditPanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.TextAreaItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class CKXXClient extends XmlSingleEditPanel{

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return "1808022128";
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
//		// TODO Auto-generated method stub
//		ToolStripButton butCopy = new ToolStripButton();
//		butCopy.setTitle("复制");
//		butCopy.setIcon("icons/page_copy.png");
//		butCopy.addClickHandler(new ClickHandler(){
//			@Override
//			public void onClick(ClickEvent event) {
//				Record selectRecord = baseListGrid.getSelectedRecord();
//				if(selectRecord == null)
//				{
//					SC.say("请选择一条进行复制");
//					return;
//				}
//				defaultValue.clear();
//				defaultValue.putAll(selectRecord.toMap());
//				defaultValue.remove("PK_CKXX");
//				doMyBaseAdd();
//			}
//		});
//		
//		baseToolStrip.addButton(butCopy);
	}
	
	/**
	 * FORM中的新增方法方法
	 * @param editForm
	 */
	@Override
	public void doBaseAdd(DynamicForm editForm)
	{
		defaultValue.clear();
		super.doBaseAdd(editForm);
	}
	
	@Override
	public void doBaseAdd()
	{
		defaultValue.clear();
		super.doBaseAdd();
	}
	
	public void doMyBaseAdd()
	{
		super.doBaseAdd();
	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		Canvas canva = canvas.get("bms/BMS_CKXXForm");
		final DynamicForm form = (DynamicForm)canva.getByLocalId("baseFormEdit");
		changeSelect(form);
		SelectItem ckItem = (SelectItem)form.getItem("CKXX_CKMC");
		ListGrid pickListProperties = new ListGrid();
		pickListProperties.setShowFilterEditor(true);
		pickListProperties.setDataSource(DataSource.get("BEIYE_CK"));
		ListGridField ZJMField = new ListGridField("CK_BM");
		ZJMField.setWidth(100);
		ListGridField bmmcField = new ListGridField("CK_MC");
		bmmcField.setWidth(200);
		ckItem.setValueField("CK_MC");
		ckItem.setAllowEmptyValue(true);
		ckItem.setPickListWidth(316);
		ckItem.setPickListFields(ZJMField, bmmcField);
		ckItem.setPickListProperties(pickListProperties);
		ckItem.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				DataSource.get("BEIYE_CK").fetchData(new Criteria("CK_MC", value), new DSCallback(){

					@Override
					public void execute(DSResponse dsResponse, Object data,
							DSRequest dsRequest) {
						Record[] records = dsResponse.getData();
						if(records != null && records.length>0)
						{
							Record value = dsResponse.getData()[0];
							form.setValue("CKXX_GS", value.getAttribute("CK_GS"));
							form.setValue("CKXX_SSQY", value.getAttribute("CK_QYBM"));
							form.setValue("CKXX_CKBM", value.getAttribute("CK_BM"));
							form.setValue("CKXX_CKLX", value.getAttribute("CK_LX"));
							form.setValue("CKXX_SZCS", value.getAttribute("CK_SZCS"));
							form.setValue("CKXX_GYS", value.getAttribute("CK_GYS"));
							form.setValue("CKXX_CKDD", value.getAttribute("CK_CKDD"));
							//form.setValue("CKXX_ZCK", value.getAttribute("CK_MC"));
						}
					}
				});
				
			}
		});
	}
	
	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_CKXXForm";
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return "bms/BMS_CKXXList";
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return "BMS_CKXX";
	}
	
	/**
	 * 项目编码下拉框
	 */
	public void changeSelect(DynamicForm boundForm) {
		
//	    SelectItem ckItem = (SelectItem)boundForm.getItem("writeType");
	    SelectItem feeType = (SelectItem)boundForm.getItem("writeType");
	 
//	    //单价公式
//		ckItem.addChangedHandler(new ChangedHandler(){
//
//			@Override
//			public void onChanged(ChangedEvent event) {
//				String value = String.valueOf(event.getValue());
//				if(value.equals("2")) {
//					showPopWin(boundForm,1);
//				}
//				
//			}
//		});
//		
		//费用公式
	    feeType.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				if(value.equals("2")) {
					showPopWin(boundForm,2);
				}
			}
		});
		
	}
	
	boolean viewWin = false;
	private void showPopWin(DynamicForm boundForm,int st)
	{
		if(viewWin)
		{
			return;
		}
		else
		{
			viewWin = true;
		}
		
		VLayout pane = new VLayout();
    	final Window popWindow = new Window();
    	
    	ToolStripButton add = new ToolStripButton("加");
    	ToolStripButton sub = new ToolStripButton("减");
    	ToolStripButton mul = new ToolStripButton("乘");
    	ToolStripButton div = new ToolStripButton("除");
    	ToolStripButton bracketsLeft = new ToolStripButton("（ ");
    	ToolStripButton bracketsRight = new ToolStripButton(" ）");
    	ToolStripButton greater = new ToolStripButton("大于");
    	ToolStripButton less = new ToolStripButton("小于");
    	ToolStripButton equal = new ToolStripButton("等于");
    	ToolStripButton ifis = new ToolStripButton("如果");
    	ToolStripButton tobe = new ToolStripButton("则");
    	ToolStripButton other = new ToolStripButton("其他");
    	ToolStripButton reset = new ToolStripButton("重置");
    	ToolStripButton end = new ToolStripButton("end");
    	ToolStripButton lead = new ToolStripButton("引入字段");
    	
    	ToolStripButton configBtn = new ToolStripButton("确定");
    	
    	final TextAreaItem queryText = new TextAreaItem();
    	queryText.setShowTitle(false);
    	queryText.setWidth(695);
    	queryText.setHeight(200);
    	queryText.setTooltip(" 请填写公式或按enter键查看公式语句");
    	
    	SelectItem ckItem = new SelectItem();
    	ckItem.setShowTitle(false);
		ListGrid pickListProperties = new ListGrid();
		pickListProperties.setShowFilterEditor(true);
		ListGridField ZJMField = new ListGridField("SOURCE_NAME");
		ZJMField.setWidth(100);
		ListGridField bmmcField = new ListGridField("FIELD_INFO");
		bmmcField.setWidth(100);
		ckItem.setValueField("FIELD");
		ckItem.setAllowEmptyValue(true);
		ckItem.setPickListWidth(216);
		ckItem.setPickListProperties(pickListProperties);
		ckItem.setPickListFields(ZJMField, bmmcField);
		ckItem.setOptionDataSource(DataSource.get("GS_ZD"));
		
    	ToolStrip baseToolStrip = new ToolStrip();
    	ToolStrip baseToolStrip2 = new ToolStrip();
    	
    	baseToolStrip.addButton(add);
        baseToolStrip.addButton(sub);
        baseToolStrip.addButton(mul);
        baseToolStrip.addButton(div);
        baseToolStrip.addButton(bracketsLeft);
        baseToolStrip.addButton(bracketsRight);
        baseToolStrip.addButton(greater);
        baseToolStrip.addButton(less);
        baseToolStrip.addButton(equal);
        baseToolStrip.addButton(ifis);
        baseToolStrip.addButton(tobe);
        baseToolStrip.addButton(other);
        baseToolStrip.addButton(end);
        baseToolStrip.addSpacer(10);
        baseToolStrip.addButton(reset);
        baseToolStrip.addFormItem(ckItem);
        baseToolStrip.addButton(lead);
//      baseToolStrip.addButton(configBtn);
//      baseToolStrip.addSpacer(11);
//      baseToolStrip.addResizer();
        
        configBtn.getElement().appendChild(DOM.createElement(BRElement.TAG)); 
        baseToolStrip2.getElement().appendChild(DOM.createElement(BRElement.TAG)); 
       
        baseToolStrip2.addFormItem(queryText);
        baseToolStrip2.addButton(configBtn);
        
        pane.addMember(baseToolStrip);
        pane.addMember(baseToolStrip2);
        
        queryText.addKeyUpHandler(new KeyUpHandler(){
			@Override
			public void onKeyUp(KeyUpEvent event) {
				if("Enter".equals(event.getKeyName()))
				{
					String qStr = queryText.getValueAsString();
			    	StringBuffer sb = new StringBuffer();
			    	
			    	if(qStr != "" && qStr != null) {
			    		qStr = qStr.toLowerCase();
			    		
			    		String[] xmArray = qStr.split(" ");
			    		for(String xmTemp : xmArray)
						{
			    			if(xmTemp == "" || xmTemp == null) {
								continue;
							}
			    			if(xmTemp.equals("case")) {
								continue;
							}
							if(xmTemp.equals("when")) {
								sb.append("如果字段 ");
								continue;
							}else
							if(xmTemp.equals("=")) {
								sb.append(" 等于 ");
								continue;
							}else
							if(xmTemp.equals("then")) {
								sb.append(" 则结果为 ");
								continue;
							}else
							if(xmTemp.equals("then")) {
								sb.append(" 结果为 ");
								continue;
							}else
							if(xmTemp.equals("+")) {
								sb.append(" 加上 ");
								continue;
							}else
							if(xmTemp.equals("-")) {
								sb.append(" 减 去 ");
								continue;
							}else
							if(xmTemp.equals("*")) {
								sb.append(" 乘以 ");
								continue;
							}else
							if(xmTemp.equals("/")) {
								sb.append(" 除以 ");
								continue;
							}else
							if(xmTemp.equals("(")) {
								sb.append(" （ ");
								continue;
							}else
							if(xmTemp.equals(")")) {
								sb.append(" ） ");
								continue;
							}else
							if(xmTemp.equals("<")) {
								sb.append(" 小于 ");
								continue;
							}else
							if(xmTemp.equals(">")) {
								sb.append(" 大于 ");
								continue;
							}else
							if(xmTemp.equals("else")) {
								sb.append(" 其他则结果为 ");
								continue;
							}else
							if(xmTemp.equals("end")) {
								sb.append(" 。 ");
//								continue;
							}else {
								sb.append(" "+xmTemp+" ");
							}
						}
			    		
//			    		queryText.setTooltip(sb.toString());
			    		SC.say("公式语句",sb.toString());
			    	} 
			    	
				}
			}
    	});
        
        //加
        add.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" + ");
					return;
				}
				queryText.setValue(qStr+" + ");
			}
        });
        
        //减
        sub.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" - ");
					return;
				}
				queryText.setValue(qStr+" - ");
			}
        });
        
        //乘
        mul.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" * ");
					return;
				}
				queryText.setValue(qStr+" * ");
			}
        });
        
        //除
        div.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" / ");
					return;
				}
				queryText.setValue(qStr+" / ");
			}
        });
        
        //括号
        bracketsLeft.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" (   ");
					return;
				}
				queryText.setValue(qStr+" (   ");
			}
        });
        
        //括号
        bracketsRight.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue("  ) ");
					return;
				}
				queryText.setValue(qStr+"  ) ");
			}
        });
        
        //大于
        greater.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" > ");
					return;
				}
				queryText.setValue(qStr+" > ");
			}
        });

        //小于
        less.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" < ");
					return;
				}
				queryText.setValue(qStr+" < ");
			}
        });

        //等于
        equal.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" = ");
					return;
				}
				queryText.setValue(qStr+" = ");
			}
        });
        
        //如果
        ifis.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					queryText.setValue(" CASE WHEN ");
					return;
				}else {
					if(qStr.trim() == null || qStr.trim() == "") {
						queryText.setValue(" CASE WHEN ");
						return;
					}
					int num = count(qStr,"(");
					int num2 = count(qStr,")");
					if(num > num2) {
						queryText.setValue(qStr+" CASE WHEN ");
						return;
					}
				}
				queryText.setValue(qStr+" WHEN ");
			}
        });
        
        //则
        tobe.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("语句不合法");
					return;
				}
				queryText.setValue(qStr+" THEN ");
			}
        });
        
        //其他
        other.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("语句不合法");
					return;
				}
				queryText.setValue(qStr+" ELSE ");
			}
        });
        
        //end
        end.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("语句不合法");
					return;
				}
				queryText.setValue(qStr+" END ");
			}
        });
        
        //重置
        reset.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					return;
				}
				queryText.setValue("");
			}
        });
        
        //引入
        lead.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				String sel = ckItem.getValueAsString();
				if( sel == null || sel == "") {
					return;
				}
				if( qStr == null || qStr == "") {
					queryText.setValue(" "+sel+" ");
					return;
				}
				
				queryText.setValue(qStr+" "+sel+" ");
			}
        });
        //确定
        configBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				 
				String qStr = queryText.getValueAsString();
				if(qStr == null || qStr == "") {
					SC.warn("请填写内容！");
					return;
				}
				
//		        boolean status = qStr.toLowerCase().contains("case");  
//		        if(status){  
//		        	boundForm.setValue("HTXX_DJGS", qStr+" END");
//					boundForm.setValue("HTXX_FYGS", qStr+" END");
//		        }else{  
				
//				if(st == 1) {
//					boundForm.setValue("HTXX_DJGS", qStr);
//				}
//				if(st == 2) {
					boundForm.setValue("HTXX_FYGS", qStr);
//				}	
				
//		        }  
//				SelectItem ckItem = (SelectItem)boundForm.getItem("writeType");
				SelectItem feeType = (SelectItem)boundForm.getItem("writeType");
//				ckItem.setValue("1");
				feeType.setValue("1");
				viewWin = false;
				popWindow.close();
			}
		});
        
        popWindow.addCloseClickHandler(new CloseClickHandler(){
			@Override
			public void onCloseClick(CloseClickEvent event) {
				// TODO Auto-generated method stub
				viewWin = false;
				popWindow.close();
			}
        });
        
        
        popWindow.setTitle("智能输入");
        popWindow.setIsModal(true);
        popWindow.setShowModalMask(true); 
        popWindow.setWidth("55%");
        popWindow.setHeight("55%");
        popWindow.centerInPage();
        popWindow.addItem(pane);
        popWindow.show();
	}
	
	
	/**
	 * 查看字符sub在字符text中出现了几次
	 * @param text
	 * @param sub
	 * @return
	 */
	 public static int count(String text,String sub){
         int count =0, start =0;
         while((start=text.indexOf(sub,start))>=0){
             start += sub.length();
             count ++;
         }
         return count;
     }
}
