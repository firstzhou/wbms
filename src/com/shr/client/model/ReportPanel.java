package com.shr.client.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.Label;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class ReportPanel extends VLayout {

	// 主布局管理器
	protected VLayout baseMainLayout;

	public HTMLPane queryHTML;

	public HTMLPane exportHTML;

	// 工具条
	public ToolStrip baseToolStrip;

	public ToolStripButton baseBtnQuery;

	public Label baseBtnExXls;

	// 查询条件
	private DynamicForm baseQueryForm;

	private StringBuffer contentBuffer;

	private String reportID;

	// 初始化字符串，用于在子类里用
	private String initStr;

	public ReportPanel(String reportID) {
		this.reportID = reportID;
		initPanel();
	}

	public ReportPanel(String reportID, String initStr) {
		this.initStr = initStr;
		this.reportID = reportID;
		initPanel();
	}

	private void initPanel() {

		baseQueryForm = createQueryForm();

		baseBtnQuery = new ToolStripButton("查询");
		baseBtnQuery.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseAdd();
			}
		});

		baseBtnExXls = new Label("");
		// baseBtnExXls.addClickHandler(new ClickHandler()
		// {
		// public void onClick(ClickEvent event)
		// {
		// doBaseAdd(true);
		// }
		// });
		contentBuffer = new StringBuffer();
		queryHTML = new HTMLPane();
		queryHTML.setWidth100();
		queryHTML.setHeight100();
		// queryHTML.setHeight("90%");

		exportHTML = new HTMLPane();
		exportHTML.setWidth100();
		exportHTML.setVisible(false);
		exportHTML.setHeight100();

		// html.setContents("<iframe name='fff' style='width:100%;height:100%;border:0' src='http://"
		// + App.ip + ":"
		// + App.port + "/CPS/openobj.jsp?Object=" + id + "'></iframe>");
		queryHTML.setContents(contentBuffer.toString());

		baseToolStrip = new ToolStrip();
		baseToolStrip.setWidth100();
		baseToolStrip.addMember(baseBtnQuery);
		baseBtnExXls.setContents("导出");
		//baseToolStrip.addMember(baseBtnExXls);

		baseMainLayout = new VLayout();

		baseMainLayout.setWidth100();
		baseMainLayout.setHeight100();
		// baseMainLayout.addMember(baseToolStrip);
		// baseMainLayout.addMember(baseQueryForm);
		baseMainLayout.addMember(queryHTML);
		//baseMainLayout.addMember(exportHTML);
		this.addMember(baseToolStrip);
		this.addMember(baseQueryForm);
		this.addMember(baseMainLayout);
	}

	protected DynamicForm createQueryForm() {
		return new DynamicForm();
	}

	protected void doBaseAdd() {
		contentBuffer = new StringBuffer();
		StringBuffer srcStrBuffer = new StringBuffer();
		String url = "";
		contentBuffer.append(
				"<iframe name='fff' style='width:100%;height:100%;border:0' ")
				.append("src=\"");

		srcStrBuffer.append("/thinkbi/prstresult/show?resid=");
		srcStrBuffer.append(encodeURI(getReportID()));
		srcStrBuffer.append("&$sys_calcnow=true&$sys_showCaptionPanel=true&$sys_showParamPanel=false");

		Map<Object, Object> dynamicItemsMap = getQueryParemValues();
		if (null != dynamicItemsMap && dynamicItemsMap.size() > 0) {
			for (Map.Entry<Object, Object> entry : dynamicItemsMap.entrySet()) {
				srcStrBuffer.append("&").append(entry.getKey()).append("=");

				// 如果是list类型则转换为字符串
				if (entry.getValue() instanceof List) {
					srcStrBuffer.append("'pinjiezifuchuan");
					for (Object value : (List) entry.getValue()) {
						srcStrBuffer.append("','");
						srcStrBuffer.append(encodeURI(String.valueOf(value)));
					}
					srcStrBuffer.append("'");
				} else {
					srcStrBuffer.append(encodeURI(String.valueOf(entry.getValue())));
				}
			}
		}
		srcStrBuffer.append("&ctime=" + new Date().getTime());
		contentBuffer.append(srcStrBuffer);
		contentBuffer.append("\"></iframe>");

		// escape("中文测试") encodeURIComponent
		url = srcStrBuffer.toString();

		baseBtnExXls.setContents("<a href=\"" + url
				+ "\" title='导出' target='_blank'>导出</a>");
		queryHTML.setContents(contentBuffer.toString());

		System.out.println(">>>>" + contentBuffer.toString());
	}

	/**
	 * 获得查询参数的值，子类可以重写该方法自定义参数值
	 * 
	 * @return
	 */
	public Map<Object, Object> getQueryParemValues() {
		Map<Object, Object> mapParem = baseQueryForm.getValues();
		mapParem.remove("dwmc");
		return mapParem;
	}

	public String getReportID() {
		return reportID;
	}

	public String getInitStr() {
		return initStr;
	}

	public void setInitStr(String initStr) {
		this.initStr = initStr;
	}

	public static native String encodeURI(String url)/*-{
		return encodeURI(url);

	}-*/;

}
