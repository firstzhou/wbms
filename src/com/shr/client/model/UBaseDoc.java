package com.shr.client.model;

import com.google.gwt.core.client.GWT;
import com.shr.client.model.SingleEditPanel2;
import com.shr.client.model.util.BasePanelType;
import com.smartgwt.client.bean.BeanFactory;
import com.smartgwt.client.data.DataSource;

public class UBaseDoc extends SingleEditPanel2 {
	
//	public interface MyMetaFactory extends BeanFactory.MetaFactory{  
//        BeanFactory<TP_POSTSelectItem> getTP_POSTSelectItemFactory();  
//    }

	@Override
	protected DataSource createDataSource() {
		return DataSource.get("");
	}

	public void beforeInit() {
//		GWT.create(MyMetaFactory.class);
//		this.sDatasourceName = "tp_employeemanager";
	}

	@Override
	protected String initSinglePanelType() {
		return BasePanelType.BASE_PANEL_MORE;
	}
}
