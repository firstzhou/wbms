package com.shr.client.model.xml;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.shr.client.model.SingleEditPanel;
import com.shr.client.model.SingleQueryPanel;
import com.shr.client.model.util.BasePanelType;
import com.shr.client.model.xml.util.XmlBaseStr;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public abstract class XmlFiliationEditPanel extends XmlSingleEditPanel {

	//存放子表结构的面板
    protected HLayout baseFiliationEditPanel;
    
    protected TabSet baseFiliationTabSet;
    
    //被选择tab的id
  	protected String childSelectedTabID;
    
    //编辑子模板类型，暂时包括两种
  	protected String baseChildPanelType;
  	
  	//存放子表数据对象
    protected HashMap<String, Map<Integer, Object>> childObject;
    
	public XmlFiliationEditPanel()
	{
		init();
	}
	
	/**
	 * 初始化方法
	 */
	private void init()
	{
		childObject = new HashMap<String, Map<Integer, Object>> ();
	}
	
	/**
	 * 创建子表内容
	 * 目前必须包括三部分
	 * childMap.put(XmlBaseStr.BASE_CHILD_TITLE, 子表标题);
     * childMap.put(XmlBaseStr.BASE_CHILD_PANEL, 子表的实体);
     * childMap.put(XmlBaseStr.BASE_CHILD_KEYMAP, 子表与主表的对于关系Map<子表字段, 主表字段>);
	 * @return
	 */
	public abstract List<Map<String, Object>> buildChildContent();
	
	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * 显示与编辑区区域为模板默认
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 子表与主表对应关系Map<子表字段，值>
     */
	public void addModelChild(String title, Canvas childPanel, Map<String, String> fields)
	{
		Tab tabChild = new Tab(title);
		tabChild.addTabSelectedHandler(new TabSelectedHandler(){

            @Override
            public void onTabSelected(TabSelectedEvent event) {
                doChildTabSelected(event);
            }
        });
		tabChild.setPane(childPanel);
		baseFiliationTabSet.addTab(tabChild);
		//保存子表信息
		Map<Integer, Object> childObjs = new  HashMap<Integer, Object>();
		childObjs.put(BasePanelType.CHILD_PANEL_INDEX, childPanel);
		childObjs.put(BasePanelType.CHILD_GRID_CRITERIA_INDEX, fields);
		childObject.put(tabChild.getID(), childObjs);
		
	}
	
	/**
     * 子表Tab选择事件，设置浏览界面对应的panle
     * @param event
     */
    public void doChildTabSelected(TabSelectedEvent event){
        childSelectedTabID = event.getID();
        Map<Integer, Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null)
        {
        	Canvas childPanel = (Canvas)selectedChildObject.get(BasePanelType.CHILD_PANEL_INDEX);
        	final HashMap<String, String> tempFields = (HashMap<String, String>)(selectedChildObject.get(BasePanelType.CHILD_GRID_CRITERIA_INDEX));
        	if(childPanel instanceof XmlSingleEditPanel)
        	{
        		final XmlSingleEditPanel cp = (XmlSingleEditPanel)childPanel;
        		//给子表传递过滤条件
        		for(String mainfield : tempFields.keySet())
        		{
            		cp.addDefaultValue(mainfield, tempFields.get(mainfield));
            		cp.addDefaultCriteria(mainfield, tempFields.get(mainfield));
                 }
        		cp.doBaseQuery();
        	}
        	else if(childPanel instanceof SingleEditPanel)
        	{
        		final SingleEditPanel cp = (SingleEditPanel)childPanel;
        		//给子表传递过滤条件
        		for(String mainfield : tempFields.keySet())
        		{
        			cp.addDefaultValue(mainfield, tempFields.get(mainfield));
        			cp.addDefaultCriteria(mainfield, tempFields.get(mainfield));
                 }
        		cp.doBaseQuery();
        	}
        	else if(childPanel instanceof SingleQueryPanel)
        	{
        		final SingleQueryPanel cp = (SingleQueryPanel)childPanel;
        		//给子表传递过滤条件
        		for(String mainfield : tempFields.keySet())
        		{
        			cp.addDefaultCriteria(mainfield, tempFields.get(mainfield));
                }
        		cp.doBaseQuery();
        	}
        	//子类插入自己的设置
        	doDiyChildAction(childPanel);
        }
        onChildTabSelected(event);
    }
    
    /**
     * 子类重写该方法插入自己设置
     * @param child
     */
    public void doDiyChildAction(Canvas child){}
    
    
    /**
     * 显示编辑面板
     */
    @Override
    protected void showEditPanel(final String editType)
    {
		
    	RPCManager.cacheScreens(buildFormUIStr(), new Function() {
			@Override
			public void execute() {
				baseFiliationEditPanel = new HLayout();
				baseFiliationTabSet = new TabSet();
				baseFiliationEditPanel.addMember(baseFiliationTabSet);
				
				Tab tabEdit = new Tab(panelTitle+"概要");
				VLayout editpanel = new VLayout();
				Canvas formStrip = RPCManager.createScreen(uiFormToolStrip);
				Canvas editPanel = RPCManager.createScreen(uiEditForm);
				DynamicForm editFormTemp = null;
				if(editPanel == null)
				{
					if(baseDataSource != null)
					{
						editPanel = new Canvas();
						editFormTemp = new DynamicForm();
						editFormTemp.setWidth100();
						editFormTemp.setHeight100();
						editFormTemp.setDataSource(baseDataSource);
						editPanel.addChild(editFormTemp);
					}
					else
					{
						System.out.println("form的ui页面及ds文件都是null，无法生成EdifForm页面。");
					}
				}
				else
				{
					editFormTemp = (DynamicForm)editPanel.getByLocalId(XmlBaseStr.BASE_FORM_EDIT);
				}
				editpanel.addMember(formStrip);
				editpanel.addMember(editPanel);
				
				final DynamicForm editForm = editFormTemp;

				final ToolStripButton baseBtnSave = (ToolStripButton)formStrip.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_SAVE);
				if(baseBtnSave != null)
				{
					baseBtnSave.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							doBaseSave(editForm);
							//baseBtnSave.disable(); //初始时设置保存按钮失效
						}
					});
				}
				ToolStripButton baseBtnNew = (ToolStripButton)formStrip.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_NEW);
				if(baseBtnNew != null)
				{
					baseBtnNew.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							doBaseSaveAndAdd(editForm);
						}
					});
					if("edit".equals(editType))
					{
						baseBtnNew.setVisible(false);
					}
				}
				//如果是双击查看数据则隐藏保存和新增按钮
				if("view".equals(editType))
				{
					baseBtnNew.setVisible(false);
					baseBtnSave.setVisible(false);
				}
				tabEdit.setPane(editpanel);
				baseFiliationTabSet.addTab(tabEdit);
				
				//下面是方法补充
				if("add".equals(editType))
				{
					showEditPanel_(editForm , formStrip, editPanel, editType, baseFiliationEditPanel);
				}
				else if("edit".equals(editType) || "view".equals(editType))
				{
					final ListGridRecord record = baseListGrid.getSelectedRecord();
					List<Map<String, Object>> childList = buildChildContent();
					for(Map<String, Object> child : childList)
					{
						String title = (String)child.get(XmlBaseStr.BASE_CHILD_TITLE);
						Canvas childPane = (Canvas)child.get(XmlBaseStr.BASE_CHILD_PANEL);
						Map<String, String> childKey = (Map<String, String>)child.get(XmlBaseStr.BASE_CHILD_KEYMAP);
						//将主表与子表的关联数据传递给子表
						Map<String, String> childKeyValue = new HashMap<String, String>();
						for(String mainfield : childKey.keySet())
						{
							childKeyValue.put(mainfield, record.getAttribute(childKey.get(mainfield)));
		                }
						addModelChild(title, childPane, childKeyValue);
					}
					showEditPanel_(editForm , formStrip, editPanel, editType, baseFiliationEditPanel);
				}
			}
		});
    	
    }
    
    /**
	 * 新增保存之后将子表的tab页加上去
	 * @param editForm
	 */
	public void doFiliationBaseSaveAfter(DynamicForm editForm)
	{
		if(BasePanelType.EDIT_TYPE_ADD.equals(editType))
		{
			List<Map<String, Object>> childList = buildChildContent();
			for(Map<String, Object> child : childList)
			{
				String title = (String)child.get(XmlBaseStr.BASE_CHILD_TITLE);
				Canvas childPane = (Canvas)child.get(XmlBaseStr.BASE_CHILD_PANEL);
				Map<String, String> childKey = (Map<String, String>)child.get(XmlBaseStr.BASE_CHILD_KEYMAP);
				//将主表与子表的关联数据传递给子表
				Map<String, String> childKeyValue = new HashMap<String, String>();
				for(String mainfield : childKey.keySet())
				{
					childKeyValue.put(mainfield, editForm.getField(childKey.get(mainfield)).getValue().toString());
                }
				addModelChild(title, childPane, childKeyValue);
			}
		}
	}

    /**
	 * 模板删除按钮方法
	 */
	public void doBaseDelete(){
		if (null != baseListGrid.getSelectedRecord())
        {
            SC.confirm("确认删除这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，并且确认子表数据已删除，请谨慎操作！", new BooleanCallback()
            {
                public void execute(Boolean value)
                {
                    if (value != null && value)
                    {
                        baseListGrid.removeSelectedData();
                    }
                }
            });
        }
        else
        {
            SC.say("请选择需要删除的数据！");
        }
	}
    /**
     * 子表的tab select事件
     * @param event
     */
    protected void onChildTabSelected(TabSelectedEvent event){}
}
