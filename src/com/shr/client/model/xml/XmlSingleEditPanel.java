package com.shr.client.model.xml;

import java.util.HashMap;
import java.util.Map;

import com.shr.client.App;
import com.shr.client.model.DataImportPanel;
import com.shr.client.model.util.BasePanelType;
import com.shr.client.model.xml.util.XmlBaseStr;
import com.shr.client.tool.ClientTool;
import com.shr.server.ServerApp;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public abstract class XmlSingleEditPanel extends XmlBasePanel {
	
	//模版菜单条面板
	protected Canvas toolStripPanel;
	
	//模版Form菜单条面板
	protected Canvas formToolStripPanel;
	
	//模版查询条件面板
	protected Canvas queryPanel;
	
	//模版grid面板
	protected Canvas gridPanel;
	
	//模版查询条件Form
	protected DynamicForm queryForm;
	
	//模版listgrid
	protected ListGrid baseListGrid;
	
	//模版菜单条
	protected ToolStrip baseToolStrip;
	
	//模版FORM菜单条
	protected ToolStrip baseFormToolStrip;
	
	//模版新增按钮
	protected ToolStripButton baseBtnNew;
	
	//模版复制按钮
	protected ToolStripButton baseBtnCopy;
	
	//模版编辑按钮
	protected ToolStripButton baseBtnEdit;
	
	//模版编辑按钮
	protected ToolStripButton baseBtnDelete;
	
	//模版查询按钮
	protected ToolStripButton baseBtnQuery;
	
	//模版导出按钮
	protected ToolStripButton baseBtnExport;
	
	//模版导入按钮
	protected ToolStripButton baseBtnImp;
	
	//模版评论按钮
	protected ToolStripButton baseBtnDiscuss;
	
	//编辑窗口高度
  	protected String baseDataEditHeight;
  	
  	//编辑窗口宽度
  	protected String baseDataEditWidth;
  	
  	//存储编辑时候的默认值
  	protected Map<String, Object> defaultValue ;
  	
  	//刷新的时候默认过滤条件
  	private Map<String, Object> defaultCriteria;
  	
  	//功能标题
  	protected String panelTitle;
  	
  	//自定义菜单条
  	protected String uiToolStrip ;
  	
  	//自定义Form菜单条
  	protected String uiFormToolStrip ;
  	
  	//模版打开编辑页面样式
  	protected String panelType;
  	
  	//编辑Form文件
  	protected String uiEditForm;
  	
  	//查询面板
  	protected String uiQueryForm;
  	
  	//ListGrid文件
  	protected String uiListGrid;
  	
  	//自定义UI文件名
  	protected String[] diyUI;
  	
  	//自定义FORM界面的UI
  	protected String[] diyFormUI;
  	
  	//单表数据源
  	protected DataSource baseDataSource;
  	
  	//编辑类型
  	protected String editType;
  	
  	//双击选择的record
  	protected Record doubleSelectRecord;
  	
    //复制的record
  	protected Record copySelectRecord = null;
	
	/**
	 * 初始构造函数
	 */
	public XmlSingleEditPanel() 
	{
		init();
		//begin 获得所有的UI页面
		String[] diyUIs = null;
		if(diyUI != null)
		{
			diyUIs = new String[diyUI.length+3];
			diyUIs[0] = uiToolStrip;
			diyUIs[1] = uiListGrid;
			diyUIs[2] = uiQueryForm;
			for(int i=0; i<diyUI.length; i++)
			{
				diyUIs[i+3] = diyUI[i];
			}
		}
		else
		{
			diyUIs = new String[]{uiToolStrip, uiListGrid, uiQueryForm};
		}
		//end
		
		//初始化所有页面对象
		if(diyUIs.length > 0)
		{
			RPCManager.cacheScreens(diyUIs, new Function() {
				@Override
				public void execute() {
					initToolStrip();
					initQueryForm();
					initListGrid();
					
					Map<String, Canvas> canvas = new HashMap<String, Canvas>();
					canvas.put(uiToolStrip, toolStripPanel);
					if(uiListGrid == null)
					{
						canvas.put(XmlBaseStr.BASE_LISTGRID_FILE, gridPanel);
					}
					else
					{
						canvas.put(uiListGrid, gridPanel);
					}
					canvas.put(uiQueryForm, queryPanel);
					canvas.putAll(initDiyUI(diyUI));
					initBaseUIObject(canvas);
					initUIObject(canvas);
				}
			});
		}
		else
		{
			SC.warn("找不到基类模版的布局页面。");
		}
	}
	
	/**
	 * 初始化
	 */
	private void init()
	{
		defaultValue = new HashMap<String, Object>();
	  	
	  	defaultCriteria = new HashMap<String, Object>();
		baseDataSource = createDataSource();
		panelTitle = createPanelTitle();
		if(panelTitle == null)
		{
			panelTitle = "";
		}
		uiToolStrip = createToolStripUI();
		if(uiToolStrip == null)
		{
			uiToolStrip = XmlBaseStr.BASE_TOOL_STRIP_FILE;
		}
		uiFormToolStrip = createFormToolStripUI();
		if(uiFormToolStrip == null)
		{
			uiFormToolStrip = XmlBaseStr.BASE_FORM_TOOL_STRIP_FILE;
		}
		
		panelType = initPanelType();
		if(panelType == null)
		{
			panelType = BasePanelType.XML_BASE_PANEL_TAB;
		}
		uiEditForm = createEditFormUI();
		if(uiEditForm == null)
		{
			System.out.println("没有设置编辑面板");
		}
		uiQueryForm = createQueryFormUI();
		uiListGrid = createListGridUI();
		
		diyUI = createDiyUI();
		
		diyFormUI = createFormDiyUI();
	}
	
	/**
	 * 返回自定义的UI对象列表
	 * @return
	 */
	private Map<String, Canvas> initDiyUI(String[] uis)
	{
		Map<String, Canvas> diyCanvas = new HashMap<String, Canvas>();
		if(uis != null && uis.length > 0)
		{
			for(String ui : uis)
			{
				Canvas c = RPCManager.createScreen(ui);
				diyCanvas.put(ui, c);
			}
		}
		return diyCanvas;
	}
	
	/**
	 * 该方法定义给rightPanel用
	 * @param canvas
	 */
	public void initBaseUIObject(Map<String, Canvas> canvas)
	{
		if(getAMID()==null)
		{
			return;
		}
		String rightStr = App.curUserRight.get(getAMID());
		Canvas tool = canvas.get(createToolStripUI());
		if(tool == null)
		{
			tool = canvas.get(XmlBaseStr.BASE_TOOL_STRIP_FILE);
		}
		if(tool != null && rightStr != null)
		{
			String[] rightStrs = rightStr.split(";");
			if(rightStrs != null && rightStrs.length > 0)
			{
				for(String str : rightStrs)
				{
					String[] rs = str.split("=");
					if(rs != null && rs.length == 2)
					{
						//权限字符串中的关键字必须和xml文件中的id一样
						Canvas btn = tool.getByLocalId(rs[0]);
						if(btn != null && "N".equals(rs[1]))
						{
							btn.setVisible(false);
						}
					}
				}
			}
		}
	}
	
	/**
	 * 子类重写改方法返回当前功能的AMID
	 * 根据AMID和用户获得该用户的权限
	 * @return
	 */
	protected abstract String getAMID();
	
	/**
	 * 初始化单表数据源
	 * 子类重写该方法，创建数据源
	 * @return DataSource
	 */
	protected abstract DataSource createDataSource();
	
	/**
	 * 子类重写该方法获得UI文件上的对象
	 * @param map中包含当前加载的面板。
	 * 目前为三个面板，toolStrip，list, queryPanel，另加上自定义的UI
	 * key值为面板的文件名。
	 */
	public abstract void initUIObject(Map<String, Canvas> canvas);
	
	
	/**
	 * 子类重写该方法获取FormUI文件上的对象
	 * 
	 * @param canvas map中包含当前FOrm页面的UI文件
	 * 目前为两个，formToolStrip，editFormPanel
	 */
	public abstract void initFormUIObject(Map<String, Canvas> canvas);
	
	/**
	 * 子类调用此方法返回菜单条xml文件名
	 * 默认为XmlBaseStr.BASE_TOOL_STRIP_FILE;
	 * 菜单条的ID必须为
	 * XmlBaseStr.BASE_TOOL_STRIP = "baseToolStrip"
	 * @return
	 */
	public abstract String createToolStripUI();
	
	/**
	 * 返回该功能的名称
	 * @return
	 */
	public abstract String createPanelTitle();
	
	/**
	 * 子类调用此方法返回form菜单条xml文件名
	 * 菜单条的ID必须为
	 * XmlBaseStr.BASE_FORM_TOOL_STRIP_FILE = "BaseFormToolStrip"
	 * @return
	 */
	public abstract String createFormToolStripUI();
	
	/**
	 * 子类调用此方法返回编辑面板xml文件名
	 * 编辑Form的ID必须为
	 * XmlBaseStr.BASE_FORM_EDIT = "baseFormEdit"
	 * @return
	 */
	public abstract String createEditFormUI();
	
	/**
	 * 子类调用此方法返回查询条件面板xml文件名
	 * 查询Form的ID必须为
	 * XmlBaseStr.BASE_FORM_QUERY = "baseFormQuery"
	 * 如果返回null则不显示查询条件
	 * @return
	 */
	public abstract String createQueryFormUI();
	
	/**
	 * 子类调用此方法返回列表xml文件名
	 * 列表的ID必须为
	 * XmlBaseStr.BASE_LIST_GRID = "baseListGrid"
	 * @return
	 */
	public abstract String createListGridUI();
	
	/**
	 * 创建自己定义的UI文件
	 * @return
	 */
	public abstract String[] createDiyUI();
	
	/**
	 * 创建自己定义的编辑面板UI文件
	 * @return
	 */
	public abstract String[] createFormDiyUI();
	
	/**
	 * 设置表格在编辑时候的样式
	 * BasePanelType.XML_BASE_PANEL_TAB 为使用tab页打开
	 * BasePanelType.XML_BASE_PANEL_MORT为使用窗口打开
	 * 默认为BasePanelType.XML_BASE_PANEL_TAB
	 * @return
	 */
	public abstract String initPanelType();
	
	/**
	 * 初始化面板
	 */
	private void initToolStrip()
	{
		if(uiToolStrip != null)
		{
			toolStripPanel = RPCManager.createScreen(uiToolStrip);
			baseToolStrip = (ToolStrip)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP);
			baseBtnNew = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_NEW);
			if(baseBtnNew != null)
			{
				baseBtnNew.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseAdd();
					}
				});
			}
			baseBtnEdit = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_EDIT);
			if(baseBtnEdit != null)
			{
				baseBtnEdit.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseEdit();
					}
				});
			}
			
			baseBtnCopy = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_COPY);
			if(baseBtnCopy != null)
			{
				baseBtnCopy.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseCopy();
					}
				});
			}
			
			baseBtnDelete = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_DELETE);
			if(baseBtnDelete != null)
			{
				baseBtnDelete.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseDelete();
					}
				});
			}
			baseBtnQuery = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_QUERY);
			if(baseBtnQuery != null)
			{
				baseBtnQuery.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseQuery();
					}
				});
			}
			
			baseBtnExport = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_EXPORT);
			if(baseBtnExport != null)
			{
				baseBtnExport.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseExport();
					}
				});
				baseBtnExport.setTooltip("默认导出当前页数据。");
			}
			
			baseBtnImp = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_IMPORT);
			if(baseBtnImp != null)
			{
				baseBtnImp.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseImp();
					}
				});
				
				//如果没有定义导入字符串则隐藏导入按钮
				if(importKeyStr() == null)
				{
					baseBtnImp.setVisible(false);
				}
			}
			
			baseBtnDiscuss = (ToolStripButton)toolStripPanel.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_DISCUSS);
			if(baseBtnDiscuss != null)
			{
				baseBtnDiscuss.addClickHandler(new ClickHandler() {
					public void onClick(ClickEvent event) {
						doBaseDiscuss(false);
					}
				});
				
				//如果没有定义评论字符串则隐藏评论按钮
				if(viewDiscuss() == null || "".equals(viewDiscuss()))
				{
					baseBtnDiscuss.setVisible(false);
				}
			}
			
			baseAddMember(toolStripPanel);
		}
	}
	
	/**
	 * 初始化查询面板
	 */
	private void initQueryForm()
	{
		if(uiQueryForm != null)
		{
			queryPanel = RPCManager.createScreen(uiQueryForm);
			queryForm = (DynamicForm)queryPanel.getByLocalId(XmlBaseStr.BASE_FORM_QUERY);
			baseAddMember(queryPanel);
		}
	}
	
	
	/**
	 * 初始化listGrid
	 */
	private void initListGrid()
	{
		gridPanel = RPCManager.createScreen(uiListGrid);
		if(gridPanel == null)
		{
			if(baseDataSource != null)
			{
				gridPanel = new Canvas();
				baseListGrid = createDSListGrid();
				baseListGrid.setDataSource(baseDataSource);
				gridPanel.addChild(baseListGrid);
			}
			else
			{
				System.out.println("没有定义list的ui界面，也没有定义ds文件，无法生成listgrid。");
			}
		}
		else
		{
			baseListGrid = (ListGrid)gridPanel.getByLocalId(XmlBaseStr.BASE_LIST_GRID);
		}
		baseListGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler() {
			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				// TODO Auto-generated method stub
				doubleSelectRecord = event.getRecord();
				baseListGrid.selectRecord(doubleSelectRecord);
				if(viewDiscuss() == null || "".equals(viewDiscuss()))
				{
					doBaseEdit();
				}
				else
				{
					doBaseEdit();
					//doBaseDiscuss(true);
				}
				
			}
		});
		baseListGrid.setShowRowNumbers(true);
		baseAddMember(gridPanel);
	}
	
	/**
	 * 如果只设置ds文件则自动通过ds生成List
	 * 子类也可以重写该方法进行自定义ListGrid
	 * @return
	 */
	public ListGrid createDSListGrid()
	{
		ListGrid listGrid = new ListGrid();
		listGrid.setWidth100();
		listGrid.setHeight100();
		listGrid.setShowFilterEditor(true);
		listGrid.setAutoFetchData(true);
		return listGrid;
	}
	
	/**
	 * 新增的时候添加默认值
	 * 子类调用该方法设置默认值
	 * @param field 字段名
	 * @param value 值
	 */
	public void addDefaultValue(String field, Object value)
	{
		defaultValue.put(field, value);
	}
	
	/**
	 * 表格刷新的时候添加默认过滤条件
	 * 子类调用该方法添加默认条件，可以添加多个
	 * @param field 字段名
	 * @param value 值
	 */
	public void addDefaultCriteria(String field, Object value)
	{
		defaultCriteria.put(field, value);
	}
	
	/**
	 * 模板新增按钮方法
	 */
	public void doBaseAdd()
	{
		editType = BasePanelType.EDIT_TYPE_ADD;
		showEditPanel("add");
	}
	
	/**
	 * 保存并新增
	 * @param editForm
	 */
	public void doBaseSaveAndAdd(final DynamicForm editForm)
	{
		editForm.validate(false);
		if(validateForm(editForm) && !editForm.hasErrors())
		{
			editForm.saveData(new DSCallback(){
				@Override
				public void execute(DSResponse dsResponse, Object data,
						DSRequest dsRequest) {
					doBaseSaveAfter(editForm);
					doBaseAdd(editForm);
				}
			});
		}
	}
	
	/**
	 * FORM中的新增方法方法
	 * @param editForm
	 */
	public void doBaseAdd(DynamicForm editForm)
	{
		editForm.editNewRecord();
		editForm.setValues(defaultValue);
	}
	
	/**
	 * 模板编辑按钮方法
	 */
	public void doBaseEdit()
	{
		if(baseListGrid.getSelectedRecords() == null || baseListGrid.getSelectedRecords().length != 1)
		{
			SC.warn("请选择一条数据进行编辑！");
		}
		else
		{
			editType = BasePanelType.EDIT_TYPE_EDIT;
			showEditPanel("edit");
		}
	}
	
	/**
	 * 模板复制按钮方法
	 */
	public void doBaseCopy()
	{
		if(baseListGrid.getSelectedRecords() == null || baseListGrid.getSelectedRecords().length != 1)
		{
			SC.warn("请选择一条数据进行复制！");
		}
		else
		{
			for(Record r : baseListGrid.getSelectedRecords())
			{
				copySelectRecord = r;
			}
			doBaseAdd();
		}
	}
	
	/**
	 * 模板编辑按钮方法
	 */
	public void doBaseView()
	{
		if(baseListGrid.getSelectedRecords() == null || baseListGrid.getSelectedRecords().length != 1)
		{
			SC.warn("请选择一条数据进行编辑！");
		}
		else
		{
			showEditPanel("view");
		}
	}
	
	/**
	 * 显示评论页面
	 */
	public void doBaseDiscuss(final boolean isDoubleClick)
	{
		if(baseListGrid.getSelectedRecords() == null || baseListGrid.getSelectedRecords().length != 1)
		{
			SC.warn("请选择一条数据进行评论！");
		}
		else
		{
			RPCManager.cacheScreens(buildFormUIStr(), new Function() {
				@Override
				public void execute() {
					//begin 初始化Form面板，给按钮添加事件
					Canvas editPanel = RPCManager.createScreen(uiEditForm);
					DynamicForm editFormTemp = null;
					if(editPanel == null)
					{
						if(baseDataSource != null)
						{
							editFormTemp = createDSEditForm();
							editFormTemp.setDataSource(baseDataSource);
						}
						else
						{
							System.out.println("form的ui页面及ds文件都是null，无法生成EdifForm页面。");
						}
					}
					else
					{
						editFormTemp = (DynamicForm)editPanel.getByLocalId(XmlBaseStr.BASE_FORM_EDIT);
					}
					final DynamicForm editForm = editFormTemp;
					editForm.editSelectedData(baseListGrid);
					//获得主键的值
					String pkObject = null;
					String viewObject = null;
					String tableName = null;
					String primaryKey = null;
					String whrid = null;
					DataSource ds = baseListGrid.getDataSource();
					tableName = ds.getID(); //表名
					Record fields = ds.getPrimaryKeyFields();
			    	String[] keyField = fields.getAttributes();
			    	
					if(keyField != null && keyField.length >= 1)
					{
						primaryKey = keyField[0];
						whrid = baseListGrid.getSelectedRecord().getAttribute("WHRID");
						pkObject = baseListGrid.getSelectedRecord().getAttribute(keyField[0]);
						viewObject = baseListGrid.getSelectedRecord().getAttribute(viewDiscuss());
						if(pkObject == null)
						{
							SC.warn("List中必须包含主键属性才能用此功能！");
						}
					}
					else
					{
						SC.warn("必须设置主键才能用此功能！");
					}
					XmlDiscussClient dis = new XmlDiscussClient(editForm, tableName, primaryKey, 
							pkObject, viewObject, whrid, isDoubleClick);
					ClientTool.open("pinglun"+pkObject.replace("-", ""), "评论"+panelTitle, dis);
				}
			});
		}
	}
	
	/**
	 * 模板编辑按钮方法
	 */
	public void doBaseEdit(DynamicForm editForm)
	{
		editForm.editSelectedData(baseListGrid);
	}
	
	/**
	 * 弹出编辑面板
	 * @param editType 编辑类型 新增（"add"），编辑（"edit"）
	 */
	protected void showEditPanel(final String editType)
	{
		RPCManager.cacheScreens(buildFormUIStr(), new Function() {
			@Override
			public void execute() {
				VLayout editpanel = new VLayout();
				
				//begin 初始化Form面板，给按钮添加事件
				Canvas formStrip = RPCManager.createScreen(uiFormToolStrip);
				Canvas editPanel = RPCManager.createScreen(uiEditForm);
				DynamicForm editFormTemp = null;
				if(editPanel == null)
				{
					if(baseDataSource != null)
					{
						editPanel = new Canvas();
						editFormTemp = createDSEditForm();
						editFormTemp.setDataSource(baseDataSource);
						editPanel.addChild(editFormTemp);
					}
					else
					{
						System.out.println("form的ui页面及ds文件都是null，无法生成EdifForm页面。");
					}
				}
				else
				{
					editFormTemp = (DynamicForm)editPanel.getByLocalId(XmlBaseStr.BASE_FORM_EDIT);
				}
				editpanel.addMember(formStrip);
				editpanel.addMember(editPanel);
				
				final DynamicForm editForm = editFormTemp;

				final ToolStripButton baseBtnSave = (ToolStripButton)formStrip.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_SAVE);
				if(baseBtnSave != null)
				{
					baseBtnSave.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							doBaseSave(editForm);
						}
					});
				}
				ToolStripButton baseBtnNew = (ToolStripButton)formStrip.getByLocalId(XmlBaseStr.BASE_TOOL_STRIP_BUTTON_NEW);
				if(baseBtnNew != null)
				{
					baseBtnNew.addClickHandler(new ClickHandler() {
						public void onClick(ClickEvent event) {
							doBaseSaveAndAdd(editForm);
						}
					});
					if("edit".equals(editType))
					{
						baseBtnNew.setVisible(false);
					}
				}
				//如果是双击查看数据则隐藏保存和新增按钮
				if("view".equals(editType))
				{
					baseBtnNew.setVisible(false);
					baseBtnSave.setVisible(false);
				}
				//end
				//下面是方法补充
				showEditPanel_(editForm , formStrip, editPanel, editType, editpanel);
			}
		});
	}
	
	/**
	 * 建立Form的定义的UI文件
	 * @return
	 */
	protected String[] buildFormUIStr()
	{
		//begin 获得所有的UI页面
		String[] diyFormUIs = null;
		if(diyFormUI != null)
		{
			diyFormUIs = new String[diyFormUI.length+2];
			diyFormUIs[0] = uiFormToolStrip;
			diyFormUIs[1] = uiEditForm;
			for(int i=0; i<diyFormUI.length; i++)
			{
				diyFormUIs[i+2] = diyFormUI[i];
			}
		}
		else
		{
			diyFormUIs = new String[]{uiFormToolStrip, uiEditForm};
		}
		//end
		return diyFormUIs;
	}
	
	/**
	 * showEditPanel方法的补充，主要为主从模版不用重新再写该方法使用
	 * @param editForm
	 * @param tool
	 * @param editPanel
	 * @param editType
	 * @param panel
	 */
	protected void showEditPanel_(DynamicForm editForm , Canvas tool, Canvas editPanel, String editType, Canvas panel)
	{
		/**
		 * begin 调用子类自定义的一些事件或处理
		 */
		Map<String, Canvas> canvas = new HashMap<String, Canvas>();
		canvas.put(uiFormToolStrip, tool);
		if(uiEditForm == null)
		{
			canvas.put(XmlBaseStr.BASE_FORM_FILE, editPanel);
		}
		else
		{
			canvas.put(uiEditForm, editPanel);
		}
		Map<String, Canvas> temMap = initDiyUI( buildFormUIStr());
		temMap.remove(uiFormToolStrip);
		temMap.remove(uiEditForm);
		canvas.putAll(temMap);
		initFormUIObject(canvas);
		/**
		 * end
		 */
		
		if("add".equals(editType))
		{
			if(copySelectRecord != null )
			{
				editForm.editNewRecord(copySelectRecord);
				copySelectRecord = null;
			}
			else
			{
				editForm.editNewRecord();
				editForm.setValues(defaultValue);
			}
			
			if(BasePanelType.XML_BASE_PANEL_MORE.equals(panelType))
			{
				ClientTool.showWindow("新增"+panelTitle, panel, baseDataEditWidth, baseDataEditHeight);
			}
			else
			{
				//设置用tab打开
				String dsid = editForm.getDataSource().getID();
				ClientTool.open("new"+dsid, "新增"+panelTitle, panel);
			}
		}
		else if("edit".equals(editType))
		{
			
			editForm.editRecord(baseListGrid.getSelectedRecord());
			if(BasePanelType.XML_BASE_PANEL_MORE.equals(panelType))
			{
				ClientTool.showWindow("编辑"+panelTitle, panel, baseDataEditWidth, baseDataEditHeight);
			}
			else
			{
				/**设置使用tab打开**/
				String id = null;
				DataSource ds = editForm.getDataSource();
				Record fields = ds.getPrimaryKeyFields();
            	String[] keyField = fields.getAttributes();
        		if(keyField != null && keyField.length >= 1)
        		{
        			id = "tab_"+baseListGrid.getSelectedRecord().getAttribute(keyField[0])
        					.replace("-", "").replace(":", "").replace(".", "")
        					+"_edit";
        		}
        		else
        		{
        			SC.warn("必须设置主键才能编辑！");
        		}
				ClientTool.open(id, "编辑"+panelTitle, panel);
			}
		}
		else if("view".equals(editType))
		{
			editForm.editSelectedData(baseListGrid);
			//editForm.setDisabled(true);
			if(BasePanelType.XML_BASE_PANEL_MORE.equals(panelType))
			{
				ClientTool.showWindow("查看"+panelTitle, panel, baseDataEditWidth, baseDataEditHeight);
			}
			else
			{
				/**设置使用tab打开**/
				String id = null;
				DataSource ds = editForm.getDataSource();
				Record fields = ds.getPrimaryKeyFields();
            	String[] keyField = fields.getAttributes();
        		if(keyField != null && keyField.length >= 1)
        		{
        			id = "tab_"+baseListGrid.getSelectedRecord().getAttribute(keyField[0]).replace("-", "")+"_view";
        		}
        		else
        		{
        			SC.warn("必须设置主键才能查看！");
        		}
				ClientTool.open(id, "查看"+panelTitle, panel);
			}
		}
		doFormDataLoadAction(editForm);
	}
	
	/**
	 * form 加载数据后事件
	 * @param editForm
	 */
	public void doFormDataLoadAction(DynamicForm editForm){}
	
	
	/**
	 * 定义通过ds生成的Form
	 * 子类可以重写改方法定义自己的Form
	 * @return
	 */
	public DynamicForm createDSEditForm()
	{
		DynamicForm ef = new DynamicForm();
		
		ef.setWidth100();
		ef.setHeight100();
		return ef;
	}
	
	/**
	 * 子类重写校验是否可以删除
	 * @return
	 */
	public boolean doBaseDeleteBefore()
	{
		return true;
	}
	
	/**
	 * 模板删除按钮方法
	 */
	public void doBaseDelete(){
		
		if (null != baseListGrid.getSelectedRecord())
        {
			if(doBaseDeleteBefore())
			{
	            SC.confirm("确认删除这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
	                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！", new BooleanCallback()
	            {
	                public void execute(Boolean value)
	                {
	                    if (value != null && value)
	                    {
	                        baseListGrid.removeSelectedData();
	                    }
	                }
	            });
			}
        }
        else
        {
            SC.say("请选择需要删除的数据！");
        }
	}
	
	/**
	 * 模板保存按钮方法
	 */
	public void doBaseSave(final DynamicForm editForm)
	{
		
		editForm.validate(false);
		if(validateForm(editForm) && !editForm.hasErrors())
		{
			SC.showPrompt("正在保存，请稍后。。。");
			//放在回调函数里进行提示保存成功，为可以增加校验控制。
			editForm.saveData(new DSCallback(){
				@Override
				public void execute(DSResponse dsResponse, Object data,
						DSRequest dsRequest) {
					doFiliationBaseSaveAfter(editForm);
					doBaseSaveAfter(editForm);
					SC.clearPrompt();
					SC.say("保存完成。", new BooleanCallback(){
						@Override
						public void execute(Boolean value) {
							// TODO Auto-generated method stub
							ClientTool.closeTab(ClientTool.CHILD_SELECT_TAB);
						}
					});
				}
			});
		}
	}
	
	/**
	 * 保存之后执行方法
	 * 该方法在保存操作已经提交之后调用
	 * @param editForm
	 */
	public void doBaseSaveAfter(DynamicForm editForm)
	{
		
	}
	
	/**
	 * 保存之后执行方法
	 * 该方法在保存操作已经提交之后调用
	 * @param editForm
	 */
	public void doFiliationBaseSaveAfter(DynamicForm editForm)
	{
		
	}
	
	/**
	 * 子类重写该方法，对form中的数据进行校验
	 * @param editForm
	 * @return
	 */
	public boolean validateForm(DynamicForm editForm)
	{
		return true;
	}
	
	/**
	 * 模板查询按钮方法
	 */
	public void doBaseQuery()
	{
		baseListGrid.invalidateCache();
		Criteria criteria = getDefaultCriteria();
		AdvancedCriteria dcriteria = null;
		if(criteria != null)
		{
			dcriteria = criteria.asAdvancedCriteria();
		}
		//添加默认的过滤条件
		for(String fileld : defaultCriteria.keySet())
		{
			if(dcriteria == null)
			{
				dcriteria = new AdvancedCriteria();
			}
			dcriteria.addCriteria(fileld, OperatorId.EQUALS, String.valueOf(defaultCriteria.get(fileld)));
		}
		
		if(queryForm != null)
		{
			if(dcriteria == null)
			{
				dcriteria = new AdvancedCriteria();
			}
			dcriteria.addCriteria(queryForm.getValuesAsCriteria());
		}
		if(dcriteria == null)
		{
			baseListGrid.fetchData();
		}
		else
		{
			baseListGrid.fetchData(dcriteria);
		}
	}
	
	/**
	 * 获得默认查询条件
	 * 子类重写该方法，设置自定义的过滤条件
	 * @return
	 */
	public abstract Criteria getDefaultCriteria();
	
	/**
	 * 模板导出按钮方法
	 */
	public void doBaseExport()
	{
		DSRequest dsRequestProperties = new DSRequest();  
		//以xls格式导出
        dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
        dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
        //baseListGrid.exportClientData(dsRequestProperties);
        baseListGrid.exportData(dsRequestProperties);
	}
	
	 /**
     * 子类重写该方法，返回导入模版的键值
     * 该键值作为模版名称，和配置文件中的Mapping的ID
     * @return
     */
    public abstract String importKeyStr();
	
	/**
	 * 模板导入按钮方法
	 */
	public void doBaseImp(){
		if(importKeyStr() != null)
		{
			DataImportPanel impPanel = new DataImportPanel(importKeyStr());
			final Window winModal = new Window();
			winModal.setWidth(500);
			winModal.setHeight(200);
			winModal.setTitle("导入数据");
			winModal.setShowMinimizeButton(false);
			winModal.setIsModal(true);
			winModal.setShowModalMask(true);
			winModal.centerInPage();
			winModal.addItem(impPanel);
			winModal.show();
		}
		else
		{
			SC.say("没有设定导入参数！");
		}
	}
	
	/**
	 * 控制模版是否显示评论按钮
	 * @return 返回需要评论的标题字段
	 */
	public String viewDiscuss()
	{
		return null;
	}
	
	/**
     * 获得编辑窗口高度，只有模板类型为SINGLE_PANEL_MORE时才有效
     * @return
     */
	public String getBaseDataEditHeight() {
		return baseDataEditHeight;
	}

	/**
	 * 设置编辑窗口高度，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @param baseDataEditHeight
	 */
	public void setBaseDataEditHeight(String baseDataEditHeight) {
		this.baseDataEditHeight = baseDataEditHeight;
	}

	/**
	 * 获得编辑窗口宽度，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @return
	 */
	public String getBaseDataEditWidth() {
		return baseDataEditWidth;
	}

	/**
	 * 设置编辑窗口宽度，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @param baseDataEditWidth
	 */
	public void setBaseDataEditWidth(String baseDataEditWidth) {
		this.baseDataEditWidth = baseDataEditWidth;
	}
	
	
	/**
	 * 添加批量导出功能
	 */
	public void addExport(final String expKey, final String pk)
	{
//		baseListGrid.setSelectionType(SelectionStyle.SIMPLE);
//		baseListGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
		baseToolStrip.addFill();
		
		ToolStripButton butExp = new ToolStripButton();
		butExp.setTitle("打印导出");
		butExp.setIcon("icons/page_go.png");
		butExp.setTooltip("按住Ctr或Shift键，可以选择多个单据一起导出。");
		//新增单据
		butExp.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) 
			{
				ListGridRecord[] records = baseListGrid.getSelectedRecords();
				if(records == null || records.length == 0)
				{
					SC.warn("请选择单据再导出。");
					return;
				}
				//导入参数，多个用分号隔开
				String params = "";
				for(ListGridRecord rec : records)
				{
					params += rec.getAttribute(pk);
					params += ";";
				}
				SC.showPrompt("正在导出，请稍后...");
				DMI.call("shr", "DataExport",
						"excutExp", new RPCCallback() {
							@Override
							public void execute(RPCResponse response, Object rawData,
									RPCRequest request) {
								SC.clearPrompt();
								if(rawData != null)
								{
									SC.say("<a href='/"+App.zhangtao+"/UploadFiles/"+String.valueOf(rawData)+"'>右击目标另存为下载</a>");
								}
								else
								{
									SC.say("下载失败！");
								}
							}
				}, new Object[]{expKey, params});
			}
		});
		baseToolStrip.addButton(butExp);
	}
}
