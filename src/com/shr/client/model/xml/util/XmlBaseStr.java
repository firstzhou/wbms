package com.shr.client.model.xml.util;

public class XmlBaseStr 
{
	/**
	 * XmlBasePanel的对应的ui文件名
	 */
	public static final String BASE_TOOL_STRIP_FILE = "BaseToolStrip";

	public static final String BASE_FORM_TOOL_STRIP_FILE = "BaseFormToolStrip";
	
	public static final String BASE_FORM_FILE = "BaseEditForm";
	
	public static final String BASE_LISTGRID_FILE = "BaseListGrid";
	
	public static final String BASE_TOOL_STRIP = "baseToolStrip";
	
	public static final String BASE_FORM_TOOL_STRIP = "baseFormToolStrip";
	
	public static final String BASE_TOOL_STRIP_BUTTON_NEW = "butNew";
	
	public static final String BASE_TOOL_STRIP_BUTTON_COPY = "butCopy";
	
	public static final String BASE_TOOL_STRIP_BUTTON_EDIT = "butEdit";
	
	public static final String BASE_TOOL_STRIP_BUTTON_DELETE = "butDel";
	
	public static final String BASE_TOOL_STRIP_BUTTON_SAVE = "butSave";
	
	public static final String BASE_TOOL_STRIP_BUTTON_QUERY = "butQuery";
	
	public static final String BASE_TOOL_STRIP_BUTTON_VIEW = "butView";
	
	public static final String BASE_TOOL_STRIP_BUTTON_CANCEL = "butCancel";
	
	public static final String BASE_TOOL_STRIP_BUTTON_EXPORT = "butExport";
	
	public static final String BASE_TOOL_STRIP_BUTTON_IMPORT = "butImport";
	
	public static final String BASE_TOOL_STRIP_BUTTON_DISCUSS = "butDiscuss";
	
	public static final String BASE_FORM_QUERY = "baseFormQuery";
	
	public static final String BASE_FORM_EDIT = "baseFormEdit";
	
	public static final String BASE_LIST_GRID = "baseListGrid";
	
	//表示主从表的标题键
	public static final String BASE_CHILD_TITLE = "BASE_CHILD_TITLE";
	
	//表示主从表子表对象键
	public static final String BASE_CHILD_PANEL = "BASE_CHILD_PANEL";
	
	//表示主从表的对应关系键
	public static final String BASE_CHILD_KEYMAP = "BASE_CHILD_KEYMAP";
	
}
