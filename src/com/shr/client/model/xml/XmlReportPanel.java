package com.shr.client.model.xml;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.shr.client.App;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public abstract class XmlReportPanel extends XmlBasePanel {
	
	protected ToolStripButton butQuery = null;
	
	protected ToolStripButton butExp = null;
	
	protected ToolStripButton butSubmit = null;
	
	protected DynamicForm queryForm = null;
	
	protected HTMLPane htmlPane = null;
	
	StringBuffer contentBuffer;
	
	@Override
	public String[] canvasUI()
	{
		return new String[]{getReportUI()};
	}
	
	@Override
	public void initChild(Map<String,Canvas> baseCanvas)
	{
		super.initChild(baseCanvas);
		
		Canvas canvas = baseCanvas.get(getReportUI());
		butQuery = (ToolStripButton)canvas.getByLocalId("butQuery");
		butExp = (ToolStripButton)canvas.getByLocalId("butExp");
		butSubmit = (ToolStripButton)canvas.getByLocalId("butSubmit");
		
		htmlPane = (HTMLPane)canvas.getByLocalId("htmlPane");
		
		queryForm = (DynamicForm)canvas.getByLocalId("queryForm");
		
		butQuery.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				doBaseQuery();
			}
		});
	}
	
	protected void doBaseQuery() {
		contentBuffer = new StringBuffer();
		StringBuffer srcStrBuffer = new StringBuffer();
		String url = "";
		contentBuffer.append(
				"<iframe name='fff' style='width:100%;height:100%;border:0' ")
				.append("src=\"");

		srcStrBuffer.append("/thinkbi/prstresult/show?resid=");
		srcStrBuffer.append(getReportKey());
		srcStrBuffer.append("&$sys_calcnow=true&$sys_showCaptionPanel=true&$sys_showParamPanel=false");

		Map<Object, Object> dynamicItemsMap = getQueryParemValues();
		if (null != dynamicItemsMap && dynamicItemsMap.size() > 0) {
			for (Map.Entry<Object, Object> entry : dynamicItemsMap.entrySet()) {
				srcStrBuffer.append("&").append(entry.getKey()).append("=");

				// 如果是list类型则转换为字符串
				if (entry.getValue() instanceof List) {
					srcStrBuffer.append("'pinjiezifuchuan");
					for (Object value : (List) entry.getValue()) {
						srcStrBuffer.append("','");
						srcStrBuffer.append(encodeURI(String.valueOf(value)));
					}
					srcStrBuffer.append("'");
				} else {
					srcStrBuffer.append(encodeURI(String.valueOf(entry.getValue())));
				}
			}
		}
		srcStrBuffer.append("&user=thinkBQ");
		srcStrBuffer.append("&password=123qwe$r");
		
		srcStrBuffer.append("&ctime=" + new Date().getTime());
		srcStrBuffer.append("&cuser=" + App.curUserId);
		srcStrBuffer.append("&cdepart=" + App.curUserDepart);
		srcStrBuffer.append("&cuserName=" + App.curUserName);
		srcStrBuffer.append("&cdepartName=" + App.curUserDepartName);
		srcStrBuffer.append("&crole=" + App.curUserRole);
		srcStrBuffer.append("&curUserDataRight=" + App.curUserDataRight);
		srcStrBuffer.append("&curUserRightContent=" + App.curUserRightContent);
		contentBuffer.append(srcStrBuffer);
		contentBuffer.append("\"></iframe>");

		// escape("中文测试") encodeURIComponent
		url = srcStrBuffer.toString();

		htmlPane.setContents(contentBuffer.toString());

		System.out.println(">>>>" + contentBuffer.toString());
	}
	
	public abstract String getReportUI();
	
	public abstract Map<Object, Object> getQueryParemValues();
	
	public abstract String getReportKey();
	
	public static native String encodeURI(String url)/*-{
		return encodeURI(url);
    }-*/;
}
