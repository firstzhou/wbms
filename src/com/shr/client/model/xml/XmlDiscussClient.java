package com.shr.client.model.xml;

import java.util.ArrayList;

import com.shr.client.App;
import com.shr.shared.ClientTools;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.RichTextEditor;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.viewer.DetailViewer;
import com.smartgwt.client.widgets.viewer.DetailViewerRecord;
public class XmlDiscussClient extends XmlBasePanel
{
	
	DynamicForm viewForm;
	String pkObject;
	String tableName;
	String primaryColumn;
	String viewObject;
	String objectWHR;
	boolean showForm;
	
	public XmlDiscussClient(DynamicForm viewForm, String tableName, String primaryKey, 
			String pkObject, String vewObject, String objectWHR, boolean showForm)
	{
		this.pkObject = pkObject;
		this.viewForm = viewForm;
		this.tableName = tableName;
		this.primaryColumn = primaryKey;
		this.viewObject = vewObject;
		this.objectWHR = objectWHR;
		this.showForm = showForm;
		init();
	}
	
	private void init()
	{
		final Canvas layout = new Canvas();
		String[] sScteens = new String[1];
		sScteens[0] = "CDiscuss";

		RPCManager.cacheScreens(sScteens, new Function() {
			@Override
			public void execute() {
				Canvas discussScreen = RPCManager.createScreen("CDiscuss");
				final RichTextEditor richTextEditor = (RichTextEditor)discussScreen.getByLocalId("richTextEditor");

				final VLayout gyCanvas = (VLayout)discussScreen.getByLocalId("VLayout6");
				final HLayout plCanvas = (HLayout)discussScreen.getByLocalId("HLayout0");
				
				gyCanvas.addMember(viewForm);
				final DetailViewer discussView = (DetailViewer)discussScreen.getByLocalId("DetailViewer0");
				discussView.setEmptyMessage("没有评论");
				final Criteria criteria = new Criteria();
				criteria.addCriteria("pk_object",pkObject);
				discussView.fetchData(criteria, new DSCallback(){
					@Override
					public void execute(DSResponse dsResponse, Object data,
							DSRequest dsRequest) {
						DetailViewerRecord[] record = discussView.getData();
						//记录该评论已经被查看
						if(record != null && record.length > 0)
						{
							String[] sql = new String[2];
							sql[0] = "delete from CDiscuss_temp where pk_object='" +pkObject+"' and WHRID='"+App.curUserId+"'";
							sql[1] = "insert into CDiscuss_temp(pk_object, WHRID) values('"+pkObject+"','"+App.curUserId+"')";
							ClientTools.exeSQLs(sql);
						}
					}
				});
				
				final DynamicForm discussForm = (DynamicForm)discussScreen.getByLocalId("DynamicForm1");
				IButton discussBut = (IButton)discussScreen.getByLocalId("IButton1");
				discussBut.addClickHandler(new ClickHandler(){

					@Override
					public void onClick(ClickEvent event) {
						String content = richTextEditor.getValue();
						if(content != null && !"".equals(content))
						{
							discussForm.editNewRecord();
							discussForm.setValue("pk_object", pkObject);
							discussForm.setValue("objectTable", tableName);
							discussForm.setValue("objectColumnKey", primaryColumn);
							discussForm.setValue("viewObject", viewObject);
							discussForm.setValue("discussContent", content);
							discussForm.setValue("objectuserid", objectWHR);
							
							discussForm.saveData(new DSCallback(){
								@Override
								public void execute(DSResponse dsResponse,
										Object data, DSRequest dsRequest) 
								{
									richTextEditor.setValue("");
									discussView.fetchData(criteria);
									
									//记录该评论已经被查看
									String[] sql = new String[2];
									sql[0] = "delete from CDiscuss_temp where pk_object='" +pkObject+"'";
									sql[1] = "insert into CDiscuss_temp(pk_object, WHRID) values('"+pkObject+"','"+App.curUserId+"')";
									ClientTools.exeSQLs(sql);
								}
							});
						}
					}});
				
				//结案
				final DynamicForm jaForm = (DynamicForm)discussScreen.getByLocalId("DynamicForm2");
				jaForm.fetchData(new Criteria("pk_object", pkObject), new DSCallback(){

					@Override
					public void execute(DSResponse dsResponse, Object data,
							DSRequest dsRequest) {
						// TODO Auto-generated method stub
						//SC.say(String.valueOf(value.get("pk_object")));
						if(!"".equals(data.toString()))
						{
							plCanvas.setVisible(false);
						}
					}
					
				});
				
				IButton jaBut = (IButton)discussScreen.getByLocalId("IButton2");
				jaBut.addClickHandler(new ClickHandler(){

					@Override
					public void onClick(ClickEvent event) {
						SC.ask("结案后将不能再添加评论，是否继续？", new BooleanCallback(){

							@Override
							public void execute(Boolean value) {
								// TODO Auto-generated method stub
								if(value)
								{
									jaForm.editNewRecord();
									jaForm.setValue("pk_object", pkObject);
									jaForm.setValue("objectTable", tableName);
									jaForm.saveData();
									plCanvas.setVisible(false);
								}
							}
						});
					}
				});
				//Canvas mainCanvas = (Canvas)discussScreen.getByLocalId("DataView0");
				discussScreen.setWidth100();
				discussScreen.setHeight100();
				layout.addChild(discussScreen);
				layout.setHeight("100%");
				layout.setWidth100();
				if(showForm)
				{
					//ts.sectionIsExpanded("SectionStackSection0");
				}
			}
		});
		this.addMember(layout);
	}
}
