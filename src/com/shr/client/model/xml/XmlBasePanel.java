package com.shr.client.model.xml;

import java.util.LinkedHashMap;
import java.util.Map;

import com.smartgwt.client.core.Function;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class XmlBasePanel extends HLayout 
{
	private VLayout mainLayout;
	
	public Map<String,Canvas> baseCanvas = new LinkedHashMap<String,Canvas>();
	
	public XmlBasePanel()
	{
		mainLayout = new VLayout();
		this.addMember(mainLayout);
		if(canvasUI() != null)
		{
			RPCManager.cacheScreens(canvasUI(), new Function() {
				@Override
				public void execute() {
					for(String canvas : canvasUI())
					{
						baseCanvas.put(canvas, RPCManager.createScreen(canvas));
					}
					initChild(baseCanvas);
					
					mainLayout.setWidth100();
					mainLayout.setHeight100();
				}
			});
			this.setWidth100();
			this.setHeight100();
		}
	}

	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	public String[] canvasUI()
	{
		return null;
	}
	
	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	public void initChild(Map<String,Canvas> baseCanvas)
	{
		for(String canvas : baseCanvas.keySet())
		{
			mainLayout.addMember(baseCanvas.get(canvas));
		}
	}
	
	public void baseAddMember(Canvas canvas)
	{
		mainLayout.addMember(canvas);
	}
	
	public void addPrintButton(ToolStrip toolStrip)
	{
		toolStrip.addFill();
		//保存按钮
		ToolStripButton tsBut01 = new ToolStripButton();
		tsBut01.setTitle("打印");
		tsBut01.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				doPrint();
			}
		});
		toolStrip.addButton(tsBut01);
	}
	
	private void doPrint(){Canvas.showPrintPreview(this);}
}
