package com.shr.client.model;


public class BaseEditPanel extends BasePanel
{
	
	public BaseEditPanel()
	{
		init();
	}
	
	private void init()
	{
		//添加按钮
        baseToolStrip.addMember(this.baseBtnNew, 0);
        baseToolStrip.addMember(this.baseBtnEdit, 1);
        baseToolStrip.addMember(this.baseBtnSave, 2);
        
        baseBtnSave.enable();
	}
	
}
