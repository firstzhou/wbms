package com.shr.client.model.util;

public class BasePanelType {

	public final static String BASE_PANEL_MORE = "BASE_PANEL_MORE";
	//public final static String BASE_PANEL_TAB = "BASE_PANEL_TAB";
	public final static String BASE_PANEL_SIMPLE = "BASE_PANEL_SIMPLE";
	
	public final static String XML_BASE_PANEL_MORE = "XML_BASE_PANEL_MORE";
	public final static String XML_BASE_PANEL_TAB = "XML_BASE_PANEL_TAB";
	
	//定义子表对象存放索引
    public final static int CHILD_DATA_SOURCE_INDEX = 0;
    public final static int CHILD_LIST_GRID_INDEX = 1;
    public final static int CHILD_DATA_VIEW_INDEX = 2;
    public final static int CHILD_GRID_CRITERIA_INDEX = 3;
    public final static int CHILD_EDIT_FORM_INDEX = 4;
    public final static int CHILD_PANEL_INDEX = 5;
    
    //存放主从的显示类型，查询和编辑
    public final static String ACTION_TYPE_QUERY = "ACTION_TYPE_QUERY";
    public final static String ACTION_TYPE_EDIT = "ACTION_TYPE_EDIT";
	
    //控制模版是编辑还是新建数据
    public final static String EDIT_TYPE_ADD = "EDIT_TYPE_ADD";
    public final static String EDIT_TYPE_EDIT = "EDIT_TYPE_EDIT";
}
