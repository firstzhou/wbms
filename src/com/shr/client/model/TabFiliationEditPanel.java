package com.shr.client.model;

import java.util.HashMap;
import java.util.Map;

import com.shr.client.model.util.BasePanelType;
import com.shr.client.tool.ClientTool;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

/**
 * 主从模板根据BasePanelType的不同可以分为两种
 * 字段少的情况选择第一种类型，字段比较多选择第二种
 * 主表和从表类型可以分开设置，主表表重写initSinglePanelType() ,从表重写initChildPanelType();
 * 2.主从模板(BasePanelType.BASE_PANEL_MORE)
 *——————————————————————————————————————————————————————-————————————
 *|新建 | 编辑 | 删除 | 保存 | 取消 | 查询  | 查看                                                                   |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|__________________________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|      列表                  /    编辑                                                                                                             |
 *|__________________________________________________________________|
 *| 新建 | 编辑 | 删除 | 保存 | 取消 | 查看                                                                                  |
 *|_______   ______                                                  |
 *| 明细表 |__|_编辑_|________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *————————————————————————————————————————————————————————————————————
 * 点击查看浏览窗口
 *              
 * 主从表浏览继承该模板；分别实现下列方法以展示单表数据。
 * 子类实现 createQueryForm(); 方法返回主表查询条件面板；
 * 子类实现 createDataSource(); 方法返回主表数据源；
 * 子类实现 createDataViewPanel(); 方法返回主表浏览面板的布局样式；
 * 
 * 子类调用addChild(String title, DataSource childDS, DetailViewer dataView, DynamicForm editForm)方法
 * 以实现添加字表信息，可以添加多个
 * 
 * 子类还可以通过baseToolStrip属性获得模板的工具栏，以达到添加特定按钮的需要；
 * 例如在模板的基础上添加 “导出” 按钮
 * 子类可以实现代码如下
 *    baseToolStrip.addMember(new ToolStripButton("导出"));
 * 子类还可通过此方法删除不用的按钮
 *      baseToolStrip.removeMember(this.baseBtnCancel); //移除取消按钮
 *      
 * 子类还可以通过baseChildPanel.baseToolStrip属性获得从表模板的工具栏，进行编辑，类似主表。
 *    
 * 这样的模板是不是可以给你带来方便呢？ ：）
 * @author zw
 *
 */
public abstract class TabFiliationEditPanel extends SingleEditPanel{
	
    //存放子表结构的面板
    protected HLayout baseFiliationEditPanel;
    
    protected TabSet baseFiliationTabSet;
    
    //被选择tab的id
  	protected String childSelectedTabID;
    
    //编辑子模板类型，暂时包括两种
  	protected String baseChildPanelType;
  	
  	//子表tab的顺序
  	private int tabIndex;
  	
  	//判断是否是新增操作
  	private boolean isAdd;
  	
  //存放子表数据对象
    protected HashMap<String, Map<Integer, Object>> childObject;
    
	public TabFiliationEditPanel()
	{
		init();
	}
	
	/**
	 * 初始化子模板类型
	 * 子类可以重写该方法改变模板类型
	 * 默认为简单型的
	 * @return
	 */
	@Override
	protected String initSinglePanelType()
	{
		return BasePanelType.BASE_PANEL_MORE;
	}
	
	/**
	 * 初始化方法
	 */
	private void init()
	{
		childObject = new HashMap<String, Map<Integer, Object>> ();
		baseFiliationEditPanel = new HLayout();
		baseFiliationTabSet = new TabSet();
		
		Tab tabEdit = new Tab("概要");
		tabEdit.setPane(baseEditPanel);
		baseFiliationTabSet.addTab(tabEdit, tabIndex++);
		
		baseFiliationEditPanel.addMember(baseFiliationTabSet);
	}
	
	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * 显示与编辑区区域为模板默认
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 子表与主表对应关系Map<子表字段，主表字段>
     */
	public void addModelChild(String title, BasePanel childPanel, HashMap<String, String> fields)
	{
		Tab tabChild = new Tab(title);
		tabChild.addTabSelectedHandler(new TabSelectedHandler(){

            @Override
            public void onTabSelected(TabSelectedEvent event) {
                doChildTabSelected(event);
            }
        });
		tabChild.setPane(childPanel);
		baseFiliationTabSet.addTab(tabChild);
		//保存子表信息
		Map<Integer, Object> childObjs = new  HashMap<Integer, Object>();
		childObjs.put(BasePanelType.CHILD_PANEL_INDEX, childPanel);
		childObjs.put(BasePanelType.CHILD_GRID_CRITERIA_INDEX, fields);
		childObject.put(tabChild.getID(), childObjs);
		
	}
	
	/**
     * 子表Tab选择事件，设置浏览界面对应的panle
     * @param event
     */
    public void doChildTabSelected(TabSelectedEvent event){
        childSelectedTabID = event.getID();
        Map<Integer, Object> selectedChildObject = childObject.get(childSelectedTabID);
        final ListGridRecord record = baseListGrid.getSelectedRecord();
        if(selectedChildObject != null)
        {
        	BasePanel childPanel = (BasePanel)selectedChildObject.get(BasePanelType.CHILD_PANEL_INDEX);
        	final HashMap<String, String> tempFields = (HashMap<String, String>)(selectedChildObject.get(BasePanelType.CHILD_GRID_CRITERIA_INDEX));
        	if(childPanel instanceof SingleEditPanel)
        	{
        		final SingleEditPanel cp = (SingleEditPanel)childPanel;
        		//子表点击新建的时候将主表的对应设置上去
        		cp.baseBtnNew.addClickHandler(new ClickHandler() {
        			public void onClick(ClickEvent event) {
        				if(null != tempFields && tempFields.size() > 0){
                        	for(String mainfield : tempFields.keySet()){
                        		FormItem item = cp.baseEditForm.getField(mainfield);
                        		if(item != null)
                        		{
                        			item.setValue(record.getAttribute(tempFields.get(mainfield)));
                        		}
                        		else
                        		{
                        			SC.warn("没有获得到主表关联字段！");
                        		}
                             }
                         }
        			}
        		});
        	}
        	if(childPanel instanceof SingleQueryPanel)
        	{
        		SingleQueryPanel cp = (SingleQueryPanel)childPanel;
        		cp.baseListGrid.invalidateCache();
                Criteria criteria = new Criteria();
                if(null != tempFields && tempFields.size() > 0){
                	for(String mainfield : tempFields.keySet()){
                         String pkValue = record.getAttribute(tempFields.get(mainfield));
                         criteria.addCriteria(mainfield,pkValue);
                     }
                 }
                cp.baseListGrid.fetchData(criteria);
        	}
        	else
        	{
        		childPanel.doBaseQuery();
        	}
        }
        onChildTabSelected(event);
    }
    
    /**
	 * 模板新增按钮方法
	 */
    @Override
	public void doBaseAdd()
    {
    	this.isAdd = true;
    	super.doBaseAdd();
    }
    
    /**
     * 显示编辑面板
     */
	@Override
    public void showEditPanel()
    {
		if(isAdd)
		{
			ClientTool.showWindow("新增", baseEditPanel, baseDataEditWidth, baseDataEditHeight);
			//ClientTool.open(id, name, baseEditPanel);
			isAdd = false;
		}
		else
		{
			ClientTool.showWindow("编辑", baseFiliationEditPanel, baseDataEditWidth, baseDataEditHeight);
			//ClientTool.open(id, name, baseFiliationEditPanel);
		}
    }

    /**
     * 子表的tab select事件
     * @param event
     */
    protected void onChildTabSelected(TabSelectedEvent event){}
}
