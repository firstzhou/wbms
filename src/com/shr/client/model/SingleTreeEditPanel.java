package com.shr.client.model;

import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public abstract class SingleTreeEditPanel extends SingleEditPanel
{
	/**
	 * 树控件
	 */
	protected TreeGrid baseTreeGrid;
	
	/**
	 * 树列表显示字段
	 */
	private String[] treeFields;
	
	/**
	 * 新增同级
	 */
	protected ToolStripButton baseTreeBtnNewSam;
	
	/**
	 * 新增下级
	 */
	protected ToolStripButton baseTreeBtnNewNext;
	
	/**
	 * 新增类型，1表示新增同级，2表示新增下级
	 */
	private static int ADD_TYPE = 1;
	
	/**
	 * 设置树是否能够拖动
	 */
	private boolean canDrag = false;
	

	/**
	 * 构造树
	 */
	public SingleTreeEditPanel()
	{
		init();
	}
	
	/**
	 * 初始化树
	 */
	private void init()
	{
		baseTreeGrid.setShowFilterEditor(false);
		baseTreeGrid.setFilterOnKeypress(false);
		//设置是否能拖动
		baseTreeGrid.setCanDrag(isCanDrag());
		
		baseTreeBtnNewSam = new ToolStripButton("新增同级");
		baseTreeBtnNewSam.addClickHandler(new ClickHandler()
		{
			@Override
			public void onClick(ClickEvent event) {
				doBaseAddSame();
			}
		});
		
		baseTreeBtnNewNext = new ToolStripButton("新增下级");
		baseTreeBtnNewNext.addClickHandler(new ClickHandler(){

			@Override
			public void onClick(ClickEvent event) {
				doBaseAddNext();
			}
		});
		//添加按钮
		baseToolStrip.removeMember(this.baseBtnNew);
        baseToolStrip.addMember(this.baseTreeBtnNewSam, 0);
        baseToolStrip.addMember(this.baseTreeBtnNewNext, 1);
	}
	
	/**
	 * 创建树显示的字段
	 * @return
	 */
	public abstract String[] buildTreeFields();
	
	/**
	 * 返回树节点的字段
	 * @return
	 */
	public abstract String buildForeignKey();
	
	/**
	 * 返回树的父节点字段
	 * @return
	 */
	public abstract String buildFatherKey();

	/**
	 * 创建树列表
	 */
	@Override
	protected ListGrid createBaseGrid()
	{
		baseTreeGrid = new TreeGrid();
		
		baseTreeGrid.setCanFreezeFields(true);  
		baseTreeGrid.setCanReparentNodes(true);
		baseTreeGrid.setShowFilterEditor(false);
		baseTreeGrid.setFilterOnKeypress(false);
		baseTreeGrid.setLoadDataOnDemand(false);
		
		//设置树显示的列
		treeFields = buildTreeFields();
		if(treeFields != null)
		{
			TreeGridField[] treeGridField = new TreeGridField[treeFields.length];
			for(int i=0; i<treeFields.length; i++)
			{
				treeGridField[i] = new TreeGridField(treeFields[i]);
			}
			baseTreeGrid.setFields(treeGridField);
		}
		
		return baseTreeGrid;
	}
	
	/**
	 * 新增同级
	 */
	protected void doBaseAddSame()
	{
		ADD_TYPE = 1;
		doBaseAdd();
	}
	
	/**
	 * 新增下级
	 */
	protected void doBaseAddNext()
	{
		ADD_TYPE = 2;
		doBaseAdd();
	}
	
	/**
     * 模板保存按钮方法
     */
    @Override
    public void doBaseSave()
    {
    	doBaseEditSave();
    }
    
	/**
     * 使用代理方法保存主要是为弹出面板用
     */
    @Override
    protected void doBaseEditSave()
    {
    	ListGridRecord record = baseTreeGrid.getSelectedRecord();
    	String fatherKey = buildFatherKey();
    	String foreignKey = buildForeignKey();
    	if(record == null)
    	{
    		baseEditForm.setValue(fatherKey, "");
    	}
    	else
    	{
	    	//新增同级
	    	if(ADD_TYPE == 1)
	    	{
	    		String fk = record.getAttribute(fatherKey);
	    		if(fk == null || "".equals(fk))
	    		{
	    			fk = "";
	    		}
	    		baseEditForm.setValue(fatherKey, fk);
	    	}
	    	//新增下级
	    	else if(ADD_TYPE == 2)
	    	{
	    		String fk = record.getAttribute(foreignKey);
	    		if(fk == null || "".equals(fk))
	    		{
	    			fk = "";
	    		}
	    		baseEditForm.setValue(fatherKey, fk);
	    	}
    	}
    	super.doBaseEditSave();
    }
    
    /**
     * 重写该方法设置树是否能够拖动
     * @return
     */
    public boolean isCanDrag() 
    {
		return canDrag;
	}
    
    /**
     * 该方法设置树是否能够拖动
     * @return
     */
    public boolean setCanDrag(boolean canDrag) 
    {
    	baseTreeGrid.setCanDrag(canDrag);
		return this.canDrag = canDrag;
	}
}
