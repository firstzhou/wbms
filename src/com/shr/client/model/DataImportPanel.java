package com.shr.client.model;

import com.google.gwt.core.client.GWT;
import com.smartgwt.client.types.Encoding;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.events.SubmitValuesEvent;
import com.smartgwt.client.widgets.form.events.SubmitValuesHandler;
import com.smartgwt.client.widgets.form.fields.CheckboxItem;
import com.smartgwt.client.widgets.form.fields.SubmitItem;
import com.smartgwt.client.widgets.form.fields.UploadItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.layout.VLayout;

public class DataImportPanel extends VLayout 
{
	
	public DataImportPanel(final String mapKey)
	{
		String SERVLET_FILEUPLOAD = "DataImport.do?replaceData=true&types=ExcelImp&mappingID="+mapKey;
		if(null != buildContent())
		{
			SERVLET_FILEUPLOAD += "&buildContent="+buildContent();
		}
		//create upload form with an upload item and a submit item
		final DynamicForm uploadForm = new DynamicForm();
		uploadForm.setNumCols(4);
		uploadForm.setTarget("fileUploadFrame"); //set target of FORM to the IFRAME
		uploadForm.setEncoding(Encoding.MULTIPART);
		uploadForm.setAction(GWT.getModuleBaseURL()+SERVLET_FILEUPLOAD); //call to a FileUpload servlet on the webserver
		//creates a HTML formitem with a textfield and a browse button
		final UploadItem uploadItem = new UploadItem("导入文件","选择xls文件");
		uploadItem.setWidth(400);
		uploadItem.setColSpan(3);
		CheckboxItem replace = new CheckboxItem();
		replace.setName("replaceData");
		replace.setTitle("覆盖数据");
		replace.setValue(true);
		replace.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				// TODO Auto-generated method stub
				String value = String.valueOf(event.getValue());
				if("true".equals(value))
				{
					uploadForm.setAction(GWT.getModuleBaseURL()+"DataImport.do?replaceData=true&types=ExcelImp&mappingID="+mapKey);
				}
				else
				{
					uploadForm.setAction(GWT.getModuleBaseURL()+"DataImport.do?replaceData=false&types=ExcelImp&mappingID="+mapKey);
				}
			}
			
		});
		SubmitItem submitItem = new SubmitItem("upload", "导入");
		submitItem.setStartRow(false);
		submitItem.setEndRow(false);
		
		Canvas downloadFile = new Canvas();
		downloadFile.setWidth(180);
		downloadFile.setContents("<a href='DownloadFiles/"+mapKey+".xls' title='下载模版' target='_blank'>下载模版</a>");

		uploadForm.setItems(uploadItem, replace, submitItem);

		uploadForm.addSubmitValuesHandler(new SubmitValuesHandler() {
			
			public void onSubmitValues(SubmitValuesEvent event) {
				//maybe do some filename verification
				//String filename = (String)uploadItem.getValue();
				uploadForm.submitForm();
			}
		});

		this.addMember(uploadForm);
		this.addMember(downloadFile);
	}

	/**
	 * 子类重写该方法，返回导入自定义参数
	 * @return
	 */
	public String buildContent()
	{
		return null;
	}
}
