package com.shr.client.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.shr.client.model.util.BasePanelType;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.viewer.DetailViewer;

public class FiliationChildPanel extends BasePanel{

	//子表布局
    private HLayout baseChildTabLayout;
    
    //子表主窗口面板
    protected TabSet baseChildTabSetList, baseChildTabSetEdit;
    
    //子表浏览界面
    private Tab childTabViewer;
    
    //子表编辑界面
    private Tab childTabEdit;
    
    //存放子表数据对象
    public HashMap<String, ArrayList<Object>> childObject = new HashMap<String, ArrayList<Object>> ();
    
    //编辑子模板类型，暂时包括两种
  	protected String baseChildPanelType;
  	
  	//选择tab的id
  	public String childSelectedTabID;
  	
  	//字表弹出显示框的高度
  	private String baseChildDataViewHeight;
  	
  	//子表弹出显示框的宽度
	private String baseChildDataViewWidth;
	
	//主表的grid
	private ListGrid baseListGrid;
	
	private String actionType;
    
    public FiliationChildPanel(String actionType, String panelType, ListGrid baseListGrid){
    	this.actionType = actionType;
    	baseChildPanelType = panelType;
    	this.baseListGrid = baseListGrid;
    	init();
    }
    
    /**
     * 初始化方法
     */
    private void init(){
        
    	if(BasePanelType.ACTION_TYPE_EDIT.equals(actionType))
    	{
	        baseToolStrip.addMember(baseBtnNew);
	    	baseToolStrip.addMember(baseBtnEdit);
	    	baseToolStrip.addMember(baseBtnSave);
	    	baseToolStrip.addMember(baseBtnCancel);
	    	baseToolStrip.addMember(baseBtnDelete);
    	}
    	
    	if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType)){
            baseToolStrip.addMember(baseBtnView);
        }
        
        baseChildTabLayout = new HLayout();
        baseChildTabSetList = new TabSet();
        childTabViewer = new Tab("浏览");
        childTabEdit = new Tab("编辑");
        
        if(BasePanelType.BASE_PANEL_SIMPLE.equals(baseChildPanelType)){
            baseChildTabSetList.setWidth("60%");
            baseChildTabSetEdit = new TabSet();
            baseChildTabLayout.addMember(baseChildTabSetList);
            baseChildTabLayout.addMember(baseChildTabSetEdit);
            baseChildTabSetEdit.addTab(childTabViewer);
            if(BasePanelType.ACTION_TYPE_EDIT.equals(actionType))
        	{
            	baseChildTabSetEdit.addTab(childTabEdit);
        	}
        } else if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType)){
        	baseChildDataViewHeight = "60%";
    		baseChildDataViewWidth = "50%";
            baseChildTabSetList.setWidth100();
            if(BasePanelType.ACTION_TYPE_EDIT.equals(actionType))
        	{
            	baseChildTabSetList.addTab(childTabEdit);
        	}
            baseChildTabLayout.addMember(baseChildTabSetList);
        }
        baseMainLayout.addMember(baseChildTabLayout);
    }
    
    /**
     * 获得子表ToolStrip
     * 可以通过该方法添加新按钮
     * @return
     */
    public ToolStrip getBaseToolStrip()
    {
    	return baseToolStrip;
    }
    
    /**
	 * 获得子表弹出浏览窗口的高度
	 * @return
	 */
	public String getBaseChildDataViewHeight() {
		return baseChildDataViewHeight;
	}

	/**
	 * 设置子表弹出窗口的高度
	 * @param baseChildDataViewHeight
	 */
	public void setBaseChildDataViewHeight(String baseChildDataViewHeight) {
		this.baseChildDataViewHeight = baseChildDataViewHeight;
	}

	/**
	 * 获得子表弹出窗口的宽度
	 * @return
	 */
	public String getBaseChildDataViewWidth() {
		return baseChildDataViewWidth;
	}

	/**
	 * 设置子表弹出窗口的宽度
	 * @return
	 */
	public void setBaseChildDataViewWidth(String baseChildDataViewWidth) {
		this.baseChildDataViewWidth = baseChildDataViewWidth;
	}
	
	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * 显示与编辑区区域为模板默认
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 主子表关联关系
     */
	public ListGrid addModelChild(String title, DataSource childDS, HashMap<String, String> fields){
		return addModelChild(title, childDS, fields, new DetailViewer(), new DynamicForm(), new ListGrid());
	}

	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 主子表关联关系
     * @param dataView 子表数据显示面板
     */
    public ListGrid addModelChild(String title, DataSource childDS, 
            HashMap<String, String> fields, DetailViewer dataView, DynamicForm editForm, ListGrid childListGrid){
    	if(null != childDS){
            ArrayList<Object> childObjs = new  ArrayList<Object>();
            Tab childTabList = null;
            
            if(null != title){
                childTabList = new Tab(title);
            }
            else
            {
                childTabList = new Tab("列表");
            }
            
            childTabList.addTabSelectedHandler(new TabSelectedHandler(){

                @Override
                public void onTabSelected(TabSelectedEvent event) {
                    doChildTabSelected(event);
                }
            });
            if(childListGrid == null)
            {
            	childListGrid = new ListGrid();
            }
            childListGrid.setDataSource(childDS);
            childListGrid.setShowFilterEditor(true);
            childListGrid.setFilterOnKeypress(true);
            childListGrid.addRecordClickHandler(new RecordClickHandler() {
                public void onRecordClick(RecordClickEvent event) {
                    doChildRecordClick(event);
                }
            });
            
            childListGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler(){
				@Override
				public void onRecordDoubleClick(RecordDoubleClickEvent event) {
					if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType)){
						//doChildView();
					}
				}
            });
            if(null == dataView.getDataSource())
            {
            	dataView.setDataSource(childDS);
            }
            if(null == editForm.getDataSource())
            {
            	editForm.setDataSource(childDS);
            }
            childTabList.setPane(childListGrid);
            
            childObjs.add(BasePanelType.CHILD_DATA_SOURCE_INDEX, childDS);
            childObjs.add(BasePanelType.CHILD_LIST_GRID_INDEX, childListGrid);
            childObjs.add(BasePanelType.CHILD_DATA_VIEW_INDEX, dataView);
            childObjs.add(BasePanelType.CHILD_GRID_CRITERIA_INDEX, fields);
            childObjs.add(BasePanelType.CHILD_EDIT_FORM_INDEX, editForm);
            childObject.put(childTabList.getID(), childObjs);
            
            fireAddChild(childTabList, dataView, editForm);
            
            return childListGrid;
        }
    	else
    	{
    		return null;
    	}
    }
    
    /**
     * 触发添加字表信息
     * @param title
     * @param childDS
     * @param dataView
     */
    private void fireAddChild(Tab childTabList, DetailViewer dataView, DynamicForm editForm){
    	baseChildTabSetList.addTab(childTabList, 0);
    	baseChildTabSetList.selectTab(0);
        childTabViewer.setPane(dataView);
        childTabEdit.setPane(editForm);
    }
    
    
    /**
     * 子表Tab选择事件，设置浏览界面对应的panle
     * @param event
     */
    public void doChildTabSelected(TabSelectedEvent event){
        childSelectedTabID = event.getID();
        ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null){
            DetailViewer dataView = (DetailViewer)selectedChildObject.get(BasePanelType.CHILD_DATA_VIEW_INDEX);
            if(null != dataView){
                childTabViewer.setPane(dataView);
            }
            
            DynamicForm editForm = (DynamicForm)selectedChildObject.get(BasePanelType.CHILD_EDIT_FORM_INDEX);
            if(null != dataView){
                childTabEdit.setPane(editForm);
            }
        }
        onChildTabSelected(event);
    }
    
    /**
     * 子表的select事件
     * @param event
     */
    protected void onChildTabSelected(TabSelectedEvent event)
    {
    }
    
    /**
     * 选择子表数据事件
     * @param event
     */
    public void doChildRecordClick(RecordClickEvent event){
        
        ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null){
            DetailViewer dataView = (DetailViewer)selectedChildObject.get(BasePanelType.CHILD_DATA_VIEW_INDEX);
            ListGrid grid = (ListGrid)selectedChildObject.get(BasePanelType.CHILD_LIST_GRID_INDEX);
            if(null != dataView && null != grid){
                dataView.viewSelectedData(grid);
            }
        }
    }
    
    /**
     * 主表的点击事件
     * @param event
     */
    public void doFetchData(RecordClickEvent event) {
    	doChildQuery(event.getRecord());
    }
    
    /**
     * 主表的点击事件
     * @param event
     */
    protected void doChildQuery(Record record) {
        
        if(null != record) {
            //查询对应的明细
            Collection<ArrayList<Object>> objects = childObject.values();
            for(ArrayList<Object> objs : objects){
                ListGrid tempGrid = (ListGrid)(objs.get(BasePanelType.CHILD_LIST_GRID_INDEX));
                HashMap<String, String> tempFields = (HashMap<String, String>)(objs.get(BasePanelType.CHILD_GRID_CRITERIA_INDEX));
                tempGrid.invalidateCache();
                Criteria criteria = new Criteria();
                if(null != tempFields && tempFields.size() > 0){
                	for(String mainfield : tempFields.keySet()){
                        String pkValue = record.getAttribute(tempFields.get(mainfield));
                        criteria.addCriteria(mainfield,pkValue);
                    }
                }
                tempGrid.fetchData(criteria);
            }
        }else{
            SC.say("当前未选中任何记录！");
        }
    }
    
    /**
     * 子表的查看按钮
     */
    public void doChildView(){
    	ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null){
        	Canvas dataView = (Canvas)selectedChildObject.get(BasePanelType.CHILD_DATA_VIEW_INDEX);
            ListGrid grid = (ListGrid)selectedChildObject.get(BasePanelType.CHILD_LIST_GRID_INDEX);
            if (null != grid.getSelectedRecord()) {
                Window winModal = new Window();
                winModal.setWidth(baseChildDataViewWidth);
                winModal.setHeight(baseChildDataViewHeight);
                winModal.setTitle("子表信息");
                winModal.setShowMinimizeButton(false);
                winModal.setIsModal(true);
                winModal.setShowModalMask(true);
                winModal.centerInPage();
                winModal.addItem(dataView);
                winModal.show();
            } else {
                SC.say("请先选择一条数据进行查看！");
            }
        }
    }
    
    /**
     * 子表的添加按钮
     */
    public void doChildAdd(){
    	ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null)
        {
        	if(null != baseListGrid.getSelectedRecord()) {
        		DynamicForm editForm = (DynamicForm)selectedChildObject.get(BasePanelType.CHILD_EDIT_FORM_INDEX);
        		HashMap<String, String> tempFields = (HashMap<String, String>)(selectedChildObject.get(BasePanelType.CHILD_GRID_CRITERIA_INDEX));
                childTabEdit.setPane(editForm);
                editForm.editNewRecord();
                
                if(null != tempFields && tempFields.size() > 0){
                    for(String mainfield : tempFields.keySet()){
                        String pkValue = tempFields.get(mainfield);
                        editForm.setValue(mainfield, baseListGrid.getSelectedRecord().getAttributeAsString(pkValue));
                    }
                }
                doCildAddAfter();
                if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType))
                {
                    baseChildTabSetList.selectTab(childTabEdit);
                }else{
                    baseChildTabSetEdit.selectTab(childTabEdit);
                }
                baseBtnSave.enable();
			}else{
				SC.say("请选择对应的主表数据进行新增！");
			}
        }
    }
    
    /**
     * 点击子表新增后操作
     * 可以给数据赋初始值
     */
    protected void doCildAddAfter(){}
    
    /**
     * 子表的编辑按钮
     */
    public void doChildEdit(){
    	ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null){
            DynamicForm editForm = (DynamicForm)selectedChildObject.get(BasePanelType.CHILD_EDIT_FORM_INDEX);
            ListGrid grid = (ListGrid)selectedChildObject.get(BasePanelType.CHILD_LIST_GRID_INDEX);
            if(null != grid.getSelectedRecord()) {
                childTabEdit.setPane(editForm);
                editForm.editRecord(grid.getSelectedRecord());
                baseBtnSave.enable();
                if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType))
                {
                    baseChildTabSetList.selectTab(childTabEdit);
                }else{
                    baseChildTabSetEdit.selectTab(childTabEdit);
                }
                doCildEditAfter();
            } else {
                SC.say("请选择一条数据进行编辑！");
            }
        }
    }
    
    /**
     * 点击子表编辑后操作
     * 可以给数据赋初始值
     */
    protected void doCildEditAfter(){}
    
    /**
     * 子表的删除按钮
     */
    public void doChildDelete(){
    	ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null){
            final ListGrid grid = (ListGrid)selectedChildObject.get(BasePanelType.CHILD_LIST_GRID_INDEX);
            if(null != grid.getSelectedRecord()) {
                SC.confirm("确认删除这<font color='red'>【<strong>"+grid.getSelectedRecords().length+"</strong>】</font>条数据吗？" +
                        "删除后信息将无法恢复，请谨慎操作！",
                        new BooleanCallback() {
                            public void execute(Boolean value) {
                                if (value != null && value) {
                                    grid.removeSelectedData();
                                }
                            }
                        });
            } else {
                SC.say("请选择需要删除的数据！");
            }
        }
    }
    
    
    /**
	 * 子类继承该方法处理处理保存前的一些数据
	 * 例如：
	 * 保存产品字段时，将产品的id赋值给form
	 */
	protected void doChildSaveBefore(){}
	
	/**
	 * 子表的保持按钮
	 */
    public void doChildSave(){
    	ArrayList<Object> selectedChildObject = childObject.get(childSelectedTabID);
        if(selectedChildObject != null){
        	doChildSaveBefore();
            DynamicForm editForm = (DynamicForm)selectedChildObject.get(BasePanelType.CHILD_EDIT_FORM_INDEX);
            editForm.saveData();
            if(!editForm.hasErrors()){
                editForm.clearValues();
                baseBtnSave.disable();
                if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType)){
	    			baseChildTabSetList.selectTab(childSelectedTabID);
	    		}else{
	    			baseChildTabSetEdit.selectTab(childTabViewer);
	    		}
            }
        }
    }
    
    /**
     * 子表的取消按钮
     */
    public void doChildCance(){
    	baseBtnSave.disable();
        if(BasePanelType.BASE_PANEL_MORE.equals(baseChildPanelType)){
			baseChildTabSetList.selectTab(0);
		}else{
			baseChildTabSetEdit.selectTab(childTabViewer);
		}
    }
    
}
