package com.shr.client.model;

import com.shr.client.App;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.HTMLPane;

public class UREPORTPanel extends Canvas {

	

	public UREPORTPanel(String id) 
	{
		StringBuffer contentBuffer = new StringBuffer();
		contentBuffer.append("<iframe name='fff' style='width:100%;height:100%;border:0' ");
		contentBuffer.append("src=\"");
		contentBuffer.append("/thinkbi/prstresult/show?resid=");
		contentBuffer.append(encodeURI(id));
		contentBuffer.append("&user_id=" + App.curUserId);
		contentBuffer.append("&$sys_showCaptionPanel=false&$sys_showParamPanel=true&$sys_calcnow=true&user=thinkBQ&password=123456");
		contentBuffer.append("&curUserDataRight=" + App.curUserDataRight);
		contentBuffer.append("&curUserRightContent=" + App.curUserRightContent);
		contentBuffer.append("\"></iframe>");
		
		HTMLPane html = new HTMLPane();
		html.setWidth100();
		html.setHeight100();
		html.setContents(contentBuffer.toString());
		this.addChild(html);
	}

	public static native String encodeURI(String url)/*-{
		return encodeURI(url);
	}-*/;
}
