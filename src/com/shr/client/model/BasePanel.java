package com.shr.client.model;

import com.shr.client.model.util.BasePanelType;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

/**
 * 模板的基类
 * 实现工具条上的部分按钮，其余工作需要具体模板实现
 * @author zw
 *
 */
public class BasePanel extends VLayout {

	//主布局管理器
	public VLayout baseMainLayout;
	
	//工具条
	public ToolStrip baseToolStrip;
	
	//新建按钮
	public ToolStripButton baseBtnNew;
	
	//编辑按钮
	public ToolStripButton baseBtnEdit;
	
	//保存按钮
	public ToolStripButton baseBtnSave;
	
	//取消按钮
	public ToolStripButton baseBtnCancel;
	
	//删除按钮
	public ToolStripButton baseBtnDelete;
	
	//查询按钮
	public ToolStripButton baseBtnQuery;
	
	//查看按钮
	public ToolStripButton baseBtnView;
	
	//导出按钮
	public ToolStripButton baseBtnExport;
	
	//导入按钮
	public ToolStripButton baseBtnImp;
	
	//模版评论按钮
	protected ToolStripButton baseBtnDiscuss;
	
	//编辑模板类型，暂时包括两种
	public String basePanelType;
	
	private boolean isDiyButton;
	
	/**
	 * 初始构造函数
	 */
	public BasePanel() {
		if(initBaseUI() != null)
		{
			RPCManager.cacheScreens(initBaseUI(), new Function() {
				@Override
				public void execute() {
					init();
					initToolBarForm();
					initBase();
				}
			});
		}
		else
		{
			init();
			initToolBarForm();
			initBase();
		}
	}
	
	/**
	 * 子类重新该方法以保证父类加载完页面后在回掉函数里再初始化子类
	 */
	public void initBase(){};
	
	/**
	 * 子类重写该方法，加载ui文件
	 * 返回ui文件名称
	 */
	public String[] initBaseUI(){return null;};
	
	/**
	 * 初始化
	 */
	private void init(){
		baseMainLayout = new VLayout();
		baseToolStrip = createBaseToolScrip();
		if(baseToolStrip == null)
		{
			isDiyButton = false;
			baseToolStrip = new ToolStrip();
		}
		else
		{
			isDiyButton = true;
		}
		basePanelType = initSinglePanelType();
		
		baseMainLayout.addMember(baseToolStrip);
		this.addMember(baseMainLayout);
	}
	
	/**
	 * 子类重写该方法添加自己的菜单
	 * @return
	 */
	public ToolStrip createBaseToolScrip()
	{
		return null;
	}
	
	/**
	 * 初始化模板类型
	 * 子类可以重写该方法改变模板类型
	 * 默认为简单型的
	 * @return
	 */
	protected String initSinglePanelType(){
		return BasePanelType.BASE_PANEL_SIMPLE;
	}
	
	/**
	 * 初始化工具条
	 */
	private void initToolBarForm() {
		
		baseBtnNew = new ToolStripButton("新建","icons/add.png");
		baseBtnNew.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseAdd();
			}
		});
		baseBtnEdit = new ToolStripButton("编辑","icons/page_edit.png");
		baseBtnEdit.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseEdit();
			}
		});
		baseBtnDelete = new ToolStripButton("删除","icons/page_delete.png");
		baseBtnDelete.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseDelete();
			}
		});
		baseBtnSave = new ToolStripButton("保存","icons/accept.png");
		baseBtnSave.setShowDisabledIcon(false);
		baseBtnSave.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseSave();
			}
		});
		baseBtnCancel = new ToolStripButton("取消","icons/arrow_left.png");
		baseBtnCancel.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseCance();
			}
		});
		baseBtnQuery = new ToolStripButton("查询","icons/page_find.png");
		baseBtnQuery.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseQuery();
			}
		});
		
		baseBtnView = new ToolStripButton("查看","icons/application_view_detail.png");
		baseBtnView.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseView();
			}
		});
		
		baseBtnExport = new ToolStripButton("导出","icons/folder_go.png");
		baseBtnExport.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseExport();
			}
		});
		baseBtnExport.setTooltip("导出当前列表中的数据到excel文件");
		
		baseBtnImp = new ToolStripButton("导入");
		baseBtnImp.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doBaseImp();
			}
		});
		baseBtnDiscuss = new ToolStripButton("评论", "icons/comments.png");
		if(baseBtnDiscuss != null)
		{
			baseBtnDiscuss.addClickHandler(new ClickHandler() {
				public void onClick(ClickEvent event) {
					doBaseDiscuss();
				}
			});
		}
		
		baseBtnSave.disable(); //初始时设置保存按钮失效
	}
	
	/**
	 * 模板新增按钮方法
	 */
	public void doBaseAdd(){}
	
	/**
	 * 模板编辑按钮方法
	 */
	public void doBaseEdit(){}
	
	/**
	 * 模板删除按钮方法
	 */
	public void doBaseDelete(){}
	
	/**
	 * 模板保存按钮方法
	 */
	public void doBaseSave(){}
	
	/**
	 * 模板取消按钮方法
	 */
	public void doBaseCance(){}
	
	/**
	 * 模板查询按钮方法
	 */
	public void doBaseQuery(){}
	
	/**
	 * 模板查看按钮方法
	 */
	public void doBaseView(){}
	
	/**
	 * 模板导出按钮方法
	 */
	public void doBaseExport(){}
	
	/**
	 * 模板导入按钮方法
	 */
	public void doBaseImp(){}
	
	/**
	 * 评论
	 */
	public void doBaseDiscuss(){}
}
