package com.shr.client.model;

import com.shr.client.model.util.BasePanelType;
import com.smartgwt.client.rpc.LoadScreenCallback;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

/**
 * 单表模板根据SiglePanelType的不同可以分为两种 字段少的情况选择第一种类型，字段比较多选择第二种
 * 1.单表模板(SiglePanelType.SINGLE_EDIT_PANEL_SIMPLE)
 * ——————————————————————————————————————————————————————-———————————— | 新建 | 编辑
 * | 删除 | 保存 | 取消 | 查询 |
 * |—————————————————————————————————————————————————————————————————-| | | |
 * 查询条件 | |________ _______ _________ | | 列表
 * |_____________________|__浏览_|_|_新建/编辑|_______________| | | | | | | | | | | 列表
 * | 控件摆放 | | | | | | | | | | | | | | | |
 * ————————————————————————————————————————————————————————————————————
 * 
 * 2.单表模板(SiglePanelType.SINGLE_PANEL_MORE)
 * ——————————————————————————————————————————————————————-———————————— | 新建 | 编辑
 * | 删除 | 保存 | 取消 | 查询 | 查看 |
 * |—————————————————————————————————————————————————————————————————-| | | |
 * 查询条件 | |________ __________ | | 列表
 * |_|_新建/编辑|_____________________________________________| | | | | | | | 列表 /
 * 编辑 | | | | | | | | | | |
 * ———————————————————————————————————————————————————————————————————— 点击查看浏览窗口
 * 
 * 单表维护继承该模板；分别实现下列方法以展示单表数据维护。 子类实现 createQueryForm(); 方法返回查询条件面板； 子类实现
 * createDataSource(); 方法返回数据源； 子类实现 createDataViewPanel(); 方法返回浏览面板的布局样式； 子类实现
 * createDataEditForm(); 方法返回新建/编辑面板控件布局样式。
 * 
 * 子类还可以实现类似 doXXXBefore() 或 doXXXAfter() 方法来实现模板按钮点击之前或 点击之后需要做的操作； 例如：
 * 在编辑按钮给控件设置些默认值则实现doEditAfter()方法； 在保存之前设置控件的值则实现doSaveBefore()方法。
 * 
 * 子类还可以通过baseToolStrip属性获得模板的工具栏，以达到添加特定按钮的需要； 例如在模板的基础上添加 “导出” 按钮 子类可以实现代码如下
 * baseToolStrip.addMember(new ToolStripButton("导出")); 子类还可通过此方法删除不用的按钮
 * baseToolStrip.removeMember(this.btnCancel); //移除取消按钮
 * 
 * 这样的模板是不是可以给你带来方便呢？ ：）
 * 
 * @author zw
 * 
 */
public abstract class SingleEditPanel2 extends SingleQueryPanel {

	// 主面板新增编辑面板
	protected VLayout baseEditLayout;

	// 主窗口面板
	protected Tab baseTabEdit;

	// 主面板新增编辑Form
	protected DynamicForm baseEditForm;

	protected String sEditorFormID;

	/**
	 * 初始构造函数
	 */
	public SingleEditPanel2() {
		init();
	}

	private void init() {
		baseEditLayout = new VLayout();
		//sEditorFormID = this.sDatasourceName + "Form";

		procBaseEditFormBegin();

		if (baseTabEdit == null) {
			baseTabEdit = new Tab("新建/编辑");

			RPCManager.loadScreen(sEditorFormID, new LoadScreenCallback() {
				@Override
				public void execute() {
					if (this.getScreen() == null) {
						baseEditForm = createDataEditForm();
					} else {
						baseEditForm = (DynamicForm) this.getScreen();
					}
					if (null == baseEditForm.getDataSource()) {
						baseEditForm.setDataSource(baseDataSource);
					}

					// 处理编辑form
					procBaseEditFormAfter(baseEditForm);

					baseEditLayout.addMember(baseEditForm);
				}
			});

		}

		baseTabEdit.setPane(baseEditLayout);
		if (BasePanelType.BASE_PANEL_SIMPLE.equals(basePanelType)) {
			baseTabSetEdit.addTab(baseTabEdit);
		} else if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType)) {
			//baseTabSetList.addTab(baseTabEdit);
		}

		// 添加按钮
		baseToolStrip.addMember(this.baseBtnNew, 0);
		baseToolStrip.addMember(this.baseBtnEdit, 1);
		baseToolStrip.addMember(this.baseBtnSave, 2);
		baseToolStrip.addMember(this.baseBtnCancel, 3);
		baseToolStrip.addMember(this.baseBtnDelete, 4);
	}

	/**
	 * [简要描述]:初始化编辑列表form前action<br/>
	 * [详细描述]:<br/>
	 * 
	 * @author zhangfanghai
	 * @param baseEditForm2
	 */
	protected void procBaseEditFormBegin() {

	}

	/**
	 * [简要描述]:初始化编辑列表后action<br/>
	 * [详细描述]:<br/>
	 * 
	 * @author zhangfanghai
	 * @param baseEditForm2
	 */
	protected void procBaseEditFormAfter(DynamicForm baseEditForm) {

	}

	/**
	 * 初始化编辑面板 子类重写该方法，创建编辑面板
	 * 
	 * @return DynamicForm
	 */
	protected DynamicForm createDataEditForm() {
		return new DynamicForm();
	}

	/**
	 * 模板新增按钮方法
	 */
	@Override
	public void doBaseAdd() {
		if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType)) {
			//baseTabSetList.selectTab(baseTabEdit);
		} else {
			baseTabSetEdit.selectTab(baseTabEdit);
		}
		baseEditForm.editNewRecord();
		baseBtnSave.enable();
		doAddAfter();
	}

	/**
	 * 子类继承该方法处理处理点击新增后的一些控制 例如： 设置属性的默认值
	 */
	protected void doAddAfter() {
	}

	/**
	 * 模板编辑按钮方法
	 */
	@Override
	public void doBaseEdit() {
		if (null != baseListGrid.getSelectedRecord()) {
			if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType)) {
				//baseTabSetList.selectTab(baseTabEdit);
			} else {
				baseTabSetEdit.selectTab(baseTabEdit);
			}
			baseEditForm.editRecord(baseListGrid.getSelectedRecord());
			baseBtnSave.enable();
			doEditAfter();
		} else {
			SC.say("请选择一条数据进行编辑！");
		}

	}

	/**
	 * 子类继承该方法处理处理点击编辑后的一些控制 例如： 设置属性的默认值
	 */
	protected void doEditAfter() {
	}

	/**
	 * 模板删除按钮方法
	 */
	@Override
	public void doBaseDelete() {
		if (null != baseListGrid.getSelectedRecord()) {
			SC.confirm(
					"确认删除这<font color='red'>【<strong>"
							+ baseListGrid.getSelectedRecords().length
							+ "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！",
					new BooleanCallback() {
						public void execute(Boolean value) {
							if (value != null && value) {
								baseListGrid.removeSelectedData();
							}
						}
					});
		} else {
			SC.say("请选择需要删除的数据！");
		}
	}

	/**
	 * 子类继承该方法处理处理保存前的一些数据 例如： 保存产品字段时，将产品的id赋值给form
	 */
	protected void doSaveBefore() {
	}

	/**
	 * 子类继承该方法处理处理保存前的一些数据 例如： 保存产品字段时，将产品的id赋值给form
	 */
	protected void doSavAfter() {
	}

	/**
	 * 模板保存按钮方法
	 */
	@Override
	public void doBaseSave() {
		doSaveBefore();
		baseEditForm.saveData();
		if (!baseEditForm.hasErrors()) {
			doSavAfter();
			baseEditForm.clearValues();
			baseBtnSave.disable();
			if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType)) {
				//baseTabSetList.selectTab(this.baseTabList);
			} else {
				baseTabSetEdit.selectTab(this.baseTabViewer);
			}
		}
	}

	/**
	 * 模板取消按钮方法
	 */
	@Override
	public void doBaseCance() {
		baseBtnSave.disable();
		if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType)) {
			//baseTabSetList.selectTab(0);
		} else {
			baseTabSetEdit.selectTab(baseTabViewer);
		}
	}

	/**
	 * 获得单表模板编辑页面的Form
	 * 
	 * @return DynamicForm
	 */
	public DynamicForm getBaseDateForm() {
		return this.baseEditForm;
	}

	/**
	 * 设置单表模板编辑页面的Form
	 * 
	 * @return DynamicForm
	 */
	public void setBaseEditForm(DynamicForm baseEditForm) {
		this.baseEditForm = baseEditForm;
	}
}
