package com.shr.client.model;

import java.util.HashMap;
import java.util.Map;

import com.shr.client.model.util.BasePanelType;
import com.shr.client.tool.ClientTool;
import com.smartgwt.client.core.Function;
import com.smartgwt.client.data.DataSourceField;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.RPCManager;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;

/**
 * 单表模板根据SiglePanelType的不同可以分为两种
 * 字段少的情况选择第一种类型，字段比较多选择第二种
 * 1.单表模板(SiglePanelType.SINGLE_EDIT_PANEL_SIMPLE)
 *——————————————————————————————————————————————————————-————————————
 *| 新建 | 编辑 | 删除 | 保存 | 取消 | 查询                                                                                 |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|______________________________                                    |
 *|  列表                                                                |__浏览_|_|_新建/编辑|_______________|
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *|      列表                                                      |           控件摆放                                            |
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *————————————————————————————————————————————————————————————————————
 *
 * 2.单表模板(SiglePanelType.SINGLE_PANEL_MORE)
 *——————————————————————————————————————————————————————-————————————
 *| 新建 | 编辑 | 删除 | 保存 | 取消 | 查询  | 查看                                                                 |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|__________________________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|      列表                  /    编辑弹出窗口                                                                                         |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *————————————————————————————————————————————————————————————————————
 * 点击查看浏览窗口
 *              
 * 单表维护继承该模板；分别实现下列方法以展示单表数据维护。
 * 子类实现 createQueryForm(); 方法返回查询条件面板；
 * 子类实现 createDataSource(); 方法返回数据源；
 * 子类实现 createDataViewPanel(); 方法返回浏览面板的布局样式；
 * 子类实现 createDataEditForm(); 方法返回新建/编辑面板控件布局样式。
 * 
 * 子类还可以实现类似 doXXXBefore() 或 doXXXAfter() 方法来实现模板按钮点击之前或 点击之后需要做的操作；
 * 例如：
 * 在编辑按钮给控件设置些默认值则实现doEditAfter()方法；
 * 在保存之前设置控件的值则实现doSaveBefore()方法。
 * 
 * 子类还可以通过baseToolStrip属性获得模板的工具栏，以达到添加特定按钮的需要；
 * 例如在模板的基础上添加 “导出” 按钮
 * 子类可以实现代码如下
 *    baseToolStrip.addMember(new ToolStripButton("导出"));
 * 子类还可通过此方法删除不用的按钮
 *		baseToolStrip.removeMember(this.btnCancel); //移除取消按钮
 *    
 * 这样的模板是不是可以给你带来方便呢？ ：）
 * @author zw
 *
 */
public abstract class SingleEditPanel extends SingleQueryPanel
{

    //主面板新增编辑面板
    protected VLayout baseEditLayout;
    
    //主窗口面板
    protected Tab baseTabEdit;

    //主面板新增编辑Form
    protected DynamicForm baseEditForm;
    
    //编辑窗口面板
    protected BasePanel baseEditPanel;
    
    //编辑窗口高度，只有模板类型为SINGLE_PANEL_MORE时才有效
    protected String baseDataEditHeight;
  	
  	//编辑窗口宽度，只有模板类型为SINGLE_PANEL_MORE时才有效
  	protected String baseDataEditWidth;
  	
  	//编辑窗口标题，只有模板类型为SINGLE_PANEL_MORE时才有效
  	protected String baseDataEditTitle;
  	
  	//存储编辑时候的默认值
  	private Map<String, Object> defaultValue = new HashMap<String, Object>();

    @Override
    public void initBaseQuery()
    {
    	init();
    }

    private void init()
    {
        baseEditLayout = new VLayout();
        
        //构建新增编辑面板
        baseEditForm = createDataEditForm();
        if (null == baseEditForm.getDataSource())
        {
            baseEditForm.setDataSource(baseDataSource);
        }

        baseEditLayout.addMember(baseEditForm);
        
        //添加按钮
        baseToolStrip.addMember(this.baseBtnNew, 0);
        baseToolStrip.addMember(this.baseBtnEdit, 1);
        baseToolStrip.addMember(this.baseBtnSave, 2);
        baseToolStrip.addMember(this.baseBtnCancel, 3);
        baseToolStrip.addMember(this.baseBtnDelete, 4);
        if(importKeyStr() != null)
        {
        	baseToolStrip.addMember(this.baseBtnImp, 5);
        }
        
        if (BasePanelType.BASE_PANEL_SIMPLE.equals(basePanelType))
        {
        	baseTabEdit = new Tab("编辑");
        	baseTabEdit.setPane(baseEditLayout);
            baseTabSetEdit.addTab(baseTabEdit);
        }
        else if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType) 
        		/**|| BasePanelType.BASE_PANEL_TAB.equals(basePanelType)**/)
        {
        	baseEditPanel = new BaseEditPanel()
        	{
        		/**
        		 * 模板新增按钮方法
        		 */
        		public void doBaseAdd()
        		{
        			baseEditForm.editNewRecord();
        			baseEditPanel.baseBtnSave.enable();
        			doAddAfter();
        		}
        		
        		/**
        		 * 模板编辑按钮方法
        		 */
        		public void doBaseEdit()
        		{
        			baseEditForm.editSelectedData(baseListGrid);
        			baseEditPanel.baseBtnSave.enable();
        			doEditAfter();
        		}
        		
        		/**
        		 * 模板保存按钮方法
        		 */
        		public void doBaseSave()
        		{
        			doBaseEditSave();
        			baseBtnSave.disable();
        		}
        	};
        	baseEditPanel.baseMainLayout.addMember(baseEditLayout);
        	//设置编辑窗口大小默认值
			baseDataEditHeight = "60%";
			baseDataEditWidth = "50%";
			baseDataEditTitle = null == baseDataEditTitle ? "编辑" : baseDataEditTitle;
			
			//不需要取消按钮
			baseToolStrip.removeMember(this.baseBtnCancel);
			baseToolStrip.removeMember(this.baseBtnSave);
        }
    }
    
    /**
     * 子类重写该方法，返回导入模版的键值
     * 该键值作为模版名称，和配置文件中的Mapping的ID
     * @return
     */
    protected String importKeyStr()
    {
    	return null;
    }

    /**
     * 初始化编辑面板
     * 子类重写该方法，创建编辑面板
     * @return DynamicForm
     */
    protected DynamicForm createDataEditForm()
    {
    	DynamicForm boundForm = new DynamicForm();
		boundForm.setNumCols(2);
        return boundForm;
    }
    
    /**
	 * 新增的时候添加默认值
	 * 子类调用该方法设置默认值
	 * @param field 字段名
	 * @param value 值
	 */
	public void addDefaultValue(String field, Object value)
	{
		defaultValue.put(field, value);
	}
    
    /**
     * 模板新增按钮方法
     */
    @Override
    public void doBaseAdd()
    {
        if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType))
        {
        	showEditPanel();
        	baseEditPanel.baseBtnSave.enable();
        }
        /**
        else if(BasePanelType.BASE_PANEL_TAB.equals(basePanelType))
        {
        	showEditTab(baseDataSource.getID()+"_new","新增");
        	baseEditPanel.baseBtnSave.enable();
        }
        **/
        else
        {
            baseTabSetEdit.selectTab(baseTabEdit);
            baseBtnSave.enable();
        }
        baseEditForm.editNewRecord();
        baseEditForm.setValues(defaultValue);
        doAddAfter();
    }

    /**
     * 子类继承该方法处理处理点击新增后的一些控制
     * 例如：
     * 设置属性的默认值
     */
    protected void doAddAfter()
    {
    }

    /**
     * 模板编辑按钮方法
     */
    @Override
    public void doBaseEdit()
    {
    	ListGridRecord selectedRecord = baseListGrid.getSelectedRecord();
        if (null != selectedRecord)
        {
            if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType))
            {
            	showEditPanel();
            	baseEditPanel.baseBtnSave.enable();
            }
            /**
            else if(BasePanelType.BASE_PANEL_TAB.equals(basePanelType))
            {
            	String id = null;
            	Record fields = this.baseDataSource.getPrimaryKeyFields();
            	String[] keyField = fields.getAttributes();
        		if(keyField != null && keyField.length >= 1)
        		{
        			id = "tab_"+selectedRecord.getAttribute(keyField[0]).replace("-", "")+"_edit";
        		}
        		else
        		{
        			SC.warn("必须设置主键才能编辑！");
        		}
            	showEditTab(id, "编辑");
            	baseEditPanel.baseBtnSave.enable();
            }
            **/
            else
            {
                baseTabSetEdit.selectTab(baseTabEdit);
                baseBtnSave.enable();
            }
            baseEditForm.editRecord(selectedRecord);
            doEditAfter();
        }
        else
        {
            SC.say("请选择一条数据进行编辑！");
        }

    }

    /**
     * 子类继承该方法处理处理点击编辑后的一些控制
     * 例如：
     * 设置属性的默认值
     */
    protected void doEditAfter()
    {
    }

    /**
     * 模板删除按钮方法
     */
    @Override
    public void doBaseDelete()
    {
        if (null != baseListGrid.getSelectedRecord())
        {
            SC.confirm("确认删除这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
                    + "</strong>】</font>条数据吗？" + "删除后信息将无法恢复，请谨慎操作！", new BooleanCallback()
            {
                public void execute(Boolean value)
                {
                    if (value != null && value)
                    {
                        baseListGrid.removeSelectedData();
                    }
                }
            });
        }
        else
        {
            SC.say("请选择需要删除的数据！");
        }
    }

    /**
     * 子类继承该方法处理处理保存前的一些数据
     * 例如：
     * 保存产品字段时，将产品的id赋值给form
     */
    protected void doSaveBefore()
    {
    }
    
    /**
     * 子类继承该方法处理处理保存前的一些数据
     * 例如：
     * 保存产品字段时，将产品的id赋值给form
     */
    protected void doSavAfter()
    {
    }

    /**
     * 模板保存按钮方法
     */
    @Override
    public void doBaseSave()
    {
    	doBaseEditSave();
    }
    
    /**
     * 使用代理方法保存主要是为弹出面板用
     */
    protected void doBaseEditSave()
    {
    	if(validateForm(baseEditForm))
    	{
	    	doSaveBefore();
	        baseEditForm.saveData();
	        if (!baseEditForm.hasErrors())
	        {
	            doSavAfter();
	            baseEditForm.clearValues();
	            baseBtnSave.disable();
	            if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType) 
	            		/**	|| BasePanelType.BASE_PANEL_TAB.equals(basePanelType)**/)
	            {
	                //baseTabSetList.selectTab(this.baseTabList);
	            }
	            else
	            {
	                baseTabSetEdit.selectTab(this.baseTabViewer);
	            }
	        }
    	}
    }
    
    /**
	 * 子类重写该方法，对form中的数据进行校验
	 * @param editForm
	 * @return
	 */
	public boolean validateForm(DynamicForm editForm)
	{
		return true;
	}

    /**
     * 模板取消按钮方法
     */
    @Override
    public void doBaseCance()
    {
        baseBtnSave.disable();
        if (BasePanelType.BASE_PANEL_MORE.equals(basePanelType) 
        		/**|| BasePanelType.BASE_PANEL_TAB.equals(basePanelType)**/)
        {
        }
        else
        {
            baseTabSetEdit.selectTab(baseTabViewer);
        }
    }
    
    /**
	 * 模板导入按钮方法
	 */
    @Override
	public void doBaseImp()
	{
    	DataImportPanel impPanel = new DataImportPanel(importKeyStr());
		final Window winModal = new Window();
		winModal.setWidth(500);
		winModal.setHeight(200);
		winModal.setTitle("导入数据");
		winModal.setShowMinimizeButton(false);
		winModal.setIsModal(true);
		winModal.setShowModalMask(true);
		winModal.centerInPage();
		winModal.addItem(impPanel);
		winModal.show();
	}

    /**
     * 获得单表模板编辑页面的Form
     * @return DynamicForm
     */
    public DynamicForm getBaseDateForm()
    {
        return this.baseEditForm;
    }

    /**
     * 设置单表模板编辑页面的Form
     * @return DynamicForm
     */
    public void setBaseEditForm(DynamicForm baseEditForm)
    {
        this.baseEditForm = baseEditForm;
    }
    
    /**
     * 显示编辑面板
     */
    public void showEditPanel()
    {
    	final Window winModal = new Window();
		winModal.setWidth(baseDataEditWidth);
		winModal.setHeight(baseDataEditHeight);
		winModal.setTitle(baseDataEditTitle);
		winModal.setShowMinimizeButton(false);
		winModal.setIsModal(true);
		winModal.setShowModalMask(true);
		winModal.centerInPage();
		winModal.addItem(baseEditPanel);
		winModal.show();
    }
    
    /**
     * 获得编辑窗口高度，只有模板类型为SINGLE_PANEL_MORE时才有效
     * @return
     */
	public String getBaseDataEditHeight() {
		return baseDataEditHeight;
	}

	/**
	 * 设置编辑窗口高度，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @param baseDataEditHeight
	 */
	public void setBaseDataEditHeight(String baseDataEditHeight) {
		this.baseDataEditHeight = baseDataEditHeight;
	}

	/**
	 * 获得编辑窗口宽度，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @return
	 */
	public String getBaseDataEditWidth() {
		return baseDataEditWidth;
	}

	/**
	 * 设置编辑窗口宽度，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @param baseDataEditWidth
	 */
	public void setBaseDataEditWidth(String baseDataEditWidth) {
		this.baseDataEditWidth = baseDataEditWidth;
	}

	/**
	 * 获得编辑窗口标题，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @return
	 */
	public String getBaseDataEditTitle() {
		return baseDataEditTitle;
	}

	/**
	 * 设置编辑窗口标题，只有模板类型为SINGLE_PANEL_MORE时才有效
	 * @param baseDataEditTitle
	 */
	public void setBaseDataEditTitle(String baseDataEditTitle) {
		this.baseDataEditTitle = baseDataEditTitle;
	}
    
}
