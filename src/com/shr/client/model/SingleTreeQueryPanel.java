package com.shr.client.model;

import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.tree.TreeGrid;
import com.smartgwt.client.widgets.tree.TreeGridField;

public abstract class SingleTreeQueryPanel extends SingleQueryPanel
{
	
	/**
	 * 树控件
	 */
	protected TreeGrid baseTreeGrid;
	
	/**
	 * 树列表显示字段
	 */
	private String[] treeFields;
	
	/**
	 * 构造树
	 */
	public SingleTreeQueryPanel()
	{
		init();
	}
	
	/**
	 * 初始化树
	 */
	private void init()
	{
		baseTreeGrid.setShowFilterEditor(false);
		baseTreeGrid.setFilterOnKeypress(false);
		//设置是否能够拖动
		baseTreeGrid.setCanDrag(false);
	}
	
	/**
	 * 创建树显示的字段
	 * @return
	 */
	public abstract String[] buildTreeFields();

	/**
	 * 创建树列表
	 */
	@Override
	protected ListGrid createBaseGrid()
	{
		baseTreeGrid = new TreeGrid();
		
		baseTreeGrid.setCanFreezeFields(true);
		baseTreeGrid.setCanReparentNodes(true);
		baseTreeGrid.setFilterOnKeypress(false);
		
		baseTreeGrid.setAnimateFolders(true);
		baseTreeGrid.setLoadDataOnDemand(false);
		baseTreeGrid.setShowFilterEditor(false);
		
		treeFields = buildTreeFields();
		//设置树显示的列
		if(treeFields != null)
		{
			TreeGridField[] treeGridField = new TreeGridField[treeFields.length];
			for(int i=0; i<treeFields.length; i++)
			{
				treeGridField[i] = new TreeGridField(treeFields[i]);
			}
			baseTreeGrid.setFields(treeGridField);
		}
		
		return baseTreeGrid;
	}
}
