package com.shr.client.model;

import java.util.HashMap;

import com.shr.client.model.util.BasePanelType;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.viewer.DetailViewer;

/**
 * 主从模板根据BasePanelType的不同可以分为两种
 * 字段少的情况选择第一种类型，字段比较多选择第二种
 * 主表和从表类型可以分开设置，主表表重写initSinglePanelType() ,从表重写initChildPanelType();
 * 1.主从模板(BasePanelType.BASE_EDIT_PANEL_SIMPLE)
 *——————————————————————————————————————————————————————-————————————
 *| 新建 | 编辑 | 删除 | 保存 | 取消 | 查询                                                                                 |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|______________________________ _______   ______                   |
 *|                              |__浏览_|__|_编辑_|_________________|
 *|                              |                                   |
 *|                              |   控件摆放                                                                 |
 *|                              |                                   |
 *|______________________________|___________________________________|
 *|________                       _________   ______                 |
 *| 明细表    |_____________________| 明细浏览 |__|_编辑|________________|
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *————————————————————————————————————————————————————————————————————
 *
 * 2.主从模板(BasePanelType.BASE_PANEL_MORE)
 *——————————————————————————————————————————————————————-————————————
 *|新建 | 编辑 | 删除 | 保存 | 取消 | 查询  | 查看                                                                   |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|__________________________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|      列表                  /    编辑                                                                                                             |
 *|__________________________________________________________________|
 *| 新建 | 编辑 | 删除 | 保存 | 取消 | 查看                                                                                  |
 *|_______   ______                                                  |
 *| 明细表 |__|_编辑_|________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *————————————————————————————————————————————————————————————————————
 * 点击查看浏览窗口
 *              
 * 主从表浏览继承该模板；分别实现下列方法以展示单表数据。
 * 子类实现 createQueryForm(); 方法返回主表查询条件面板；
 * 子类实现 createDataSource(); 方法返回主表数据源；
 * 子类实现 createDataViewPanel(); 方法返回主表浏览面板的布局样式；
 * 
 * 子类调用addChild(String title, DataSource childDS, DetailViewer dataView, DynamicForm editForm)方法
 * 以实现添加字表信息，可以添加多个
 * 
 * 子类还可以通过baseToolStrip属性获得模板的工具栏，以达到添加特定按钮的需要；
 * 例如在模板的基础上添加 “导出” 按钮
 * 子类可以实现代码如下
 *    baseToolStrip.addMember(new ToolStripButton("导出"));
 * 子类还可通过此方法删除不用的按钮
 *      baseToolStrip.removeMember(this.baseBtnCancel); //移除取消按钮
 *      
 * 子类还可以通过baseChildPanel.baseToolStrip属性获得从表模板的工具栏，进行编辑，类似主表。
 *    
 * 这样的模板是不是可以给你带来方便呢？ ：）
 * @author zw
 *
 */
public abstract class FiliationEditPanel extends SingleEditPanel{
	
    //存放子表结构的面板
    protected FiliationChildPanel baseChildPanel;
    
    //编辑子模板类型，暂时包括两种
  	protected String baseChildPanelType;
    
	public FiliationEditPanel(){
		init();
		//initChildToolBar();
	}
	
	/**
	 * 初始化子模板类型
	 * 子类可以重写该方法改变模板类型
	 * 默认为简单型的
	 * @return
	 */
	protected String initChildPanelType(){
		return BasePanelType.BASE_PANEL_SIMPLE;
	}
	
	/**
	 * 初始化方法
	 */
	private void init(){
	    
		super.baseTabLayout.setShowResizeBar(true);
		baseChildPanelType = initChildPanelType();
		initChildPanel();
        this.baseMainLayout.addMember(baseChildPanel);
	}
	
	/**
	 * 处理子表的编辑条
	 */
	private void initChildToolBar()
	{
		baseChildPanel.baseToolStrip.setVisible(false);
		baseToolStrip.addResizer();
		
	}
	
	/**
	 * 初始化子类模板
	 */
	private void initChildPanel(){
		baseChildPanel = new FiliationChildPanel(BasePanelType.ACTION_TYPE_EDIT, baseChildPanelType, baseListGrid){
			@Override
			public void doBaseAdd(){
				doBaseChildAdd();
			}
			@Override
			public void doBaseEdit(){
				doBaseChildEdit();
			}
			@Override
			public void doBaseDelete(){
				doBaseChildDelete();
			}
			@Override
			public void doBaseSave(){
				doBaseChildSave();
			}
			@Override
			public void doBaseCance(){
				doBaseChildCance();
			}
			@Override
			public void doBaseView(){
				doBaseChildView();
			}
			@Override
			public void doBaseQuery(){
				doBaseChildQuery();
			}
			@Override
			protected void doCildAddAfter(){
				doBaseCildAddAfter();
			}
			@Override
			protected void doChildSaveBefore(){
				doBaseChildSaveBefore();
			}
			@Override
			protected void doCildEditAfter(){
				doBaseCildEditAfter();
			}
			@Override
			protected void onChildTabSelected(TabSelectedEvent event)
		    {
				onBaseChildTabSelected(event);
		    }
		};
	}
	
	/**
     * 获得子表ToolStrip
     * 可以通过该方法添加新按钮
     * @return
     */
    public ToolStrip getBaseChildToolStrip()
    {
    	return baseChildPanel.getBaseToolStrip();
    }
	
	/**
	 * 获得子表弹出浏览窗口的高度
	 * @return
	 */
	public String getBaseChildDataViewHeight() {
		return baseChildPanel.getBaseChildDataViewHeight();
	}

	/**
	 * 设置子表弹出窗口的高度
	 * @param baseChildDataViewHeight
	 */
	public void setBaseChildDataViewHeight(String baseChildDataViewHeight) {
		baseChildPanel.setBaseChildDataViewHeight(baseChildDataViewHeight);
	}

	/**
	 * 获得子表弹出窗口的宽度
	 * @return
	 */
	public String getBaseChildDataViewWidth() {
		return baseChildPanel.getBaseChildDataViewWidth();
	}

	/**
	 * 设置子表弹出窗口的宽度
	 * @return
	 */
	public void setBaseChildDataViewWidth(String baseChildDataViewWidth) {
		baseChildPanel.setBaseChildDataViewWidth(baseChildDataViewWidth);
	}
	
	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * 显示与编辑区区域为模板默认
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 子表与主表对应关系Map<子表字段，主表字段>
     */
	public void addModelChild(String title, DataSource childDS, HashMap<String, String> fields){
		baseChildPanel.addModelChild(title, childDS, fields, new DetailViewer(), new DynamicForm(), new ListGrid());
	}

	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 子表与主表对应关系Map<子表字段，主表字段>
     * @param dataView 子表数据显示面板
     */
    public ListGrid addModelChild(String title, DataSource childDS, 
            HashMap<String, String> fields, DetailViewer dataView, DynamicForm editForm, ListGrid grid){
    	return baseChildPanel.addModelChild(title, childDS, fields, dataView, editForm, grid);
    }
    
    
    /**
     * 主表的点击事件
     * @param event
     */
    @Override
    public void doRecordClick(RecordClickEvent event) {
        super.doRecordClick(event);
        baseChildPanel.doFetchData(event);
    }
    
    /**
     * 子表的查看按钮
     */
    public void doBaseChildView(){
    	baseChildPanel.doChildView();
    }
    
    /**
     * 子表新增按钮方法
     */
    public void doBaseChildAdd(){
    	baseChildPanel.doChildAdd();
    }
    
    /**
     * 子表刷新方法
     */
    public void doBaseChildQuery(){
    	baseChildPanel.doChildQuery(this.baseListGrid.getSelectedRecord());
    }
    
    
    /**
     * 点击子表新增后操作
     * 可以给数据赋初始值
     */
    protected void doBaseCildAddAfter(){}
    
    /**
     * 子表编辑按钮方法
     */
    public void doBaseChildEdit(){
    	baseChildPanel.doChildEdit();
    }
    
    /**
     * 点击子表编辑后操作
     * 可以给数据赋初始值
     */
    protected void doBaseCildEditAfter(){}
    
    /**
     * 子表删除按钮方法
     */
    public void doBaseChildDelete(){
    	baseChildPanel.doChildDelete();
    }
    
    /**
	 * 子类继承该方法处理处理保存前的一些数据
	 * 例如：
	 * 保存产品字段时，将产品的id赋值给form
	 */
	protected void doBaseChildSaveBefore(){}
    
    /**
     * 子表保存按钮方法
     */
    public void doBaseChildSave(){
    	baseChildPanel.doChildSave();
    }
    
    /**
     * 子表取消按钮方法
     */
    public void doBaseChildCance(){
    	baseChildPanel.doChildCance();
    }
    
    /**
     * 子表的tab select事件
     * @param event
     */
    protected void onBaseChildTabSelected(TabSelectedEvent event){}
}
