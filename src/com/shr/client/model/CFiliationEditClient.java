package com.shr.client.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gwt.http.client.Response;
import com.shr.client.model.xml.XmlFiliationEditPanel;
import com.shr.client.model.xml.XmlSingleEditPanel;
import com.shr.client.model.xml.util.XmlBaseStr;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.docs.DsResponse;
import com.smartgwt.client.widgets.Canvas;

public class CFiliationEditClient extends XmlFiliationEditPanel {

	@Override
	public List<Map<String, Object>> buildChildContent() {
		List<Map<String, Object>> childList = new ArrayList<Map<String, Object>>();
		DataSource dsDetailPage = DataSource.get("CDetailPage");
		Criteria param = new Criteria();
		param.addCriteria("pk_cpage", "");
//		DsResponse
//		dsDetailPage.fetchData(param, new DSCallback() {
//			
//			@Override
//			public void execute(DSResponse dsResponse, Object data, DSRequest dsRequest) {
//				// TODO Auto-generated method stub
//				
//			}
//		});
		
		
		XmlSingleEditPanel dicValuePanel = new XmlSingleEditPanel()
		{
			@Override
			public void initUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void initFormUIObject(Map<String, Canvas> canvas) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public String createToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createPanelTitle() {
				// TODO Auto-generated method stub
				return "从表";
			}

			@Override
			public String createFormToolStripUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createEditFormUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createQueryFormUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String createListGridUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] createDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String[] createFormDiyUI() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String initPanelType() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public Criteria getDefaultCriteria() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public String importKeyStr() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			protected DataSource createDataSource() {
				// TODO Auto-generated method stub
				return DataSource.get("CDetailPage");
			}

			@Override
			protected String getAMID() {
				// TODO Auto-generated method stub
				return null;
			}
			
		};
		
		
		
		HashMap<String, String> ValueFields = new HashMap<String, String>();
		ValueFields.put("PK_CPage", "PK_CPage");
        
        Map<String, Object> childDicValueMap = new HashMap<String, Object>();
        childDicValueMap.put(XmlBaseStr.BASE_CHILD_TITLE, "从表");
        childDicValueMap.put(XmlBaseStr.BASE_CHILD_PANEL, dicValuePanel);
        childDicValueMap.put(XmlBaseStr.BASE_CHILD_KEYMAP, ValueFields);
        childList.add(childDicValueMap);
        
		return childList;
	}

	@Override
	public void initUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub

	}

	@Override
	public void initFormUIObject(Map<String, Canvas> canvas) {
		// TODO Auto-generated method stub

	}

	@Override
	public String createToolStripUI() {
		// TODO Auto-generated method stub
		// return "DICCATEGORYToolStrip";
		return null;
	}

	@Override
	public String createPanelTitle() {
		// TODO Auto-generated method stub
		return "页面设计";
	}

	@Override
	public String createFormToolStripUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createEditFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createQueryFormUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String createListGridUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] createFormDiyUI() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String initPanelType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Criteria getDefaultCriteria() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String importKeyStr() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected DataSource createDataSource() {
		// TODO Auto-generated method stub
		return DataSource.get("CPage");
	}

	@Override
	protected String getAMID() {
		// TODO Auto-generated method stub
		return null;
	}

}
