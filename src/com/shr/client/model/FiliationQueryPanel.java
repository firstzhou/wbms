package com.shr.client.model;

import java.util.HashMap;

import com.shr.client.model.util.BasePanelType;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.viewer.DetailViewer;

/**
 * 主从模板根据BasePanelType的不同可以分为两种
 * 字段少的情况选择第一种类型，字段比较多选择第二种
 * 主表和从表类型可以分开设置，主表表重写initSinglePanelType() ,从表重写initChildPanelType();
 * 1.主从模板(BasePanelType.BASE_EDIT_PANEL_SIMPLE)
 *——————————————————————————————————————————————————————-————————————
 *| 查询                                                                                                                                                                   |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|________                       _______                            |
 *|  主表      |_____________________|__浏览_|___________________________|
 *|                              |                                   |
 *|                              |   控件摆放                                                                 |
 *|                              |                                   |
 *|______________________________|___________________________________|
 *|________                       _________                          |
 *| 明细表    |_____________________| 明细浏览 |_________________________|
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *————————————————————————————————————————————————————————————————————
 *
 * 2.主从模板(BasePanelType.BASE_PANEL_MORE)
 *——————————————————————————————————————————————————————-————————————
 *| 查询  | 查看                                                                                                                                                  |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|________                                                          |
 *|  主表      |_________________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|      列表                  /    编辑                                                                                                             |
 *|__________________________________________________________________|
 *| 查看                                                                                                                                                                   |
 *|_______                                                           |
 *| 明细表 |__________________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *————————————————————————————————————————————————————————————————————
 * 点击查看浏览窗口
 *              
 * 主从表浏览继承该模板；分别实现下列方法以展示单表数据。
 * 子类实现 createQueryForm(); 方法返回查询条件面板；
 * 子类实现 createDataSource(); 方法返回数据源；
 * 子类实现 createDataViewPanel(); 方法返回浏览面板的布局样式；
 * 
 * 子类调用addChild(String title, DataSource childDS, DetailViewer dataView)方法
 * 以实现添加字表信息，可以添加多个
 * 
 * 子类还可以通过baseToolStrip属性获得模板的工具栏，以达到添加特定按钮的需要；
 * 例如在模板的基础上添加 “导出” 按钮
 * 子类可以实现代码如下
 *    baseToolStrip.addMember(new ToolStripButton("导出"));
 * 子类还可通过此方法删除不用的按钮
 *		baseToolStrip.removeMember(this.baseBtnCancel); //移除取消按钮
 *    
 * 子类还可以通过baseChildPanel.baseToolStrip属性获得从表模板的工具栏，进行编辑，类似主表。
 *    
 * 这样的模板是不是可以给你带来方便呢？ ：）
 * @author zw
 *
 */
public abstract class FiliationQueryPanel extends SingleQueryPanel{
	
	//存放子表结构的面板
	protected FiliationChildPanel baseChildPanel;
	
	//编辑子模板类型，暂时包括两种
  	protected String baseChildPanelType;
  	
	/**
	 * 初始构造函数
	 */
	public FiliationQueryPanel() {
		init();
		initChildToolBar();
	}
	
	private void init(){
		
		super.baseTabLayout.setShowResizeBar(true);
		baseChildPanelType = initChildPanelType();
		initChildPanel();
		this.baseMainLayout.addMember(baseChildPanel);
	}
	
	private void initChildToolBar()
	{
		baseChildPanel.baseToolStrip.setVisible(true);
	}
	
	/**
	 * 初始化子类模板
	 */
	private void initChildPanel(){
		baseChildPanel = new FiliationChildPanel(BasePanelType.ACTION_TYPE_QUERY, baseChildPanelType, baseListGrid){
			@Override
			public void doBaseView(){
				doBaseChildView();
			}
		};
	}
	
	/**
	 * 初始化子表模板类型
	 * 子类可以重写该方法改变模板类型
	 * 默认为简单型的
	 * @return
	 */
	protected String initChildPanelType(){
		return BasePanelType.BASE_PANEL_SIMPLE;
	}
	
	/**
	 * 添加字表信息，包括标题，数据子表源，子表数据显示面板
	 * @param title 包括标题
	 * @param childDS 数据子表源
	 * @param fields 子表与主表对应关系Map<子表字段，主表字段>
	 * @param dataView 子表数据显示面板
	 */
	public ListGrid addModelChild(String title, DataSource childDS, 
			HashMap<String, String> fields, DetailViewer dataView){
		return baseChildPanel.addModelChild(title, childDS, fields, dataView, new DynamicForm(), new ListGrid());
	}
	
	/**
     * 添加字表信息，包括标题，数据子表源，子表数据显示面板
     * @param title 包括标题
     * @param childDS 数据子表源
     * @param fields 子表与主表对应关系Map<子表字段，主表字段>
     * @param dataView 子表数据显示面板
     */
    public ListGrid addModelChild(String title, DataSource childDS, 
            HashMap<String, String> fields, DetailViewer dataView, DynamicForm editForm, ListGrid grid){
    	return baseChildPanel.addModelChild(title, childDS, fields, dataView, editForm, grid);
    }
	
	/**
	 * 子表的点击事件
	 * @param event
	 */
	@Override
	public void doRecordClick(RecordClickEvent event) {
		super.doRecordClick(event);
		baseChildPanel.doFetchData(event);
	}
	
	/**
	 * 子表的查看按钮
	 */
	public void doBaseChildView(){
		baseChildPanel.doChildView();
	}
	
	/**
	 * 获得子表弹出浏览窗口的高度
	 * @return
	 */
	public String getBaseChildDataViewHeight() {
		return baseChildPanel.getBaseChildDataViewHeight();
	}

	/**
	 * 设置子表弹出窗口的高度
	 * @param baseChildDataViewHeight
	 */
	public void setBaseChildDataViewHeight(String baseChildDataViewHeight) {
		baseChildPanel.setBaseChildDataViewHeight(baseChildDataViewHeight);
	}

	/**
	 * 获得子表弹出窗口的宽度
	 * @return
	 */
	public String getBaseChildDataViewWidth() {
		return baseChildPanel.getBaseChildDataViewWidth();
	}

	/**
	 * 设置子表弹出窗口的宽度
	 * @return
	 */
	public void setBaseChildDataViewWidth(String baseChildDataViewWidth) {
		baseChildPanel.setBaseChildDataViewWidth(baseChildDataViewWidth);
	}
	
}
