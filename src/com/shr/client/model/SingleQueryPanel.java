package com.shr.client.model;


import java.util.HashMap;
import java.util.Map;

import com.shr.client.model.util.BasePanelType;
import com.shr.client.model.xml.XmlDiscussClient;
import com.shr.client.tool.ClientTool;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordClickHandler;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

/**
 * 单表模板根据BasePanelType的不同可以分为两种
 * 字段少的情况选择第一种类型，字段比较多选择第二种
 * 1.单表模板(BasePanelType.BASE_EDIT_PANEL_SIMPLE)
 *——————————————————————————————————————————————————————-————————————
 *| 查询                                                                                                                                                                   |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|______________________________ _______                            |
 *|                              |__浏览_|___________________________|
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *|      列表                                                      |           控件摆放                                            |
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *|                              |                                   |
 *————————————————————————————————————————————————————————————————————
 *
 * 2.单表模板(BasePanelType.BASE_PANEL_MORE)
 *——————————————————————————————————————————————————————-————————————
 *| 查询  | 查看                                                                                                                                                  |
 *|—————————————————————————————————————————————————————————————————-|
 *|                                                                  |
 *|   查询条件                                                                                                                                                    |
 *|__________________________________________________________________|
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|      列表                  /    编辑                                                                                                             |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *|                                                                  |
 *————————————————————————————————————————————————————————————————————
 * 点击查看浏览窗口
 *              
 * 单表浏览继承该模板；分别实现下列方法以展示单表数据。
 * 子类实现 createQueryForm(); 方法返回查询条件面板；
 * 子类实现 createDataSource(); 方法返回数据源；
 * 子类实现 createDataViewPanel(); 方法返回浏览面板的布局样式；
 * 
 * 子类还可以通过baseToolStrip属性获得模板的工具栏，以达到添加特定按钮的需要；
 * 例如在模板的基础上添加 “导出” 按钮
 * 子类可以实现代码如下
 *    baseToolStrip.addMember(new ToolStripButton("导出"));
 * 子类还可通过此方法删除不用的按钮
 *		baseToolStrip.removeMember(this.baseBtnCancel); //移除取消按钮
 *    
 * 这样的模板是不是可以给你带来方便呢？ ：）
 * @author zw
 *
 */
public abstract class SingleQueryPanel extends BasePanel {
	
	//查询条件
	protected DynamicForm baseQueryForm;
	
	//列表与编辑tab布局
	protected HLayout baseTabLayout;
	
	//主窗口面板
	protected HLayout baseTabSetList;
	
	//浏览页面
	protected TabSet baseTabSetEdit;
	
	//主窗口面板
	protected HLayout baseTabList;
	
	//展示面板
	protected Tab baseTabViewer;
	
	//主面板列表
	protected ListGrid baseListGrid;
	
	//主面板浏览
	protected DynamicForm baseDataView;
	
	//浏览窗口高度，只有模板类型为SINGLE_PANEL_MORE时才有效
	private String baseDataViewHeight;
	
	//浏览窗口宽度，只有模板类型为SINGLE_PANEL_MORE时才有效
	private String baseDataViewWidth;
	
	//浏览窗口标题，只有模板类型为SINGLE_PANEL_MORE时才有效
	private String baseDataViewTitle;
	
	//单表数据源
	protected DataSource baseDataSource;
	
	//刷新的时候默认过滤条件
  	private Map<String, Object> defaultCriteria;
	
	@Override
	public void initBase()
	{
		init();
		initToolBarForm();
		initBaseQuery();
	};
	
	/**
	 * 子类重写该方法，保证子类的初始化操作在该方法里
	 */
	public void initBaseQuery(){};
	
	private void init(){
		defaultCriteria = new HashMap<String, Object>();
		baseTabLayout = new HLayout();
		baseTabList = new HLayout();
		baseTabViewer = new Tab("浏览");
		baseTabSetList = new HLayout();
		
		baseQueryForm = createQueryForm();
		baseDataSource = createDataSource();
		baseListGrid = createBaseGrid();
		//构建表单
		initListGrid();
		//构建浏览面板
		baseDataView = createDataViewPanel();
		if(null == baseDataView.getDataSource())
		{
			baseDataView.setDataSource(baseDataSource);
		}
		
		baseTabList.addMember(baseListGrid);
		
		if(BasePanelType.BASE_PANEL_SIMPLE.equals(basePanelType)){
			baseTabSetList.setWidth("65%");
			baseTabSetList.addMember(baseTabList);
			baseTabSetEdit = new TabSet();
			baseTabSetEdit.addTab(baseTabViewer);
			baseTabLayout.addMember(baseTabSetList);
			baseTabLayout.addMember(baseTabSetEdit);
			baseTabViewer.setPane(baseDataView);
		} else if(BasePanelType.BASE_PANEL_MORE.equals(basePanelType) 
				/**|| BasePanelType.BASE_PANEL_TAB.equals(basePanelType)**/){
			baseTabSetList.setWidth100();
			baseTabSetList.addMember(baseTabList);
			baseTabLayout.addMember(baseTabSetList);
			//设置浏览窗口大小默认值
			baseDataViewHeight = "60%";
			baseDataViewWidth = "50%";
			baseDataViewTitle = null == baseDataViewTitle ? "查看" : baseDataViewTitle;
		}
		if(baseQueryForm.getFields() != null && baseQueryForm.getFields().length != 0)
		{
			baseMainLayout.addMember(baseQueryForm);
		}
		baseMainLayout.addMember(baseTabLayout);
		baseListGrid.addRecordDoubleClickHandler(new RecordDoubleClickHandler(){

			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				// TODO Auto-generated method stub
				doBaseView();
			}
		});
		if(isAutoFetchData())
		{
			this.doBaseQuery();
		}
	}

	/**
	 * 初始化工具条
	 */
	private void initToolBarForm() {
		baseToolStrip.addMember(baseBtnQuery);
		baseToolStrip.addMember(baseBtnView);
		baseToolStrip.addMember(baseBtnDiscuss);
		//如果没有定义评论字符串则隐藏评论按钮
		if(viewDiscuss() == null || "".equals(viewDiscuss()))
		{
			baseBtnDiscuss.setVisible(false);
		}
		if(BasePanelType.BASE_PANEL_SIMPLE.equals(basePanelType)){
			baseToolStrip.removeMember(baseBtnView);
		}
		
		baseToolStrip.addMember(baseBtnExport);
	}
	
	/**
	 * 创建列表
	 * @return ListGrid
	 */
	private void initListGrid(){
		if(null == baseListGrid){
			baseListGrid = new ListGrid();
		}
		baseListGrid.setDataSource(baseDataSource);

		baseListGrid.setShowFilterEditor(true);
		baseListGrid.setFilterOnKeypress(true);
		//隔行变色
		baseListGrid.setAlternateRecordStyles(true);
		//显示行号
		//baseListGrid.setShowRowNumbers(true);  
		
		baseListGrid.addRecordClickHandler(new RecordClickHandler() {
			public void onRecordClick(RecordClickEvent event) {
				doRecordClick(event);
			}
		});
	}
	
	/**
	 * 子类重写该方法，设置grid是否自动加载
	 * 默认自动加载
	 * @return
	 */
	protected boolean isAutoFetchData()
	{
		return true;
	}
	
	/**
	 * 初始化查询条件
	 * 子类重写该方法，添加查询条件
	 * @return DynamicForm
	 */
	protected DynamicForm createQueryForm(){
		return new DynamicForm();
	}
	
	/**
	 * 初始化列表对象
	 * 子类重写该方法，如重写改方法创建树形表单
	 * @return ListGrid
	 */
	protected ListGrid createBaseGrid(){
		return new ListGrid();
	}
	
	/**
	 * 初始化单表数据源
	 * 子类重写该方法，创建数据源
	 * @return DataSource
	 */
	protected abstract DataSource createDataSource();
	
	/**
	 * 初始化浏览面板
	 * 子类重写该方法，创建浏览面板
	 * @return DetailViewer
	 */
	protected DynamicForm createDataViewPanel(){
		return new DynamicForm();
	}
	
	/**
	 * 模板查询按钮方法
	 */
	@Override
	public void doBaseQuery()
	{
		baseBtnSave.disable();
		baseListGrid.invalidateCache();
		Criteria dcriteria = getDefaultCriteria();
		if(dcriteria == null)
		{
			dcriteria = new Criteria();
		}
		//添加默认的过滤条件
		for(String fileld : defaultCriteria.keySet())
		{
			dcriteria.addCriteria(fileld, defaultCriteria.get(fileld));
		}
		if(baseQueryForm != null)
		{
			dcriteria.addCriteria(baseQueryForm.getValuesAsCriteria());
		}
		baseListGrid.fetchData(dcriteria);
	}
	
	/**
	 * 获得默认查询条件
	 * @return
	 */
	public Criteria getDefaultCriteria()
	{
		return new Criteria();
	}
	
	/**
	 * 表格刷新的时候添加默认过滤条件
	 * 子类调用该方法添加默认条件，可以添加多个
	 * @param field 字段名
	 * @param value 值
	 */
	public void addDefaultCriteria(String field, Object value)
	{
		defaultCriteria.put(field, value);
	}
	
	/**
	 * 模板查看按钮方法
	 */
	@Override
	public void doBaseView(){
		if (null != baseListGrid.getSelectedRecord()) {
			Window winModal = new Window();
			winModal.setWidth(baseDataViewWidth);
			winModal.setHeight(baseDataViewHeight);
			winModal.setTitle(baseDataViewTitle);
			winModal.setShowMinimizeButton(false);
			winModal.setIsModal(true);
			winModal.setShowModalMask(true);
			winModal.centerInPage();
			winModal.addItem(baseDataView);
			winModal.show();
		} else {
			SC.say("请先选择一条数据进行查看！");
		}
	}
	
	/**
	 * 模板导出按钮方法
	 */
	@Override
	public void doBaseExport(){
		
		DSRequest dsRequestProperties = new DSRequest();  
		//以xls格式导出
        dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
        dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);

        baseListGrid.exportClientData(dsRequestProperties);
	}
	
	/**
	 * 显示评论页面
	 */
	public void doBaseDiscuss()
	{
		if(baseListGrid.getSelectedRecords() == null || baseListGrid.getSelectedRecords().length != 1)
		{
			SC.warn("请选择一条数据进行评论！");
		}
		else
		{
			
			final DynamicForm editForm = new DynamicForm();
			
			//获得主键的值
			String pkObject = null;
			String viewObject = null;
			String tableName = null;
			String primaryKey = null;
			String objectWHR = null;
			DataSource ds = baseListGrid.getDataSource();
			tableName = ds.getID(); //表名
		
			editForm.setDataSource(ds);
			editForm.editSelectedData(baseListGrid);
			
			Record fields = ds.getPrimaryKeyFields();
	    	String[] keyField = fields.getAttributes();
	    	
			if(keyField != null && keyField.length >= 1)
			{
				primaryKey = keyField[0];
				objectWHR = baseListGrid.getSelectedRecord().getAttribute("WHRID");
				pkObject = baseListGrid.getSelectedRecord().getAttribute(keyField[0]);
				viewObject = baseListGrid.getSelectedRecord().getAttribute(viewDiscuss());
				if(pkObject == null)
				{
					SC.warn("List中必须包含主键属性才能用此功能！");
				}
			}
			else
			{
				SC.warn("必须设置主键才能用此功能！");
			}
			XmlDiscussClient dis = new XmlDiscussClient(editForm, tableName, primaryKey, 
					pkObject, viewObject, objectWHR, false);
			ClientTool.open("pinglun"+pkObject.replace("-", ""), "评论", dis);
		}
	}
	
	/**
	 * 控制模版是否显示评论按钮
	 * @return 返回需要评论的标题字段
	 */
	public String viewDiscuss()
	{
		return null;
	}
	
	/**
	 * 模板选择数据触发方法
	 */
	public void doRecordClick(RecordClickEvent event){
		baseDataView.editRecord(baseListGrid.getSelectedRecord());
		//baseDataView.setData(baseListGrid.getSelectedRecords());
	}
	
	/**
	 * 动态设置列表标题
	 * @param name
	 */
	public void setListTebTitle(String name){
		baseTabList.setTitle(name);
	}
	
	/**
	 * 获得listGrid上选择的记录
	 * @return ListGridRecord
	 */
	public ListGridRecord getGridSelectedRecord(){
		return this.baseListGrid.getSelectedRecord();
	}
	

	public String getDataViewHeight() {
		return baseDataViewHeight;
	}

	public void setDataViewHeight(String baseDataViewHeight) {
		this.baseDataViewHeight = baseDataViewHeight;
	}

	public String getDataViewWidth() {
		return baseDataViewWidth;
	}

	public void setDataViewWidth(String baseDataViewWidth) {
		this.baseDataViewWidth = baseDataViewWidth;
	}

	public String getDataViewTitle() {
		return baseDataViewTitle;
	}

	public void setDataViewTitle(String baseDataViewTitle) {
		this.baseDataViewTitle = baseDataViewTitle;
	}
}
