package com.shr.client.wbms;

import java.util.HashMap;
import java.util.Map;

import com.google.gwt.user.client.Window;
import com.shr.client.model.xml.*;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.form.fields.FormItem;
import com.smartgwt.client.widgets.form.fields.SelectItem;
import com.smartgwt.client.widgets.form.fields.events.ChangedEvent;
import com.smartgwt.client.widgets.form.fields.events.ChangedHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class JFGZ_HClient extends XmlBasePanel{
    
	ListGrid baseListGrid =null;
	
	ListGrid baseListGrid_b =null;
	
	ToolStripButton butNew = null;
	
	ToolStripButton butDel = null;
	
	ToolStripButton btnSave = null;
	
	ToolStripButton butImport = null;
	
	DynamicForm  boundForm =null;
	
	DynamicForm  boundFormB =null;
	
	ToolStripButton btnEdit = null;
	
	ToolStripButton butClose = null; //取消
	
	ToolStripButton butCopy = null;
	
	ToolStripButton butView = null; //查看
	
    ToolStripButton butExp = null;
	
	TabSet  set = null;
	
	TabSet  setB = null;
	
	int tabNum = -1;//标签行
	
	int VE = -1;//查看或修改（1查看，0修改，新增）
	
	//计费字段
	Map<String , String > map = new HashMap<String ,String >();
	
	
//	map1.费用类型为进出费、装卸费、订单处理费，进出类型为进，WMS字段选择为：
//	订单类型、总数量、总重量、总体积
//
//	map2.费用类型为进出费、装卸费、订单处理费，进出类型为出，WMS字段选择为：
//	订单类型、总数量、总重量、总体积、详细地址、收件人、客户名称
//
//	map3.费用类型为仓储租金，WMS字段选择为：
//	总重量，总体积，总数量，基础数据（项目档案）里面的仓库面积
	//WMS字段
	Map<String , String > map1 = new HashMap<String ,String >();
	Map<String , String > map2 = new HashMap<String ,String >();
	Map<String , String > map3 = new HashMap<String ,String >();
	
	
	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	@Override
	public String[] canvasUI() {
		
		return new String [] {"wbms/ORDER_JFGZ_H"}  ;
	}
	
	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	@Override
	public void initChild(Map<String, Canvas> baseCanvas) {
		super.initChild(baseCanvas);
	    Canvas  canvas =  baseCanvas.get("wbms/ORDER_JFGZ_H");
	    
	    baseListGrid = (ListGrid)canvas.getByLocalId("baseListGrid"); 
	    baseListGrid_b = (ListGrid)canvas.getByLocalId("baseListGrid_B"); 
	    //显示行号
	    baseListGrid.setShowRowNumbers(true);
	    baseListGrid_b.setShowRowNumbers(true);
	    //显示复选框
	    baseListGrid.setSelectionType(SelectionStyle.SIMPLE);
	    baseListGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);
	    butExp = (ToolStripButton) canvas.getByLocalId("butExp");
	    butNew =(ToolStripButton) canvas.getByLocalId("butNew");
	    butDel =(ToolStripButton) canvas.getByLocalId("butDel");
	    butCopy =(ToolStripButton) canvas.getByLocalId("butCopy");
	    btnSave =(ToolStripButton) canvas.getByLocalId("btnSave");
	    boundForm =(DynamicForm) canvas.getByLocalId("JFGZ_HForm");
	    boundFormB =(DynamicForm) canvas.getByLocalId("JFGZ_BForm");
	    butClose = (ToolStripButton) canvas.getByLocalId("butClose");
	    butView = (ToolStripButton) canvas.getByLocalId("butView");
	    btnEdit = (ToolStripButton) canvas.getByLocalId("butEdit");
	    set = (TabSet) canvas.getByLocalId("TabSet2");
	    setB = (TabSet) canvas.getByLocalId("TabSetB");
	    
	    set.setVisible(false);//隐藏新增修改表格
	    setB.setVisible(false);//隐藏新增修改表格
	    btnSave.setVisible(false);//隐藏保存按钮
	    butClose.setVisible(false);//隐藏取消按钮
	    baseListGrid_b.setVisible(false);
	   
	    boundForm.getItem("decimal_digits").setVisible(false);
	    boundForm.getItem("INOUT").setVisible(false);
	    
	    decimalSelect();//小数点处理方式
	    changeSelect();//项目编码下拉
	    FYLXSelect();//费用类型（计费字段）
//	    autoMap();//WMS字段
	    
	    set.addTabSelectedHandler(new TabSelectedHandler(){

            @Override
            public void onTabSelected(TabSelectedEvent event) {

            	tabNum = event.getTabNum();
	        	 if(tabNum == 0) {
	        		
	        		 //VE （1查看，0修改新增）
	        		 if(VE == 0) {
	        			 butNew.setVisible(true); 
	        			 btnSave.setVisible(true);//显示保存按钮
        			 }	
    				 butNew.setVisible(false); 
    				 btnEdit.setVisible(false);
    				 butDel.setVisible(false);// 删除按钮
	        		 
	        	 }else if(tabNum == 1){
	        		 AutoFYLXSelect();
	        		 autoMap();
//	        		 boundFormB.setVisible(false);
	        		 FormItem value = boundForm.getField("ID");
	        		 String vv = (String) value._getValue();
	        		 if(vv == null || vv == "" || vv == "undefined") {
	        			 btnEdit.setVisible(false);
	        			 butNew.setVisible(false);
	        			 btnSave.setVisible(false);//隐藏保存按钮
	        			 SC.warn("请先保存主表信息");
	        		 }else {
	        			 if(VE == 0) {
		        			 btnEdit.setVisible(true);
		        			 butNew.setVisible(true); 
		        			 btnSave.setVisible(false);//显示保存按钮
		        			 butDel.setVisible(true);// 删除按钮
	        			 }
	        		 }
	        	 }
	        	 butClose.setVisible(true);
        			 
            }
        });
	    
	    //新建
	    butNew.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				VE = 0;
				
				btnSave.setVisible(true);
				butClose.setVisible(true);
				baseListGrid.setVisible(false);
				baseListGrid_b.setVisible(true);
				butView.setVisible(false);//查看按钮
				btnEdit.setVisible(false);
				butNew.setVisible(false);
				butDel.setVisible(false);
				butCopy.setVisible(false);
				btnSave.enable();
				
				if(tabNum == 1) {
					 SelectItem JFZD = (SelectItem)boundFormB.getItem("JFZD");
					 JFZD.setValueMap(map);
					 FormItem value = boundForm.getField("ID");
	        		 String vv = (String) value._getValue();
					 baseListGrid_b.setCriteria(new Criteria("PK_HEAD", vv));
					 btnSave.setVisible(true);//保存按钮
					 setB.setVisible(true);
					 boundFormB.editNewRecord();
				}else {
					baseListGrid_b.setCriteria(new Criteria("PK_HEAD", "PK_HEAD"));
					set.setVisible(true);
					boundForm.editNewRecord();
				}
			}
		});
	    
	    //复制
        butCopy.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				VE = 0;
				// TODO Auto-generated method stub
				if(tabNum == 1) {
					if (null != baseListGrid_b.getSelectedRecord() && baseListGrid_b.getSelectedRecords().length == 1)
			        {
						setB.setVisible(true);
						btnSave.setVisible(true);
						butClose.setVisible(true);
						baseListGrid.setVisible(false);
						baseListGrid_b.setVisible(true);
						butView.setVisible(false);//查看按钮
						butDel.setVisible(false);// 删除按钮
						butCopy.setVisible(false);
						butExp.setVisible(false);
						//btnSave.enable();
						//boundForm.editNewRecord();
						
			        }else {
			        	SC.warn("请选择一条数据进行复制！");
					}	
				}else {
					if (null != baseListGrid.getSelectedRecord() && baseListGrid.getSelectedRecords().length == 1)
			        {
						set.setVisible(true);
						btnSave.setVisible(true);
						butClose.setVisible(true);
						baseListGrid.setVisible(false);
						baseListGrid_b.setVisible(true);
						butView.setVisible(false);//查看按钮
						butDel.setVisible(false);//隐藏删除按钮
						butNew.setVisible(false);//隐藏删除按钮
						btnEdit.setVisible(false);
						butCopy.setVisible(false);
						butExp.setVisible(false);
						//btnSave.enable();
						//boundForm.editNewRecord();
					
			        }else {
			        	SC.warn("请选择一条数据进行复制！");
					}
				}
				
			}
		});
	    
	    //查看
	    butView.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				VE = 1;
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord() && baseListGrid.getSelectedRecords().length == 1)
		        {
					FormItem digits = (FormItem)boundForm.getItem("decimal_digits");
					String keep = baseListGrid.getSelectedRecord().getAttribute("decimal_keep");
					
					SelectItem INOUT = (SelectItem)boundForm.getItem("INOUT");
					String FYLX = baseListGrid.getSelectedRecord().getAttribute("FYLX");
					
					String value = String.valueOf(keep);
					if ("3".equals(value)) {
						digits.show();
					} else {
						digits.setHidden(true);
					}
					String value2 = String.valueOf(FYLX);
					if(value2.equals("100002") || value2.equals("100003") || value2.equals("100007")) {
						INOUT.show();
					} else {
						INOUT.setHidden(true);
					}
					set.setVisible(true);
					btnSave.setVisible(false);
					butClose.setVisible(true);
					baseListGrid.setVisible(false);
					baseListGrid_b.setVisible(true);
					butView.setVisible(false);//查看按钮
					btnEdit.setVisible(false);
					butDel.setVisible(false);
					butNew.setVisible(false);
					butCopy.setVisible(false);
					butExp.setVisible(false);
					String vv = baseListGrid.getSelectedRecord().getAttribute("ID");
					baseListGrid_b.setCriteria(new Criteria("PK_HEAD", vv));
		        }else {
		        	SC.warn("请选择一条数据进行查看");
		        }
			}
		});
			    
	     //取消
	    butClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				set.selectTab(0);
				// TODO Auto-generated method stub
				set.setVisible(false);//新增修改表格
				setB.setVisible(false);//新增修改表格
				btnSave.setVisible(false);//保存
				butClose.setVisible(false);//取消
				butView.setVisible(true);//查看按钮
				baseListGrid.setVisible(true);
				baseListGrid_b.setVisible(false);
				btnEdit.setVisible(true);
				butDel.setVisible(true);
				butNew.setVisible(true);
				butCopy.setVisible(true);
				butExp.setVisible(true);
				boundForm.getItem("decimal_digits").setVisible(false);
				boundForm.getItem("INOUT").setVisible(false);
				baseListGrid.deselectAllRecords();
				baseListGrid_b.deselectAllRecords();
				boundFormB.clearValues();
				boundForm.clearValues();
				tabNum = -1;
			}
		});
	    
	    btnEdit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				VE = 0;
				// TODO Auto-generated method stub
				
				if(tabNum == 1) {
					if (null != baseListGrid_b.getSelectedRecord() && baseListGrid_b.getSelectedRecords().length == 1)
			        {
//						SelectItem JFZD = (SelectItem)boundFormB.getItem("JFZD");
//						JFZD.setValueMap(map);
						setB.setVisible(true);
						btnSave.setVisible(true);
						butClose.setVisible(true);
						baseListGrid.setVisible(false);
						baseListGrid_b.setVisible(true);
						butView.setVisible(false);//查看按钮
						butDel.setVisible(true);// 删除按钮
						butCopy.setVisible(false);
						
			        }else {
			        	SC.warn("请选择一条数据进行修改！");
					}
					
				}else {
					if (null != baseListGrid.getSelectedRecord() && baseListGrid.getSelectedRecords().length == 1)
			        {
						set.setVisible(true);
						btnSave.setVisible(true);
						butClose.setVisible(true);
						baseListGrid.setVisible(false);
						baseListGrid_b.setVisible(true);
						butView.setVisible(false);//查看按钮
						String vv = baseListGrid.getSelectedRecord().getAttribute("ID");
						baseListGrid_b.setCriteria(new Criteria("PK_HEAD", vv));
			       
						butDel.setVisible(false);//隐藏删除按钮
						butNew.setVisible(false);//隐藏删除按钮
						btnEdit.setVisible(false);
						butCopy.setVisible(false);
						FormItem digits = (FormItem)boundForm.getItem("decimal_digits");
						String keep = baseListGrid.getSelectedRecord().getAttribute("decimal_keep");
						
						SelectItem INOUT = (SelectItem)boundForm.getItem("INOUT");
						String FYLX = baseListGrid.getSelectedRecord().getAttribute("FYLX");
						
						String value = String.valueOf(keep);
						if ("3".equals(value)) {
							digits.show();
						} else {
							digits.setHidden(true);
						}
						
						String value2 = String.valueOf(FYLX);
						if(value2.equals("100002") || value2.equals("100003") || value2.equals("100007")) {
							INOUT.show();
						} else {
							INOUT.setHidden(true);
						}
						

						
			        }else {
			        	SC.warn("请选择一条数据进行修改！");
					}
				}
				
			}
		});
		
	    //删除
	    butDel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				if(tabNum == 1) {
					if (null != baseListGrid_b.getSelectedRecord())
			         {
					    SC.ask("确定要删除？",new BooleanCallback() {
							
							@Override
							public void execute(Boolean value) {
								if (value != null && value)
			                    {
									baseListGrid_b.removeSelectedData();
									
			                    	boundFormB.clearValues();
			                    }
								
							}
						});
					}
				}else {
					if (null != baseListGrid.getSelectedRecord())
			         {
					    SC.ask("确定要删除？",new BooleanCallback() {
							
							@Override
							public void execute(Boolean value) {
								if (value != null && value)
			                    {
									baseListGrid.removeSelectedData();
									
									String vv = baseListGrid.getSelectedRecord().getAttribute("ID");
									
									DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
											"delChild", new RPCCallback() 
									{
										public void execute(RPCResponse response, Object rawData, RPCRequest request) 
										{
											SC.clearPrompt();
											final String result = String.valueOf(rawData);
											if(result.startsWith("ERROR:"))
											{
												SC.warn(result);
											}
											else
											{
//												SC.say("上传完成。");
											}
										}
									},new Object[] {vv});
									
			                    	boundFormB.clearValues();
			                    }
								
							}
						});
					}
				}
				
				
			}
		});
	        
	    //保存
	    btnSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				boundForm.validate(false);
				if (!boundForm.hasErrors()) {
					if(tabNum == 1) {
						FormItem value = boundForm.getField("ID");
		        		String vv = (String) value._getValue();
		        		boundFormB.getField("PK_HEAD").setDefaultValue(vv);
						boundFormB.saveData();
						baseListGrid.setVisible(false);
						baseListGrid_b.setVisible(true);
						butClose.setVisible(true);
						baseListGrid_b.setCriteria(new Criteria("PK_HEAD", vv));
						butClose.setVisible(true);
						butCopy.setVisible(true);
						butExp.setVisible(true);
					}else {
						boundForm.saveData();
						baseListGrid.setVisible(true);
						baseListGrid_b.setVisible(false);
						set.setVisible(false);
						butView.setVisible(true);
						butClose.setVisible(false);
						butCopy.setVisible(true);
						butExp.setVisible(true);
					}
					SC.say("保存成功");
					
					setB.setVisible(false);//新增修改表格
				    btnSave.setVisible(false);
				    
				    btnEdit.setVisible(true);
					butNew.setVisible(true);
					butDel.setVisible(true);
				    baseListGrid.deselectAllRecords();
				    baseListGrid_b.deselectAllRecords();
				    baseListGrid.fetchData();
				}
				
			}
		});
	 		    
	    //编辑
	    baseListGrid.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundForm.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    //编辑
	    baseListGrid_b.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundFormB.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    
	  //导出
	    butExp.addClickHandler(new ClickHandler() {
	    	
	    	@Override
	    	public void onClick(ClickEvent event) {
	    		DSRequest dsRequestProperties = new DSRequest();  
	    		//以xls格式导出
	    		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
	    		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	    		baseListGrid.exportClientData(dsRequestProperties);
	    		//baseListGrid.exportData(dsRequestProperties);
	    	}
	    });
	    
	}
	

	/**
	 * 项目编码下拉框
	 */
	public void changeSelect() {
		
	    SelectItem ckItem = (SelectItem)boundForm.getItem("XMBM");
		ListGrid pickListProperties = new ListGrid();
		pickListProperties.setShowFilterEditor(true);
		pickListProperties.setDataSource(DataSource.get("BEIYE_XM"));
		ListGridField ZJMField = new ListGridField("XM_BM");
		ZJMField.setWidth(100);
		ListGridField bmmcField = new ListGridField("XM_MC");
		bmmcField.setWidth(200);
		ckItem.setValueField("XM_BM");
		ckItem.setAllowEmptyValue(true);
		ckItem.setPickListWidth(316);
		ckItem.setPickListFields(ZJMField, bmmcField);
		ckItem.setPickListProperties(pickListProperties);
		ckItem.addChangedHandler(new ChangedHandler(){

			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				DataSource.get("BEIYE_XM").fetchData(new Criteria("XM_BM", value), new DSCallback(){

					@Override
					public void execute(DSResponse dsResponse, Object data,
							DSRequest dsRequest) {
						Record[] records = dsResponse.getData();
						if(records != null && records.length>0)
						{
							Record value = dsResponse.getData()[0];
							boundForm.setValue("XMBM", value.getAttribute("XM_BM"));
							boundForm.setValue("XMMC", value.getAttribute("XM_MC"));
						}
					}
				});
				
			}
		});
	    
		
	}
	
	/**
	 * 小数位下拉
	 */
	public void decimalSelect() {
		FormItem keep = (FormItem)boundForm.getItem("decimal_keep");//小数位处理方式
		FormItem digits = (FormItem)boundForm.getItem("decimal_digits");//保留小数位
		
		SelectItem FYLX = (SelectItem)boundForm.getItem("FYLX");//费用类型
		SelectItem INOUT = (SelectItem)boundForm.getItem("INOUT");//进出
		
		keep.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				if ("3".equals(value)) {
					digits.setVisible(true);
				} else {
					digits.setVisible(false);
				}
				//	重绘
				boundForm.redraw();
			}
		});
		
		FYLX.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				String value = String.valueOf(event.getValue());
				if(value.equals("100002") || value.equals("100003") || value.equals("100007")) {
					INOUT.setVisible(true);
				} else {
					INOUT.setVisible(false);
				}
				//	重绘
				boundForm.redraw();
			}
		});
		
		INOUT.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				autoMap();//WMS字段
			}
		});
	}
	
	/**
	 * 费用类型下拉下拉
	 */
	public void FYLXSelect() {
		
		 SelectItem FYLX = (SelectItem)boundForm.getItem("FYLX");
		 SelectItem JFZD = (SelectItem)boundFormB.getItem("JFZD");
		 SelectItem INOUT = (SelectItem)boundForm.getItem("INOUT");
		 
		 FYLX.addChangedHandler(new ChangedHandler() {
			
			@Override
			public void onChanged(ChangedEvent event) {
				
				String value = String.valueOf(FYLX.getValue());
				
				if(value == null  || value == "undefined") {
					JFZD.setValueMap(map);
					return;
				}
//				
				
				if(value.equals("100002") || value.equals("100003") || value.equals("100007")) {
					INOUT.show();
				} else {
					INOUT.setHidden(true);
				}
				
				
				
				DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
						"selectByTable", new RPCCallback() {
							@SuppressWarnings("unchecked")
							@Override
							public void execute(RPCResponse response, Object rawData,
									RPCRequest request) {

								map = (Map<String, String>) response.getDataAsMap();
								
								JFZD.setValueMap(map);
							}
				},new Object[] {value});
				
//				Window.alert(value);
//				//	重绘
//				boundFormB.redraw();
			}
		});
	}
	

	/**
	 * 费用类型下拉下拉
	 */
	public void AutoFYLXSelect() {
		
		SelectItem FYLX = (SelectItem)boundForm.getItem("FYLX");
		SelectItem JFZD = (SelectItem)boundFormB.getItem("JFZD");
		 
		String value = String.valueOf(FYLX.getValue());
		
		if(value == null  || value == "undefined") {
			JFZD.setValueMap(map);
			return;
		}
//				
		DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
				"selectByTable", new RPCCallback() {
					@SuppressWarnings("unchecked")
					@Override
					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {

						map = (Map<String, String>) response.getDataAsMap();
						
						JFZD.setValueMap(map);
					}
		},new Object[] {value});
 
	}
	
	
	/**
	 * 自动装配map（用于WMS下拉）
	 */
	public void autoMap() {
		
		map1.put("type_id", "订单类型");
		map1.put("qty_num", "总数量");
		map1.put("weight", "总重量");
		map1.put("volume", "总体积");
		
		map2.put("type_id", "订单类型");
		map2.put("qty_num", "总数量");
		map2.put("weight", "总重量");
		map2.put("volume", "总体积");
		map2.put("ship_to_addr2", "详细地址");
		map2.put("ship_to_name", "收件人");
		map2.put("customer_name", "客户名称");

		map3.put("weight", "总重量");
		map3.put("volume", "总体积");
		map3.put("qty_num", "总数量");
		map3.put("CKMJ", "仓库面积");
		
//		100001 仓储租金  100002 进出费  100003 装卸费  100007 订单处理费 
		SelectItem FYLX = (SelectItem)boundForm.getItem("FYLX");//费用类型 
//		1 进 2出
		SelectItem INOUT = (SelectItem)boundForm.getItem("INOUT");//进出
		SelectItem WMS_ZD = (SelectItem)boundFormB.getItem("WMS_ZD");//WMS字段
		
		String value = String.valueOf(FYLX.getValue());
		String value2 = String.valueOf(INOUT.getValue());
		
		if(value != null && value != "undefined") {
			
			if(value.equals("100001")) {
				WMS_ZD.setValueMap(map3);
			}
			
			if(value2 != null && value2 != "undefined") {
				if(value2.equals("1")) {
					if(value.equals("100002") || value.equals("100003") || value.equals("100007")) {
						WMS_ZD.setValueMap(map1);
					}
					
				}else {
					
					if(value.equals("100002") || value.equals("100003") || value.equals("100007")) {
						WMS_ZD.setValueMap(map2);
					}
				}
			}
			
		}
		
	}

}
