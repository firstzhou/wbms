package com.shr.client.wbms;

import java.util.Map;

import com.google.gwt.user.client.Window;
import com.shr.client.model.xml.XmlBasePanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class CKDDXX_HClient extends XmlBasePanel{
    
	ListGrid baseListGrid =null;
	
	ListGrid baseListGrid_b =null;
	
	ToolStripButton butNew = null;
	
	ToolStripButton butDel = null;
	
	ToolStripButton btnSave = null;
	
	ToolStripButton butImport = null;
	
	DynamicForm  boundForm =null;
	
	ToolStripButton btnEdit = null;
	
	ToolStripButton butClose = null; //取消
	
	ToolStripButton butView = null; //查看
	
	ToolStripButton butWith= null;	//同步
	
	ToolStripButton butExp = null;
	
	TabSet  set = null;
	
	
	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	@Override
	public String[] canvasUI() {
		
		return new String [] {"wbms/WBMS_CKDDXX_H"}  ;
	}


	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	@Override
	public void initChild(Map<String, Canvas> baseCanvas) {
		super.initChild(baseCanvas);
	    Canvas  canvas =  baseCanvas.get("wbms/WBMS_CKDDXX_H");
	    
	    baseListGrid = (ListGrid)canvas.getByLocalId("baseListGrid"); 
	    baseListGrid_b = (ListGrid)canvas.getByLocalId("baseListGrid_B"); 
	    baseListGrid.setShowRowNumbers(true);
	    baseListGrid_b.setShowRowNumbers(true);
	    butNew =(ToolStripButton) canvas.getByLocalId("butNew");
	    butDel =(ToolStripButton) canvas.getByLocalId("butDel");
	    btnSave =(ToolStripButton) canvas.getByLocalId("btnSave");
	    boundForm =(DynamicForm) canvas.getByLocalId("CKDDXX_HForm");
	    butClose = (ToolStripButton) canvas.getByLocalId("butClose");
	    btnEdit = (ToolStripButton) canvas.getByLocalId("butEdit");
	    butView = (ToolStripButton) canvas.getByLocalId("butView");
		butExp = (ToolStripButton) canvas.getByLocalId("butExp");
		butWith = (ToolStripButton) canvas.getByLocalId("butWith");
	    set = (TabSet) canvas.getByLocalId("TabSet2");
	    
	    set.setVisible(false);//隐藏新增修改表格
	    btnSave.setVisible(false);//隐藏保存按钮
	    butClose.setVisible(false);//隐藏取消按钮
	    baseListGrid_b.setVisible(false);
//	    baseListGrid_b = new ListGrid();
//	    final DataSource ds = DataSource.get("WBMS_RKDDSJ_B");
//	    ds.getDisplayValue("po_number" ,"123sds");
//	    Window.alert(ds);
//	    baseListGrid_b.setDataSource(ds);
//	    baseListGrid_b.setFields(new ListGridField("pk_rkddsj_h", "Name"));
//	    baseListGrid_b.selectSingleRecord(1);
	   
	    //新建
	    butNew.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(true);
				btnSave.setVisible(true);
				
				boundForm.editNewRecord();
				btnSave.enable();
				
			}
		});
	    
	    //	同步
	    butWith.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				if(baseListGrid.getSelectedRecords() == null || baseListGrid.getSelectedRecords().length != 1)
				{
					SC.warn("请选择数据进行同步！");
				}
				else
				{
					int i = 0;
					for(Record r : baseListGrid.getSelectedRecords())
					{
						Map<Object,Object> map= r.toMap();
						int size = map.size();
						if (i <= size) {
							DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
									"ckSynchronous", new RPCCallback() 
							{
								public void execute(RPCResponse response, Object rawData, RPCRequest request) 
								{
									SC.clearPrompt();
									final String result = String.valueOf(rawData);
									if(result.startsWith("ERROR:"))
									{
										SC.warn(result);
									}
									else
									{
//										SC.say("同步成功！");
									}
								}
							},new Object[] {map});
						}
						i++;
						SC.say("同步完成！");
					}
				}
			}
		});
	    
	    butView.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					set.setVisible(true);
					btnSave.setVisible(false);
					butClose.setVisible(true);
					baseListGrid.setVisible(false);
					baseListGrid_b.setVisible(true);
					butView.setVisible(false);//查看按钮
					
					String vv = baseListGrid.getSelectedRecord().getAttribute("pk_ckddxx_h");
					baseListGrid_b.setCriteria(new Criteria("pk_ckddxx_h", vv));
		       
		        }
			}
		});
		

	    
	  //取消
	    butClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(false);//新增修改表格
				btnSave.setVisible(false);//保存
				butClose.setVisible(false);//取消
				butView.setVisible(true);//查看按钮
				baseListGrid.setVisible(true);
				baseListGrid_b.setVisible(false);
			}
		});
	    
	    btnEdit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					set.setVisible(true);
					btnSave.setVisible(true);
					butClose.setVisible(true);
					baseListGrid.setVisible(false);
					baseListGrid_b.setVisible(true);
					String vv = baseListGrid.getSelectedRecord().getAttribute("pk_ckddxx_h");
					baseListGrid_b.setCriteria(new Criteria("pk_ckddxx_h", vv));
//					baseListGrid_b.setFields(new ListGridField("pk_rkddsj_h", "Name"));
//					String vv = baseListGrid.getSelectedRecord().getAttribute("pk_rkddsj_h");
//					Window.alert("123--------"+list2);
		       
		        }else {
		        	SC.warn("请选择一条数据进行修改！");
				}
			}
		});
		
	    //删除
	    butDel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		         {
				    SC.ask("确定要删除？",new BooleanCallback() {
						
						@Override
						public void execute(Boolean value) {
							if (value != null && value)
		                    {
								baseListGrid.removeSelectedData();
		                    	boundForm.clearValues();
		                    }
							
						}
					});
				}
			}
		});
	    
	    //保存
	    btnSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				boundForm.saveData();
				SC.say("保存成功");
				set.setVisible(false);
			    btnSave.setVisible(false);
			    butClose.setVisible(false);
			    
				baseListGrid.setVisible(true);
				baseListGrid_b.setVisible(false);
			    
			    baseListGrid.selectAllRecords();
			    baseListGrid_b.selectAllRecords();
			}
		});
	    
	    //编辑
	    baseListGrid.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundForm.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    
	    
//	  //导出
//	    butExp.addClickHandler(new ClickHandler() {
//	    	
//	    	@Override
//	    	public void onClick(ClickEvent event) {
//	    		DSRequest dsRequestProperties = new DSRequest();  
//	    		//以xls格式导出
//	    		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
//	    		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
//	    		
//	    		baseListGrid.exportClientData(dsRequestProperties);
//	    		//baseListGrid.exportData(dsRequestProperties);
//	    	}
//	    });
	    
	    
	    //导出
	    butExp.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
					if (null != baseListGrid.getSelectedRecord())
			         {
					 new BooleanCallback() {
							@Override
							public void execute(Boolean value) {
								if (value != null && value)
			                    {
									baseListGrid.removeSelectedData();								
									String vv = baseListGrid.getSelectedRecord().getAttribute("ID");
									DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
											"expChild", new RPCCallback() 
									{
										public void execute(RPCResponse response, Object rawData, RPCRequest request) 
										{
											SC.clearPrompt();
											final String result = String.valueOf(rawData);
										}
									},new Object[] {vv});
						   }	
				      	}	
		         	};
	           }
			}
	   });
	} 
}
