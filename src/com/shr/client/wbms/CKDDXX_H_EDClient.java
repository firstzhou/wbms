package com.shr.client.wbms;

import java.util.HashMap;
import java.util.Map;

import org.apache.jasper.tagplugins.jstl.core.ForEach;

import com.google.gwt.user.client.Window;
import com.shr.client.model.util.GridPager;
import com.shr.client.model.xml.XmlBasePanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class CKDDXX_H_EDClient extends XmlBasePanel{
    
	ListGrid baseListGrid =null;
	
	ListGrid baseListGrid_b =null;
	
	ToolStripButton butNew = null;
	
	ToolStripButton butDel = null;
	
	ToolStripButton btnSave = null;
	
	ToolStripButton butImport = null;
	
	DynamicForm  boundForm =null;
	
	ToolStripButton btnEdit = null;
	
	ToolStripButton butClose = null; //取消
	
	ToolStripButton butView = null; //查看
	
	ToolStripButton confirm = null; //确认订单
	
	ToolStripButton cancel = null; //取消订单
	
	ToolStripButton butExp = null;

	TabSet  set = null;
	
	
	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	@Override
	public String[] canvasUI() {
		
		return new String [] {"wbms/WBMS_CKDDXX_H_ED"}  ;
	}


	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	@Override
	public void initChild(Map<String, Canvas> baseCanvas) {
		super.initChild(baseCanvas);
	    Canvas  canvas =  baseCanvas.get("wbms/WBMS_CKDDXX_H_ED");
	    
	    baseListGrid = (ListGrid)canvas.getByLocalId("baseListGrid"); 
	    baseListGrid_b = (ListGrid)canvas.getByLocalId("baseListGrid_B_ED"); 
		butExp = (ToolStripButton) canvas.getByLocalId("butExp");
	    baseListGrid.setShowRowNumbers(true);
	    baseListGrid_b.setShowRowNumbers(true);
	    //显示复选框
	    
	    baseListGrid.setSelectionType(SelectionStyle.SIMPLE);
	    baseListGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);

//	    GridPager gridPager = new GridPager(baseListGrid,16);
//	    VLayout gridPanel=new VLayout();
//	    gridPanel.addMember(baseListGrid);
//	    gridPanel.addMember(gridPager) ;
	    
	    butNew =(ToolStripButton) canvas.getByLocalId("butNew");
	    butDel =(ToolStripButton) canvas.getByLocalId("butDel");
	    btnSave =(ToolStripButton) canvas.getByLocalId("btnSave");
	    boundForm =(DynamicForm) canvas.getByLocalId("CKDDXX_H_EDForm");
	    butClose = (ToolStripButton) canvas.getByLocalId("butClose");
	    btnEdit = (ToolStripButton) canvas.getByLocalId("butEdit");
	    butView = (ToolStripButton) canvas.getByLocalId("butView");
	    confirm = (ToolStripButton) canvas.getByLocalId("confirm");
	    cancel = (ToolStripButton) canvas.getByLocalId("cancel");
	    set = (TabSet) canvas.getByLocalId("TabSet2");
	    
	    set.setVisible(false);//隐藏新增修改表格
	    btnSave.setVisible(false);//隐藏保存按钮
	    butClose.setVisible(false);//隐藏取消按钮
	    baseListGrid_b.setVisible(false);
//	    baseListGrid_b = new ListGrid();
//	    final DataSource ds = DataSource.get("WBMS_RKDDSJ_B");
//	    ds.getDisplayValue("po_number" ,"123sds");
//	    Window.alert(ds);
//	    baseListGrid_b.setDataSource(ds);
//	    baseListGrid_b.setFields(new ListGridField("pk_rkddsj_h", "Name"));
//	    baseListGrid_b.selectSingleRecord(1);
	   
	    //新建
	    butNew.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(true);
				btnSave.setVisible(true);
				
				boundForm.editNewRecord();
				btnSave.enable();
				
			}
		});
	    
	  //确认订单
	    confirm.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					SC.confirm("确认这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
		                    + "</strong>】</font>条订单吗？", new BooleanCallback()
		            {

						@Override
						public void execute(Boolean value) {
							
							if(value != null && value) {
								
								ListGridRecord[] listRc = baseListGrid.getSelectedRecords();
								StringBuffer buffer = new StringBuffer();
								String ids = "";
								for (ListGridRecord o : listRc) {
									
									String pk_ckddxx_h = o.getAttribute("pk_ckddxx_h");
									String wmsStatus = o.getAttribute("wms_status");
									String order_number = o.getAttribute("order_number");
									
									if(!wmsStatus.equals("1")) {
										buffer.append("单号"+order_number+"已确认过，无需再次确认<br/>");
									
									}else {
										if(ids == "") {
											ids = "'"+pk_ckddxx_h+"'";
										}else {
											ids = ids+","+ "'"+pk_ckddxx_h+"'";
										}
										
//										if(caozuo(pk_ckddxx_h,"2").equals("true")) {
											buffer.append("单号"+order_number+"已确认<br/>");
//										}
//										o.setAttribute("wms_status", "2");
//										baseListGrid.updateData(o);
										
									}
									caozuo(ids,"2");
									SC.say(buffer.toString());
//									baseListGrid.fetchData();
//									baseListGrid.refreshData();
								}
								
	//							Record record = baseListGrid.getSelectedRecord();
	//							record.setAttribute("status", "2");
	//							baseListGrid.updateData(record);
//								baseListGrid.selectAllRecords();
	//							boundForm.clearValues();
							}
						}
						
		            });
		        }else {
		        	SC.say("请选择需要确认的数据！");
				}
//				baseListGrid.fetchData();
				
			}
		});
	    
	    cancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					SC.confirm("确定取消这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
		                    + "</strong>】</font>条订单？", new BooleanCallback()
		            {

						@Override
						public void execute(Boolean value) {
							
							if(value != null && value) {
								
								ListGridRecord[] listRc = baseListGrid.getSelectedRecords();
								
								StringBuffer buffer = new StringBuffer();
								String ids = "";
								for (ListGridRecord o : listRc) {
									
									String pk_ckddxx_h = o.getAttribute("pk_ckddxx_h");
									String wmsStatus = o.getAttribute("wms_status");
									String order_number = o.getAttribute("order_number");
									
									if(!wmsStatus.equals("2")) {
										buffer.append("单号"+order_number+"不能取消确认<br/>");
									
									}else {
										if(ids == "") {
											ids = "'"+pk_ckddxx_h+"'";
										}else {
											ids = ids+","+ "'"+pk_ckddxx_h+"'";
										}
//										if(caozuo(pk_ckddxx_h,"1").equals("true")) {
											buffer.append("单号"+order_number+"已取消确认<br/>");
//										}
//										o.setAttribute("wms_status", "1");
//										baseListGrid.updateData(o);
//										buffer.append("单号"+order_number+"已取消确认<br/>");
									}
									
								}
								caozuo(ids,"1");
								SC.say(buffer.toString());
//								baseListGrid.fetchData();
//								baseListGrid.refreshData();
	//							Record record = baseListGrid.getSelectedRecord();
	//							record.setAttribute("status", "1");
	//							baseListGrid.updateData(record);
//								baseListGrid.selectAllRecords();
							}
						}
						
		            });
		        }else {
		        	SC.say("请选择需要取消确认的数据！");
				}
//				baseListGrid.fetchData();
//				baseListGrid.refreshData();
			}
		});
	    
	    butView.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
//				 GridPager gridPager = new GridPager(baseListGrid,16);
//				 gridPager.goToPage(2);
				
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					set.setVisible(true);
					btnSave.setVisible(false);
					butClose.setVisible(true);
					baseListGrid.setVisible(false);
					baseListGrid_b.setVisible(true);
					butView.setVisible(false);//查看按钮
					confirm.setVisible(false);
				    cancel.setVisible(false);
					String vv = baseListGrid.getSelectedRecord().getAttribute("pk_ckddxx_h");
					baseListGrid_b.setCriteria(new Criteria("pk_ckddxx_h", vv));
		       
		        }
			}
		});
		

	    
	  //取消
	    butClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(false);//新增修改表格
				btnSave.setVisible(false);//保存
				butClose.setVisible(false);//取消
				butView.setVisible(true);//查看按钮
				baseListGrid.setVisible(true);
				baseListGrid_b.setVisible(false);
				confirm.setVisible(true);
			    cancel.setVisible(true);
			}
		});
	    
	    btnEdit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					confirm.setVisible(false);
				    cancel.setVisible(false);
					set.setVisible(true);
					btnSave.setVisible(true);
					butClose.setVisible(true);
					baseListGrid.setVisible(false);
					baseListGrid_b.setVisible(true);
					butView.setVisible(false);//查看按钮
					String vv = baseListGrid.getSelectedRecord().getAttribute("pk_ckddxx_h");
					baseListGrid_b.setCriteria(new Criteria("pk_ckddxx_h", vv));
//					baseListGrid_b.setFields(new ListGridField("pk_rkddsj_h", "Name"));
//					String vv = baseListGrid.getSelectedRecord().getAttribute("pk_rkddsj_h");
//					Window.alert("123--------"+list2);
		       
		        }else {
		        	SC.warn("请选择一条数据进行修改！");
				}
			}
		});
		
	    //删除
	    butDel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		         {
				    SC.ask("确定要删除？",new BooleanCallback() {
						
						@Override
						public void execute(Boolean value) {
							if (value != null && value)
		                    {
								baseListGrid.removeSelectedData();
		                    	boundForm.clearValues();
		                    }
							
						}
					});
				}
			}
		});
	    
	    //保存
	    btnSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				boundForm.saveData();
				SC.say("保存成功");
				set.setVisible(false);
			    btnSave.setVisible(false);
			    butClose.setVisible(false);
			    confirm.setVisible(true);
			    cancel.setVisible(true);
				baseListGrid.setVisible(true);
				baseListGrid_b.setVisible(false);
				butView.setVisible(true);//查看按钮
			    baseListGrid.selectAllRecords();
			    baseListGrid_b.selectAllRecords();
			}
		});
	    
	    //编辑
	    baseListGrid.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundForm.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    
	  //导出
	    butExp.addClickHandler(new ClickHandler() {
	    	
	    	@Override
	    	public void onClick(ClickEvent event) {
	    		DSRequest dsRequestProperties = new DSRequest();  
	    		//以xls格式导出
	    		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
	    		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	    		baseListGrid.exportClientData(dsRequestProperties);
	    		//baseListGrid.exportData(dsRequestProperties);
	    	}
	    });
	    
	}
	
	/**
	 * 确认/取消确认
	 * @param value
	 * @param state 确认2/取消确认1
	 * @return
	 */
	public String caozuo(String value,String state) {
		
//		Map<String , String > map = new HashMap<String ,String >();

		DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
				"chuKquRen", new RPCCallback() {
					@SuppressWarnings("unchecked")
					@Override
					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {
						baseListGrid.fetchData();
//						map = (Map<String, String>) response.getDataAsMap();
						baseListGrid.refreshData();
					}
		},new Object[] {value,state});
		
		return "true";
	}

}
