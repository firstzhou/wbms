package com.shr.client.wbms;

import java.util.Map;

import com.shr.client.model.xml.XmlBasePanel;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class CKDDXX_B_EDClient extends XmlBasePanel{
    
	ListGrid baseListGrid =null;
	
	ToolStripButton butNew = null;
	
	ToolStripButton butDel = null;
	
	ToolStripButton btnSave = null;
	
	ToolStripButton butImport = null;
	
	DynamicForm  boundForm =null;
	
	ToolStripButton btnEdit = null;
	
	ToolStripButton butClose = null; //取消
	
	TabSet  set = null;
	
	ToolStripButton butExp = null;
	
	
	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	@Override
	public String[] canvasUI() {
		
		return new String [] {"wbms/WBMS_CKDDXX_B_ED"}  ;
	}


	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	@Override
	public void initChild(Map<String, Canvas> baseCanvas) {
		super.initChild(baseCanvas);
	    Canvas  canvas =  baseCanvas.get("wbms/WBMS_CKDDXX_B_ED");
	    
	    baseListGrid = (ListGrid)canvas.getByLocalId("baseListGrid"); 
	    
	    butNew =(ToolStripButton) canvas.getByLocalId("butNew");
	    butDel =(ToolStripButton) canvas.getByLocalId("butDel");
	    btnSave =(ToolStripButton) canvas.getByLocalId("btnSave");
	    boundForm =(DynamicForm) canvas.getByLocalId("CKDDXX_B_EDForm");
	    butClose = (ToolStripButton) canvas.getByLocalId("butClose");
	    btnEdit = (ToolStripButton) canvas.getByLocalId("butEdit");
	    set = (TabSet) canvas.getByLocalId("TabSet2");
	    butExp = (ToolStripButton) canvas.getByLocalId("butExp");
	    set.setVisible(false);//隐藏新增修改表格
	    btnSave.setVisible(false);//隐藏保存按钮
	    butClose.setVisible(false);//隐藏取消按钮
	    
	    //新建
	    butNew.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(true);
				btnSave.setVisible(true);
				
				boundForm.editNewRecord();
				btnSave.enable();
				
			}
		});
	    
	  //取消
	    butClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(false);//新增修改表格
				btnSave.setVisible(false);//保存
				butClose.setVisible(false);//取消
				
			}
		});
	    
	    btnEdit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					set.setVisible(true);
					btnSave.setVisible(true);
					butClose.setVisible(true);
		        }else {
		        	SC.warn("请选择一条数据进行修改！");
				}
			}
		});
		
	    //删除
	    butDel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		         {
				    SC.ask("确定要删除？",new BooleanCallback() {
						
						@Override
						public void execute(Boolean value) {
							if (value != null && value)
		                    {
								baseListGrid.removeSelectedData();
		                    	boundForm.clearValues();
		                    }
							
						}
					});
				}
			}
		});
	    
	    //保存
	    btnSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				boundForm.saveData();
				SC.say("保存成功");
				set.setVisible(false);
			    btnSave.setVisible(false);
			    butClose.setVisible(false);
			    baseListGrid.selectAllRecords();
			}
		});
	    
	    //编辑
	    baseListGrid.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundForm.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    
	  //导出
	    butExp.addClickHandler(new ClickHandler() {
	    	
	    	@Override
	    	public void onClick(ClickEvent event) {
	    		DSRequest dsRequestProperties = new DSRequest();  
	    		//以xls格式导出
	    		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
	    		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	    		baseListGrid.exportClientData(dsRequestProperties);
	    		//baseListGrid.exportData(dsRequestProperties);
	    	}
	    });
	    	
	    
	}

}
