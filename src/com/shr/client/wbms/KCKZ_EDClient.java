package com.shr.client.wbms;

import java.util.Map;

import com.shr.client.model.xml.XmlBasePanel;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.types.ExportDisplay;
import com.smartgwt.client.types.ExportFormat;
import com.smartgwt.client.types.SelectionAppearance;
import com.smartgwt.client.types.SelectionStyle;
import com.smartgwt.client.util.BooleanCallback;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.util.EnumUtil;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.form.DynamicForm;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.SelectionChangedHandler;
import com.smartgwt.client.widgets.grid.events.SelectionEvent;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;

public class KCKZ_EDClient extends XmlBasePanel {
	
	ListGrid baseListGrid =null;
	
	ListGrid baseListGrid_b =null;
	
	ToolStripButton btnSave = null;
	
	ToolStripButton butImport = null;
	
	DynamicForm  boundForm =null;
	
	ToolStripButton btnEdit = null;
	
	ToolStripButton butClose = null;
	
	ToolStripButton butExp = null;
	
	DynamicForm  boundFormB =null;
	
	TabSet  set = null;
	
	ToolStripButton confirm = null; //确认订单
	
	ToolStripButton cancel = null; //取消订单
	
	ToolStripButton butView = null; //查看
	
	ToolStripButton butDel = null;
	
	/**
	 * 子类重写该方法，返回UIxml文件的名称
	 * @return
	 */
	@Override
	public String[] canvasUI() {
		
		return new String [] {"wbms/WBMS_KCKZ_ED"}  ;
		                                                   
	}


	/**
	 * 子类重写该方法，获得xml中控件，并对其进行操作
	 * 子类所有的对控件的操作都要从这个方法走
	 * @param baseCanvas
	 */
	@Override
	public void initChild(Map<String, Canvas> baseCanvas) {
		super.initChild(baseCanvas);
	    Canvas  canvas =  baseCanvas.get("wbms/WBMS_KCKZ_ED");
	    
	    baseListGrid = (ListGrid)canvas.getByLocalId("baseListGrid"); 
	    baseListGrid_b = (ListGrid)canvas.getByLocalId("baseListGrid_B_ED"); 
	    butExp = (ToolStripButton) canvas.getByLocalId("butExp");
	    btnSave =(ToolStripButton) canvas.getByLocalId("btnSave");
	    boundForm =(DynamicForm) canvas.getByLocalId("KCKZ_EDForm");
	    confirm = (ToolStripButton) canvas.getByLocalId("confirm");
	    cancel = (ToolStripButton) canvas.getByLocalId("cancel");
	    butClose = (ToolStripButton) canvas.getByLocalId("butClose");
	    btnEdit = (ToolStripButton) canvas.getByLocalId("butEdit");
	    butDel =(ToolStripButton) canvas.getByLocalId("butDel");
	    set = (TabSet) canvas.getByLocalId("TabSet2");
	    butView = (ToolStripButton) canvas.getByLocalId("butView");
	    baseListGrid.setVisible(true);
	    butClose.setVisible(false);
	    set.setVisible(false);//隐藏新增修改表格
	    btnSave.setVisible(false);//隐藏保存按钮
	    baseListGrid.setShowRowNumbers(true);
	    //baseListGrid_b.setShowRowNumbers(true);
	    //显示复选框
	    baseListGrid.setSelectionType(SelectionStyle.SIMPLE);
	    baseListGrid.setSelectionAppearance(SelectionAppearance.CHECKBOX);


	    
	    //	编辑按钮
	    btnEdit.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					baseListGrid.setVisible(false);
					butClose.setVisible(true);
					set.setVisible(true);
					btnSave.setVisible(true);
		        }else {
		        	SC.warn("请选择一条数据进行修改！");
				}
			}
		});
		
	    
	    //保存
	    btnSave.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				
				boundForm.saveData();
				SC.say("保存成功");
				baseListGrid.setVisible(true);
				butClose.setVisible(false);
				set.setVisible(false);
			    btnSave.setVisible(false);
			    baseListGrid.selectAllRecords();
			}
		});
	    
	    //编辑
	    baseListGrid.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundForm.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    
	    
	    
	  //导出
	    butExp.addClickHandler(new ClickHandler() {
	    	
	    	@Override
	    	public void onClick(ClickEvent event) {
	    		DSRequest dsRequestProperties = new DSRequest();  
	    		//以xls格式导出
	    		dsRequestProperties.setExportAs((ExportFormat)EnumUtil.getEnum(ExportFormat.values(), "xls"));  
	    		dsRequestProperties.setExportDisplay(ExportDisplay.DOWNLOAD);
	    		baseListGrid.exportClientData(dsRequestProperties);
	    		//baseListGrid.exportData(dsRequestProperties);
	    	}
	    });
	    
	    //确认订单
	    confirm.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					SC.confirm("确认这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
		                    + "</strong>】</font>条订单吗？", new BooleanCallback()
		            {

						@Override
						public void execute(Boolean value) {
							
							if(value != null && value) {
								
								ListGridRecord[] listRc = baseListGrid.getSelectedRecords();
								StringBuffer buffer = new StringBuffer();
								for (ListGridRecord o : listRc) {
									
									String wmsStatus = o.getAttribute("wms_status");
									String order_number = o.getAttribute("sto_id");
									
									if(!wmsStatus.equals("1")) {
										buffer.append("库存序列号"+order_number+"已确认过，无需再次确认<br/>");
									
									}else {
										
										o.setAttribute("wms_status", "2");
										baseListGrid.updateData(o);
										buffer.append("库存序列号"+order_number+"已确认<br/>");
									}
									SC.say(buffer.toString());
								}
								
							}
						}
						
		            });
		        }else {
		        	SC.ask("需要确认所有的数据？",new BooleanCallback() {
						
						@Override
						public void execute(Boolean value) {
							if (value != null && value)
		                    {
								baseListGrid.removeSelectedData();
								
								//String vv = baseListGrid.getSelectedRecord().getAttribute("id");
								
								DMI.call("bms", "com.shr.server.wbms.ORDER_JFGZServer",
										"updateAll", new RPCCallback() 
								{
									public void execute(RPCResponse response, Object rawData, RPCRequest request) 
									{
										SC.clearPrompt();
										final String result = String.valueOf(rawData);
										if(result.startsWith("ERROR:"))
										{
											SC.warn(result);
										}
										else
										{
											SC.say("确认成功。");
											//baseListGrid.fetchData();
											baseListGrid.refreshData();
										}
									}
								},new Object[] {});
								
		                    	boundFormB.clearValues();
		                    }
							
						}
					});
				}
				
			}
		});
	    
	    //取消订单
	    cancel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					SC.confirm("确定取消这<font color='red'>【<strong>" + baseListGrid.getSelectedRecords().length
		                    + "</strong>】</font>条订单？", new BooleanCallback()
		            {

						@Override
						public void execute(Boolean value) {
							
							if(value != null && value) {
								
								ListGridRecord[] listRc = baseListGrid.getSelectedRecords();
								
								StringBuffer buffer = new StringBuffer();
								for (ListGridRecord o : listRc) {
									
									String wmsStatus = o.getAttribute("wms_status");
									String order_number = o.getAttribute("sto_id");
									
									if(!wmsStatus.equals("2")) {
										buffer.append("库存序列号"+order_number+"不能取消确认<br/>");
									
									}else {
										
										o.setAttribute("wms_status", "1");
										baseListGrid.updateData(o);
										buffer.append("库存序列号"+order_number+"已取消确认<br/>");
									}
									
								}
								SC.say(buffer.toString());
	//							Record record = baseListGrid.getSelectedRecord();
	//							record.setAttribute("status", "1");
	//							baseListGrid.updateData(record);
//								baseListGrid.selectAllRecords();
							}
						}
						
		            });
		        }else {
		        	SC.say("请选择需要取消确认的数据！");
				}
				
			}
		});
	    
	    //查看
       butView.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		        {
					confirm.setVisible(false);
				    cancel.setVisible(false);
					set.setVisible(true);
					btnSave.setVisible(false);
					butClose.setVisible(true);
					baseListGrid.setVisible(false);
					baseListGrid_b.setVisible(true);
					butView.setVisible(false);//查看按钮
					
					String vv = baseListGrid.getSelectedRecord().getAttribute("id");
					baseListGrid_b.setCriteria(new Criteria("id", vv));
		       
		        }
			}
		});


	    
	    //取消
	    butClose.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				set.setVisible(false);//新增修改表格
				btnSave.setVisible(false);//保存
				butClose.setVisible(false);//取消
				butView.setVisible(true);//查看按钮
				confirm.setVisible(true);
			    cancel.setVisible(true);
				baseListGrid.setVisible(true);
				baseListGrid_b.setVisible(false);
			}
		});
	    
	    //删除
	    butDel.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				if (null != baseListGrid.getSelectedRecord())
		         {
				    SC.ask("确定要删除？",new BooleanCallback() {
						
						@Override
						public void execute(Boolean value) {
							if (value != null && value)
		                    {
								baseListGrid.removeSelectedData();
		                    	boundForm.clearValues();
		                    }
							
						}
					});
				}
			}
		});
	    
	  //编辑
	    baseListGrid.addSelectionChangedHandler(new SelectionChangedHandler(){

			@Override
			public void onSelectionChanged(SelectionEvent event) {
				ListGridRecord selectRecord = event.getSelectedRecord();
				if(selectRecord != null)
				{
					boundForm.editRecord(event.getSelectedRecord());
				}
			}
	    	
	    });
	    
     
	    
	}

}
