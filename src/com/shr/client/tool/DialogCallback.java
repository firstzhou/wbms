package com.shr.client.tool;

public interface DialogCallback 
{
	void executeYes();
	void executeCancel();
}
