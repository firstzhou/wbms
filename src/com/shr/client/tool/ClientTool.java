package com.shr.client.tool;

import java.util.Date;

import com.google.gwt.i18n.client.DateTimeFormat;
import com.smartgwt.client.types.Alignment;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.IButton;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;
import com.smartgwt.client.widgets.tab.events.TabSelectedEvent;
import com.smartgwt.client.widgets.tab.events.TabSelectedHandler;

public class ClientTool 
{
	public static String SELECT_TAB;
	public static String CHILD_SELECT_TAB;
	
	/**
	 * 打开一个标签页
	 * 每个标签都有一个id，如果id存在则表示该标签已经打开
	 * @param id 
	 * @param name
	 * @param content
	 */
	public static void open(String id, String name,Canvas content) 
	{
		TabSet tabset = (TabSet) Canvas.getById("Frame_MainTabSet");
		Tab[] tabs = tabset.getTabs();
		boolean found = false;
		for (int i = 0; i < tabs.length; i++) 
		{
			if (tabs[i].getID().equals(id)) 
			{
				found = true;
				tabset.selectTab(id);
			}
		}
		if (!found) 
		{// 如果未找到标签，新建一个
			final Tab tab = new Tab();
			tab.addTabSelectedHandler(new TabSelectedHandler()
			{
				@Override
				public void onTabSelected(TabSelectedEvent event) 
				{
					ClientTool.CHILD_SELECT_TAB = tab.getID();
				}
			});
			tab.setCanClose(true);
			tab.setTitle(name);
			tab.setID(id);
			content.setWidth100();
			tab.setPane(content);
			tabset.addTab(tab);
			tabset.selectTab(id);
		}
	}

	/**
	 * 弹出窗口
	 * @param title
	 * @param panel
	 * @param width
	 * @param height
	 * @return
	 */
	public static Window showWindow(String title, Canvas panel, String width, String height)
	{
		final Window winModal = new Window();
		winModal.setWidth(width == null ? "50%" : width);
		winModal.setHeight(height == null ? "60%" : height);
		winModal.setTitle(title);
		winModal.setShowMinimizeButton(false);
		winModal.setIsModal(true);
		winModal.setShowModalMask(true);
		winModal.centerInPage();
		winModal.addItem(panel);
		winModal.show();
		
		return winModal;
	}
	
	/**
	 * 根据ID关闭tab
	 * @param id
	 */
	public static void closeTab(String id)
	{
		String selecttab = SELECT_TAB;
		TabSet tabset = (TabSet) Canvas.getById("Frame_MainTabSet");
		tabset.removeTab(id);
		
		Tab[] tabs = tabset.getTabs();
		for (int i = 0; i < tabs.length; i++) {
			if (tabs[i].getID().equals(selecttab)) {
				tabset.selectTab(selecttab);
			}
		}
	}
	
	
	/**
	 * 弹出窗口，带有确定，取消按钮
	 * @param title
	 * @param panel
	 * @param width
	 * @param height
	 * @param callback
	 * @return
	 */
	public static Window showYesNoDialog(String title, Canvas panel, String width, String height, final DialogCallback callback)
	{
		VLayout main = new VLayout();
		final Window winModal = new Window();
		HLayout butLayouttop = new HLayout(20);
		butLayouttop.setHeight(5);
		main.addMember(butLayouttop);
		main.addMember(panel);
		HLayout butLayout = new HLayout(20);
		butLayout.setAlign(Alignment.CENTER);
		butLayout.setHeight(30);
		IButton butYes = new IButton("确定");
		IButton butCance = new IButton("取消");
		butLayout.addMember(butYes);
		butLayout.addMember(butCance);
		butYes.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				callback.executeYes();
				winModal.close();
			}
		});
		butCance.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				// TODO Auto-generated method stub
				callback.executeCancel();
				winModal.close();
			}
		});
		HLayout butLayoutbum = new HLayout(20);
		butLayoutbum.setHeight(20);
		main.addMember(butLayoutbum);
		main.addMember(butLayout);
		
		winModal.setWidth(width == null ? "50%" : width);
		winModal.setHeight(height == null ? "60%" : height);
		winModal.setTitle(title);
		winModal.setShowMinimizeButton(false);
		winModal.setIsModal(true);
		winModal.setShowModalMask(true);
		winModal.centerInPage();
		winModal.addItem(main);
		winModal.show();
		
		return winModal;
	}
	
	/**
	 * 日期转换为字符串 yyyy-MM-dd
	 * @param date
	 * @return
	 */
	public static String convertDateToString(Date date)
	{
		DateTimeFormat dateForm = DateTimeFormat.getFormat("yyyy-MM-dd");
		return dateForm.format(date);
	}
}
