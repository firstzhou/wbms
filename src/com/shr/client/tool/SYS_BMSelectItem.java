package com.shr.client.tool;

import com.smartgwt.client.bean.BeanFactory;
import com.smartgwt.client.data.AdvancedCriteria;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.types.OperatorId;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Window;
import com.smartgwt.client.widgets.events.ClickEvent;
import com.smartgwt.client.widgets.events.ClickHandler;
import com.smartgwt.client.widgets.events.CloseClickEvent;
import com.smartgwt.client.widgets.events.CloseClickHandler;
import com.smartgwt.client.widgets.form.fields.PickerIcon;
import com.smartgwt.client.widgets.form.fields.TextItem;
import com.smartgwt.client.widgets.form.fields.events.FocusEvent;
import com.smartgwt.client.widgets.form.fields.events.FocusHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemClickHandler;
import com.smartgwt.client.widgets.form.fields.events.FormItemIconClickEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpEvent;
import com.smartgwt.client.widgets.form.fields.events.KeyUpHandler;
import com.smartgwt.client.widgets.grid.ListGrid;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.grid.ListGridRecord;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickEvent;
import com.smartgwt.client.widgets.grid.events.RecordDoubleClickHandler;
import com.smartgwt.client.widgets.layout.HLayout;
import com.smartgwt.client.widgets.layout.VLayout;
import com.smartgwt.client.widgets.toolbar.ToolStrip;
import com.smartgwt.client.widgets.toolbar.ToolStripButton;
import com.smartgwt.client.widgets.tree.TreeGrid;

@BeanFactory.Generate
public class SYS_BMSelectItem extends TextItem 
{	
	boolean viewWin = false;
	DataSource dsDepartmentInfor = null;
//	static TreeGrid pickListProperties = new TreeGrid();
	public SYS_BMSelectItem()
	{
		dsDepartmentInfor = DataSource.get("SYS_BM");
		this.setTitle("部门");
		this.setOptionDataSource(dsDepartmentInfor);
		this.setDisplayField("BM_MC");
		this.setValueField("BM_BMID");
		//this.setHint("点击放大镜选择项目");
		
		this.addFocusHandler(new FocusHandler(){

			@Override
			public void onFocus(FocusEvent event) {
				// TODO Auto-generated method stub
				setValue("");
				showPopWin();
			}
		});
		
		this.addKeyUpHandler(new KeyUpHandler(){

			@Override
			public void onKeyUp(KeyUpEvent event) {
				setValue("");
				showPopWin();
			}
		});
		
		PickerIcon dwItemPicker = new PickerIcon(PickerIcon.SEARCH, new FormItemClickHandler() {
			
			@Override
			public void onFormItemClick(FormItemIconClickEvent event) {
				// TODO Auto-generated method stub
				showPopWin();
			}
		});
		this.setIcons(dwItemPicker);
	}
	
	private void showPopWin()
	{
		if(viewWin)
		{
			return;
		}
		else
		{
			viewWin = true;
		}
		VLayout pane = new VLayout();
		HLayout listPane = new HLayout();
    	final Window popWindow = new Window();
    	ToolStripButton configBtn = new ToolStripButton("确定");
    	final TextItem queryText = new TextItem();
    	queryText.setShowTitle(false);
    	queryText.setWidth(200);
    	queryText.setTooltip("模糊匹配，不同关键字用空格隔开，点击查询。");
    	ToolStripButton queryBtn = new ToolStripButton("查询");
    	
    	ToolStrip baseToolStrip = new ToolStrip();
    	baseToolStrip.addFormItem(queryText);
    	baseToolStrip.addButton(queryBtn);
        baseToolStrip.addButton(configBtn);
        
        pane.addMember(baseToolStrip);
        final TreeGrid list = new TreeGrid();
        list.setDataSource(dsDepartmentInfor);
        list.setFetchOperation("enable");
        list.setWidth(400);
        
        queryText.addKeyUpHandler(new KeyUpHandler(){

			@Override
			public void onKeyUp(KeyUpEvent event) {
				if("Enter".equals(event.getKeyName()))
				{
					String qStr = queryText.getValueAsString();
					if(qStr != null)
					{
						qStr = qStr.replace(" ", "%");
						AdvancedCriteria acri = new AdvancedCriteria();
						acri.addCriteria("BM_MC", OperatorId.CONTAINS, qStr);
						acri.addCriteria("BM_TYBZ", OperatorId.EQUALS, "N");
						list.fetchData(acri);
					}
					else
					{
						list.fetchData(new Criteria("BM_TYBZ","N"));
					}
				}
			}
    	});
        
        queryBtn.addClickHandler(new ClickHandler(){
			@Override
			public void onClick(ClickEvent event) {
				String qStr = queryText.getValueAsString();
				if(qStr != null)
				{
					qStr = qStr.replace(" ", "%");
					AdvancedCriteria acri = new AdvancedCriteria();
					acri.addCriteria("BM_MC", OperatorId.CONTAINS, qStr);
					acri.addCriteria("BM_TYBZ", OperatorId.EQUALS, "N");
					list.fetchData(acri);
				}
				else
				{
					list.fetchData(new Criteria("BM_TYBZ","N"));
				}
			}
        });
        
        configBtn.addClickHandler(new ClickHandler() {
			
			@Override
			public void onClick(ClickEvent event) {
				ListGridRecord record = list.getSelectedRecord();
				if(record == null)
				{
					SC.warn("请先选择一个部门！");
					return;
				}
				String pkc = record.getAttribute("BM_BMID");
				setMyValue(pkc);
				viewWin = false;
				popWindow.close();
			}
		});
        
        list.addRecordDoubleClickHandler(new RecordDoubleClickHandler(){

			@Override
			public void onRecordDoubleClick(RecordDoubleClickEvent event) {
				ListGridRecord record = list.getSelectedRecord();
				if(record == null)
				{
					SC.warn("请先选择一个部门！");
					return;
				}
				String pkc = record.getAttribute("BM_BMID");
				setMyValue(pkc);
				viewWin = false;
				popWindow.close();
			}
        });
        
        popWindow.addCloseClickHandler(new CloseClickHandler(){
			@Override
			public void onCloseClick(CloseClickEvent event) {
				// TODO Auto-generated method stub
				viewWin = false;
				popWindow.close();
			}
        });
        
        listPane.addMember(list);
        pane.addMember(listPane);
        
        popWindow.setTitle("请选择部门");
        popWindow.setIsModal(true);
        popWindow.setShowModalMask(true); 
        popWindow.setWidth(860);
        popWindow.setHeight(550);
        popWindow.centerInPage();
        popWindow.addItem(pane);
        popWindow.show();
	}
	private void setMyValue(String value)
	{
		this.setValue(value);
	}
}
