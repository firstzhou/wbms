package com.shr.shared;

import com.shr.client.ModuleFactor;
import com.shr.client.OpenModuleCallBack;
import com.smartgwt.client.data.Criteria;
import com.smartgwt.client.data.DSCallback;
import com.smartgwt.client.data.DSRequest;
import com.smartgwt.client.data.DSResponse;
import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.data.Record;
import com.smartgwt.client.rpc.DMI;
import com.smartgwt.client.rpc.RPCCallback;
import com.smartgwt.client.rpc.RPCRequest;
import com.smartgwt.client.rpc.RPCResponse;
import com.smartgwt.client.util.SC;
import com.smartgwt.client.widgets.Canvas;
import com.smartgwt.client.widgets.tab.Tab;
import com.smartgwt.client.widgets.tab.TabSet;

public class ClientTools {
	static public String trim(Object Value) {
		if (Value == null)
			return "";
		else
			return Value.toString().trim();

	}

	static public void openModule(String id, final OpenModuleCallBack callback) {
		DataSource ds = DataSource.get("query_user_right");
		// ds.a
		Criteria param = new Criteria("amid", id);
		ds.fetchData(param, new DSCallback() {

			public void execute(DSResponse response, Object rawData,
					DSRequest request) {
				Record[] records = response.getData();
				if (records == null || records.length == 0) {
					SC.say("没有权限，或者不存在此功能！");
					callback.execute(null);
				} else {
					Canvas module = openMenu(
							records[0].getAttribute("AMTEMPLET"),
							records[0].getAttribute("AMPARAM"),
							records[0].getAttribute("AMNAME"),
							records[0].getAttribute("ORMDID"));
					callback.execute(module);
				}

			}
		});
	}

	static private Canvas openMenu(String type, final String param,
			String menuName, final String AMID) {
		Canvas module = null;
		TabSet mainTabSet = (TabSet) Canvas.getById("Frame_MainTabSet");
		Tab[] tabs = mainTabSet.getTabs();
		boolean found = false;
		for (int i = 0; i < tabs.length; i++) {
			if (tabs[i].getTitle().equals(menuName)) {
				found = true;
				module = tabs[i].getPane();
				mainTabSet.selectTab(tabs[i].getID());
			}
		}
		if (!found) 
		{// 如果未找到标签，新建一个
			Tab tab = new Tab();
			tab.setCanClose(true);
			tab.setTitle(menuName);
			
			module = ModuleFactor.createModule(AMID);
			tab.setPane(module);
			mainTabSet.addTab(tab);
			mainTabSet.selectTab(tab.getID());
		}
		return module;
	}

	static public void exeSQL(String sSQL) {

		DMI.call("shr", "PublicServer", "exeSQL",
				new RPCCallback() {

					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {
						// TODO Auto-generated method stub

					}
				}, new Object[] { sSQL });
	}

	static public void exeSQLs(String[] sSQL) {
		SC.showPrompt("正在处理请求，请稍后...");

		DMI.call("shr", "PublicServer", "exeSQLs",
				new RPCCallback() {

					public void execute(RPCResponse response, Object rawData,
							RPCRequest request) {
						// TODO Auto-generated method stub
						SC.clearPrompt();
					}
				}, new Object[] { sSQL });
	}

	static public boolean isNullOrEmpty(Object object) {

		return object == null || trim(object).equals("");
	}

	static public String replaceString(String s) {
		return ClientTools.trim(s.replace("[", "").replace("]", ""));
	}

	static public String[] trimMultiple(Object o) {
		return ClientTools.trim(o).replace("[", "").replace("]", "").split(",");
	}

}
