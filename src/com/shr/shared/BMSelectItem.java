package com.shr.shared;

import com.smartgwt.client.data.DataSource;
import com.smartgwt.client.widgets.form.fields.ComboBoxItem;
import com.smartgwt.client.widgets.grid.ListGridField;
import com.smartgwt.client.widgets.tree.TreeGrid;

public class BMSelectItem extends ComboBoxItem {
	
	public BMSelectItem(String name){
		super(name);
		DataSource dsBM = DataSource.get("SYS_BM");
		TreeGrid pickListProperties = new TreeGrid();
		pickListProperties.setShowFilterEditor(true);

		ListGridField ZJMField = new ListGridField("BM_ZJM");
		ListGridField bmmcField = new ListGridField("BM_MC");
		ListGridField bmidField = new ListGridField("BM_BMID");
		
		this.setAllowEmptyValue(true);
		this.setCanEdit(true);
		this.setTitle("部门");
		this.setOptionDataSource(dsBM);
		this.setDisplayField("BM_MC");
		this.setValueField("BM_BMID");
		this.setPickListWidth(300);
		this.setPickListFields(ZJMField,bmidField, bmmcField);
		this.setPickListProperties(pickListProperties);
		
	}
}
