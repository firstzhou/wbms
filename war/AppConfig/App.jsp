<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%
    StringBuffer systemDataSources = new StringBuffer("");
	systemDataSources.append("SYS_ORGMODEL,");
	systemDataSources.append("SYS_ACTIVEMODULE,");
	systemDataSources.append("SYS_OPRIGHT,");
	systemDataSources.append("Q_OPRIGHT,");
	systemDataSources.append("SYS_BM,");
	systemDataSources.append("SYS_USERGROUP,");
	systemDataSources.append("SYS_USERS,");
	systemDataSources.append("query_user_right,");
	systemDataSources.append("SYS_DICCATEGORY,");
	systemDataSources.append("SYS_DICVALUE,");
	systemDataSources.append("SYS_GG,");
	systemDataSources.append("SYS_WF,");
	systemDataSources.append("SYS_WF_MX,");
	systemDataSources.append("SYS_DESIGN,");
	systemDataSources.append("SYS_DESIGN_MX,");
    StringBuffer projectDataSources = new StringBuffer();
	
    /**bms begin add by zhuwei **/
    projectDataSources.append("BEIYE_CB,");
    
    projectDataSources.append("BMS_TBDRSJ,");
    projectDataSources.append("BMS_CCF,");
    projectDataSources.append("BMS_GLF,");
    projectDataSources.append("BMS_JCF,");
    projectDataSources.append("BMS_ZXF,");
    projectDataSources.append("BMS_TBF,");
    projectDataSources.append("BMS_QDF,");
    projectDataSources.append("BMS_SMF,");
    projectDataSources.append("BMS_DDCLF,");
    projectDataSources.append("BMS_LZF,");
    projectDataSources.append("BMS_CBHCF,");
    projectDataSources.append("BMS_LJZF,");
    projectDataSources.append("BMS_JTPF,");
    projectDataSources.append("BMS_ZHXF,");
    projectDataSources.append("BMS_CZF,");
    projectDataSources.append("BMS_TXF,");
    projectDataSources.append("BMS_QTF,");
    projectDataSources.append("BMS_FYFL,");
    projectDataSources.append("BMS_HTXX,");
    projectDataSources.append("BMS_CBDJ,");
    projectDataSources.append("BMS_HTDJDW,");
    projectDataSources.append("BMS_YCF,");
    projectDataSources.append("BMS_CKXX,");
    projectDataSources.append("BMS_CALLOG,");
    projectDataSources.append("BMS_SDF,");
    projectDataSources.append("BMS_ZJF,");
    projectDataSources.append("BMS_ZXLWF,");
    projectDataSources.append("BMS_KQMJ,");
    projectDataSources.append("BMS_XMZY,");
    projectDataSources.append("BMS_KPXX,");
    projectDataSources.append("BMS_SRSC,");
    projectDataSources.append("BMS_ZD,");
    projectDataSources.append("BMS_ZDMX,");
    projectDataSources.append("BMS_ZDCB,");
    projectDataSources.append("BMS_ZDCBMX,");
    projectDataSources.append("BMS_CBTZ,");
    
    /**bms end add by zhuwei **/
    projectDataSources.append("BEIYE_SYB,");
    projectDataSources.append("BEIYE_QY,");
    projectDataSources.append("BEIYE_XM,");
    projectDataSources.append("BEIYE_FYLX,");
    projectDataSources.append("BEIYE_CK,");
    projectDataSources.append("BEIYE_GYS,");
    projectDataSources.append("BEIYE_KPTT,");
    projectDataSources.append("BEIYE_GS,");
    projectDataSources.append("BEIYE_HT,");
    projectDataSources.append("GS_ZD,");
    
    /**wbms end add by zhuwei **/
    projectDataSources.append("WBMS_GOODS,");
    projectDataSources.append("WBMS_ORDERTYPE,");
    projectDataSources.append("WBMS_STOCKSTATUS,");
    projectDataSources.append("WBMS_ORDERSTATUS,");
    projectDataSources.append("WBMS_SPDWZH,");
    projectDataSources.append("WBMS_HZSJ,");
    projectDataSources.append("WBMS_CKSJ,");
    projectDataSources.append("WBMS_KQXX,");
    projectDataSources.append("WBMS_KCKZ,");
    projectDataSources.append("WBMS_KCKZ_ED,");
    projectDataSources.append("WBMS_HWXX,");
    projectDataSources.append("WBMS_RKDDSJ_H,");
    projectDataSources.append("WBMS_RKDDSJ_B,");
    projectDataSources.append("WBMS_CKDDXX_H,");
    projectDataSources.append("WBMS_CKDDXX_B,");
    projectDataSources.append("WBMS_RKDDSJ_H_ED,");
    projectDataSources.append("WBMS_RKDDSJ_B_ED,");
    projectDataSources.append("WBMS_CKDDXX_H_ED,");
    projectDataSources.append("WBMS_CKDDXX_B_ED,");
    projectDataSources.append("ORDER_JFGZ_H,");
    projectDataSources.append("ORDER_JFGZ_B,");
    /*  projectMange     end   */
    
    String sLoadDataSource = systemDataSources.toString().trim() + projectDataSources.toString().trim();

    application.setAttribute("LoadDataSource", sLoadDataSource);
%>