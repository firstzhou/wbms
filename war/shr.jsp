<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<!-- The HTML 4.01 Transitional DOCTYPE declaration-->
<!-- above set at the top of the file will set     -->
<!-- the browser's rendering engine into           -->
<!-- "Quirks Mode". Replacing this declaration     -->
<!-- with a "Standards Mode" doctype is supported, -->
<!-- but may lead to some differences in layout.   -->

<%@ page contentType="text/html; charset=UTF-8"%>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta name="gwt:property" content="locale=zh_CN">
<!--                                           -->
<!-- Any title is fine                         -->
<!--                                           -->
<title>贝业WBMS系统</title>

<!-- IMPORTANT : You must set the variable isomorphicDir to [MODULE_NAME]/sc/ so that the SmartGWT resource are 
	  correctly resolved -->
<script>
	var isomorphicDir = "shr/sc/";
	window.isc_expirationOff = true;
</script>

<!--                                           -->
<!-- This script loads your compiled module.   -->
<!-- If you add any GWT meta tags, they must   -->
<!-- be added before this line.                -->
<!--                                           -->
<script type="text/javascript" language="javascript"
	src="shr/shr.nocache.js"></script>
	<link href="css/gridCell.css" rel="stylesheet" type="text/css"/>	
</head>

<!--                                           -->
<!-- The body can have arbitrary html, or      -->
<!-- you can leave the body empty if you want  -->
<!-- to create a completely dynamic UI.        -->
<!--                                           -->
<body>
	<script type="text/javascript" src='shr/sc/modules/ISC_Core.js?isc_version=11.1p_2017-07-01.js'></script>
	<script type="text/javascript" src='shr/sc/modules/ISC_Foundation.js?isc_version=11.1p_2017-07-01.js'></script>
	<script type="text/javascript" src='shr/sc/modules/ISC_Containers.js?isc_version=11.1p_2017-07-01.js'></script>
	<script type="text/javascript" src='shr/sc/modules/ISC_Grids.js?isc_version=11.1p_2017-07-01.js'></script>
	<script type="text/javascript" src='shr/sc/modules/ISC_Forms.js?isc_version=11.1p_2017-07-01.js'></script>
	<script type="text/javascript" src='shr/sc/modules/ISC_DataBinding.js?isc_version=11.1p_2017-07-01.js'></script>
	<script type="text/javascript" src='shr/sc/modules/ISC_Tools.js?isc_version=11.1p_2017-07-01.js'></script>
	
	<!--load the datasources-->
	<%="<script src=\"shr/sc/DataSourceLoader?dataSource="
					+ application.getAttribute("LoadDataSource")
					+ "\"></script>"%>
	<script type="text/javascript">
		// Determine which skin to load
		var currentSkin = "EnterpriseBlue";
		// Load the skin
		document.write("<"+"script type='text/javascript' src='shr/sc/skins/" + currentSkin + 
        "/load_skin.js?isc_version=11.1p_2017-07-01.js'><"+"/script>");
	</script>
	<!-- OPTIONAL: include this if you want history support -->
	<iframe src="javascript:''" id="__gwt_historyFrame" tabIndex='-1'
		style="position: absolute; width: 0; height: 0; border: 0"></iframe>

</body>
</html>
