<!DOCTYPE html>
<%@ page contentType="text/html; charset=UTF-8"%>
<html lang="zh">
<head>
<meta charset="utf-8">
<meta name="description" content="">
<title></title>
<style type="text/css">
	* {
		margin:0;
		padding:0;
	}
	body, html {
		width:100%;
		height:100%;
		font-size:14px;
		font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'PingFang SC', 'Hiragino Sans GB',
		'Microsoft YaHei', 'Helvetica Neue', Helvetica, Arial, sans-serif, 'Apple Color Emoji', 'Segoe UI Emoji', 'Segoe UI Symbol';;
	}
	ul,li,ol {
		padding:0;
		margin:0;
		list-style:none
	}
	body {
		background:#f0f2f5;
	}
	.main{
		display: flex;
		flex-direction: column;
		min-height:100%;
		background:#f0f2f5;
		background:url(images/main.svg) no-repeat;
		background-position: center 110px;
		background-position-x: center;
		background-position-y: 110px;
		background-size:100% 100%;
	}
	.mains{
		margin-top:40px;
		padding:32px 0 24px;
	}
	.top{
		text-align: center;
		display: block;
	}
	.top_title{
		font-size:33px;
		color:black;
		position: relative;
		vertical-align: middle;
		font-family: 'Myriad Pro', 'Helvetica Neue', Arial, Helvetica, sans-serif;
	}
	.item{
		display: block;
		width:368px;
		margin:0 auto;
	}
	.item_box{
		width:368px;
		height:286px;
		display: block;
	}
	/*.top {width:100%;height:8%;background:#ffd042;border-bottom:2px solid #ff0;}*/
	.form{
		width:368px;
		height:286px;
		box-sizing:border-box;
		color: rgba(0, 0, 0, 0.65);
		font-size: 14px;
		font-variant: tabular-nums;
		line-height: 1.5;
		list-style: none;
	}
	.form_box{
		box-sizing: border-box;
		margin: 0;
		padding: 0;
		color: rgba(0, 0, 0, 0.65);
		font-size: 14px;
		font-variant: tabular-nums;
		line-height: 1.5;
		list-style: none;
		position: relative;
		overflow: hidden;
		zoom: 1;
	}
	.box_title{
		width:368px;
		height:47px;
		border-bottom: 0;
		margin-bottom: 24px;
		text-align: center;
	}
	.box1{
		width:368px;
		height:47px;
		position: relative;
		box-sizing: border-box;
		margin-bottom: -1px;
		overflow: hidden;
		font-size: 14px;
		line-height: 1.5;
		white-space: nowrap;
		transition: padding 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
		zoom: 1;
	}
	.box2{
		width:128px;
		height:48px;
		margin:0 auto;
	}
	.box3{
		width:128px;
		margin-top:45px;
		position: absolute;
		bottom: 1px;
		left: 116px;
		z-index: 1;
		box-sizing: border-box;
		height: 2px;
		background-color: #1890ff;
		-webkit-transform-origin: 0 0;
		transform-origin: 0 0;
	}
	.title2{
		font-size: 16px;
		line-height: 24px;
		color:#1890ff;
		position: relative;
		display: inline-block;
		box-sizing: border-box;
		height: 100%;
		margin: 0 32px 0 0;
		padding: 12px 16px;
		text-decoration: none;
		cursor: pointer;
		transition: color 0.3s cubic-bezier(0.645, 0.045, 0.355, 1);
	}
	.login_bottom{
		width:677px;
		height:50px;
		display: block;
		padding:0 16px;
		margin:48px -160px 24px;
		text-align:center;
	}
	.links{
		width:677px;
		height:21px;
		margin-bottom:8px;
	}
	.links-item:not(:last-child){
		margin-right:40px;
	}
	.links-item{
		display: inline-block;
		color: rgba(0, 0, 0, 0.45);
		transition: all 0.3s;
	}
	a{
		text-decoration: none;
		background-color: transparent;
		outline: none;
		cursor: pointer;
		text-align: center;
	}
	.bot{
		width:677px;
		height:21px;
	}
	.action{
		display: inline-block;
		color: inherit;
		font-style: normal;
		line-height: 0;
		text-align: center;
		text-transform: none;
		vertical-align: -0.125em;
		text-rendering: optimizeLegibility;
		-webkit-font-smoothing: antialiased;
	}
	svg{
		width:14px;
		height:14px;
		text-align: center;
	}
	.company{
		color: #1890ff;
		touch-action: manipulation;
		text-decoration: none;
		background-color: transparent;
		outline: none;
		cursor: pointer;
		transition: color 0.3s;
		text-align: center;
		font-size: 14px;
	}
	.col{
		display: inline-block;
		color: rgba(0, 0, 0, 0.45);
		transition: all 0.3s;
	}
	.left {
		width:50%;
		height:96px;
		background:url(images/left.png) no-repeat 80% center;
		background-size:auto 100%;
		position: absolute;
		left:12%;
		top:22%;
	}
	.footer{width:100%;height:200px;background:url(images/footer.png) no-repeat 0 0;background-size:100% 100%;position:fixed;bottom:-20px;}
	.middle {height:90%;background: -webkit-linear-gradient(#4593ff , #05bcfe);background: -o-linear-gradient(#4593ff , #05bcfe);background: -moz-linear-gradient(#4593ff , #05bcfe);background: linear-gradient(#4593ff , #05bcfe);}
	/*.left {width:50%;height:80%;background:url(images/left.png) no-repeat 80% center;background-size:auto 100%;float:left;}*/
	.right{width:35%;float:right;}
	/*.form {width:60%;min-width:250px;margin-top:15%;background:#f3f3f3;border-radius:5px;box-shadow:0 0 5px rgba(0,0,0,0.3);}*/
	.header {height:50px;line-height:50px;text-align:center;color:#fff;font-size:18px;background:#394754;text-shadow:2px 2px 0 rgba(0,0,0,0.3)}
	.input {padding:30px 20px 20px 20px;}
	.input li {text-align:center;height:40px;line-height:40px;padding:10px 0 20px 0;position:relative;}
	.input input {width:100%;height:40px;line-height:40px;border:none;text-indent:40px;background:#fff;border-radius:3px;}
	.input button {width:80%;height:40px;color:#fff;font-size:18px;font-weight:700;border:none;border-radius:3px;background:-webkit-linear-gradient(#4fa2ff , #2072f9);background: -o-linear-gradient(#4fa2ff , #2072f9);background:-moz-linear-gradient(#4fa2ff , #2072f9);background:linear-gradient(#4fa2ff , #2072f9);}
	i[class*='icon-'] {display:block;width:40px;height:40px;position:absolute;left:0;top:10px;}
	.icon-account {background:url(images/account.png) no-repeat center center;background-size:40% 40%;}
	.icon-psw {background:url(images/password.png) no-repeat center center;background-size:40% 40%;}
</style>
</head>
<jsp:include page="AppConfig/App.jsp" />
<body>
<%--<div class="top"></div>--%>
<%--<div class="middle">--%>
	<%--<div class="left"></div>--%>
	<%--<div class="right">--%>
		<%--<div class="form">--%>
			<%--<div class="header">贝业WBMS系统</div>--%>
			<%--<form action='servlet/Login.do' method="post">--%>
			<%--<input name="yearSelectId" id="yearSelectId" type="hidden" value='bms'/>--%>
			<%--<ul class="input">--%>
				<%--<li><input id="usrid" name="usrid" type="text" value="" placeholder="请输入账号"><i class="icon-account"></i></li>--%>
				<%--<li><input id="password" name="password" type="password" value="" placeholder="请输入密码"><i class="icon-psw"></i></li>--%>
				<%--<li><button>登&nbsp;&nbsp;&nbsp;&nbsp;录</button></li>--%>
			<%--</ul>--%>
			<%--<input name="action" type="hidden" value='Login'>--%>
			<%--<input	name="codesvr" type="hidden" value='<%=request.getParameter("gwt.codesvr") %>'>--%>
			<%--</form>--%>
		<%--</div>--%>
	<%--</div>--%>
<%--</div>--%>
<%--<div class="footer"></div>--%>
 <div class="main">
	<div class="mains">
		<div class="top">
			<div>
				<span class="top_title">贝业WBMS系统</span>
			</div>
		</div>
		<div class="left"></div>
		<div class="item">
			<div class="item_box">
				<form action='servlet/Login.do' method="post" class="form">
					<div class="form_box">
						<div class="box_title">
							<div class="box1">
								<div class="box2">
									<span class="title2">账户密码登录</span>
									<div class="box3"></div>
								</div>
							</div>
						</div>
					</div>
					<input name="yearSelectId" id="yearSelectId" type="hidden" value='bms'/>
					<ul class="input">
						<li><input id="usrid" name="usrid" type="text" value="" placeholder="请输入账号"><i class="icon-account"></i></li>
						<li><input id="password" name="password" type="password" value="" placeholder="请输入密码"><i class="icon-psw"></i></li>
						<li><button>登&nbsp;&nbsp;&nbsp;&nbsp;录</button></li>
					</ul>
					<input name="action" type="hidden" value='Login'>
					<input name="codesvr" type="hidden" value='<%=request.getParameter("gwt.codesvr") %>'>
				</form>
				<div class="login_bottom">
                <span class="links">
					<a class="links-item">帮助</a>
					<a class="links-item">隐私</a>
					<a class="links-item">条款</a>
				</span>
					<div class="bot">
						<span class="col">Copyright</span>
						<i class="action">
							<svg t="1565329381524" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1499" width="16" height="16"><path d="M480.311123 16.446859c-260.743086 0-472.118406 211.348132-472.118406 472.118406 0 260.712877 211.37532 472.122434 472.118406 472.122434 260.738051 0 472.147608-211.409557 472.147608-472.122434C952.45873 227.794991 741.049173 16.446859 480.311123 16.446859zM480.311123 897.737906c-225.974387 0-409.168614-183.189192-409.168614-409.172641 0-225.980429 183.194226-409.169621 409.168614-409.169621 226.002582 0 409.197816 183.189192 409.197816 409.169621C889.508938 714.548715 706.314712 897.737906 480.311123 897.737906z" p-id="1500" fill="#707070"></path><path d="M563.635968 626.332003c-20.899685 17.399454-46.349873 26.063433-76.378759 26.063433-24.711071 0-47.703242-6.452671-68.972486-19.118354-21.1474-12.726101-36.762501-31.966298-46.842282-57.660173-10.02339-25.7553-15.062274-55.820437-15.062274-90.312218 0-26.739111 4.243376-52.742126 12.726101-77.885188 8.481718-25.202473 23.114015-45.245226 44.139572-60.247082 20.897671-14.874977 46.963119-22.313473 78.194328-22.313473 27.076446 0 49.572181 6.7024 67.465051 20.102164 17.886828 13.52765 31.535314 35.038567 40.940424 64.485423l52.803551-12.477379c-10.879316-37.376754-30.060102-66.392626-57.783023-87.046611-27.660489-20.654991-61.782713-31.104331-102.381774-31.104331-35.779698 0-68.546538 8.232996-98.297499 24.588221-29.816415 16.290779-52.742126 40.199294-68.911061 71.613772-16.167928 31.473889-24.224704 68.297816-24.224704 110.592616 0 38.668698 7.10116 74.93879 21.45654 108.629023 14.262739 33.810062 35.165445 59.567376 62.518808 77.331353 27.357391 17.827417 62.88736 26.678693 106.657373 26.678693 42.201152 0 77.979843-11.554994 107.178984-34.666994 29.263587-23.177454 49.488602-56.805254 60.737477-100.88038l-53.67156-13.526643C598.552691 583.302114 584.417838 609.058421 563.635968 626.332003z" p-id="1501" fill="#707070"></path>
							</svg>
						</i>
						<span class="col">2019</span>
						<a class="company">萃颠信息科技有限公司</a>
						<span class="col">技术支持</span>
					</div>
				</div>

			</div>
		</div>
	</div>
 </div>
</body>
</htm>